using System;

public class Prod_1_6_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;
    public bool PNPT = false; 

    public Prod_1_6_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    // Алгоритм расчета давления в нагнетательной скважине от забоя
    public void Diagram_1_1()
    {
        p.PTxt("p16.Diagram_1_1");
        fp.p16.Vzhek(fp.Q); // 1.7
        fp.p16.Reek(); // 1.8
        fp.p16.LambdaEk(fp.Reek); // 1.9
        fp.p16.Ppr1(fp.Vzhek); // 1.10
        fp.p16.Vzhnkt(fp.Q); // 1.11
        fp.p16.Renkt(); // 1.12
        fp.p16.LambdaNkt(fp.Renkt); // 1.13
        fp.p16.Pbufpk(fp.Vzhnkt); // 1.14
        fp.p16.Pbuf2(fp.Q); // 1.15
    }

    // Алгоритм расчета давления в нагнетательной скважине от устья
    public void Diagram_1_2()
    {
        p.PTxt("p16.Diagram_1_2");
        fp.p16.Vzhnkt(fp.Q); // 1.16
        fp.p16.Renkt(); // 1.17
        fp.p16.LambdaNkt(fp.Renkt); // 1.18
        fp.p16.Vzhek(fp.Q); // 1.19
        fp.p16.Reek(); // 1.20
        fp.p16.LambdaEk(fp.Reek); // 1.21
        fp.p16.Ppr2(); // 1.22
        fp.p16.Pzpk(); // 1.23
    }

    // Алгоритм вывода работы нагнетательной скважины на режим
    public void Diagram_1_3(double time)
    {
        p.PTxt("p16.Diagram_1_3");
        fp.p16.Pz1(time); // 1.1.1
        if (fp.Pz < fp.Pgrp)  fp.p16.Q1(fp.Pz); // 1.2
        else
        {
            fp.p16.Qgrp(); // 1.6
            fp.p16.Ans(fp.Pz); // 1.4
            fp.p16.Bns(fp.Pz); // 1.5
            fp.p16.Q2(fp.Pz); // 1.3
        }
        Diagram_1_1();
        p.PTxt("Continue p16.Diagram_1_3");
        fp.p16.Pbuf2(fp.Q); // 1.15
        if (1.0E-5 < fp.dsht)
        {
            if (fp.Pbufpk <= fp.Pbuf2)  Diagram_1_4(time);
        } 
        else Diagram_1_5(time);
    }

    // Алгоритм установившегося режима работы нагнетательной скважины
    public void Diagram_1_4(double time)
    {
        p.PTxt("p16.Diagram_1_4");
        var Pbufmdl = 0.5 * (fp.Pbuf2 + fp.Pbufpk);
        p.POutC("p16.Diagram_1_4", "Pbufmdl", Pbufmdl);
        Diagram_1_2();
        p.PTxt("Continue p16.Diagram_1_4");
        var Pzmdl = 0.5 * (fp.Pz_1 + fp.Pz);
        p.POutC("p16.Diagram_1_4", "Pzmdl", Pzmdl);
        if (Pzmdl < fp.Pgrp)  fp.p16.Q1(Pzmdl); // 1.2
        else
        {
            fp.p16.Qgrp(); // 1.6
            fp.p16.Ans(Pzmdl); // 1.4
            fp.p16.Bns(Pzmdl); // 1.5
            fp.p16.Q2(Pzmdl); // 1.3
        }
        fp.p16.Pbuf2(fp.Q); // 1.15
        Diagram_1_1();
        if (1.0E-5 < fp.dsht)
        {
            if (1.0E-5 < Math.Abs(fp.dsht - fp.dsht_1))  Diagram_1_3(time);
        } 
        else Diagram_1_5(time);
        }

    // Алгоритм остановки работы нагнетательной скважины
    public void Diagram_1_5(double time)
    {
        p.PTxt("p16.Diagram_1_5");
        if (!PNPT)
        {
            fp.p16.DPpzo1(); // 1.24
            fp.p16.tzab1(); // 1.25
            PNPT = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) fp.p16.Pz(time); // 1.26, 1.27
    }

}
using System;
using System.Collections.Generic;

public class Prod_1_6
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_6(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (1.1.1)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjuzh", fp.Mjuzh);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("1.1.1", parameters);
        fp.Pz = fp.Pzrezh + fp.Qrezh * fp.Mjuzh * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("1.1.1", "Pz", fp.Pz);
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (1.1.2)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz2(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjuzh", fp.Mjuzh);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("1.1.2", parameters);
        fp.Pz = fp.Pzrezh - fp.Qrezh * fp.Mjuzh * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("1.1.2", "Pz", fp.Pz);
    }
        
    /// <summary>
    /// Дебит скважины (м3/с) (1.2)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Q1(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Mjuzh", fp.Mjuzh);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.2", parameters);
        fp.Q =  2.0 * fp.pi * fp.k * fp.h * (fp.reservoirPressurePa - Pz) / 
                (fp.Mjuzh * Math.Log(fp.Rk / (fp.rc * Math.Exp(-fp.skinFactor))));
        p.POutC("1.2", "Q", fp.Q);
    }
        
    /// <summary>
    /// Дебит скважины (м3/с) (1.3)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Q2(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Ans", fp.Ans);
        parameters.Add("Bns", fp.Bns);
        p.PInC("1.3", parameters);
        fp.Q = Math.Sqrt((Pz - fp.Bns) / fp.Ans);
        p.POutC("1.3", "Q", fp.Q);
    }
        
    /// <summary>
    /// Коэффициент (1.4)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ans(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Pgrp", fp.Pgrp);
        parameters.Add("Qgrp", fp.Qgrp);
        p.PInC("1.4", parameters);
        fp.Ans = 0.11 * (Pz - fp.Pgrp) / Math.Pow(fp.Qgrp, 2);
        p.POutC("1.4", "Ans", fp.Ans);
    }
        
    /// <summary>
    /// Коэффициент (1.5)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Bns(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Pgrp", fp.Pgrp);
        p.PInC("1.5", parameters);
        fp.Bns = 0.87 * (Pz - fp.Pgrp);
        p.POutC("1.5", "Bns", fp.Bns);
    }
        
    /// <summary>
    /// Расход скважины при давлении гидроразрыва, м3/с (1.6)
    /// </summary>
    public void Qgrp()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pgrp", fp.Pgrp);
        parameters.Add("Mjuzh", fp.Mjuzh);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.6", parameters);
        fp.Qgrp =  2.0 * fp.pi * fp.k * fp.h * (fp.reservoirPressurePa - fp.Pgrp) / 
                   (fp.Mjuzh * Math.Log(fp.Rk / (fp.rc * Math.Exp(-fp.skinFactor))));
        p.POutC("1.6", "Qgrp", fp.Qgrp);
    }
    
    /// <summary>
    /// Скорость подъема жидкости в ЭК (м/с) (1.7, 1.19)
    /// </summary>
    /// <param name="Q">Дебит скважины, м3/с</param>
    public void Vzhek(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.7, 1.19", parameters);
        fp.Vzhek = 4.0 * Q / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("1.7, 1.19", "Vzhek", fp.Vzhek);
    }
    
    /// <summary>
    /// Число Рейнольдса в ЭК (1.8, 1.20)
    /// </summary>
    public void Reek()
    {
        parameters.Clear();
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Mjuzh", fp.Mjuzh);
        p.PInC("1.8, 1.20", parameters);
        fp.Reek = fp.Vzhek * fp.Rozh * fp.ECInnerDiameter / fp.Mjuzh;
        p.POutC("1.8, 1.20", "Reek", fp.Reek);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК (1.9, 1.21)
    /// </summary>
    /// <param name="Reek">Число Рейнольдса в ЭК</param>
    public void LambdaEk(double Reek)
    {
        parameters.Clear();
        parameters.Add("Reek", Reek);
        parameters.Add("EpsEk", fp.EpsEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.9, 1.21", parameters);
        fp.LambdaEk = 0.067 * (158.0 / Reek + 2.0 * fp.EpsEk / fp.ECInnerDiameter);
        p.POutC("1.9, 1.21", "LambdaEk", fp.LambdaEk);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (1.10)
    /// </summary>
    /// <param name="Vzhek">Скорость подъема жидкости в ЭК, м/с</param>
    public void Ppr1(double Vzhek)
    {
        parameters.Clear();
        parameters.Add("Vzhek", Vzhek);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.10", parameters);
        fp.Ppr = fp.Pz - (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                             fp.LambdaEk * Vzhek * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.10", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Скорость подъема жидкости в НКТ (м/с) (1.11, 1.16)
    /// </summary>
    /// <param name="Q">Дебит скважины, м3/с</param>
    public void Vzhnkt(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.11, 1.16", parameters);
        fp.Vzhnkt = 4.0 * Q / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("1.11, 1.16", "Vzhnkt", fp.Vzhnkt);
    }
    
    /// <summary>
    /// Число Рейнольдса в НКТ (1.12, 1.17)
    /// </summary>
    public void Renkt()
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Mjuzh", fp.Mjuzh);
        p.PInC("1.12, 1.17", parameters);
        fp.Renkt = fp.Vzhnkt * fp.Rozh * fp.dnkt / fp.Mjuzh;
        p.POutC("1.12, 1.17", "Renkt", fp.Renkt);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в НКТ (1.13, 1.18)
    /// </summary>
    /// <param name="Renkt">Число Рейнольдса в НКТ</param>
    public void LambdaNkt(double Renkt)
    {
        parameters.Clear();
        parameters.Add("Renkt", Renkt);
        parameters.Add("EpsNkt", fp.EpsNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.13, 1.18", parameters);
        fp.LambdaNkt = 0.067 * (158.0 / Renkt + 2.0 * fp.EpsNkt / fp.dnkt);
        p.POutC("1.13, 1.18", "LambdaNkt", fp.LambdaNkt);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное при помощи распределения давления (Па) (1.14)
    /// </summary>
    /// <param name="Vzhnkt">Скорость подъема жидкости в НКТ, м/с</param>
    public void Pbufpk(double Vzhnkt)
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", Vzhnkt);
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.14", parameters);
        fp.Pbufpk = fp.Ppr + fp.Lcp * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                       fp.LambdaNkt * Vzhnkt * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("1.14", "Pbufpk", fp.Pbufpk);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное через штуцер (Па) (1.15)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Pbuf2(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        parameters.Add("g", fp.g);
        p.PInC("1.15", parameters);
        fp.Pbuf2 = fp.Plin + Math.Pow(Q, 2) / (78.8768 * Math.Pow(fp.dsht, 4) * fp.g);
        p.POutC("1.15", "Pbuf2", fp.Pbuf2);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (1.22)
    /// </summary>
    public void Ppr2()
    {
        fp.Pens1 = fp.Pbuf2; // Д. Гуменюк, отладка
        parameters.Clear();
        parameters.Add("Pens1", fp.Pens1);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.22", parameters);
        fp.Ppr = fp.Pens1 + fp.Lcp * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                      fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.22", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Забойное давление, рассчитанное при помощи распределения давления (Па) (1.23)
    /// </summary>
    public void Pzpk()
    {
        fp.Pens2 = fp.Ppr; // Д. Гуменюк, отладка
        parameters.Clear();
        parameters.Add("Pens2", fp.Pens2);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.23", parameters);
        fp.Pzpk = fp.Pens2 + (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                 fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("1.23", "Pzpk", fp.Pzpk);
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (1.24, 2.226)
    /// </summary>
    public void DPpzo1()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pzost", fp.Pzost);
        p.PInC("1.24", parameters);
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
        p.POutC("1.24", "DPpzo1", fp.DPpzo1);
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (1.25)
    /// </summary>
    public void tzab1()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjuzh", fp.Mjuzh);
        parameters.Add("rc", fp.rc);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("1.25", parameters);
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.Mjuzh)) * fp.rc / 
                   (2.25 * fp.piezoconductivityOfTheFormation); 
        p.POutC("1.25", "tzab1", fp.tzab1);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (1.26, 1.27)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("tzab1", fp.tzab1);
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjuzh", fp.Mjuzh);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.26, 1.27", parameters);
        if (time < fp.tzab1)  fp.Pz = fp.Pzost - fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        else  fp.Pz = fp.Pzost - fp.Qrezh * fp.Mjuzh / (4.0 * fp.pi * fp.k * fp.h) *
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
        p.POutC("1.26, 1.27", "Pz", fp.Pz);
    }
    
}
using System;
using System.Collections.Generic;

public class HydroDynInv_1_1
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public HydroDynInv_1_1(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
    
    /// <summary>
    /// Дебит скважины (м3/с) (2.1)
    /// </summary>
    public void Q1()
    {
        parameters.Clear();
        parameters.Add("Kp", fp.Kp);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pz", fp.Pz);
        p.PInC("2.1", parameters);
        fp.Q = fp.Kp * (fp.reservoirPressurePa - fp.Pz);
        p.POutC("2.1", "Q", fp.Q);
    }
    
    /// <summary>
    /// Удельный коэффициент продуктивности скважины (м2/(с* Па)) (2.2)
    /// </summary>
    public void Kpud()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("h", fp.h);
        p.PInC("2.2", parameters);
        fp.Kpud = fp.Q / ((fp.reservoirPressurePa - fp.Pz) * fp.h);
        p.POutC("2.2", "Kpud", fp.Kpud);
    }
    
    /// <summary>
    /// Коэффициент продуктивности скважины (м3/(с*Па)) (2.3)
    /// </summary>
    public void Kp1()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("bOil", fp.bOil);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pz", fp.Pz);
        p.PInC("2.3", parameters);
        fp.Kp = fp.Q * fp.bOil / (fp.reservoirPressurePa - fp.Pz);
        p.POutC("2.3", "Kp", fp.Kp);
    }
    
    /// <summary>
    /// Решение уравнения упругого режима (пьезопроводности) (Па) (2.4)
    /// </summary>
    /// <param name="viscosity">Вязкость, Па•с</param>
    /// <param name="time">Время замера, секунды</param>
    public void Pqv(double viscosity, double time)
    {
        parameters.Clear();
        parameters.Add("viscosity", viscosity);
        parameters.Add("time", time);
        parameters.Add("Q", fp.Q);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.4", parameters);
        fp.Pqv = fp.Q * viscosity * (Math.Log(2.25 * fp.piezoconductivityOfTheFormation / 
                                              Math.Pow(fp.rc, 2)) + Math.Log(time)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("2.4", "Pqv", fp.Pqv);
    }
    
    /// <summary>
    /// Аппроксимация зависимости депрессии на пласт от логарифма времени (2.5)
    /// </summary>
    /// <param name="time">Время замера, секунды</param>
    public void DP1(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Adp", fp.Adp);
        parameters.Add("idp", fp.idp);
        p.PInC("2.5", parameters);
        fp.DP = fp.Adp + fp.idp * Math.Log(time);
        p.POutC("2.5", "DP", fp.DP);
    }
    
    /// <summary>
    /// Тангенс угла наклона зависимости депрессии на пласт от логарифма времени (2.6)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    /// <param name="DPz1">Депрессия на пласт во время первого замера, Па</param>
    /// <param name="DPz1">Депрессия на пласт во время второго замера, Па</param>
    public void idp1(double tz1, double tz2, double DPz1, double DPz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("DPz1", DPz1);
        parameters.Add("DPz2", DPz2);
        p.PInC("2.6", parameters);
        fp.idp = (DPz1 - DPz2) / (Math.Log(tz2) - Math.Log(tz1));
        p.POutC("2.6", "idp", fp.idp);
    }
    
    /// <summary>
    /// Аппроксимация зависимости депрессии на пласт от логарифма времени (2.7.1)
    /// </summary>
    /// <param name="viscosity">Вязкость, Па•с</param>
    public void Adp(double viscosity)
    {
        parameters.Clear();
        parameters.Add("viscosity", viscosity);
        parameters.Add("Q", fp.Q);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.7.1", parameters);
        fp.Adp = fp.Q * viscosity * Math.Log(2.25 * fp.piezoconductivityOfTheFormation / 
                                             Math.Pow(fp.rc,2)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("2.7.1", "Adp", fp.Adp);
    }
    
    /// <summary>
    /// Тангенс угла наклона зависимости депрессии на пласт от логарифма времени (2.7.2)
    /// </summary>
    /// <param name="viscosity">Вязкость, Па•с</param>
    public void idp2(double viscosity)
    {
        parameters.Clear();
        parameters.Add("viscosity", viscosity);
        parameters.Add("Q", fp.Q);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.7.2", parameters);
        fp.idp = fp.Q * viscosity / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("2.7.2", "idp", fp.idp);
    }
    
    /// <summary>
    /// Гидропроводность (м3/(с*Па)) (2.8)
    /// </summary>
    public void eps1()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("pi", fp.pi);
        parameters.Add("idp", fp.idp);
        p.PInC("2.8", parameters);
        fp.eps = fp.Q / (4.0 * fp.pi * fp.idp);
        p.POutC("2.8", "eps", fp.eps);
    }
    
    /// <summary>
    /// Проницаемость пласта (м2) (2.9, 2.20, 2.26)
    /// </summary>
    /// <param name="viscosity">Вязкость, Па•с</param>
    public void k(double viscosity)
    {
        parameters.Clear();
        parameters.Add("viscosity", viscosity);
        parameters.Add("eps", fp.eps);
        parameters.Add("h", fp.h);
        p.PInC("2.9, 2.20, 2.26", parameters);
        fp.k = fp.eps * viscosity / fp.h;
        p.POutC("2.9, 2.20, 2.26", "k", fp.k);
    }
    
    /// <summary>
    /// Приведенный радиус скважины (м) (2.10)
    /// </summary>
    public void rpr()
    {
        parameters.Clear();
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("Adp", fp.Adp);
        parameters.Add("idp", fp.idp);
        p.PInC("2.10", parameters);
        fp.rpr = Math.Sqrt(2.25 * fp.piezoconductivityOfTheFormation * Math.Exp(-fp.Adp / fp.idp));
        p.POutC("2.10", "rpr", fp.rpr);
    }
    
    /// <summary>
    /// Коэффициент продуктивности скважины (м3/(с*Па)) (2.11)
    /// </summary>
    /// <param name="rc">Радиус скважины, м</param>
    public void Kp2(double rc)
    {
        parameters.Clear();
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("eps", fp.eps);
        parameters.Add("Rk", fp.Rk);
        p.PInC("2.11", parameters);
        fp.Kp = 2.0 * fp.pi * fp.eps / Math.Log(fp.Rk / rc);
        p.POutC("2.11", "Kp", fp.Kp);
    }
    
    /// <summary>
    /// Коэффициент совершенства скважины (2.12)
    /// </summary>
    /// <param name="rcd">Радиус скважины по долоту, м</param>
    public void Delc(double rcd)
    {
        parameters.Clear();
        parameters.Add("rcd", rcd);
        parameters.Add("Rk", fp.Rk);
        p.PInC("2.12", parameters);
        fp.Delc = Math.Log(fp.Rk / rcd) / Math.Log(fp.Rk / rcd);
        p.POutC("2.12", "Delc", fp.Delc);
    }
    
    /// <summary>
    /// Период стабилизации (c) (2.13)
    /// </summary>
    /// <param name="Kstab">Коэффициент, находящийся в диапазоне 0.12 - 0.15 </param>
    public void Tstab(double Kstab)
    {
        parameters.Clear();
        parameters.Add("Kstab", Kstab);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("2.13", parameters);
        fp.Tstab = Kstab * Math.Pow(fp.Rk, 2) / fp.piezoconductivityOfTheFormation;
        p.POutC("2.13", "Tstab", fp.Tstab);
    }
    
    /// <summary>
    /// Зависимость депрессии на пласт от значения забойного давления (2.14)
    /// </summary>
    /// <param name="Pzab">Значение забойного давления, Па</param>
    public void DP2(double Pzab)
    {
        parameters.Clear();
        parameters.Add("Pzab", Pzab);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInC("2.14", parameters);
        fp.DP = fp.reservoirPressurePa - Pzab;
        p.POutC("2.14", "DP", fp.DP);
    }
    
    /// <summary>
    /// Коэффициент продуктивности скважины (м3/(с*Па)) (2.15)
    /// </summary>
    /// <param name="alfa">Угол наклона прямой, характеризующей зависимость депрессии от дебита, рад</param>
    public void Kp3(double alfa)
    {
        parameters.Clear();
        parameters.Add("alfa", alfa);
        p.PInC("2.15", parameters);
        fp.Kp = 1.0 / Math.Tan(alfa);
        p.POutC("2.15", "Kp", fp.Kp);
    }
    
    /// <summary>
    /// Коэффициент продуктивности скважины, м3/(с*Па) (2.16)
    /// </summary>
    /// <param name="DPz1">Депрессия на пласт во время первого замера, Па</param>
    /// <param name="DPz1">Депрессия на пласт во время второго замера, Па</param>
    /// <param name="Qz1">Дебит скважины во время первого замера, м3/с</param>
    /// <param name="Qz2">Дебит скважины во время второго замера, м3/с</param>
    public void Kp4(double DPz1, double DPz2, double Qz1, double Qz2)
    {
        parameters.Clear();
        parameters.Add("DPz1", DPz1);
        parameters.Add("DPz2", DPz2);
        parameters.Add("Qz1", Qz1);
        parameters.Add("Qz2", Qz2);
        p.PInC("2.16", parameters);
        fp.Kp = (Qz2 - Qz1) / (DPz2 - DPz1);
        p.POutC("2.16", "Kp", fp.Kp);
    }
    
    /// <summary>
    /// Продолжительность исследования (секунды) (2.17, 2.23)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void dt(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("t0", fp.t0);
        p.PInC("2.17, 2.23", parameters);
        fp.dt = time - fp.t0;
        p.POutC("2.17, 2.23", "dt", fp.dt);
    }
    
    /// <summary>
    /// Гидропроводность (м3/(с*Па)) (2.18, 2.24)
    /// </summary>
    public void eps2()
    {
        parameters.Clear();
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("oilViscosity", fp.oilViscosity);
        p.PInC("2.18, 2.24", parameters);
        fp.eps = fp.k * fp.h / fp.oilViscosity;
        p.POutC("2.18, 2.24", "eps", fp.eps);
    }
    
    /// <summary>
    /// Коэффициент подвижности пласта (м2/(с*Па)) (2.19, 2.25)
    /// </summary>
    public void Kpodv()
    {
        parameters.Clear();
        parameters.Add("eps", fp.eps);
        parameters.Add("h", fp.h);
        p.PInC("2.19, 2.25", parameters);
        fp.Kpodv = fp.eps / fp.h;
        p.POutC("2.19, 2.25", "Kpodv", fp.Kpodv);
    }
    
    /// <summary>
    /// Скин-фактор (2.21)
    /// </summary>
    /// <param name="Pzost">Забойное давление в скважине в момент остановки скважины на исследование, Па</param>
    /// <param name="Pzost1h">Забойное давление после 1 ч с момента остановки скважины на исследование, Па</param>
    /// <param name="mln">Угловой коэффициент</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void skinFactor1(double Pzost, double Pzost1h, double mln, double rc)
    {
        parameters.Clear();
        parameters.Add("Pzost", Pzost);
        parameters.Add("Pzost1h", Pzost1h);
        parameters.Add("mln", mln);
        parameters.Add("rc", rc);
        parameters.Add("k", fp.k);
        parameters.Add("m", fp.m);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Bpl", fp.Bpl);
        p.PInC("2.21", parameters);
        fp.skinFactor = 0.5 * ((Pzost - Pzost1h) / mln - Math.Log(fp.k / (fp.m * fp.oilViscosity * fp.Bpl * Math.Pow(rc, 2))) +
                               7.12034);
        p.POutC("2.21", "skinFactor", fp.skinFactor);
    }
    
    /// <summary>
    /// Время Хорнера (с) (2.22)
    /// </summary>
    public void dte()
    {
        parameters.Clear();
        parameters.Add("dt", fp.dt);
        parameters.Add("tp", fp.tp);
        p.PInC("2.22", parameters);
        fp.dte = (fp.dt + fp.tp) / fp.dt;
        p.POutC("2.22", "dte", fp.dte);
    }
    
    /// <summary>
    /// Скин-фактор (2.27)
    /// </summary>
    /// <param name="Pzost">Забойное давление в скважине в момент остановки скважины на исследование, Па</param>
    /// <param name="Pzost1h">Забойное давление после 1 ч с момента остановки скважины на исследование, Па</param>
    /// <param name="mln">Угловой коэффициент</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void skinFactor2(double Pzost, double Pzost1h, double mln, double rc)
    {
        parameters.Clear();
        parameters.Add("Pzost", Pzost);
        parameters.Add("Pzost1h", Pzost1h);
        parameters.Add("mln", mln);
        parameters.Add("rc", rc);
        parameters.Add("tp", fp.tp);
        parameters.Add("k", fp.k);
        parameters.Add("m", fp.m);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Bpl", fp.Bpl);
        p.PInC("2.27", parameters);
        fp.skinFactor = 0.5 * ((Pzost1h - Pzost) / mln + Math.Log((fp.tp + 1) / fp.tp) - 
                                 Math.Log(fp.k / (fp.m * fp.oilViscosity * fp.Bpl * Math.Pow(rc, 2))) + 7.12034);
        p.POutC("2.27", "skinFactor", fp.skinFactor);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле (Па) (2.28)
    /// </summary>
    public void Pbuf1()
    {
        parameters.Clear();
        parameters.Add("Qg", fp.Qg);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        p.PInC("2.28", parameters);
        fp.Pbuf1 = 0.0729 * Math.Pow(1.0, 2) * fp.Qg * 86400.0 * fp.densityOfGas * fp.Plin /
                   Math.Pow((1000.0 * fp.dsht), 2);
        p.POutC("2.28", "Pbuf1", fp.Pbuf1);
    }
    
    /// <summary>
    /// Расход газа (м3/с) (2.29)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Qg(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Vgvust", fp.Vgvust);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pbufm", fp.Pbufm);
        p.PInC("2.29", parameters);
        fp.Qg = Q * fp.Vgvust * fp.Pst * fp.Tu * fp.Z / (fp.Pbufm * fp.Tst);
        p.POutC("2.29", "Qg", fp.Qg);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле (Па) (2.30)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Pbuf2(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        parameters.Add("g", fp.g);
        p.PInC("2.30", parameters);
        fp.Pbuf2 = fp.Plin + Math.Pow(Q, 2) / (78.8768 * Math.Pow(1000.0 * fp.dsht, 4) * fp.g);
        p.POutC("2.30", "Pbuf2", fp.Pbuf2);
    }
    
    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по формулам (Па) (2.31)
    /// </summary>
    /// <param name="Pbuf1">Буферное давление, рассчитанное по первой формуле, Па</param>
    /// <param name="Pbuf2">Буферное давление, рассчитанное по второй формуле, Па</param>
    public void Pbufm(double Pbuf1, double Pbuf2)
    {
        parameters.Clear();
        parameters.Add("Pbuf1", Pbuf1);
        parameters.Add("Pbuf2", Pbuf2);
        p.PInC("2.31", parameters);
        fp.Pbufm = Math.Max(Pbuf1, Pbuf2);
        p.POutC("2.31", "Pbufm", fp.Pbufm);
    }

    /// <summary>
    /// Давление на затрубе (Па) (2.32)
    /// </summary>
    /// <param name="Ppr">Давление на приеме в НКТ, Па</param>
    /// <param name="hdin">Динамический уровень в скважине, м</param>
    /// <param name="Tu">Температура на устье, К</param>
    public void Pzatr(double Ppr, double hdin, double Tu)
    {
        parameters.Clear();
        parameters.Add("Ppr", Ppr);
        parameters.Add("hdin", hdin);
        parameters.Add("Tu", Tu);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Z", fp.Z);
        p.PInC("2.32", parameters);
        fp.Pzatr = (Ppr - fp.densityOfOil * fp.g * (fp.Lcp - hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * Math.Pow(hdin, -1.0) * fp.densityOfGas / (Tu * fp.Z));
        p.POutC("2.32", "Pzatr", fp.Pzatr);
    }

    /// <summary>
    /// Количество свободного газа (д. ед.) (2.33)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void ng(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("nv", fp.nv);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Z", fp.Z);
        parameters.Add("Tst", fp.Tst);
        p.PInC("2.33", parameters);
        var tempVar = fp.gasFactor * (Pz - fp.Pnas0) / (fp.Pst - fp.Pnas0) * 
                      (1.0 - fp.nv) * fp.Pst * fp.reservoirPressurePa * fp.Z / (Pz * fp.Tst);
        fp.ng = tempVar / (1.0 + tempVar);
        p.POutC("2.33", "ng", fp.ng);
    }

    /// <summary>
    /// Показатель степени в формуле (2.35) (д. ед.) (2.34)
    /// </summary>
    public void nst()
    {
        parameters.Clear();
        parameters.Add("ng", fp.ng);
        p.PInC("2.34", parameters);
        fp.nst = 1.0 - 0.03 * fp.ng;
        p.POutC("2.34", "nst", fp.nst);
    }

    /// <summary>
    /// Дебит скважины (м3 / с) (2.35)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void Q2(double Pz, double rc)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("nst", fp.nst);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("2.35", parameters);
        fp.Q = 2.0 * fp.pi * fp.k * fp.h * Math.Pow(fp.reservoirPressurePa - Pz, fp.nst) / 
               (fp.oilViscosity * Math.Log(fp.Rk / (rc * Math.Exp(-fp.skinFactor))));
        p.POutC("2.35", "Q", fp.Q);
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (2.36, 2.37)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("deltaPpred", fp.deltaPpred);
        parameters.Add("A1", fp.A1);
        parameters.Add("A2", fp.A2);
        p.PInC("2.36, 2.37", parameters);
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
        p.POutC("2.36, 2.37", "MP", fp.MP);
    }

    /// <summary>
    /// Обводненность продукции (д. ед.) (2.38)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void nv(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("productWaterCut0", fp.productWaterCut0);
        parameters.Add("productWaterCutMax", fp.productWaterCutMax);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("DPmax", fp.DPmax);
        p.PInC("2.38", parameters);
        fp.nv = fp.productWaterCut0 + (fp.productWaterCutMax - fp.productWaterCut0) * 
            Math.Pow(fp.reservoirPressurePa - Pz, 2) / fp.DPmax;
        p.POutC("2.38", "nv", fp.nv);
    }
}
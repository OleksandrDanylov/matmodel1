public class Debug_MainPart
{
    private Print p;
    private FormulaParameters fp;
    private Prod_1_2_Debug p12d;
    
    public Debug_MainPart(FormulaParameters _fp, Print _p)
    {
        p = _p;
        fp = _fp;
        p12d = new Prod_1_2_Debug(fp, p);
    }

    public void Debug_Main()
    {
        p12d.Prod_1_2_Debug_Main();
    }
}
using System;

public class BlockSchemes_MainPart
{
    private Print p;
    private FormulaParameters fp;
    private HydroDynInv_1_3_BlockSchemes h13bs;
    private HydroDynInv_1_6_BlockSchemes h16bs;
    private Overhaul_1_1_BlockSchemes o11bs;
    private Overhaul_1_3_BlockSchemes o13bs;
    private Overhaul_1_6_BlockSchemes o16bs;
    private Overhaul_1_7_BlockSchemes o17bs;
    private Prod_1_1_BlockSchemes p11bs;
    private Prod_1_2_BlockSchemes p12bs;
    private Prod_1_3_BlockSchemes p13bs;
    private Prod_1_4_BlockSchemes p14bs;
    private Prod_1_5_BlockSchemes p15bs;
    private Prod_1_6_BlockSchemes p16bs;
    private Debug_MainPart dmp;
    private double deltaTime = 1.0;
    private static int Nmax = 50; // 7200;
    private int debugging = 0; // 0 - not debug; 1 - debug;

    public BlockSchemes_MainPart()
    {
        p = new Print(debugging);
        fp = new FormulaParameters(p);
        p.Fp = fp;
        fp.time = 0.0;
        fp.time_1 = 0.0;
        h13bs = new HydroDynInv_1_3_BlockSchemes(fp, p);
        h16bs = new HydroDynInv_1_6_BlockSchemes(fp, p);
        o11bs = new Overhaul_1_1_BlockSchemes(fp, p);
        o13bs = new Overhaul_1_3_BlockSchemes(fp, p);
        o16bs = new Overhaul_1_6_BlockSchemes(fp, p);
        o17bs = new Overhaul_1_7_BlockSchemes(fp, p);
        p11bs = new Prod_1_1_BlockSchemes(fp, p);
        p12bs = new Prod_1_2_BlockSchemes(fp, p);
        p13bs = new Prod_1_3_BlockSchemes(fp, p);
        p14bs = new Prod_1_4_BlockSchemes(fp, p);
        p15bs = new Prod_1_5_BlockSchemes(fp, p);
        p16bs = new Prod_1_6_BlockSchemes(fp, p);
        dmp = new Debug_MainPart(fp, p);
        if (debugging == 0)
        {
            p.PTxt("BlockSchemes_MainPart constructor");
            fp.Pz_1 = fp.Pz;
            p.POutC("BlockSchemes_MainPart constructor", "Pz_1", fp.Pz_1);
            fp.dsht_1 = fp.dsht;
            p.POutC("BlockSchemes_MainPart constructor", "dsht_1", fp.dsht_1);
        }
    }

    private void Cycle()
    {
        p.PTxt("************************************Cycle*****************************************");
        fp.time_1 = fp.time;
        fp.time += deltaTime;
        fp.tn += deltaTime;
        fp.tz1 = fp.time - deltaTime;
        fp.tz2 = fp.time;
        p.PT(fp.time);
        Console.WriteLine("Cycle(): time = " + fp.time);

        // task attribute:
        // 0   - Overhaul_1_3,
        // 10  - HydroDynInv_1_1,
        // 20  - Prod_1_5 (Diagram_1_1 - Diagram_1_5),
        // 21  - Prod_1_5 (Diagram_1_6),
        // 25  - Prod_1_5 (Diagram_2_1 - Diagram_2_5),
        // 26  - Prod_1_5 (Diagram_2_6),
        // 30  - HydroDynInv_1_3 (Diagram_4_1 - Diagram_4_3),
        // 31  - HydroDynInv_1_3 (Diagram_4_4),
        // 32  - HydroDynInv_1_3 (Diagram_4_5),
        // 40  - Prod_1_4,
        // 50  - Prod_1_2 (Diagram_4_1 - Diagram_4_3),
        // 51  - Prod_1_2 (Diagram_4_4),
        // 52  - Prod_1_2 (Diagram_4_5),
        // 60  - Overhaul_1_6,
        // 70  - Prod_1_6 (Diagram_1_1 - Diagram_1_3),
        // 71  - Prod_1_6 (Diagram_1_4),
        // 72  - Prod_1_6 (Diagram_1_5),
        // 80  - HydroDynInv_1_6
        // 90  - Prod_1_1 (Diagram_1_1 - Diagram_1_3)
        // 91  - Prod_1_1 (Diagram_1_1, Diagram_1_2,  Diagram_1_4)
        // 92  - Prod_1_1 ( Diagram_1_5)
        // 100 - Prod_1_3 (Diagram_2_1 - Diagram_2_6)
        // 101 - Prod_1_3 (Diagram_2_1 - Diagram_2_7)
        // 102 - Prod_1_3 (Diagram_2_8)
        // 110 - Overhaul_1_7
        // 120 - Overhaul_1_1 (Diagram_1)
        // 121 - Overhaul_1_1 (Diagram_2)
        // 130 - Часть IX. Ремонтное цементирование под давлением через отверстия перфорации
        switch (fp.pTask)
        {
            case 0:
                o13bs.Diagram(fp.time);
                break;
            case 10: 
                break;
            case 20: 
                p15bs.Diagram_1_1();
                p15bs.Diagram_1_2();
                p15bs.Diagram_1_3();
                p15bs.Diagram_1_4(fp.time);
                p15bs.Diagram_1_5(fp.time);
                break;
            case 21: 
                p15bs.Diagram_1_6(fp.time);
                break;
            case 25: 
                p15bs.Diagram_2_1();
                p15bs.Diagram_2_2();
                p15bs.Diagram_2_3();
                p15bs.Diagram_2_4(fp.time);
                p15bs.Diagram_2_5(fp.time);
                break;
            case 26: 
                p15bs.Diagram_2_6(fp.time);
                break;
            case 30: // HydroDynInv_1_3 (Diagram_4_1 - Diagram_4_3)
                if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    h13bs.Diagram_4_3(fp.time_1, fp.time);
                break;
            case 31: // HydroDynInv_1_3 (Diagram_4_4)
                if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    h13bs.Diagram_4_4(fp.time_1, fp.time);
                break;
            case 32: // HydroDynInv_1_3 (Diagram_4_5)
                if (fp.dsht <= 1.0E-5) h13bs.Diagram_4_5(fp.time_1, fp.time);
                break;
            case 40: break;
            case 50: // Prod_1_2 (Diagram_4_1 - Diagram_4_3)
                if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    p12bs.Diagram_4_3(fp.time_1, fp.time);
                break;
            case 51: // Prod_1_2 (Diagram_4_4)
                if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    p12bs.Diagram_4_4(fp.time_1, fp.time);
                break;
            case 52: // Prod_1_2 (Diagram_4_5)
                if (fp.dsht <= 1.0E-5) p12bs.Diagram_4_5(fp.time_1, fp.time);
                break;
            case 60: // Overhaul_1_6
                o16bs.Diagram(fp.time);
                break;
            case 70: // Prod_1_6 (Diagram_1_1 - Diagram_1_3)
                if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    p16bs.Diagram_1_3(fp.time);
                break;
            case 71: // Prod_1_6 (Diagram_1_4)
                if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    p16bs.Diagram_1_4(fp.time);
                break;
            case 72: // Prod_1_6 (Diagram_1_5)
                if (fp.dsht <= 1.0E-5) p16bs.Diagram_1_5(fp.time);
                break;
            case 80: 
                break; 
            case 90: // Prod_1_1 (Diagram_1_1 - Diagram_1_3)
                p11bs.Diagram_1_1();
                p11bs.Diagram_1_2();
                p11bs.Diagram_1_3(fp.time);
                break;
            case 91: // Prod_1_1 (Diagram_1_1, Diagram_1_2,  Diagram_1_4)
                p11bs.Diagram_1_1();
                p11bs.Diagram_1_2();
                p11bs.Diagram_1_4(fp.time);
                break;
            case 92: // Prod_1_1 ( Diagram_1_5)
                p11bs.Diagram_1_5(fp.time);
                break;
            case 100: // Prod_1_3 (Diagram_2_1 - Diagram_2_6)
                p13bs.Diagram_2_1();
                p13bs.Diagram_2_2();
                p13bs.Diagram_2_3();
                p13bs.Diagram_2_4();
                p13bs.Diagram_2_5();
                p13bs.Diagram_2_6(fp.time);
                break;
            case 101: // Prod_1_3 (Diagram_2_1 - Diagram_2_7)
                p13bs.Diagram_2_1();
                p13bs.Diagram_2_2();
                p13bs.Diagram_2_3();
                p13bs.Diagram_2_4();
                p13bs.Diagram_2_5();
                p13bs.Diagram_2_6(fp.time);
                p13bs.Diagram_2_7(fp.time);
                break;
            case 102: // Prod_1_3 (Diagram_2_8)
                p13bs.Diagram_2_8(fp.time);
                break;
            case 110: // Overhaul_1_7
                o17bs.Diagram_1(fp.time);
                break;
            case 120: // Overhaul_1_1 (Diagram_1)
                o11bs.Diagram_1(fp.time);
                break;
            case 121: // Overhaul_1_1 (Diagram_2)
                o11bs.Diagram_2(fp.time);
                break;
            case 130: // Часть IX. Ремонтное цементирование под давлением через отверстия перфорации 
                break;        
            default: break;
        }
        // Beautiful print
        p.POutBP(fp.time);
        // Other calculations
        fp.Pz_1 = fp.Pz;
        p.POutC("Cycle", "Pz_1", fp.Pz_1);
        fp.dsht_1 = fp.dsht;
        p.POutC("Cycle", "dsht_1", fp.dsht_1);
    }

    static void Main(string[] args)
    {
        BlockSchemes_MainPart BS_MP = new BlockSchemes_MainPart();

        // Debugging
        if (BS_MP.debugging != 0) BS_MP.dmp.Debug_Main();
        // Not debugging 
        else
        {
            for (var N = 0; N < Nmax; N++) BS_MP.Cycle();
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;

public class Print
{
    private Dictionary<string, double> bpp = new Dictionary<string, double>();
    private int varLength = 20;

    private FormulaParameters fp;
    public FormulaParameters Fp
    {
        set => fp = value;
    }
    private StreamWriter swC, swD, swBP;
    private string filePathC = "c:\\_MatModelDebug\\MatModelCycle.txt";
    private string filePathD = "c:\\_MatModelDebug\\MatModelDebug.txt";
    private string filePathBP = "c:\\_MatModelDebug\\BeautifulPrint.txt";

    public Print(int debugging)
    {
        bool exists = Directory.Exists("c:\\_MatModelDebug");
        if (!exists) Directory.CreateDirectory("c:\\_MatModelDebug");
        // 0 - not debug; 1 - debug
        switch (debugging)
        {
            case 0:
                swC = (File.Exists(filePathC)) ? File.CreateText(filePathC) : File.AppendText(filePathC);
                swC.Close();
                swBP = (File.Exists(filePathBP)) ? File.CreateText(filePathBP) : File.AppendText(filePathBP);
                swBP.Close();
                break;
            case 1:
                swD = (File.Exists(filePathD)) ? File.CreateText(filePathD) : File.AppendText(filePathD);
                swD.Close();
                break;
            default: goto case 0;
        }
    }

    // Print text (Debug)
    public void PfNameD(string methodNumber)
    {
        using (swD = File.AppendText(filePathD))
        {
            swD.WriteLine();
            swD.WriteLine();
            swD.WriteLine("Формула " + methodNumber + ":");
            swD.WriteLine();
            swD.Close();
        }
    }    
    
    // Print input parameter (Debug)
    public void PInD(string parameterName, double parameterValue)
    {
        using (swD = File.AppendText(filePathD))
        {
            swD.WriteLine("Входной параметр " + parameterName + " = " + parameterValue + ";");
            swD.Close();
        }
    }
    
    // Print output parameter (Debug)
    public void POutD(string parameterName, double parameterValue)
    {
        using (swD = File.AppendText(filePathD))
        {
            swD.WriteLine();
            swD.WriteLine("Выходной параметр " + parameterName + " = " + parameterValue + ";");
            swD.Close();
        }
    }

    // Print time (Cycle)
    public void PT(double time)
    {
        using (swC = File.AppendText(filePathC))
        {
            swC.WriteLine();
            swC.WriteLine();
            swC.WriteLine("Значения входных и выходных параметров алгоритмов для time = " + time + ":");
            swC.Close();
        }     
    }
    
    // Print text (Cycle)
    public void PTxt(string text)
    {
        using (swC = File.AppendText(filePathC))
        {
            swC.WriteLine();
            swC.WriteLine();
            swC.WriteLine();
            swC.WriteLine(text);
            swC.Close();
        }
    }
    
    // Print input parameters (Cycle)
    public void PInC(string methodNumber, Dictionary<string, double> parameters)
    {
        using (swC = File.AppendText(filePathC))
        {
            swC.WriteLine();
            swC.WriteLine();
            swC.WriteLine("Метод (" + methodNumber + "), входные параметры:");
            var maxLength = 0;
            foreach (var keyValue in parameters)
            {
                var keyLength = keyValue.Key.Length;
                if (maxLength < keyLength) maxLength = keyLength;
            }
            foreach (var keyValue in parameters)
            {
                swC.WriteLine(keyValue.Key.PadRight(maxLength) + " = " + keyValue.Value + ";");
            }
        }
        swC.Close();
    }
    
    // Print output parameter (Cycle)
    public void POutC(string methodNumber, string parameterName, double parameterValue)
    {
        using (swC = File.AppendText(filePathC))
        {
            swC.WriteLine();
            swC.WriteLine("Метод (" + methodNumber + "), выходной параметр:  " + parameterName + " = " + parameterValue + ";");
            swC.Close();
        }
    }
    
    // Print input parameter (Block-schemes in cycle)
    public void PInBS(string blockSchemeName, Dictionary<string, double> parameters)
    {
        using (swC = File.AppendText(filePathC))
        {
            swC.WriteLine();
            swC.WriteLine();
            swC.WriteLine("Блок-схема " + blockSchemeName + ", входные параметры:");
            var maxLength = 0;
            foreach (var keyValue in parameters)
            {
                var keyLength = keyValue.Key.Length;
                if (maxLength < keyLength) maxLength = keyLength;
            }
            foreach (var keyValue in parameters)
            {
                swC.WriteLine(keyValue.Key.PadRight(maxLength) + " = " + keyValue.Value + ";");
            }        
        }
    }
    
    // Beautiful print
    public void POutBP(double time)
    {
        bpp.Clear();
        bpp.Add("time", fp.time);
        bpp.Add("Pbuf1", fp.Pbuf1);
        bpp.Add("Pbuf2", fp.Pbuf2);
        bpp.Add("Q", fp.Q);
        bpp.Add("Qzh", fp.Qzh);
        bpp.Add("Lc", fp.Lc);
        bpp.Add("Ppr", fp.Ppr);
        bpp.Add("Tpr", fp.Tpr);
        bpp.Add("DPsep", fp.DPsep);
        bpp.Add("Pdikt1", fp.Pdikt1);
        bpp.Add("Pdikt2", fp.Pdikt2);
        bpp.Add("Pz", fp.Pz);
        using (swBP = File.AppendText(filePathBP))
        {
            if (Math.Abs(((int)time - 1) % 50) < 1.0E-5)
            {
                var str1 = "";
                var str2 = "";
                var nVar = 0;
                foreach (var keyValue in bpp)
                {
                    nVar++;
                    var nVarStr = "'" + nVar + "'";
                    var deltaLength =  varLength - nVarStr.Length;
                    var lD = deltaLength / 2 + nVarStr.Length;
                    str1 += nVarStr.PadLeft(lD).PadRight(varLength);
                    deltaLength =  varLength - keyValue.Key.Length;
                    lD = deltaLength / 2 + keyValue.Key.Length;
                    str2 += keyValue.Key.PadLeft(lD).PadRight(varLength);
                }
                swBP.WriteLine(str1);
                swBP.WriteLine(str2);
            }
            var str3 = "";
            foreach (var keyValue in bpp)
            {
                var value = keyValue.Value.ToString();
                var deltaLength =  varLength - value.Length;
                var lD = deltaLength / 2 + value.Length;
                str3 += value.PadLeft(lD).PadRight(varLength);
            }
            swBP.WriteLine(str3);
            swBP.Close();
        }
    }
}
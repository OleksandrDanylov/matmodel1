using System;
using System.Collections.Generic;

public class Prod_1_3
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_3(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
                
    /// <summary>
    /// Рабочее давление закачки газа (затрубное давление) (Па) (2.1)
    /// </summary>
    public void Ppz1()
    {
        parameters.Clear();
        parameters.Add("Lkl[0]", fp.Lkl[0]);
        parameters.Add("htr", fp.htr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("g", fp.g);
        parameters.Add("hzatr", fp.hzatr);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("2.1", parameters);
        fp.Ppz = ((fp.Lkl[0] - fp.htr) * fp.densityOfGas * fp.g - (fp.Lkl[0] - fp.hzatr) * fp.densityOfGas * fp.g) / 
                 Math.Exp(0.03415 * fp.hzatr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("2.1", "Ppz", fp.Ppz);
    }
    
    /// <summary>
    /// Объемный расход закачки газа (м3/с) (2.2, 2.66)
    /// </summary>
    public void Qzakg()
    {
        parameters.Clear();
        parameters.Add("Pzak", fp.Pzak);
        parameters.Add("dshtzatr", fp.dshtzatr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Ppz", fp.Ppz);
        p.PInC("2.2, 2.66", parameters);
        fp.Qzakg = fp.Pzak * Math.Pow(1000.0 * fp.dshtzatr, 2) / 
                   (0.0729 * Math.Pow(1.0, 2) * 86400.0 * fp.densityOfGas * fp.Ppz);
        p.POutC("2.2, 2.66", "Qzakg", fp.Qzakg);
    }

    /// <summary>
    /// Объем газа, поступивший в затрубное пространство (м3) (2.3, 2.67)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Vgzatr1(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Qzakg", fp.Qzakg);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("Tnu", fp.Tnu);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Ppz", fp.Ppz);
        p.PInC("2.3, 2.67", parameters);
        fp.Vgzatr = fp.Qzakg * time * fp.Pnu * fp.Tu * fp.Z / (fp.Ppz * fp.Tnu);
        p.POutC("2.3, 2.67", "Vgzatr", fp.Vgzatr);
    }
    
    /// <summary>
    /// Уровень жидкости в затрубном пространстве (м) (2.4, 2.68)
    /// </summary>
    public void hzatr()
    {
        parameters.Clear();
        parameters.Add("Vgzatr", fp.Vgzatr);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("2.4, 2.68", parameters);
        fp.hzatr += 4.0 * fp.Vgzatr / (fp.pi * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)));
        p.POutC("2.4, 2.68", "hzatr", fp.hzatr);
    }

    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.5)
    /// </summary>
    public void htr1()
    {
        parameters.Clear();
        parameters.Add("Vgzatr", fp.Vgzatr);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.5", parameters);
        fp.htr -= 4.0 * fp.Vgzatr / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("2.5", "htr", fp.htr);
    }

    /// <summary>
    /// Объемный расход газа из скважины на устье (м3/с) (2.6)
    /// </summary>
    public void Qgu1()
    {
        parameters.Clear();
        parameters.Add("Qzakg", fp.Qzakg);
        p.PInC("2.6", parameters);
        fp.Qgu = 0.5 * fp.Qzakg;
        p.POutC("2.6", "Qgu", fp.Qgu);
    }
        
    /// <summary>
    /// Температурный градиент потока (К/м) (2.7, 2.29, 2.46, 2.91, 2.106)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Tgrp(double InnerDiam)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Gtgr", fp.Gtgr);
        parameters.Add("Q", fp.Q);
        p.PInC("2.7, 2.29, 2.46, 2.91, 2.106", parameters);
        fp.Tgrp = (0.0034 + 0.79 * fp.Gtgr) / Math.Pow(10, fp.Q / (20.0 * Math.Pow(InnerDiam, 2.67)));
        p.POutC("2.7, 2.29, 2.46, 2.91, 2.106", "Tgrp", fp.Tgrp);
    }

    /// <summary>
    /// Температура на устье скважины (K) (2.8)
    /// </summary>
    public void Tu()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgrp", fp.Tgrp);
        parameters.Add("Lk", fp.Lk);
        p.PInC("2.8", parameters);
        fp.Tu = fp.Tpl - fp.Tgrp * fp.Lk;
        p.POutC("2.8", "Tu", fp.Tu);
    }
        
    /// <summary>
    /// Давление насыщения нефти газом при Tуст (Па) (2.9)
    /// </summary>
    public void Pnasust()
    {
        parameters.Clear();
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Nc", fp.Nc);
        parameters.Add("Na", fp.Na);
        p.PInC("2.9", parameters);
        fp.Pnasust = fp.Pnas0 - (fp.Tpl - fp.Tu) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
        p.POutC("2.9", "Pnasust", fp.Pnasust);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (2.10)
    /// </summary>
    public void RfP1()
    {
        parameters.Clear();
        parameters.Add("Pu", fp.Pu);
        parameters.Add("Pnasust", fp.Pnasust);
        p.PInC("2.10", parameters);
        fp.RfP = (1.0 + Math.Log10(1.0E-6 * fp.Pu)) / (1.0 + Math.Log10(1.0E-6 * fp.Pnasust)) - 1.0;
        p.POutC("2.10", "RfP", fp.RfP);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (2.11)
    /// </summary>
    public void mfT1()
    {
        parameters.Clear();
        parameters.Add("Tu", fp.Tu);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.11", parameters);
        fp.mfT = 1.0 + 0.029 * (fp.Tu - 293.0) * (fp.densityOfOil * fp.densityOfGas * 1.0E-3 - 0.7966);
        p.POutC("2.11", "mfT", fp.mfT);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (2.12)
    /// </summary>
    public void DfT1()
    {
        parameters.Clear();
        parameters.Add("Tu", fp.Tu);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.12", parameters);
        fp.DfT = 1.0E-3 * fp.densityOfOil * fp.densityOfGas * (4.5 - 0.00305 * (fp.Tu - 293.0)) - 4.785;
        p.POutC("2.12", "DfT", fp.DfT);
    }
    
    /// <summary>
    /// Удельный объем выделившегося газа из нефти на устье (м3/м3) (2.13)
    /// </summary>
    public void Vgvust()
    {
        parameters.Clear();
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("RfP", fp.RfP);
        parameters.Add("mfT", fp.mfT);
        parameters.Add("DfT", fp.DfT);
        p.PInC("2.13", parameters);
        fp.Vgvust = fp.gasFactor * fp.RfP * fp.mfT * (fp.DfT * (1 + fp.RfP) - 1.0);
        p.POutC("2.13", "Vgvust", fp.Vgvust);
    }    

    /// <summary>
    /// Объемный расход газа из скважины на устье (м3/с) (2.14)
    /// </summary>
    public void Qgu2()
    {
        parameters.Clear();
        parameters.Add("Qzakg", fp.Qzakg);
        parameters.Add("Q", fp.Q);
        parameters.Add("Vgvust", fp.Vgvust);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pu", fp.Pu);
        parameters.Add("Tnu", fp.Tnu);
        p.PInC("2.14", parameters);
        fp.Qgu = 0.5 * fp.Qzakg + fp.Q * fp.Vgvust * fp.Pnu * fp.Tu * fp.Z / (fp.Pu * fp.Tnu);
        p.POutC("2.14", "Qgu", fp.Qgu);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле (Па) (2.15, 2.125)
    /// </summary>
    public void Pbuf1()
    {
        parameters.Clear();
        parameters.Add("Qg", fp.Qg);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        p.PInC("2.15, 2.125", parameters);
        fp.Pbuf1 = 0.0729 * Math.Pow(1.0, 2) * fp.Qg * 86400.0 * fp.densityOfGas * fp.Plin /
                   Math.Pow(1000.0 * fp.dsht, 2);
        p.POutC("2.15, 2.125", "Pbuf1", fp.Pbuf1);
    }    
    
    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле (Па) (2.16, 2.127)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Pbuf2(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        parameters.Add("g", fp.g);
        p.PInC("2.16, 2.127", parameters);
        fp.Pbuf2 = fp.Plin + Math.Pow(Q, 2) / (78.8768 * Math.Pow(1000.0 * fp.dsht, 4) * fp.g);
        p.POutC("2.16, 2.127", "Pbuf2", fp.Pbuf2);
    }
      
    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по 1 и 2 формулам (Па) (2.17, 2.128)
    /// </summary>
    /// <param name="Pbuf1">Буферное давление, рассчитанное по первой формуле, Па</param>
    /// <param name="Pbuf2">Буферное давление, рассчитанное по второй формуле, Па</param>
    public void Pbufm(double Pbuf1, double Pbuf2)
    {
        parameters.Clear();
        parameters.Add("Pbuf1", Pbuf1);
        parameters.Add("Pbuf2", Pbuf2);
        p.PInC("2.17, 2.128", parameters);
        fp.Pbufm = Math.Max(Pbuf1, Pbuf2);
        p.POutC("2.17, 2.128", "Pbufm", fp.Pbufm);
    }  
    
    /// <summary>
    /// Плотность жидкости (кг/м3) (2.18, 2.79, 2.85)
    /// </summary>
    public void Rozh()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("nv", fp.nv);
        p.PInC("2.18, 2.79, 2.85", parameters);
        fp.Rozh = fp.densityOfOil * (1.0 - fp.nv) + fp.densityOfWater * fp.nv;
        p.POutC("2.18, 2.79, 2.85", "Rozh", fp.Rozh);
    }
    
    /// <summary>
    /// Скорость подъема жидкости в НКТ (м/с) (2.19, 2.84)
    /// </summary>
    public void Vzhnkt()
    {
        parameters.Clear();
        parameters.Add("Qn", fp.Qn);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.19, 2.84", parameters);
        fp.Vzhnkt = 4.0 * fp.Qn / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("2.19, 2.84", "Vzhnkt", fp.Vzhnkt);
    }
    
    /// <summary>
    /// Число Рейнольдса в НКТ (2.20, 2.86)
    /// </summary>
    public void Renkt1()
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Mjuzh", fp.Mjuzh);
        p.PInC("2.20, 2.86", parameters);
        fp.Renkt = fp.Vzhnkt * fp.Rozh * fp.dnkt / fp.Mjuzh;
        p.POutC("2.20, 2.86", "Renkt", fp.Renkt);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в НКТ (2.21, 2.87)
    /// </summary>
    public void LambdaNkt()
    {
        parameters.Clear();
        parameters.Add("Renkt", fp.Renkt);
        parameters.Add("EpsNkt", fp.EpsNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.21, 2.87", parameters);
        fp.LambdaNkt = 0.067 * (158.0 / fp.Renkt + 2.0 * fp.EpsNkt / fp.dnkt);
        p.POutC("2.21, 2.87", "LambdaNkt", fp.LambdaNkt);
    }
    
    /// <summary>
    /// Скорость подъема жидкости в ЭК (м/с) (2.22, 2.78)
    /// </summary>
    public void Vzhek()
    {
        parameters.Clear();
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.22, 2.78", parameters);
        fp.Vzhek = 4.0 * fp.Qskv / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("2.22, 2.78", "Vzhek", fp.Vzhek);
    }
    
    /// <summary>
    /// Число Рейнольдса в ЭК (2.23, 2.80)
    /// </summary>
    public void Reek()
    {
        parameters.Clear();
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("oilViscosity", fp.oilViscosity);
        p.PInC("2.23, 2.80", parameters);
        fp.Reek = fp.Vzhek * fp.Rozh * fp.ECInnerDiameter / fp.oilViscosity;
        p.POutC("2.23, 2.80", "Reek", fp.Reek);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК (2.24, 2.81)
    /// </summary>
    public void LambdaEk()
    {
        parameters.Clear();
        parameters.Add("Reek", fp.Reek);
        parameters.Add("EpsEk", fp.EpsEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.24, 2.81", parameters);
        fp.LambdaEk = 0.067 * (158.0 / fp.Reek + 2.0 * fp.EpsEk / fp.ECInnerDiameter);
        p.POutC("2.24, 2.81", "LambdaEk", fp.LambdaEk);
    }
        
    /// <summary>
    /// Давление на приеме в НКТ (Па) (2.26)
    /// </summary>
    public void Ppr1()
    {
        parameters.Clear();
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("L1", fp.L1);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.26", parameters);
        fp.Ppr = fp.P1nkt + (fp.Lcp - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("2.26", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Давление на выходе из насоса (Па) (2.27)
    /// </summary>
    public void Pzpk()
    {
        parameters.Clear();
        parameters.Add("P2ek", fp.P2ek);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("L1", fp.L1);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.27", parameters);
        fp.Pzpk = fp.P2ek + (fp.Lk - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("2.27", "Pzpk", fp.Pzpk);
    }
        
    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера (Па) (2.28)
    /// </summary>
    public void dP1()
    {
        parameters.Clear();
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Nint", fp.Nint);
        p.PInC("2.28", parameters);
        fp.dP = (fp.Pnas0 - fp.P1nkt) / fp.Nint;
        p.POutC("2.28", "dP", fp.dP);
    }
    
    /// <summary> 
    /// Температура в интервале H[j] (К) (2.30, 2.47, 2.92, 2.107)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Tj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgrp", fp.Tgrp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("H[j]", fp.H[j]);
        p.PInC("2.30, 2.47, 2.92, 2.107", parameters);
        fp.T[j] = fp.Tpl - fp.Tgrp * (fp.Lk - fp.H[j]);
        p.POutC("2.30, 2.47, 2.92, 2.107", "T[j]", fp.T[j]);
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (2.31, 2.48, 2.108)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pnasj1(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Nc", fp.Nc);
        parameters.Add("Na", fp.Na);
        p.PInC("2.31, 2.48, 2.108", parameters);
        fp.Pnas[j] = fp.Pnas0 - (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
        p.POutC("2.31, 2.48, 2.108", "Pnas[j]", fp.Pnas[j]);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (2.32, 2.49, 2.94, 2.109)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void RfP2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("Pnas[j]", fp.Pnas[j]);
        p.PInC("2.32, 2.49, 2.94, 2.109", parameters);
        fp.RfP = (1.0 + Math.Log10(1.0E-6 * fp.P[j])) / (1.0 + Math.Log10(1.0E-6 * fp.Pnas[j])) - 1.0;
        p.POutC("2.32, 2.49, 2.94, 2.109", "RfP", fp.RfP);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (2.33, 2.50, 2.95, 2.110)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void mfT2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.33, 2.50, 2.95, 2.110", parameters);
        fp.mfT = 1.0 + 0.029 * (fp.T[j] - 293.0) * (fp.densityOfOil * fp.densityOfGas * 1.0E-3 - 0.7966);
        p.POutC("2.33, 2.50, 2.95, 2.110", "mfT", fp.mfT);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (2.34, 2.51, 2.96, 2.111)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void DfT2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.34, 2.51, 2.96, 2.111", parameters);
        fp.DfT = 1.0E-3 * fp.densityOfOil * fp.densityOfGas * (4.5 - 0.00305 * (fp.T[j] - 293.0)) - 4.785;
        p.POutC("2.34, 2.51, 2.96, 2.111", "DfT", fp.DfT);
    }
    
    /// <summary>
    /// Удельный объем выделившегося газа (м3/м3) (2.35, 2.52, 2.97, 2.112)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается удельный объем выделившегося газа</param>
    public void Vgvj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("RfP", fp.RfP);
        parameters.Add("mfT", fp.mfT);
        parameters.Add("DfT", fp.DfT);
        p.PInC("2.35, 2.52, 2.97, 2.112", parameters);
        fp.Vgv[j] = fp.gasFactor * fp.RfP * fp.mfT * (fp.DfT * (1 + fp.RfP) - 1.0);
        p.POutC("2.35, 2.52, 2.97, 2.112", "Vgv[j]", fp.Vgv[j]);
    }
    
    /// <summary>
    /// Удельный объем смеси (м3/м3) (2.36, 2.53, 2.98, 2.113)
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vcm(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Vgv[j]", fp.Vgv[j]);
        parameters.Add("bOil", fp.bOil);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("Tnu", fp.Tnu);
        parameters.Add("nv", fp.nv);
        p.PInC("2.36, 2.53, 2.98, 2.113", parameters);
        fp.Vcm = fp.bOil + fp.Vgv[j] * fp.Z * fp.Pnu * fp.T[j] / (fp.P[j] * fp.Tnu) + 
                 fp.nv / (1.0 - fp.nv);
        p.POutC("2.36, 2.53, 2.98, 2.113", "Vcm", fp.Vcm);
    }
    
    /// <summary>
    /// Удельная масса смеси (кг/м3) (2.37, 2.54, 2.99, 2.114)
    /// </summary>
    public void Mcm()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("nv", fp.nv);
        p.PInC("2.37, 2.54, 2.99, 2.114", parameters);
        fp.Mcm = fp.densityOfOil + fp.densityOfGas * fp.gasFactor + 
                 fp.densityOfWater * fp.nv / (1.0 - fp.nv);
        p.POutC("2.37, 2.54, 2.99, 2.114", "Mcm", fp.Mcm);
    }
        
    /// <summary>
    /// Плотность газожидкостной смеси (кг/м3) (2.38, 2.55, 2.100, 2.115)
    /// </summary>
    public void Rocm()
    {
        parameters.Clear();
        parameters.Add("Vcm", fp.Vcm);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("2.38, 2.55, 2.100, 2.115", parameters);
        fp.Rocm = fp.Mcm / fp.Vcm;
        p.POutC("2.38, 2.55, 2.100, 2.115", "Rocm", fp.Rocm);
    }
    
    /// <summary>
    /// Число Рейнольдса (2.39, 2.56, 2.101, 2.116)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Re(double InnerDiam)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("2.39, 2.56, 2.101, 2.116", parameters);
        fp.Re = 0.99E-5 * 86400.0 * fp.Q * (1.0 - fp.nv) * fp.Mcm / InnerDiam;
        p.POutC("2.39, 2.56, 2.101, 2.116", "Re", fp.Re);
    }
    
    /// <summary>
    /// Корреляционный коэффициент (2.40, 2.57, 2.102, 2.117)
    /// </summary>
    public void f()
    {
        parameters.Clear();
        parameters.Add("Re", fp.Re);
        p.PInC("2.40, 2.57, 2.102, 2.117", parameters);
        fp.f = Math.Pow(10, 19.66 * Math.Pow(1.0 + Math.Log10(fp.Re), -0.25) - 17.713);
        p.POutC("2.40, 2.57, 2.102, 2.117", "f", fp.f);
    }
    
    /// <summary>
    /// Дифференциал (Па/м) (2.41, 2.58, 2.103, 2.118)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    /// <param name="Rocm">Плотность газожидкостной смеси, кг/м3</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dPdH(double InnerDiam, double Rocm, int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Rocm", Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("f", fp.f);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("2.41, 2.58, 2.103, 2.118", parameters);
        fp.dPdH[j] = Rocm * fp.g * Math.Cos(fp.alfa) + 
                     fp.f * Math.Pow(86400.0 * fp.Q, 2) * Math.Pow(1.0 - fp.nv, 2) * Math.Pow(fp.Mcm, 2) /
                     (2.3024E9 * Rocm * Math.Pow(InnerDiam, 5));
        p.POutC("2.41, 2.58, 2.103, 2.118", "dPdH[j]", fp.dPdH[j]);
    }
    
    /// <summary>
    /// Число, обратное дифференциалу (2.41) (м/Па) (2.42, 2.119)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dHdP(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("dPdH[j]", fp.dPdH[j]);
        p.PInC("2.42, 2.119", parameters);
        fp.dHdP[j] = 1.0 / fp.dPdH[j];
        p.POutC("2.42, 2.119", "dHdP[j]", fp.dHdP[j]);
    }    
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (2.43, 2.120)
    /// </summary>
    /// <param name="N">Количество элементов в массиве dHdP, шт.</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    /// <param name="dHdP">Массив значений дифференциала dH/dP на разных интервалах Hj</param>
    public void Hj1(int N, int j, double[] dHdP)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("j", j);
        parameters.Add("dHdP[j]", dHdP[j]);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("2.43, 2.120", parameters);
        var sum = 0.0;
        for (var i = 0; i < N; i++) sum += dHdP[i];
        fp.H[j] = fp.L1 + fp.dl * ((dHdP[1] + dHdP[j]) / 2.0 + sum);
        p.POutC("2.43, 2.120", "H[j]", fp.H[j]);
    }    
    
    /// <summary>
    /// Давление в интервале Н[j] (Па) (2.44, 2.121)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("dP", fp.dP);
        p.PInC("2.44, 2.121", parameters);
        fp.P[j] = fp.P1nkt + fp.dP * j;
        p.POutC("2.44, 2.121", "P[j]", fp.P[j]);
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (2.45)
    /// </summary>
    public void dl1()
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("L2", fp.L2);
        parameters.Add("Nint", fp.Nint);
        p.PInC("2.45", parameters);
        fp.dl = (fp.L2 - fp.L1) / fp.Nint;
        p.POutC("2.45", "dl", fp.dl);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (2.59)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Hj2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("2.59", parameters);
        fp.H[j] = fp.L1 + fp.dl * j;
        p.POutC("2.59", "H[j]", fp.H[j]);
    }

    /// <summary>
    /// Количество свободного газа (д. ед.) (2.60, 2.70)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void ng(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("nv", fp.nv);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Z", fp.Z);
        parameters.Add("Tnu", fp.Tnu);
        p.PInC("2.60, 2.70", parameters);
        var tempVar = fp.gasFactor * (Pz - fp.Pnas0) / (fp.Pnu - fp.Pnas0) * 
            (1.0 - fp.nv) * fp.Pnu * fp.Tpl * fp.Z / (Pz * fp.Tnu);
        fp.ng = tempVar / (1.0 + tempVar);
        p.POutC("2.60, 2.70", "ng", fp.ng);
    }    

    /// <summary>
    /// Показатель степени в формуле (2.62, 2.72) (д. ед.) (2.61, 2.71)
    /// </summary>
    public void nst()
    {
        parameters.Clear();
        parameters.Add("ng", fp.ng);
        p.PInC("2.61, 2.71", parameters);
        fp.nst = 1.0 - 0.03 * fp.ng;
        p.POutC("2.61, 2.71", "nst", fp.nst);
    }    

    /// <summary>
    /// Расход жидкости, поступающей из пласта  (м3 / с) (2.62, 2.72)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void Qskv1(double Pz, double rc)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("nst", fp.nst);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("2.62, 2.72", parameters);
        fp.Qskv = 2.0 * fp.pi * fp.k * fp.h * Math.Pow(fp.reservoirPressurePa - Pz, fp.nst) / 
                  (fp.oilViscosity * Math.Log(fp.Rk / (rc * Math.Exp(-fp.skinFactor))));
        p.POutC("2.62, 2.72", "Qskv", fp.Qskv);
    }    

    /// <summary>
    /// Расход жидкости, поступающей из пласта  (м3 / с) (2.63, 2.73)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void Qskv2(double Pz, double rc)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("nst", fp.nst);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("2.63, 2.73", parameters);
        fp.Qskv = 2.0 * fp.pi * fp.k * fp.h * (fp.reservoirPressurePa - Pz) / 
                  (fp.viscosityOfKillingFluid * Math.Log(fp.Rk / (rc * Math.Exp(-fp.skinFactor))));
        p.POutC("2.63, 2.73", "Qskv", fp.Qskv);
    }    
        
    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта (м3) (2.64, 2.74)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Vzhgzab(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("Vzhgzab", fp.Vzhgzab);
        p.PInC("2.64, 2.74", parameters);
        fp.Vzhgzab = fp.Qskv * (tz2 - tz1) + fp.Vzhgzab;
        p.POutC("2.64, 2.74", "Vzhgzab", fp.Vzhgzab);
    }
                
    /// <summary>
    /// Рабочее давление закачки газа (затрубное давление) (Па) (2.65)
    /// </summary>
    /// <param name="j">Номер интервала глубин газлифтных клапанов L[j], на котором рассчитывается давление</param>
    public void Ppz2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Lkl[j]", fp.Lkl[j]);
        parameters.Add("Pkl[j]", fp.Pkl[j]);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("hzatr", fp.hzatr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("2.65", parameters);
        fp.Ppz = (fp.Pkl[j] - fp.Rozh * fp.g * (fp.Lkl[j] - fp.hzatr)) / 
                 Math.Exp(0.03415 * fp.hzatr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("2.65", "Ppz", fp.Ppz);
    }    
        
    /// <summary>
    /// Давление на забое скважины (Па) (2.69.1)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.69.1", parameters);
        fp.Pz = fp.Pzrezh + fp.Qrezh * fp.oilViscosity * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("2.69.1", "Pz", fp.Pz);
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (2.69.2)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz2(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.69.2", parameters);
        fp.Pz = fp.Pzrezh - fp.Qrezh * fp.oilViscosity * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("2.69.2", "Pz", fp.Pz);
    }    
    
    /// <summary>
    /// Содержание механических примесей (кг/м3) (2.75, 2.76)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("deltaPpred", fp.deltaPpred);
        parameters.Add("A1", fp.A1);
        parameters.Add("A2", fp.A2);
        p.PInC("2.75, 2.76", parameters);
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
        p.POutC("2.75, 2.76", "MP", fp.MP);
    }

    /// <summary>
    /// Обводненность продукции (д. ед.) (2.77)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void nv(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("productWaterCut0", fp.productWaterCut0);
        parameters.Add("productWaterCutMax", fp.productWaterCutMax);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("DPmax", fp.DPmax);
        p.PInC("2.77", parameters);
        fp.nv = fp.productWaterCut0 + (fp.productWaterCutMax - fp.productWaterCut0) * 
            Math.Pow(fp.reservoirPressurePa - Pz, 2) / fp.DPmax;
        p.POutC("2.77", "nv", fp.nv);
    }     
    
    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ (м) (2.82)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg11(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.82", parameters);
        fp.Hg1 = fp.Lk - (fp.reservoirPressurePa - Pz) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                                          fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("2.82", "Hg1", fp.Hg1);
    }
        
    /// <summary>
    /// Давление на приеме в НКТ (Па) (Па) (2.83)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ppr2(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Hg1", fp.Hg1);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.83", parameters);
        if (fp.Hg1 < fp.Lcp) fp.Ppr = Pz - (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("2.83", "Ppr", fp.Ppr);
    }    
        
    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ (м) (2.88)
    /// </summary>
    /// <param name="Ppr">Давление на приеме в НКТ, Па</param>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg12(double Ppr, double Pz)
    {
        parameters.Clear();
        parameters.Add("Ppr", Ppr);
        parameters.Add("Pz", Pz);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.88", parameters);
        fp.Hg1 = fp.Lcp - (Ppr - Pz) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                        fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("2.88", "Hg1", fp.Hg1);
    }
        
    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ (м) (2.89)
    /// </summary>
    /// <param name="Ppr">Давление на приеме в НКТ, Па</param>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Pkl0(double Ppr, double Vzhnkt)
    {
        parameters.Clear();
        parameters.Add("Ppr", Ppr);
        parameters.Add("Vzhnkt", Vzhnkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.89", parameters);
        fp.Pkl[0] = Ppr - fp.Lcp * (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                    fp.LambdaNkt * Math.Pow(Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("2.89", "Pkl[0]", fp.Pkl[0]);
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (2.90)
    /// </summary>
    public void dl2()
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("L2", fp.L2);
        parameters.Add("Nint", fp.Nint);
        p.PInC("2.90", parameters);
        fp.dl = (fp.L1 - fp.L2) / fp.Nint;
        p.POutC("2.90", "dl", fp.dl);
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (2.93)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pnasj2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Nc", fp.Nc);
        parameters.Add("Na", fp.Na);
        p.PInC("2.93", parameters);
        fp.Pnas[j] = (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
        p.POutC("2.93", "Pnas[j]", fp.Pnas[j]);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (2.104)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Hj3(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("2.104", parameters);
        fp.H[j] = fp.L1 - fp.dl * j;
        p.POutC("2.104", "H[j]", fp.H[j]);
    }
        
    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера (Па) (2.105)
    /// </summary>
    public void dP2()
    {
        parameters.Clear();
        parameters.Add("P2ek", fp.P2ek);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Nint", fp.Nint);
        p.PInC("2.105", parameters);
        fp.dP = (fp.P2ek - fp.P1nkt) / fp.Nint;
        p.POutC("2.105", "dP", fp.dP);
    }
                
    /// <summary>
    /// Рабочее давление закачки газа (затрубное давление) (Па) (2.122)
    /// </summary>
    public void Ppz3()
    {
        parameters.Clear();
        parameters.Add("Pkl[0]", fp.Pkl[0]);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Lkl[0]", fp.Lkl[0]);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("2.122", parameters);
        fp.Ppz = fp.Pkl[0] / Math.Exp(0.03415 * fp.Lkl[0] * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("2.122", "Ppz", fp.Ppz);
    }

    /// <summary>
    /// Объем газа, поступивший в затрубное пространство (м3) (2.123)
    /// </summary>
    public void Vgzatr2()
    {
        parameters.Clear();
        parameters.Add("Pzak", fp.Pzak);
        parameters.Add("Ppz", fp.Ppz);
        parameters.Add("dshtzatr", fp.dshtzatr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.123", parameters);
        fp.Vgzatr = fp.Pzak * Math.Pow(1.0E3 * fp.dshtzatr, 2) / 
                    (0.00729 * Math.Pow(1.0, 2) * 86400.0 * fp.densityOfGas * fp.Ppz);
        p.POutC("2.123", "Vgzatr", fp.Vgzatr);
    }

    /// <summary>
    /// Удельный объем закачки газа (м3/м3) (2.124)
    /// </summary>
    /// <param name="Q">Дебит скважины (м3/с)</param>
    public void Vgz(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Vgzatr", fp.Vgzatr);
        p.PInC("2.124", parameters);
        fp.Vgz = fp.Vgzatr / Q;
        p.POutC("2.124", "Vgz", fp.Vgz);
    }

    /// <summary>
    /// Объемный расход газа из скважины на устье (м3/с) (2.126)
    /// </summary>
    public void Qgu3()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("Vgvust", fp.Vgvust);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pbufm", fp.Pbufm);
        parameters.Add("Tnu", fp.Tnu);
        p.PInC("2.126", parameters);
        fp.Qgu = fp.Q * fp.Vgvust * fp.Pnu * fp.Tu * fp.Z / (fp.Pbufm * fp.Tnu);
        p.POutC("2.126", "Qgu", fp.Qgu);
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (2.129)
    /// </summary>
    public void DPpzo1()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pzost", fp.Pzost);
        p.PInC("2.129", parameters);
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
        p.POutC("2.129", "DPpzo1", fp.DPpzo1);
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (2.130)
    /// </summary>
    public void tzab1()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("rc", fp.rc);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("2.130", parameters);
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.oilViscosity)) * fp.rc / 
                   (2.25 * fp.piezoconductivityOfTheFormation); 
        p.POutC("2.130", "tzab1", fp.tzab1);
    }
     
    /// <summary>
    /// Давление на забое скважины (Па) (2.131, 2.132)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("tzab1", fp.tzab1);
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("2.131, 2.132", parameters);
        if (time < fp.tzab1)  fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        else  fp.Pz = fp.Pzost + fp.Qrezh * fp.oilViscosity / (4.0 * fp.pi * fp.k * fp.h) *
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
        p.POutC("2.131, 2.132", "Pz", fp.Pz);
    }   
 
    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.133)
    /// </summary>
    public void htr2()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("2.133", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3) / (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("2.133", "htr", fp.htr);
    }   
    
    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.134)
    /// </summary>
    public void htr3()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("htr", fp.htr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("2.134", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3 * Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z))) / 
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("2.134", "htr", fp.htr);
    }
    /// <summary>
    /// Буферное давление, рассчитанное по третьей формуле (Па) (2.135)
    /// </summary>
    public void Pbuf3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("2.135", parameters);
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("2.135", "Pbuf3", fp.Pbuf3);
    }
    
    
    
    
    

}
using System;
using System.Collections.Generic;

public class Prod_1_3_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;
    public bool PNPT = false; 

    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_3_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    // Алгоритм расчета давления в газлифтной скважине от забоя при подъеме жидкости глушения в период освоения
    public void Diagram_2_1()
    {
        p.PTxt("p13.Diagram_2_1");
        fp.p13.Vzhek(); // 2.78
        fp.p13.Rozh(); // 2.79
        fp.p13.Reek(); // 2.80
        fp.p13.LambdaEk(); // 2.81
        fp.p13.Ppr2(fp.Pz); // 2.83
        fp.p13.Vzhnkt(); // 2.84
        fp.p13.Rozh(); // 2.85
        fp.p13.Renkt1(); // 2.86
        fp.p13.LambdaNkt(); // 2.87
        fp.p13.Pkl0(fp.Ppr, fp.Vzhnkt); // 2.89
        fp.L1 = fp.Lkl[0];
        fp.L2 = 0.0;
        fp.P1nkt = fp.Pkl[0];
        fp.p13.dl2(); // 2.90
        fp.p13.Tgrp(fp.dnkt); // 2.91
        for (var j = 0; j < fp.N; j++)
        {
            fp.p13.Tj(j); // 2.92
            fp.p13.Pnasj2(j); // 2.93
            fp.p13.RfP2(j); // 2.94
            fp.p13.mfT2(j); // 2.95
            fp.p13.DfT2(j); // 2.96
            fp.p13.Vgvj(j); // 2.97
            fp.p13.Vcm(j); // 2.98
            fp.p13.Mcm(); // 2.99
            fp.p13.Rocm(); // 2.100
            fp.p13.Re(fp.dnkt); // 2.101
            fp.p13.f(); // 2.102
            fp.p13.dPdH(fp.dnkt, fp.Rocm, j); // 2.103
            fp.p13.Hj3(j); // 2.104
            if (Math.Abs(fp.H[j]) < 1.0E-5)
            {
                fp.P[j] = fp.Pbufpk;
                fp.Vgv[j] = fp.Vgvust;
            }
        }
    }
    
    
    // Алгоритм расчета давления в газлифтной скважине от забоя при подъеме пластовых флюидов в период освоения
    public void Diagram_2_2()
    {
        p.PTxt("p13.Diagram_2_2");
        if (fp.Pnas0 < fp.Pz)
        {
            fp.p13.Vzhek(); // 2.78
            fp.p13.Rozh(); // 2.79
            fp.p13.Reek(); // 2.80
            fp.p13.LambdaEk(); // 2.81
            fp.p13.Hg11(fp.Pz); // 2.82
            if (fp.Lcp < fp.Hg1)
            {
                fp.p13.Ppr2(fp.Pz); // 2.83
                fp.p13.Vzhnkt(); // 2.84
                fp.p13.Rozh(); // 2.85
                fp.p13.Renkt1(); // 2.86
                fp.p13.LambdaNkt(); // 2.87
                fp.p13.Hg12(fp.Ppr, fp.Pz); // 2.88
                fp.p13.Pkl0(fp.Ppr, fp.Vzhnkt); // 2.89
            }
            else
            {
                fp.L1 = fp.Hg1;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pnas0;
            }
        }
        else
        {
            fp.L1 = fp.Lk;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pz;
        }

        if (fp.Lcp < fp.L1)
        {
            fp.p13.dl2(); // 2.90
            fp.p13.Tgrp(fp.ECInnerDiameter); // 2.91
            for (var j = 0; j < fp.N; j++)
            {
                fp.p13.Hg11(fp.Pz); // 2.82
                fp.p13.Ppr2(fp.Pz); // 2.83
                fp.p13.Vzhnkt(); // 2.84
                fp.p13.Rozh(); // 2.85
                fp.p13.Renkt1(); // 2.86
                fp.p13.LambdaNkt(); // 2.87
                fp.p13.Hg12(fp.Ppr, fp.Pz); // 2.88
                fp.p13.Pkl0(fp.Ppr, fp.Vzhnkt); // 2.89
                fp.p13.dl2(); // 2.90
                fp.p13.Tgrp(fp.ECInnerDiameter); // 2.91
                fp.p13.Tj(j); // 2.92
                fp.p13.Pnasj2(j); // 2.93
                fp.p13.RfP2(j); // 2.94
                fp.p13.mfT2(j); // 2.95
                fp.p13.DfT2(j); // 2.96
                fp.p13.Vgvj(j); // 2.97
                fp.p13.Vcm(j); // 2.98
                fp.p13.Mcm(); // 2.99
                fp.p13.Rocm(); // 2.100
                fp.p13.Re(fp.ECInnerDiameter); // 2.101
                fp.p13.f(); // 2.102
                fp.p13.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 2.103
                fp.p13.Hj3(j); // 2.104
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;
            }
        }

        if (fp.Pnas0 < fp.Ppr)
        {
            fp.p13.Vzhnkt(); // 2.84
            fp.p13.Rozh(); // 2.85
            fp.p13.Renkt1(); // 2.86
            fp.p13.LambdaNkt(); // 2.87
            fp.p13.Hg12(fp.Ppr, fp.Pz); // 2.88
            if (fp.Hg2 < fp.Lkl[0])
            {
                fp.p13.Pkl0(fp.Ppr, fp.Vzhnkt); // 2.89
                fp.L1 = fp.Lkl[0];
            }
            else
            {
                fp.L1 = fp.Hg2;
                fp.L2 = fp.Lkl[0];
                fp.P1nkt = fp.Pnas0;
            }
        }
        else
        {
            fp.L1 = fp.Lcp;
            fp.L2 = fp.Lkl[0];
            fp.P1nkt = fp.Ppr;
        }

        if (fp.Lkl[0] < fp.L1)
        {
            fp.p13.dl2(); // 2.90
            fp.p13.Tgrp(fp.dnkt); // 2.91
            for (var j = 0; j < fp.N; j++)
            {
                fp.p13.Tj(j); // 2.92
                fp.p13.Pnasj2(j); // 2.93
                fp.p13.RfP2(j); // 2.94
                fp.p13.mfT2(j); // 2.95
                fp.p13.DfT2(j); // 2.96
                fp.p13.Vgvj(j); // 2.97
                fp.p13.Vcm(j); // 2.98
                fp.p13.Mcm(); // 2.99
                fp.p13.Rocm(); // 2.100
                fp.p13.Re(fp.dnkt); // 2.101
                fp.p13.f(); // 2.102
                fp.p13.dPdH(fp.dnkt, fp.Rocm, j); // 2.103
                fp.p13.Hj3(j); // 2.104
                if (Math.Abs(fp.H[j] - fp.Lkl[0]) < 1.0E-5) fp.P[j] = fp.Pkl[0];
            }
        }
        fp.p13.Mcm(); // 2.54
        fp.p13.Rocm(); // 2.55
        fp.p13.Re(fp.dnkt); // 2.56
        fp.L1 = fp.Lkl[0];
        fp.P1nkt = fp.Pkl[0];
        fp.p13.dP2(); // 2.105
        fp.p13.Tgrp(fp.dnkt); // 2.106
        for (var j = 0; j < fp.N; j++)
        {
            fp.p13.Tj(j); // 2.107
            fp.p13.Pnasj1(j); // 2.108
            fp.p13.RfP2(j); // 2.109
            fp.p13.mfT2(j); // 2.110
            fp.p13.DfT2(j); // 2.111
            fp.p13.Vgvj(j); // 2.112
            fp.p13.Vcm(j); // 2.113
            fp.p13.Mcm(); // 2.114
            fp.p13.Rocm(); // 2.115
            fp.p13.Re(fp.dnkt); // 2.116
            fp.p13.f(); // 2.117
            fp.p13.dPdH(fp.dnkt, fp.Rocm, j); // 2.118
            fp.p13.dHdP(j); // 2.119
            fp.p13.Hj1(fp.N, j, fp.dHdP); // 2.120
            fp.p13.Pj(j); // 2.121
            fp.Vgv[j] = fp.Vgz;
            // if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hgv; // Восстановить
        }
        fp.L1 = fp.Lkl[0];
        fp.L2 = 0.0;
        fp.P1nkt = fp.Pkl[0];

        if (fp.L1 < 0)
        {
            fp.p13.dl2(); // 2.90
            fp.p13.Tgrp(fp.dnkt); // 2.91
            for (var j = 0; j < fp.N; j++)
            {
                if (fp.L1 < fp.Hg2)
                {
                    fp.p13.Tj(j); // 2.92
                    fp.Vgv0 = fp.Vgz;
                }
                else
                {
                    fp.p13.Tj(j); // 2.92
                    fp.p13.Pnasj2(j); // 2.93
                    fp.p13.RfP2(j); // 2.94
                    fp.p13.mfT2(j); // 2.95
                    fp.p13.DfT2(j); // 2.96
                    fp.p13.Vgvj(j); // 2.97
                    fp.Vgv[j] = fp.Vgz + fp.Vgv0;
                }
                fp.p13.Vcm(j); // 2.98
                fp.p13.Mcm(); // 2.99
                fp.p13.Rocm(); // 2.100
                fp.p13.Re(fp.dnkt); // 2.101
                fp.p13.f(); // 2.102
                fp.p13.dPdH(fp.dnkt, fp.Rocm, j); // 2.103
                fp.p13.Hj3(j); // 2.104
                if (Math.Abs(fp.H[j]) < 1.0E-5)
                {
                    fp.P[j] = fp.Pbufpk;
                    fp.Vgv[j] = fp.Vgvust;
                }
            }        
        }
    }

    // Алгоритм расчета давления в газлифтной скважине от устья при подъеме жидкости глушения в период освоения
    public void Diagram_2_3()
    {
        p.PTxt("p13.Diagram_2_3");
        fp.L1 = 0.0;
        fp.L2 = fp.Lkl[0];
        fp.P1nkt = fp.Pbufm;
        fp.p13.dP1(); // 2.28
        fp.p13.Tgrp(fp.dnkt); // 2.29
        for (var j = 0; j < fp.N; j++)
        {
            fp.p13.Tj(j); // 2.30
            fp.p13.Vcm(j); // 2.36
            fp.p13.Mcm(); // 2.37
            fp.p13.Rocm(); // 2.38
            fp.p13.Re(fp.dnkt); // 2.39
            fp.p13.f(); // 2.40
            fp.p13.dPdH(fp.dnkt, fp. Rocm, j); // 2.41
            fp.p13.dHdP(j); // 2.42
            fp.p13.Hj1(fp.N, j, fp.dHdP); // 2.43
            fp.p13.Pj(j); // 2.44
            fp.Vgv[j] = fp.Vgz;
            if (Math.Abs(fp.H[j] - fp.Lkl[j]) < 1.0E-5) fp.P[j] = fp.Pkl[j];
        }
        fp.p13.Rozh(); // 2.18
        fp.p13.Vzhnkt(); // 2.19
        fp.p13.Renkt1(); // 2.20
        fp.p13.LambdaNkt(); // 2.21
        // fp.p13.(); // 2.25 // Восстановить
        fp.p13.Ppr1(); // 2.26
        fp.L2 = fp.Lcp;
        fp.P2ek = fp.Ppr;
        fp.p13.Rozh(); // 2.18
        fp.p13.Vzhek(); // 2.22
        fp.p13.Reek(); // 2.23
        fp.p13.LambdaEk(); // 2.24
        fp.p13.Pzpk(); // 2.27
    } 

    // Алгоритм расчета давления в газлифтной скважине от устья при подъеме пластовых флюидов в период освоения
    public void Diagram_2_4()
    {
        p.PTxt("p13.Diagram_2_4");
        if (fp.Pnas0 <= fp.Pbufm)
        {
            fp.L1 = fp.Lkl[0];
            fp.P1nkt = fp.Pbufm;
            for (var j = 0; j < fp.N; j++)
            {
                fp.p13.Tj(j); // 2.30
                fp.p13.Vcm(j); // 2.36
                fp.p13.Mcm(); // 2.37
                fp.p13.Rocm(); // 2.38
                fp.p13.Re(fp.dnkt); // 2.39
                fp.p13.f(); // 2.40
                fp.p13.dPdH(fp.dnkt, fp. Rocm, j); // 2.41
                fp.p13.dHdP(j); // 2.42
                fp.p13.Hj1(fp.N, j, fp.dHdP); // 2.43
                fp.p13.Pj(j); // 2.44
                fp.p13.Vgvj(j); // 2.52
                fp.p13.Vcm(j); // 2.53
                fp.p13.Mcm(); // 2.54
                fp.p13.Rocm(); // 2.55
                fp.p13.Re(fp.dnkt); // 2.56
                fp.p13.f(); // 2.57
                fp.p13.dPdH(fp.dnkt, fp.Rocm, j); // 2.58
                fp.p13.Hj2(j); // 2.59
                fp.p13.ng(fp.Pz); // 2.60
                fp.p13.nst(); // 2.61
                fp.p13.Qskv1(fp.Pz, fp.rc); // 2.62
                fp.p13.Qskv2(fp.Pz, fp.rc); // 2.63
                fp.p13.Vzhgzab(fp.tz1, fp.tz2); // 2.64
                fp.p13.Ppz2(j); // 2.65
                fp.p13.Qzakg(); // 2.66
                fp.Vgv[j] = fp.Vgz;
                if (Math.Abs(fp.P[j] - fp.Pkl[j]) < 1.0E-5) fp.H[j] = fp.Lkl[j];
                fp.L1 = fp.Lkl[j];
                fp.P1nkt = fp.Pkl[j];
            }
            fp.p13.Rozh(); // 2.18
            fp.p13.Vzhnkt(); // 2.19
            fp.p13.Renkt1(); // 2.20
            fp.p13.LambdaNkt(); // 2.21
            // fp.p13.(); // 2.25 // Восстановить
            fp.p13.Ppr1(); // 2.26
            fp.L2 = fp.Lcp;
            fp.P2ek = fp.Ppr;
            fp.p13.Rozh(); // 2.18
            fp.p13.Vzhek(); // 2.22
            fp.p13.Reek(); // 2.23
            fp.p13.LambdaEk(); // 2.24
            fp.p13.Pzpk(); // 2.27
        }
        else
        {
            fp.L1 = 0.0;
            fp.P1nkt = fp.Pbufm;
            fp.p13.dl1(); // 2.45
            fp.p13.Tgrp(fp.dnkt); // 2.46
            for (var j = 0; j < fp.N; j++)
            {
                fp.p13.Tj(j); // 2.47
                fp.p13.Pnasj1(j); // 2.48
                fp.p13.RfP2(j); // 2.49
                fp.p13.mfT2(j); // 2.50
                fp.p13.DfT2(j); // 2.51
                fp.p13.Vgvj(j); // 2.52
                fp.p13.Vcm(j); // 2.53
                fp.p13.Mcm(); // 2.54
                fp.p13.Rocm(); // 2.55
                fp.p13.Re(fp.dnkt); // 2.56
                fp.p13.f(); // 2.57
                fp.p13.dPdH(fp.dnkt, fp.Rocm, j); // 2.58
                fp.p13.Hj2(j); // 2.59
                fp.Vgv[j] = fp.Vgz + fp.Vgv0;
                // if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg3; // Восстановить
                fp.L1 = 0.0;
                fp.L2 = fp.Lkl[j];
                fp.P1nkt = fp.Pbufm;
            }
            fp.p13.dP1(); // 2.28
            fp.p13.Tgrp(fp.dnkt); // 2.29
            for (var j = 0; j < fp.N; j++)
            {
                // if (fp.Hg3 < fp.L1) { // Восстановить
                    fp.p13.Tj(j); // 2.30
                    fp.p13.Pnasj1(j); // 2.31
                    fp.p13.RfP2(j); // 2.32
                    fp.p13.mfT2(j); // 2.33
                    fp.p13.DfT2(j); // 2.34
                    fp.p13.Vgvj(j); // 2.35
                    fp.Vgv[j] = fp.Vgz + fp.Vgv0;
                // } // Восстановить 
                // else  // Восстановить
                // { // Восстановить
                    fp.p13.Tj(j); // 2.30
                    fp.Vgv0 = fp.Vgz;
                // }     // Восстановить    
                fp.p13.Vcm(j); // 2.36
                fp.p13.Mcm(); // 2.37
                fp.p13.Rocm(); // 2.38
                fp.p13.Re(fp.dnkt); // 2.39
                fp.p13.f(); // 2.40
                fp.p13.dPdH(fp.dnkt, fp. Rocm, j); // 2.41
                fp.p13.dHdP(j); // 2.42
                fp.p13.Hj1(fp.N, j, fp.dHdP); // 2.43
                fp.p13.Pj(j); // 2.44
                if (Math.Abs(fp.H[j] - fp.Lkl[j]) < 1.0E-5) fp.P[j] = fp.Pkl[j];

                if (fp.Pnas0 <= fp.Pkl[j])
                {
                    fp.L1 = fp.Lkl[j];
                    fp.P1nkt = fp.Pkl[j];
                    fp.p13.Rozh(); // 2.18
                    fp.p13.Vzhnkt(); // 2.19
                    fp.p13.Renkt1(); // 2.20
                    fp.p13.LambdaNkt(); // 2.21
                    // fp.p13.(); // 2.25 // Восстановить
                    fp.p13.Ppr1(); // 2.26
                    fp.L2 = fp.Lcp;
                    fp.P2ek = fp.Ppr;
                    fp.p13.Rozh(); // 2.18
                    fp.p13.Vzhek(); // 2.22
                    fp.p13.Reek(); // 2.23
                    fp.p13.LambdaEk(); // 2.24
                    fp.p13.Pzpk(); // 2.27                    
                }
                else
                {
                    fp.L1 = fp.Lkl[j];
                    fp.P1nkt = fp.Pkl[j];
                    fp.p13.dl1(); // 2.45
                    fp.p13.Tgrp(fp.dnkt); // 2.46
                    for (var j2 = 0; j2 < fp.N; j2++)
                    {
                        fp.p13.Tj(j2); // 2.47
                        fp.p13.Pnasj1(j2); // 2.48
                        fp.p13.RfP2(j2); // 2.49
                        fp.p13.mfT2(j2); // 2.50
                        fp.p13.DfT2(j2); // 2.51
                        fp.p13.Vgvj(j2); // 2.52
                        fp.p13.Vcm(j2); // 2.53
                        fp.p13.Mcm(); // 2.54
                        fp.p13.Rocm(); // 2.55
                        fp.p13.Re(fp.dnkt); // 2.56
                        fp.p13.f(); // 2.57
                        fp.p13.dPdH(fp.dnkt, fp.Rocm, j2); // 2.58
                        fp.p13.Hj2(j2); // 2.59
                        if (Math.Abs(fp.P[j2] - fp.Pnas0) < 1.0E-5) fp.H[j2] = fp.Hg2;
                    }

                    // if (fp.Lg2 <= fp.Lcp)  // Восстановить
                    // { // Восстановить
                    //     fp.L1 = fp.Hgv; // Восстановить
                        fp.P1nkt = fp.Pnas0;
                        fp.p13.Rozh(); // 2.18
                        fp.p13.Vzhnkt(); // 2.19
                        fp.p13.Renkt1(); // 2.20
                        fp.p13.LambdaNkt(); // 2.21
                        // fp.p13.(); // 2.25 // Восстановить
                        fp.p13.Ppr1(); // 2.26
                        fp.L2 = fp.Lcp;
                        fp.P2ek = fp.Ppr;
                    // } // Восстановить
                    // else // Восстановить
                    // { // Восстановить
                        fp.L1 = fp.Lkl[j];
                        // fp.L2 = fp.Lot; // Восстановить
                        fp.P1nkt = fp.Pkl[j];
                        fp.p13.dP1(); // 2.28
                        fp.p13.Tgrp(fp.dnkt); // 2.29
                        for (var j2 = 0; j2 < fp.N; j2++)
                        {
                            fp.p13.Tj(j2); // 2.30
                            fp.p13.Vcm(j2); // 2.36
                            fp.p13.Mcm(); // 2.37
                            fp.p13.Rocm(); // 2.38
                            fp.p13.Re(fp.dnkt); // 2.39
                            fp.p13.f(); // 2.40
                            fp.p13.dPdH(fp.dnkt, fp. Rocm, j2); // 2.41
                            fp.p13.dHdP(j2); // 2.42
                            fp.p13.Hj1(fp.N, j2, fp.dHdP); // 2.43
                            fp.p13.Pj(j2); // 2.44
                            if (Math.Abs(fp.P[j2] - fp.Pkl[j2+1]) < 1.0E-5) fp.H[j2] = fp.Lkl[j2+1];
                            if (fp.Lcp < fp.H[j2]) fp.P[j2] = fp.Ppr;
                        }
                        fp.L1 = fp.Lcp;
                        fp.P1nkt = fp.Ppr; 
                        fp.p13.dl1(); // 2.45
                        fp.p13.Tgrp(fp.ECInnerDiameter); // 2.46
                        for (var j2 = 0; j2 < fp.N; j2++)
                        {
                            fp.p13.Tj(j2); // 2.47
                            fp.p13.Pnasj1(j2); // 2.48
                            fp.p13.RfP2(j2); // 2.49
                            fp.p13.mfT2(j2); // 2.50
                            fp.p13.DfT2(j2); // 2.51
                            fp.p13.Vgvj(j2); // 2.52
                            fp.p13.Vcm(j2); // 2.53
                            fp.p13.Mcm(); // 2.54
                            fp.p13.Rocm(); // 2.55
                            fp.p13.Re(fp.ECInnerDiameter); // 2.56
                            fp.p13.f(); // 2.57
                            fp.p13.dPdH(fp.ECInnerDiameter, fp.Rocm, j2); // 2.58
                            fp.p13.Hj2(j2); // 2.59
                            if (Math.Abs(fp.P[j2] - fp.Pnas0) < 1.0E-5) fp.H[j2] = fp.Hg1;
                        }

                        if (fp.Lk <= fp.Hg1)
                        {
                            fp.L2 = fp.Hg1;
                            fp.P2ek = fp.Pnas0;
                            fp.p13.Rozh(); // 2.18
                            fp.p13.Vzhek(); // 2.22
                            fp.p13.Reek(); // 2.23
                            fp.p13.LambdaEk(); // 2.24
                            fp.p13.Pzpk(); // 2.27                        
                        }
                        else
                        {
                            fp.L1 = fp.Lcp;
                            fp.L1 = fp.Lk;
                            fp.P1nkt = fp.Ppr;
                            fp.p13.dP1(); // 2.28
                            fp.p13.Tgrp(fp.ECInnerDiameter); // 2.29
                            for (var j2 = 0; j2 < fp.N; j2++)
                            {
                                fp.p13.Tj(j2); // 2.30
                                fp.p13.Pnasj1(j); // 2.31
                                fp.p13.RfP2(j); // 2.32
                                fp.p13.mfT2(j); // 2.33
                                fp.p13.DfT2(j); // 2.34
                                fp.p13.Vgvj(j); // 2.35
                                fp.p13.Vcm(j2); // 2.36
                                fp.p13.Mcm(); // 2.37
                                fp.p13.Rocm(); // 2.38
                                fp.p13.Re(fp.ECInnerDiameter); // 2.39
                                fp.p13.f(); // 2.40
                                fp.p13.dPdH(fp.ECInnerDiameter, fp. Rocm, j2); // 2.41
                                fp.p13.dHdP(j2); // 2.42
                                fp.p13.Hj1(fp.N, j2, fp.dHdP); // 2.43
                                fp.p13.Pj(j2); // 2.44
                                if (Math.Abs(fp.H[j2] - fp.Lk) < 1.0E-5) fp.P[j2] = fp.Pz;
                            }
                        }
                    // } // Восстановить
                }
            }
        }
    }

    // Алгоритм освоения газлифтной скважины
    public void Diagram_2_5()
    {
        p.PTxt("p13.Diagram_2_5");
        
        
        
    }

    // Алгоритм вывода на режим газлифтной скважины при освоении
    public void Diagram_2_6(double time)
    {
        p.PTxt("p13.Diagram_2_6");
        fp.p13.Pz1(time); // 2.69.1
        fp.p13.MP(fp.Pz); // 2.75, 2.76
        fp.p13.nv(fp.Pz); // 2.77
        if (fp.Vzhgzab <= fp.Vzhgpl)
        {
            fp.p13.Qskv2(fp.Pz, fp.rc); // 2.73
            Diagram_2_1();
            p.PTxt("p13.Diagram_2_6 - continue after p13.Diagram_2_1");
        }
        else
        {
            if (fp.Pz <= fp.Pnas0)
            {
                fp.p13.ng(fp.Pz); // 2.70
                fp.p13.nst(); // 2.71
            }  
            fp.p13.Qskv2(fp.Pz, fp.rc); // 2.73
            Diagram_2_2();
            p.PTxt("p13.Diagram_2_6 - continue after p13.Diagram_2_2");
        }
        fp.p13.Vzhgzab(fp.tz1, fp.tz2); // 2.74
        fp.p13.Ppz3(); // 2.122
        fp.p13.Vgzatr2(); // 2.123
        fp.p13.Vgz(fp.Q); // 2.124
        fp.p13.Pbuf1(); // 2.125
        fp.p13.Qgu3(); // 2.126
        fp.p13.Pbuf2(fp.Q); // 2.127
        fp.p13.Pbufm(fp.Pbuf1, fp.Pbuf2); // 2.128
        if (1.0E-5 < fp.dsht)
        {
            if (fp.Pbufpk <= fp.Pbuf2)  Diagram_2_7(time);
        } 
        else Diagram_2_8(time);
    }

    // Алгоритм установившегося режима работы газлифтной скважины при освоении
    public void Diagram_2_7(double time)
    {
        p.PTxt("p13.Diagram_2_7");
        var Pbufmdl = 0.5 * (fp.Pbuf2 + fp.Pbufpk);
        p.POutC("p13.Diagram_2_7", "Pbufmdl", Pbufmdl);
        Diagram_2_4();
        p.PTxt("p13.Diagram_2_7 - continue after p13.Diagram_2_4");
        var Pzmdl = 0.5 * (fp.Pz_1 + fp.Pz);
        p.POutC("p13.Diagram_2_7", "Pzmdl", Pzmdl);
        if (Pzmdl <= fp.Pnas0)
        {
            fp.p13.ng(Pzmdl); // 2.70
            fp.p13.nst(); // 2.71
            fp.p13.Qskv1(Pzmdl, fp.rc); // 2.72
        }
        fp.p13.MP(Pzmdl); // 2.75, 2.76
        fp.p13.nv(Pzmdl); // 2.77
        fp.p13.Pbuf1(); // 2.125
        fp.p13.Qgu3(); // 2.126
        fp.p13.Pbuf2(fp.Q); // 2.127
        fp.p13.Pbufm(fp.Pbuf1, fp.Pbuf2); // 2.128
        Diagram_2_2();        
        p.PTxt("p13.Diagram_2_7 - continue after p13.Diagram_2_2");
        if (1.0E-5 < fp.dsht)
        {
            if (1.0E-5 < Math.Abs(fp.dsht - fp.dsht_1))  Diagram_2_6(time);
        } 
        else Diagram_2_8(time);
    }

    // Алгоритм остановки работы газлифтной скважины при освоении
    public void Diagram_2_8(double time)
    {
        p.PTxt("p13.Diagram_2_8");
        if (!PNPT)
        {
            fp.p13.DPpzo1(); // 2.129
            fp.p13.tzab1(); // 2.130
            PNPT = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            fp.p13.Pz(time); // 2.131, 2.132
            fp.p13.Pbuf3(); // 2.135
            fp.p13.htr2(); // 2.133
            fp.p13.htr3(); // 2.134
        } 
    }
}
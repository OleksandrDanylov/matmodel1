using System;
using System.Collections.Generic;

public class Prod_1_4_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;
    public bool PNPT1 = false; 
    public bool PNPT2 = false; 
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_4_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    // Алгоритм расчета забойного давления в скважине через динамический уровень при эксплуатации УЭЦН
    public void Diagram_1_1()
    {
        p.PTxt("p14.Diagram_1_1");
        fp.p14.Ppr1(fp.Pzatr, fp.hdin, fp.Tu); // 1.1
        fp.p14.Rozh(); // 1.2
        if (fp.Pnas0 <= fp.Pbufm)
        {
            fp.L2 = fp.Lcp;
            fp.P2ek = fp.Ppr;
            fp.p14.Rozh(); // 1.3
            fp.p14.Vzhek(); // 1.4
            fp.p14.Reek(); // 1.5
            fp.p14.LambdaEk(); // 1.6
            fp.p14.Pzpk(); // 1.7
        }
        else
        {
            fp.L1 = fp.Lcp;
            fp.P1nkt = fp.Ppr;
            fp.p14.dP(); // 1.8
            fp.p14.Tgrp(fp.ECInnerDiameter); // 1.9
            for (var j = 0; j < fp.N; j++)
            {
                fp.p14.Tj(j, 1); // 1.10
                fp.p14.Pnasj(j); // 1.11
                fp.p14.RfP(j); // 1.12
                fp.p14.mfT(j); // 1.13
                fp.p14.DfT(j); // 1.14
                fp.p14.Vgvj(j); // 1.15
                fp.p14.Vcm(j); // 1.16
                fp.p14.Mcm(); // 1.17
                fp.p14.Rocm1(); // 1.18
                fp.p14.Re1(fp.ECInnerDiameter); // 1.19
                fp.p14.f(); // 1.20
                fp.p14.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.21
                fp.p14.dHdP(j); // 1.22
                fp.p14.Hj1(fp.N, j, fp.dHdP); // 1.23
                fp.p14.Pj(j); // 1.24
                if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg1;
            }
            if (fp.Hg1 <= fp.Lk)
            {
                fp.L2 = fp.Hg1;
                fp.P2ek = fp.Pnas0;
                fp.p14.Rozh(); // 1.3
                fp.p14.Vzhek(); // 1.4
                fp.p14.Reek(); // 1.5
                fp.p14.LambdaEk(); // 1.6
                fp.p14.Pzpk(); // 1.7
            }
            else
            {
                fp.L1 = fp.Lcp;
                fp.L2 = fp.Lk;
                fp.P1nkt = fp.Ppr;
                fp.p14.dl1(); // 1.25
                fp.p14.Tgrp(fp.ECInnerDiameter); // 1.26
                for (var j = 0; j < fp.N; j++)
                {
                    fp.p14.Tj(j, 1); // 1.27
                    fp.p14.mfT(j); // 1.28
                    fp.p14.DfT(j); // 1.29
                    fp.p14.mfT(j); // 1.30
                    fp.p14.DfT(j); // 1.31
                    fp.p14.Vgvj(j); // 1.32
                    fp.p14.Vcm(j); // 1.33
                    fp.p14.Mcm(); // 1.34
                    fp.p14.Rocm1(); // 1.35
                    fp.p14.Re1(fp.ECInnerDiameter); // 1.36
                    fp.p14.f(); // 1.37
                    fp.p14.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.38
                    fp.p14.Hj2(j); // 1.39
                    if (Math.Abs(fp.H[j] - fp.Lk) < 1.0E-5) fp.P[j] = fp.Pz;
                }
            }
        }
    }

    // Алгоритм расчета давления на приеме насоса от забоя при эксплуатации УЭЦН
    public void Diagram_1_2()
    {
        p.PTxt("p14.Diagram_1_2");
        if (fp.Pz <= fp.Pnas0)
        {
            fp.L1 = fp.Lk;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pz;
        }
        else
        {
            fp.p14.Vzhek(); // 1.46
            fp.p14.Rozh(); // 1.47
            fp.p14.Reek(); // 1.48
            fp.p14.LambdaEk(); // 1.49
            fp.p14.Hg1(fp.Pz); // 1.50
            if (fp.Hg1 < fp.Lcp)
            {
                fp.p14.Ppr2(fp.Pz); // 1.51
                fp.L1 = fp.Lcp;
            }
            else
            {
                fp.L1 = fp.Hg1;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pnas0;
            }
        }
        if (fp.Lcp < fp.L1)
        {
            fp.p14.dl2(); // 1.52
            fp.p14.Tgrp(fp.ECInnerDiameter); // 1.53
            for (var j = 0; j < fp.N; j++)
            {
                fp.p14.Tj(j, 1); // 1.54
                fp.p14.Pnasj(j); // 1.55
                fp.p14.RfP(j); // 1.56
                fp.p14.mfT(j); // 1.57
                fp.p14.DfT(j); // 1.58
                fp.p14.Vgvj(j); // 1.59
                fp.p14.Vcm(j); // 1.60
                fp.p14.Mcm(); // 1.61
                fp.p14.Rocm1(); // 1.62
                fp.p14.Re1(fp.ECInnerDiameter); // 1.63
                fp.p14.f(); // 1.64
                fp.p14.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.65
                fp.p14.Hj3(j); // 1.66
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;                
            }
        }
    }

    // Алгоритм расчета давления на выходе из насоса от устья при эксплуатации УЭЦН
    public void Diagram_1_3()
    {
        p.PTxt("p14.Diagram_1_3");
        if (fp.Pnas0 <= fp.Pbufm)
        {
            fp.L1 = 0.0;
            fp.P1nkt = fp.Pbufm;
            fp.p14.Rozh(); // 1.71
            fp.p14.Vzhnkt(); // 1.72
            fp.p14.Renkt1(); // 1.73
            fp.p14.LambdaNkt(); // 1.74
            fp.p14.Pvyh1(); // 1.75
        }
        else
        {
            fp.L1 = 0.0;
            fp.P1nkt = fp.Pbufm;
            fp.p14.dP(); // 1.76
            fp.p14.Tgrp(fp.dnkt); // 1.77
            for (var j = 0; j < fp.N; j++)
            {
                fp.p14.Tj(j, 0); // 1.78
                fp.p14.Pnasj(j); // 1.79
                fp.p14.RfP(j); // 1.80
                fp.p14.mfT(j); // 1.81
                fp.p14.DfT(j); // 1.82
                fp.p14.Vgvj(j); // 1.83
                fp.p14.Vcm(j); // 1.84
                fp.p14.Mcm(); // 1.85
                fp.p14.Rocm1(); // 1.86
                fp.p14.Re1(fp.dnkt); // 1.87
                fp.p14.f(); // 1.88
                fp.p14.dPdH(fp.dnkt, fp.Rocm, j); // 1.89
                fp.p14.dHdP(j); // 1.90
                fp.p14.Hj1(fp.N, j, fp.dHdP); // 1.91
                fp.p14.Pj(j); // 1.92
                if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg2;
            }
            if (fp.Hg2 <= fp.Lcp)
            {
                fp.L1 = fp.Hg2;
                fp.P1nkt = fp.Pnas0;
                fp.p14.Rozh(); // 1.71
                fp.p14.Vzhnkt(); // 1.72
                fp.p14.Renkt1(); // 1.73
                fp.p14.LambdaNkt(); // 1.74
                fp.p14.Pvyh1(); // 1.75
            }
            else
            {
                fp.L1 = 0.0;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pbufm;
                fp.p14.dl1(); // 1.93
                fp.p14.Tgrp(fp.dnkt); // 1.94
                for (var j = 0; j < fp.N; j++)
                {
                    fp.p14.Tj(j, 0); // 1.95
                    fp.p14.mfT(j); // 1.96
                    fp.p14.DfT(j); // 1.97
                    fp.p14.mfT(j); // 1.98
                    fp.p14.DfT(j); // 1.99
                    fp.p14.Vgvj(j); // 1.100
                    fp.p14.Vcm(j); // 1.101
                    fp.p14.Mcm(); // 1.102
                    fp.p14.Rocm1(); // 1.103
                    fp.p14.Re1(fp.dnkt); // 1.104
                    fp.p14.f(); // 1.105
                    fp.p14.dPdH(fp.dnkt, fp.Rocm, j); // 1.106
                    fp.p14.Hj2(j); // 1.107
                    if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;                
                }
            }
        }
    }

    // Алгоритм расчета параметров работы насоса
    public void Diagram_1_4()
    {
        p.PTxt("p14.Diagram_1_4");
        fp.p14.A2E(); // 1.109
        fp.p14.MjuE(); // 1.108, 1.110
        fp.p14.ng2(); // 1.111
        fp.p14.Wzhpr(); // 1.112
        fp.p14.Wgdr(); // 1.113
        fp.p14.Ksvh(); // 1.114
        fp.p14.Ksgs(); // 1.115
        fp.p14.Ks(); // 1.116
        fp.p14.ngn(); // 1.117
        fp.p14.Mjugzh(); // 1.118
        fp.p14.vgzh1(); // 1.119
        fp.p14.Rocm2(); // 1.120
        fp.p14.ns(); // 1.121
        for (var i = 0; i < 8; i++)
        {
            fp.p14.Qnvi(i); // 1.122
            fp.p14.Hnvi(i); // 1.123
            var Khq1 = 0.0;
            var Khq2 = Khq1 + 0.03;
            while (0.02 <= Math.Abs(Khq2 - Khq1))
            {
                fp.p14.Re2(); // 1.124
                fp.p14.Khq(i); // 1.125, 1.126
                Khq1 = fp.Khq;
                fp.Qnv[i] *= Khq1;
                fp.p14.Re2(); // 1.124
                fp.p14.Khq(i); // 1.125, 1.126
                Khq2 = fp.Khq;
            }
            fp.p14.Kk(i); // 1.127, 1.128
        }
        fp.p14.Pn(); // 1.129
        fp.p14.Rozh(); // 1.130
        fp.p14.Hn(); // 1.131
        fp.p14.Cn(); // 1.132
        fp.p14.EtaD(); // 1.133
        fp.p14.Ccm(); // 1.134
        fp.p14.Tvyh(); // 1.135
        fp.p14.Nh(); // 1.136
        fp.p14.SNDpot(); // 1.137
        fp.p14.tdp(); // 1.138
        fp.p14.Kt(); // 1.139
        fp.p14.Kup(); // 1.140
        fp.p14.SN(); // 1.141
        fp.p14.Td(); // 1.142
        fp.p14.I(); // 1.143
    }

    // Алгоритм работы скважины при эксплуатации УЭЦН
    public void Diagram_1_5(double time)
    {
        p.PTxt("p14.Diagram_1_5");
        Diagram_1_1();
        p.PTxt("p14.Diagram_1_5 - continue after p14.Diagram_1_1");
        if (fp.Pz <= fp.Pnas0)
        {
            fp.p14.ng1(fp.Pz); // 1.40
            fp.p14.nst(); // 1.41
        }
        fp.p14.Qskv1(fp.Pz, fp.rc); // 1.42
        fp.p14.MP(fp.Pz); // 1.43, 1.44
        fp.p14.nv(fp.Pz); // 1.45
        Diagram_1_2();
        p.PTxt("p14.Diagram_1_5 - continue after p14.Diagram_1_2");
        fp.p14.Pbuf1(); // 1.67
        fp.p14.Qg(fp.Q); // 1.68
        fp.p14.Pbuf2(fp.Q); // 1.69
        fp.p14.Pbufm(fp.Pbuf1, fp.Pbuf2); // 1.70
        Diagram_1_3();
        p.PTxt("p14.Diagram_1_5 - continue after p14.Diagram_1_3");
        Diagram_1_4();
        p.PTxt("p14.Diagram_1_5 - continue after p14.Diagram_1_4");
        fp.p14.dhdin(fp.tz1, fp.tz2); // 1.144
        fp.p14.hdin1(); // 1.145
        fp.p14.Pzatr1(); // 1.146
        if (fp.dsht <= 1.0E-5) Diagram_1_6(time);
        if (Math.Abs(fp.Qskv - fp.Qn) < 1.0E-5)
        {
            Console.WriteLine("p14.Diagram_1_5: Осуществлен выход на установившийся режим.");
            Console.WriteLine("Расчет завершен.");
            p.PTxt("p14.Diagram_1_5: Осуществлен выход на установившийся режим.");
            p.PTxt("Расчет завершен.");
            Environment.Exit(0);
        }
    }

    // Алгоритм остановки работы скважины при эксплуатации УЭЦН
    public void Diagram_1_6(double time)
    {
        p.PTxt("p14.Diagram_1_6");
        if (!PNPT1)
        {
            fp.p14.DPpzo1(); // 1.147
            fp.p14.tzab1(); // 1.148
            PNPT1 = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            fp.p14.Pz(time); // 1.149, 1.150
            fp.p14.htr1(); // 1.151
            fp.p14.htr2(); // 1.152
            fp.p14.Pbuf3(); // 1.153
            fp.p14.hdin2(); // 1.154
            fp.p14.Pzatr2(); // 1.155
        }    
    }

    // Алгоритм расчета забойного давления через динамический уровень при освоении скважины с УЭЦН
    public void Diagram_2_1()
    {
        p.PTxt("p14.Diagram_2_1");
        fp.p14.Ppr1(fp.Pzatr, fp.hdin, fp.Tu); // 2.1
        fp.p14.Rozh(); // 2.2
        if (fp.Pnas0 <= fp.Pbufm)
        {
            fp.L2 = fp.Lcp;
            fp.P2ek = fp.Ppr;
            fp.p14.Rozh(); // 2.7
            fp.p14.Vzhek(); // 2.8
            fp.p14.Reek(); // 2.9
            fp.p14.LambdaEk(); // 2.10
            fp.p14.Pzpk(); // 2.11
        }
        else
        {
            fp.L1 = fp.Lcp;
            fp.P1nkt = fp.Ppr;
            fp.p14.dP(); // 2.12
            fp.p14.Tgrp(fp.ECInnerDiameter); // 2.13
            for (var j = 0; j < fp.N; j++)
            {
                fp.p14.Tj(j, 1); // 2.14
                fp.p14.Pnasj(j); // 2.15
                fp.p14.RfP(j); // 2.16
                fp.p14.mfT(j); // 2.17
                fp.p14.DfT(j); // 2.18
                fp.p14.Vgvj(j); // 2.19
                fp.p14.Vcm(j); // 2.20
                fp.p14.Mcm(); // 2.21
                fp.p14.Rocm1(); // 2.22
                fp.p14.Re1(fp.ECInnerDiameter); // 2.23
                fp.p14.f(); // 2.24
                fp.p14.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 2.25
                fp.p14.dHdP(j); // 2.26
                fp.p14.Hj1(fp.N, j, fp.dHdP); // 2.27
                fp.p14.Pj(j); // 2.28
                if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg1;
            }
            if (fp.Hg1 <= fp.Lk)
            {
                fp.L2 = fp.Hg1;
                fp.P2ek = fp.Pnas0;
                fp.p14.Rozh(); // 2.7
                fp.p14.Vzhek(); // 2.8
                fp.p14.Reek(); // 2.9
                fp.p14.LambdaEk(); // 2.10
                fp.p14.Pzpk(); // 2.11
            }
            else
            {
                fp.L1 = fp.Lcp;
                fp.L2 = fp.Lk;
                fp.P1nkt = fp.Ppr;
                fp.p14.dl1(); // 2.29
                fp.p14.Tgrp(fp.ECInnerDiameter); // 2.30
                for (var j = 0; j < fp.N; j++)
                {
                    fp.p14.Tj(j, 1); // 2.31
                    fp.p14.mfT(j); // 2.32
                    fp.p14.DfT(j); // 2.33
                    fp.p14.mfT(j); // 2.34
                    fp.p14.DfT(j); // 2.35
                    fp.p14.Vgvj(j); // 2.36
                    fp.p14.Vcm(j); // 2.37
                    fp.p14.Mcm(); // 2.38
                    fp.p14.Rocm1(); // 2.39
                    fp.p14.Re1(fp.ECInnerDiameter); // 2.40
                    fp.p14.f(); // 2.41
                    fp.p14.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 2.42
                    fp.p14.Hj2(j); // 2.43
                    if (Math.Abs(fp.H[j] - fp.Lk) < 1.0E-5) fp.P[j] = fp.Pz;
                }
            }
        }
    }

    // Алгоритм расчета давления на приеме насоса от забоя при освоении скважины с УЭЦН
    public void Diagram_2_2()
    {
        p.PTxt("p14.Diagram_2_2");
        if (fp.Pz <= fp.Pnas0)
        {
            fp.L1 = fp.Lk;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pz;
        }
        else
        {
            fp.p14.Vzhek(); // 2.56
            fp.p14.Rozh(); // 2.57
            fp.p14.Reek(); // 2.58
            fp.p14.LambdaEk(); // 2.59
            fp.p14.Hg1(fp.Pz); // 2.60
            if (fp.Hg1 < fp.Lcp)
            {
                fp.p14.Ppr2(fp.Pz); // 2.61
                fp.L1 = fp.Lcp;
            }
            else
            {
                fp.L1 = fp.Hg1;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pnas0;
            }
        }
        if (fp.Lcp < fp.L1)
        {
            fp.p14.dl2(); // 2.62
            fp.p14.Tgrp(fp.ECInnerDiameter); // 2.63
            for (var j = 0; j < fp.N; j++)
            {
                fp.p14.Tj(j, 1); // 2.64
                fp.p14.Pnasj(j); // 2.65
                fp.p14.RfP(j); // 2.66
                fp.p14.mfT(j); // 2.67
                fp.p14.DfT(j); // 2.68
                fp.p14.Vgvj(j); // 2.69
                fp.p14.Vcm(j); // 2.70
                fp.p14.Mcm(); // 2.71
                fp.p14.Rocm1(); // 2.72
                fp.p14.Re1(fp.ECInnerDiameter); // 2.73
                fp.p14.f(); // 2.74
                fp.p14.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 2.75
                fp.p14.Hj3(j); // 2.76
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;                
            }
        }
    }

    // Алгоритм расчета давления на выходе из насоса от устья при освоении скважины с УЭЦН
    public void Diagram_2_3()
    {
        p.PTxt("p14.Diagram_2_3");
        if (fp.Pnas0 <= fp.Pbufm)
        {
            fp.L1 = 0.0;
            fp.P1nkt = fp.Pbufm;
            fp.p14.Rozh(); // 2.85
            fp.p14.Vzhnkt(); // 2.86
            fp.p14.Renkt1(); // 2.87
            fp.p14.LambdaNkt(); // 2.88
            fp.p14.Pvyh1(); // 2.89
        }
        else
        {
            fp.L1 = 0.0;
            fp.P1nkt = fp.Pbufm;
            fp.p14.dP(); // 2.90
            fp.p14.Tgrp(fp.dnkt); // 2.91
            for (var j = 0; j < fp.N; j++)
            {
                fp.p14.Tj(j, 0); // 2.92
                fp.p14.Pnasj(j); // 2.93
                fp.p14.RfP(j); // 2.94
                fp.p14.mfT(j); // 2.95
                fp.p14.DfT(j); // 2.96
                fp.p14.Vgvj(j); // 2.97
                fp.p14.Vcm(j); // 2.98
                fp.p14.Mcm(); // 2.99
                fp.p14.Rocm1(); // 2.100
                fp.p14.Re1(fp.dnkt); // 2.101
                fp.p14.f(); // 2.102
                fp.p14.dPdH(fp.dnkt, fp.Rocm, j); // 2.103
                fp.p14.dHdP(j); // 2.104
                fp.p14.Hj1(fp.N, j, fp.dHdP); // 2.105
                fp.p14.Pj(j); // 2.106
                if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg2;
            }
            if (fp.Hg2 <= fp.Lcp)
            {
                fp.L1 = fp.Hg2;
                fp.P1nkt = fp.Pnas0;
                fp.p14.Rozh(); // 2.85
                fp.p14.Vzhnkt(); // 2.86
                fp.p14.Renkt1(); // 2.87
                fp.p14.LambdaNkt(); // 2.88
                fp.p14.Pvyh1(); // 2.89
            }
            else
            {
                fp.L1 = 0.0;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pbufm;
                fp.p14.dl1(); // 2.107
                fp.p14.Tgrp(fp.dnkt); // 2.108
                for (var j = 0; j < fp.N; j++)
                {
                    fp.p14.Tj(j, 0); // 2.109
                    fp.p14.mfT(j); // 2.110
                    fp.p14.DfT(j); // 2.111
                    fp.p14.mfT(j); // 2.112
                    fp.p14.DfT(j); // 2.113
                    fp.p14.Vgvj(j); // 2.114
                    fp.p14.Vcm(j); // 2.115
                    fp.p14.Mcm(); // 2.116
                    fp.p14.Rocm1(); // 2.117
                    fp.p14.Re1(fp.dnkt); // 2.118
                    fp.p14.f(); // 2.119
                    fp.p14.dPdH(fp.dnkt, fp.Rocm, j); // 2.120
                    fp.p14.Hj2(j); // 2.121
                    if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;                
                }
            }
        }
    }

    // Алгоритм расчета параметров насоса при работе на водонефтяной смеси
    public void Diagram_2_4()
    {
        p.PTxt("p14.Diagram_2_4");
        fp.p14.A2E(); // 2.132
        fp.p14.MjuE(); // 2.131, 2.133
        fp.p14.ng2(); // 2.134
        fp.p14.Wzhpr(); // 2.135
        fp.p14.Wgdr(); // 2.136
        fp.p14.Ksvh(); // 2.137
        fp.p14.Ksgs(); // 2.138
        fp.p14.Ks(); // 2.139
        fp.p14.ngn(); // 2.140
        fp.p14.Mjugzh(); // 2.141
        fp.p14.vgzh1(); // 2.142
        fp.p14.Rocm2(); // 2.144
        fp.p14.ns(); // 2.145
        for (var i = 0; i < 8; i++)
        {
            fp.p14.Qnvi(i); // 2.146
            fp.p14.Hnvi(i); // 2.147
            var Khq1 = 0.0;
            var Khq2 = Khq1 + 0.03;
            while (0.02 <= Math.Abs(Khq2 - Khq1))
            {
                fp.p14.Re2(); // 2.148
                fp.p14.Khq(i); // 2.149, 2.150
                Khq1 = fp.Khq;
                fp.Qnv[i] *= Khq1;
                fp.p14.Re2(); // 2.148
                fp.p14.Khq(i); // 2.149, 2.150
                Khq2 = fp.Khq;
            }
            fp.p14.Kk(i); // 2.151, 2.152
        }
        fp.p14.Cn(); // 2.153
        fp.p14.EtaD(); // 2.154
        fp.p14.Ccm(); // 2.155
        fp.p14.Tvyh(); // 2.156
        fp.p14.Nh(); // 2.159
        fp.p14.SNDpot(); // 2.160
        fp.p14.tdp(); // 2.161
        fp.p14.Kt(); // 2.162
        fp.p14.Kup(); // 2.163
        fp.p14.SN(); // 2.164
        fp.p14.Td(); // 2.165
        fp.p14.I(); // 2.166
    }

    // Алгоритм расчета параметров насоса при работе на жидкости глушения
    public void Diagram_2_5()
    {
        p.PTxt("p14.Diagram_2_5");
        fp.p14.vgzh2(); // 1.143
        fp.p14.ns(); // 2.145
        for (var i = 0; i < 8; i++)
        {
            fp.p14.Qnvi(i); // 2.146
            fp.p14.Hnvi(i); // 2.147
            var Khq1 = 0.0;
            var Khq2 = Khq1 + 0.03;
            while (0.02 <= Math.Abs(Khq2 - Khq1))
            {
                fp.p14.Re2(); // 2.148
                fp.p14.Khq(i); // 2.149, 2.150
                Khq1 = fp.Khq;
                fp.Qnv[i] *= Khq1;
                fp.p14.Re2(); // 2.148
                fp.p14.Khq(i); // 2.149, 2.150
                Khq2 = fp.Khq;
            }
            fp.p14.Kk(i); // 2.151, 2.152
        }
        fp.p14.Cn(); // 2.153
        fp.p14.EtaD(); // 2.154
        fp.p14.Ccm(); // 2.155
        fp.p14.Tvyh(); // 2.156
        fp.p14.Nh(); // 2.159
        fp.p14.SNDpot(); // 2.160
        fp.p14.tdp(); // 2.161
        fp.p14.Kt(); // 2.162
        fp.p14.Kup(); // 2.163
        fp.p14.SN(); // 2.164
        fp.p14.Td(); // 2.165
        fp.p14.I(); // 2.166
    }

    // Алгоритм работы скважины с УЭЦН при освоении
    public void Diagram_2_6(double time)
    {
        p.PTxt("p14.Diagram_2_6");
        if (fp.Vzhgzab <= fp.Vzhgpl)
        {
            fp.p14.Vzhek(); // 2.3
            fp.p14.Reek2(); // 2.4
            fp.p14.LambdaEk(); // 2.5
            fp.p14.Pzpk(); // 2.6
            fp.p14.Qskv2(fp.Pz, fp.rc); // 2.47
            fp.p14.Vzhgzab(fp.tz1, fp.tz2); // 2.48
            fp.p14.MP(fp.Pz); // 2.49, 2.50
            fp.p14.nv(fp.Pz); // 2.51
            fp.p14.Vzhek(); // 2.52
            fp.p14.Reek2(); // 2.53
            fp.p14.LambdaEk(); // 2.54
            fp.p14.Ppr3(fp.Pz); // 2.55
            if (fp.htr <= 0)
            {
                fp.p14.Pbuf1(); // 2.77
                fp.p14.Qg(fp.Q); // 2.78
                fp.p14.Pbuf2(fp.Q); // 2.79
                fp.p14.Pbufm(fp.Pbuf1, fp.Pbuf2); // 2.80
                fp.p14.Vzhnkt(); // 2.81
                fp.p14.Renkt2(); // 2.82
                fp.p14.LambdaNkt(); // 2.83
                fp.p14.Pvyh2(); // 2.84
                fp.p14.Pn(); // 2.128
                fp.p14.Rozh(); // 2.129
                fp.p14.Hn(); // 2.130
            }
            else
            {
                fp.Pbufm = 0.0;
                fp.p14.htr3(fp.tz1, fp.tz2); // 2.122
                fp.p14.Vzhnkt(); // 2.123
                fp.p14.Renkt2(); // 2.124
                fp.p14.LambdaNkt(); // 2.125
                fp.p14.htren(); // 2.126
                fp.p14.Hobshu(); // 2.127
            }
            Diagram_2_5();
        }
        else
        {
            Diagram_2_1();
            if (fp.Pz <= fp.Pnas0)
            {
                fp.p14.ng1(fp.Pz); // 2.44
                fp.p14.nst(); // 2.45
                fp.p14.Qskv1(fp.Pz, fp.rc); // 2.46
            }
            fp.p14.Vzhgzab(fp.tz1, fp.tz2); // 2.48
            fp.p14.MP(fp.Pz); // 2.49, 2.50
            fp.p14.nv(fp.Pz); // 2.51
            Diagram_2_2();
            if (fp.htr <= 0)
            {
                fp.p14.Pbuf1(); // 2.77
                fp.p14.Qg(fp.Q); // 2.78
                fp.p14.Pbuf2(fp.Q); // 2.79
                fp.p14.Pbufm(fp.Pbuf1, fp.Pbuf2); // 2.80
                if (fp.Vzhgpl < fp.Vzhgpov) Diagram_2_3();
                else 
                {
                    fp.p14.Vzhnkt(); // 2.81
                    fp.p14.Renkt2(); // 2.82
                    fp.p14.LambdaNkt(); // 2.83
                    fp.p14.Pvyh2(); // 2.84
                }
                fp.p14.Pn(); // 2.128
                fp.p14.Rozh(); // 2.129
                fp.p14.Hn(); // 2.130
            }
            else
            {
                fp.Pbufm = 0.0;
                fp.p14.htr3(fp.tz1, fp.tz2); // 2.122
                fp.p14.Vzhnkt(); // 2.123
                fp.p14.Renkt2(); // 2.124
                fp.p14.LambdaNkt(); // 2.125
                fp.p14.htren(); // 2.126
                fp.p14.Hobshu(); // 2.127
            }
            Diagram_2_5();
        }
        fp.p14.Vzhgpov(fp.tz1, fp.tz2); // 2.157
        fp.p14.htr3(fp.tz1, fp.tz2); // 2.158
        fp.p14.dhdin(fp.tz1, fp.tz2); // 2.167
        fp.p14.hdin1(); // 2.168
        fp.p14.Pzatr1(); // 2.169
        if (fp.dsht <= 1.0E-5) Diagram_2_7(time);
        if (Math.Abs(fp.Qskv - fp.Qn) < 1.0E-5)
        {
            Console.WriteLine("p14.Diagram_2_6: Осуществлен выход на установившийся режим.");
            Console.WriteLine("Расчет завершен.");
            p.PTxt("p14.Diagram_2_6: Осуществлен выход на установившийся режим.");
            p.PTxt("Расчет завершен.");
            Environment.Exit(0);
        }
    }

    // Алгоритм остановки работы скважины с УЭЦН при освоении
    public void Diagram_2_7(double time)
    {
        p.PTxt("p14.Diagram_2_7");
        if (!PNPT2)
        {
            fp.p14.DPpzo1(); // 2.170
            fp.p14.tzab1(); // 2.171
            PNPT2 = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            fp.p14.Pz(time); // 2.172, 2.173
            fp.p14.htr1(); // 2.174
            fp.p14.htr2(); // 2.175
            fp.p14.Pbuf3(); // 2.176
            fp.p14.hdin2(); // 2.177
            fp.p14.Pzatr2(); // 2.178
            fp.p14.Vost(fp.hdin); // 2.179
        }
    }
}
using System;
using System.Collections.Generic;

public class Prod_1_4
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_4(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
        
    /// <summary>
    /// Давление на приеме в НКТ (Па) (1.1, 2.1)
    /// </summary>
    /// <param name="Pzatr">Давление на затрубе, Па</param>
    /// <param name="hdin">Динамический уровень в скважине, м</param>
    /// <param name="Tu">Температура на устье, К</param>
    public void Ppr1(double Pzatr, double hdin, double Tu)
    {
        parameters.Clear();
        parameters.Add("Pzatr", Pzatr);
        parameters.Add("hdin", hdin);
        parameters.Add("Tu", Tu);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Z", fp.Z);
        p.PInC("1.1, 2.1", parameters);
        fp.Ppr = fp.Rozh * fp.g * (fp.Lcp - hdin) * Math.Cos(fp.alfa) + 
                 Pzatr * Math.Exp(0.03415 * hdin * fp.densityOfGas / (Tu * fp.Z));
        p.POutC("1.1, 2.1", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Плотность жидкости (кг/м3) (1.2, 1.3, 1.47, 1.71, 1.130, 2.2, 2.7, 2.57, 2.85, 2.129)
    /// </summary>
    public void Rozh()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("nv", fp.nv);
        p.PInC("1.2, 1.3, 1.47, 1.71, 1.130, 2.2, 2.7, 2.57, 2.85, 2.129", parameters);
        fp.Rozh = fp.densityOfOil * (1.0 - fp.nv) + fp.densityOfWater * fp.nv;
        p.POutC("1.2, 1.3, 1.47, 1.71, 1.130, 2.2, 2.7, 2.57, 2.85, 2.129", "Rozh", fp.Rozh);
    }
    
    /// <summary>
    /// Скорость подъема жидкости в ЭК (м/с) (1.4, 1.46, 2.3, 2.8, 2.52, 2.56)
    /// </summary>
    public void Vzhek()
    {
        parameters.Clear();
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.4, 1.46, 2.3, 2.8, 2.52, 2.56", parameters);
        fp.Vzhek = 4.0 * fp.Qskv / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("1.4, 1.46, 2.3, 2.8, 2.52, 2.56", "Vzhek", fp.Vzhek);
    }
    
    /// <summary>
    /// Число Рейнольдса в ЭК (1.5, 1.48, 2.9, 2.58)
    /// </summary>
    public void Reek()
    {
        parameters.Clear();
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("oilViscosity", fp.oilViscosity);
        p.PInC("1.5, 1.48, 2.9, 2.58", parameters);
        fp.Reek = fp.Vzhek * fp.Rozh * fp.ECInnerDiameter / fp.oilViscosity;
        p.POutC("1.5, 1.48, 2.9, 2.58", "Reek", fp.Reek);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК (1.6, 1.49, 2.5, 2.10, 2.54, 2.59)
    /// </summary>
    public void LambdaEk()
    {
        parameters.Clear();
        parameters.Add("Reek", fp.Reek);
        parameters.Add("EpsEk", fp.EpsEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.6, 1.49, 2.5, 2.10, 2.54, 2.59", parameters);
        fp.LambdaEk = 0.067 * (158.0 / fp.Reek + 2.0 * fp.EpsEk / fp.ECInnerDiameter);
        p.POutC("1.6, 1.49, 2.5, 2.10, 2.54, 2.59", "LambdaEk", fp.LambdaEk);
    }
    
    /// <summary>
    /// Забойное давление, рассчитанное при помощи распределения давления (Па) (1.7, 2.6, 2.11)
    /// </summary>
    public void Pzpk()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.7, 2.6, 2.11", parameters);
        fp.Pzpk = fp.Ppr + (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.7, 2.6, 2.11", "Pzpk", fp.Pzpk);
    }
    
    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера (Па) (1.8, 1.76, 2.12, 2.90)
    /// </summary>
    public void dP()
    {
        parameters.Clear();
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.8, 1.76, 2.12, 2.90", parameters);
        fp.dP = (fp.Pnas0 - fp.P1nkt) / fp.Nint;
        p.POutC("1.8, 1.76, 2.12, 2.90", "dP", fp.dP);
    }
    
    /// <summary>
    /// Температурный градиент потока (К/м) (1.9, 1.26, 1.53, 1.77, 1.94, 2.13, 2.30, 2.63, 2.91, 2.108)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Tgrp(double InnerDiam)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Gtgr", fp.Gtgr);
        parameters.Add("Qopt", fp.Qopt);
        p.PInC("1.9, 1.26, 1.53, 1.77, 1.94, 2.13, 2.30, 2.63, 2.91, 2.108", parameters);
        fp.Tgrp = (0.0034 + 0.79 * fp.Gtgr) / Math.Pow(10, fp.Qopt / (20.0 * Math.Pow(InnerDiam, 2.67)));
        p.POutC("1.9, 1.26, 1.53, 1.77, 1.94, 2.13, 2.30, 2.63, 2.91, 2.108", "Tgrp", fp.Tgrp);
    }
    
    /// <summary> 
    /// Температура в интервале H[j] (К) (1.10, 1.27. 1.54, 1.78, 1.95, 2.14, 2.31, 2.64, 2.92, 2.109)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    /// <param name="PrEKNKT">Признак, характеризующий то, где производится расчет (1.10) (ЭК (PrEKNKT = 1) или НКТ (PrEKNKT = 0))</param>
    public void Tj(int j, int PrEKNKT)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("PrEKNKT", PrEKNKT);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgrp", fp.Tgrp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("H[j]", fp.H[j]);
        parameters.Add("Tvyh", fp.Tvyh);
        p.PInC("1.10, 1.27. 1.54, 1.78, 1.95, 2.14, 2.31, 2.64, 2.92, 2.109", parameters);
        if (PrEKNKT == 1) fp.T[j] = fp.Tpl - fp.Tgrp * (fp.Lk - fp.H[j]);
        else fp.T[j] = fp.Tvyh - fp.Tgrp * (fp.Lcp - fp.H[j]);
        p.POutC("1.10, 1.27. 1.54, 1.78, 1.95, 2.14, 2.31, 2.64, 2.92, 2.109", "T[j]", fp.T[j]);
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (1.11, 1.55, 1.79, 2.15, 2.65, 2.93)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pnasj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Nc", fp.Nc);
        parameters.Add("Na", fp.Na);
        p.PInC("1.11, 1.55, 1.79, 2.15, 2.65, 2.93", parameters);
        fp.Pnas[j] = fp.Pnas0 - (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
        p.POutC("1.11, 1.55, 1.79, 2.15, 2.65, 2.93", "Pnas[j]", fp.Pnas[j]);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.12, 1.56, 1.80, 2.16, 2.66, 2.94)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void RfP(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("Pnas[j]", fp.Pnas[j]);
        p.PInC("1.12, 1.56, 1.80, 2.16, 2.66, 2.94", parameters);
        fp.RfP = (1.0 + Math.Log10(1.0E-6 * fp.P[j])) / (1.0 + Math.Log10(1.0E-6 * fp.Pnas[j])) - 1.0;
        p.POutC("1.12, 1.56, 1.80, 2.16, 2.66, 2.94", "RfP", fp.RfP);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.13, 1.28, 1.30, 1.57, 1.81, 1.96, 1.98, 2.17, 2.32, 2.34, 2.67, 2.95, 2.110, 2.112)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void mfT(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("1.13, 1.28, 1.30, 1.57, 1.81, 1.96, 1.98, 2.17, 2.32, 2.34, 2.67, 2.95, 2.110, 2.112", parameters);
        fp.mfT = 1.0 + 0.029 * (fp.T[j] - 293.0) * (fp.densityOfOil * fp.densityOfGas * 1.0E-3 - 0.7966);
        p.POutC("1.13, 1.28, 1.30, 1.57, 1.81, 1.96, 1.98, 2.17, 2.32, 2.34, 2.67, 2.95, 2.110, 2.112", "mfT", fp.mfT);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.14, 1.29, 1.31, 1.58, 1.82, 1.97, 1.99, 2.18, 2.33, 2.35, 2.68, 2.96, 2.111, 2.113)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void DfT(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("1.14, 1.29, 1.31, 1.58, 1.82, 1.97, 1.99, 2.18, 2.33, 2.35, 2.68, 2.96, 2.111, 2.113", parameters);
        fp.DfT = 1.0E-3 * fp.densityOfOil * fp.densityOfGas * (4.5 - 0.00305 * (fp.T[j] - 293.0)) - 4.785;
        p.POutC("1.14, 1.29, 1.31, 1.58, 1.82, 1.97, 1.99, 2.18, 2.33, 2.35, 2.68, 2.96, 2.111, 2.113", "DfT", fp.DfT);
    }
    
    /// <summary>
    /// Удельный объем выделившегося газа (м3/м3) (1.15, 1.32, 1.59, 1.83, 1.100, 2.19, 2.36, 2.69, 2.97, 2.114)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается удельный объем выделившегося газа</param>
    public void Vgvj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("RfP", fp.RfP);
        parameters.Add("mfT", fp.mfT);
        parameters.Add("DfT", fp.DfT);
        p.PInC("1.15, 1.32, 1.59, 1.83, 1.100, 2.19, 2.36, 2.69, 2.97, 2.114", parameters);
        fp.Vgv[j] = fp.gasFactor * fp.RfP * fp.mfT * (fp.DfT * (1 + fp.RfP) - 1.0);
        p.POutC("1.15, 1.32, 1.59, 1.83, 1.100, 2.19, 2.36, 2.69, 2.97, 2.114", "Vgv[j]", fp.Vgv[j]);
    }
    
    /// <summary>
    /// Удельный объем смеси (м3/м3) (1.16, 1.33, 1.60, 1.84, 1.101, 2.20, 2.37, 2.70, 2.98, 2.115)
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vcm(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Vgv[j]", fp.Vgv[j]);
        parameters.Add("bOil", fp.bOil);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("Tnu", fp.Tnu);
        parameters.Add("nv", fp.nv);
        p.PInC("1.16, 1.33, 1.60, 1.84, 1.101, 2.20, 2.37, 2.70, 2.98, 2.115", parameters);
        fp.Vcm = fp.bOil + fp.Vgv[j] * fp.Z * fp.Pnu * fp.T[j] / (fp.P[j] * fp.Tnu) + 
                 fp.nv / (1.0 - fp.nv);
        p.POutC("1.16, 1.33, 1.60, 1.84, 1.101, 2.20, 2.37, 2.70, 2.98, 2.115", "Vcm", fp.Vcm);
    }
    
    /// <summary>
    /// Удельная масса смеси (кг/м3) (1.17, 1.34, 1.61, 1.85, 1.102, 2.21, 2.38, 2.71, 2.99, 2.116)
    /// </summary>
    public void Mcm()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("nv", fp.nv);
        p.PInC("1.17, 1.34, 1.61, 1.85, 1.102, 2.21, 2.38, 2.71, 2.99, 2.116", parameters);
        fp.Mcm = fp.densityOfOil + fp.densityOfGas * fp.gasFactor + 
                 fp.densityOfWater * fp.nv / (1.0 - fp.nv);
        p.POutC("1.17, 1.34, 1.61, 1.85, 1.102, 2.21, 2.38, 2.71, 2.99, 2.116", "Mcm", fp.Mcm);
    }
    
    /// <summary>
    /// Плотность газожидкостной смеси (кг/м3) (1.18, 1.35, 1.62, 1.86, 1.103, 2.22, 2.39, 2.72, 2.100, 2.117)
    /// </summary>
    public void Rocm1()
    {
        parameters.Clear();
        parameters.Add("Vcm", fp.Vcm);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.18, 1.35, 1.62, 1.86, 1.103, 2.22, 2.39, 2.72, 2.100, 2.117", parameters);
        fp.Rocm = fp.Mcm / fp.Vcm;
        p.POutC("1.18, 1.35, 1.62, 1.86, 1.103, 2.22, 2.39, 2.72, 2.100, 2.117", "Rocm", fp.Rocm);
    }
    
    /// <summary>
    /// Число Рейнольдса (1.19, 1.36, 1.63, 1.87, 1.104, 2.23, 2.40, 2.73, 2.101, 2.118)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Re1(double InnerDiam)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.19, 1.36, 1.63, 1.87, 1.104, 2.23, 2.40, 2.73, 2.101, 2.118", parameters);
        fp.Re = 0.99E-5 * 86400.0 * fp.Q * (1.0 - fp.nv) * fp.Mcm / InnerDiam;
        p.POutC("1.19, 1.36, 1.63, 1.87, 1.104, 2.23, 2.40, 2.73, 2.101, 2.118", "Re", fp.Re);
    }
    
    /// <summary>
    /// Корреляционный коэффициент (1.20, 1.37, 1.64, 1.88, 1.105, 2.24, 2.41, 2.74, 2.102, 2.119)
    /// </summary>
    public void f()
    {
        parameters.Clear();
        parameters.Add("Re", fp.Re);
        p.PInC("1.20, 1.37, 1.64, 1.88, 1.105, 2.24, 2.41, 2.74, 2.102, 2.119", parameters);
        fp.f = Math.Pow(10, 19.66 * Math.Pow(1.0 + Math.Log10(fp.Re), -0.25) - 17.13);
        p.POutC("1.20, 1.37, 1.64, 1.88, 1.105, 2.24, 2.41, 2.74, 2.102, 2.119", "f", fp.f);
    }
    
    /// <summary>
    /// Дифференциал (Па/м) (1.21, 1.38, 1.65, 1.89, 1.106, 2.25, 2.42, 2.75, 2.103, 2.120)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    /// <param name="Rocm">Плотность газожидкостной смеси, кг/м3</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dPdH(double InnerDiam, double Rocm, int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Rocm", Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("f", fp.f);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.21, 1.38, 1.65, 1.89, 1.106, 2.25, 2.42, 2.75, 2.103, 2.120", parameters);
        fp.dPdH[j] = Rocm * fp.g * Math.Cos(fp.alfa) + 
                     fp.f * Math.Pow(86400.0 * fp.Q, 2) * Math.Pow(1.0 - fp.nv, 2) * Math.Pow(fp.Mcm, 2) /
                     (2.3024E9 * Rocm * Math.Pow(InnerDiam, 5));
        p.POutC("1.21, 1.38, 1.65, 1.89, 1.106, 2.25, 2.42, 2.75, 2.103, 2.120", "dPdH[j]", fp.dPdH[j]);
    }
    
    /// <summary>
    /// Число, обратное дифференциалу (1.21) (м/Па) (1.22, 1.90, 2.26, 2.104)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dHdP(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("dPdH[j]", fp.dPdH[j]);
        p.PInC("1.22, 1.90, 2.26, 2.104", parameters);
        fp.dHdP[j] = 1.0 / fp.dPdH[j];
        p.POutC("1.22, 1.90, 2.26, 2.104", "dHdP[j]", fp.dHdP[j]);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.23, 1.91, 2.27, 2.105)
    /// </summary>
    /// <param name="N">Количество элементов в массиве dHdP, шт.</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    /// <param name="dHdP">Массив значений дифференциала dH/dP на разных интервалах Hj</param>
    public void Hj1(int N, int j, double[] dHdP)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("j", j);
        parameters.Add("dHdP[j]", dHdP[j]);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("1.23, 1.91, 2.27, 2.105", parameters);
        var sum = 0.0;
        for (var i = 0; i < N; i++) sum += dHdP[i];
        fp.H[j] = fp.L1 + fp.dl * ((dHdP[1] + dHdP[j]) / 2.0 + sum);
        p.POutC("1.23, 1.91, 2.27, 2.105", "H[j]", fp.H[j]);
    }
    
    /// <summary>
    /// Давление в интервале Н[j] (Па) (1.24, 1.92, 2.28, 2.106)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("dP", fp.dP);
        p.PInC("1.24, 1.92, 2.28, 2.106", parameters);
        fp.P[j] = fp.P1nkt + fp.dP * j;
        p.POutC("1.24, 1.92, 2.28, 2.106", "P[j]", fp.P[j]);
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (1.25, 1.93, 2.29, 2.107)
    /// </summary>
    public void dl1()
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("L2", fp.L2);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.25, 1.93, 2.29, 2.107", parameters);
        fp.dl = (fp.L2 - fp.L1) / fp.Nint;
        p.POutC("1.25, 1.93, 2.29, 2.107", "dl", fp.dl);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.39, 1.107, 2.43, 2.121)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Hj2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("1.39, 1.107, 2.43, 2.121", parameters);
        fp.H[j] = fp.L1 + fp.dl * j;
        p.POutC("1.39, 1.107, 2.43, 2.121", "H[j]", fp.H[j]);
    }

    /// <summary>
    /// Количество свободного газа (д. ед.) (1.40, 2.44)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void ng1(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("nv", fp.nv);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Z", fp.Z);
        parameters.Add("Tnu", fp.Tnu);
        p.PInC("1.40, 2.44", parameters);
        var tempVar = fp.gasFactor * (Pz - fp.Pnas0) / (fp.Pnu - fp.Pnas0) * 
            (1.0 - fp.nv) * fp.Pnu * fp.Tpl * fp.Z / (Pz * fp.Tnu);
        fp.ng = tempVar / (1.0 + tempVar);
        p.POutC("1.40, 2.44", "ng", fp.ng);
    }

    /// <summary>
    /// Показатель степени в формуле (1.42) (д. ед.) (1.41, 2.45)
    /// </summary>
    public void nst()
    {
        parameters.Clear();
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("ng", fp.ng);
        p.PInC("1.41, 2.45", parameters);
        if (fp.Pnas0 < fp.Pz) fp.nst = 1.0;
        else fp.nst = 1.0 - 0.03 * fp.ng;
        p.POutC("1.41, 2.45", "nst", fp.nst);
    }

    /// <summary>
    /// Расход жидкости, поступающей из пласта  (м3 / с) (1.42, 2.46)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void Qskv1(double Pz, double rc)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("nst", fp.nst);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.42, 2.46", parameters);
        fp.Qskv = 2.0 * fp.pi * fp.k * fp.h * Math.Pow(fp.reservoirPressurePa - Pz, fp.nst) / 
                  (fp.oilViscosity * Math.Log(fp.Rk / (rc * Math.Exp(-fp.skinFactor))));
        p.POutC("1.42, 2.46", "Qskv", fp.Qskv);
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (1.43, 1.44; 2.49, 2.50)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("deltaPpred", fp.deltaPpred);
        parameters.Add("A1", fp.A1);
        parameters.Add("A2", fp.A2);
        p.PInC("1.43, 1.44; 2.49, 2.50", parameters);
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
        p.POutC("1.43, 1.44; 2.49, 2.50", "MP", fp.MP);
    }

    /// <summary>
    /// Обводненность продукции (д. ед.) (1.45, 2.51)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void nv(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("productWaterCut0", fp.productWaterCut0);
        parameters.Add("productWaterCutMax", fp.productWaterCutMax);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("DPmax", fp.DPmax);
        p.PInC("1.45, 2.51", parameters);
        fp.nv = fp.productWaterCut0 + (fp.productWaterCutMax - fp.productWaterCut0) * 
            Math.Pow(fp.reservoirPressurePa - Pz, 2) / fp.DPmax;
        p.POutC("1.45, 2.51", "nv", fp.nv);
    }

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ (м) (1.50, 2.60)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg1(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.50, 2.60", parameters);
        fp.Hg1 = fp.Lk - (Pz - fp.Pnas0) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                            fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.50, 2.60", "Hg1", fp.Hg1);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (Па) (1.51, 2.61)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ppr2(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Hg1", fp.Hg1);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.51, 2.61", parameters);
        if (fp.Hg1 < fp.Lcp) fp.Ppr = Pz - (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.51, 2.61", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (1.52, 2.62)
    /// </summary>
    public void dl2()
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("L2", fp.L2);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.52, 2.62", parameters);
        fp.dl = (fp.L1 - fp.L2) / fp.Nint;
        p.POutC("1.52, 2.62", "dl", fp.dl);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.66, 2.76)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Hj3(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("1.66, 2.76", parameters);
        fp.H[j] = fp.L1 - fp.dl * j;
        p.POutC("1.66, 2.76", "H[j]", fp.H[j]);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле (Па) (1.67, 2.77)
    /// </summary>
    public void Pbuf1()
    {
        parameters.Clear();
        parameters.Add("Qg", fp.Qg);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        p.PInC("1.67, 2.77", parameters);
        fp.Pbuf1 = 0.0729 * Math.Pow(1.0, 2) * fp.Qg * 86400.0 * fp.densityOfGas * fp.Plin /
                   Math.Pow(1000.0 * fp.dsht, 2);
        p.POutC("1.67, 2.77", "Pbuf1", fp.Pbuf1);
    }
    
    /// <summary>
    /// Расход газа (м3/с) (1.68, 2.78)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Qg(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Vgvust", fp.Vgvust);
        parameters.Add("nv", fp.nv);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pbufm", fp.Pbufm);
        parameters.Add("Tst", fp.Tst);
        p.PInC("1.68, 2.78", parameters);
        fp.Qg = Q * fp.Vgvust * (1.0 - fp.nv) * fp.Pst * fp.Tbuf * fp.Z / (fp.Pbufm * fp.Tst);
        p.POutC("1.68, 2.78", "Qg", fp.Qg);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле (Па) (1.69, 2.79)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Pbuf2(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        parameters.Add("g", fp.g);
        p.PInC("1.69, 2.79", parameters);
        fp.Pbuf2 = fp.Plin + Math.Pow(Q, 2) / (78.8768 * Math.Pow(1000.0 * fp.dsht, 4) * fp.g);
        p.POutC("1.69, 2.79", "Pbuf2", fp.Pbuf2);
    }
    
    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по формулам (Па) (1.70, 2.80)
    /// </summary>
    /// <param name="Pbuf1">Буферное давление, рассчитанное по первой формуле, Па</param>
    /// <param name="Pbuf2">Буферное давление, рассчитанное по второй формуле, Па</param>
    public void Pbufm(double Pbuf1, double Pbuf2)
    {
        parameters.Clear();
        parameters.Add("Pbuf1", Pbuf1);
        parameters.Add("Pbuf2", Pbuf2);
        p.PInC("1.70, 2.80", parameters);
        fp.Pbufm = Math.Max(Pbuf1, Pbuf2);
        p.POutC("1.70, 2.80", "Pbufm", fp.Pbufm);
    }
    
    /// <summary>
    /// Скорость подъема жидкости в НКТ (м/с) (1.72, 2.81, 2.86, 2.123)
    /// </summary>
    public void Vzhnkt()
    {
        parameters.Clear();
        parameters.Add("Qn", fp.Qn);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.72, 2.81, 2.86, 2.123", parameters);
        fp.Vzhnkt = 4.0 * fp.Qn / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("1.72, 2.81, 2.86, 2.123", "Vzhnkt", fp.Vzhnkt);
    }
    
    /// <summary>
    /// Число Рейнольдса в НКТ (1.73, 2.87)
    /// </summary>
    public void Renkt1()
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Mjuzh", fp.Mjuzh);
        p.PInC("1.73, 2.87", parameters);
        fp.Renkt = fp.Vzhnkt * fp.Rozh * fp.dnkt / fp.Mjuzh;
        p.POutC("1.73, 2.87", "Renkt", fp.Renkt);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в НКТ (1.74, 2.83, 2.88, 2.125)
    /// </summary>
    public void LambdaNkt()
    {
        parameters.Clear();
        parameters.Add("Renkt", fp.Renkt);
        parameters.Add("EpsNkt", fp.EpsNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.74, 2.83, 2.88, 2.125", parameters);
        fp.LambdaNkt = 0.067 * (158.0 / fp.Renkt + 2.0 * fp.EpsNkt / fp.dnkt);
        p.POutC("1.74, 2.83, 2.88, 2.125", "LambdaNkt", fp.LambdaNkt);
    }
    
    /// <summary>
    /// Давление на выходе из насоса (Па) (1.75, 2.89)
    /// </summary>
    public void Pvyh1()
    {
        parameters.Clear();
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("L1", fp.L1);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.75, 2.89", parameters);
        fp.Pvyh = fp.P1nkt + (fp.Lcp - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                 fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("1.75, 2.89", "Pvyh", fp.Pvyh);
    }
    
    /// <summary>
    /// Вязкость эмульсии (Па•с) (1.108, 1.110; 2.131, 2.133)
    /// </summary>
    public void MjuE()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        parameters.Add("WaterViscosity", fp.WaterViscosity);
        parameters.Add("A2E", fp.A2E);
        parameters.Add("oilViscosity", fp.oilViscosity);
        p.PInC("1.108, 1.110; 2.131, 2.133", parameters);
        if (0.5 < fp.nv) fp.MjuE = fp.WaterViscosity * Math.Pow(10.0, 3.2 * (1.0 - fp.nv));
        else
        {
            if (1.0 < fp.A2E) fp.B2E = fp.A2E * fp.oilViscosity;
            else fp.B2E = fp.oilViscosity;
            parameters.Add("B2E", fp.B2E);
            fp.MjuE = fp.B2E * (1.0 + 2.9 * fp.nv) / (1.0 - fp.nv);
        }
        p.POutC("1.108, 1.110; 2.131, 2.133", "MjuE", fp.MjuE);
    }
    
    /// <summary>
    /// Параметр, учитывающий скорость сдвига (1.109, 2.132)
    /// </summary>
    public void A2E()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        p.PInC("1.109, 2.132", parameters);
        fp.A2E = (1.0 + 20.0 * Math.Pow(fp.nv, 2)) * Math.Pow(80, -0.48 * fp.nv);
        p.POutC("1.109, 2.132", "A2E", fp.A2E);
    }

    /// <summary>
    /// Количество свободного газа (д. ед.) (1.111, 2.134)
    /// </summary>
    public void ng2()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("Tnu", fp.Tnu);
        parameters.Add("Pprn", fp.Pprn);
        parameters.Add("Tprn", fp.Tprn);
        parameters.Add("Vgvpr", fp.Vgvpr);
        parameters.Add("Z", fp.Z);
        p.PInC("1.111, 2.134", parameters);
        var tempVar = fp.Vgvpr * (1.0 - fp.nv) * fp.Pnu * fp.Tprn * fp.Z / (fp.Pprn * fp.Tnu);
        fp.ng = tempVar / (1.0 + tempVar);
        p.POutC("1.111, 2.134", "ng", fp.ng);
    }
        
    /// <summary>
    /// Приведенная скорость жидкости (м/с) (1.112, 2.135)
    /// </summary>
    public void Wzhpr()
    {
        parameters.Clear();
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("nv", fp.nv);
        parameters.Add("bOil", fp.bOil);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("1.112, 2.135", parameters);
        fp.Wzhpr = 4.0 * fp.Qskv * (fp.nv + fp.bOil * (1.0 - fp.nv)) / 
                   (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2) * fp.pi);
        p.POutC("1.112, 2.135", "Wzhpr", fp.Wzhpr);
    }
    
    /// <summary>
    /// Скорость дрейфа газа (м/с) (1.113, 2.136)
    /// </summary>
    public void Wgdr()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        p.PInC("1.113, 2.136", parameters);
        fp.Wgdr = 0.17;
        if (fp.nv <= 0.5) fp.Wgdr = 0.02;
        p.POutC("1.113, 2.136", "Wgdr", fp.Wgdr);
    }
    
    /// <summary>
    /// Коэффициент сепарации газа при переходе откачиваемой продукции из кольцевого пространства скважины во всасывающую камеру насоса (1.114, 2.137)
    /// </summary>
    public void Ksvh()
    {
        parameters.Clear();
        parameters.Add("Wzhpr", fp.Wzhpr);
        parameters.Add("Wgdr", fp.Wgdr);
        parameters.Add("ng", fp.ng);
        p.PInC("1.114, 2.137", parameters);
        fp.Ksvh = 1.0 / (1.0 + 0.52 * fp.Wzhpr / (fp.Wgdr * (1.0 - 0.06 * fp.ng)));
        p.POutC("1.114, 2.137", "Ksvh", fp.Ksvh);
    }
    
    /// <summary>
    /// Коэффициент сепарации за счет работы газосепаратора, если такой установлен (1.115, 2.138)
    /// </summary>
    public void Ksgs()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        p.PInC("1.115, 2.138", parameters);
        fp.Ksgs = 0.8;
        if (fp.nv <= 0.5) fp.Ksgs = 0.6;
        p.POutC("1.115, 2.138", "Ksgs", fp.Ksgs);
    }
    
    /// <summary>
    /// Коэффициент сепарации (1.116, 2.139)
    /// </summary>
    public void Ks()
    {
        parameters.Clear();
        parameters.Add("Ksvh", fp.Ksvh);
        parameters.Add("Ksgs", fp.Ksgs);
        p.PInC("1.116, 2.139", parameters);
        fp.Ks = 1.0 - (1.0 - fp.Ksvh) * (1.0 - fp.Ksgs);
        p.POutC("1.116, 2.139", "Ks", fp.Ks);
    }
    
    /// <summary>
    /// Количество свободного газа, поступившего в насос (д. ед.) (1.117, 2.140)
    /// </summary>
    public void ngn()
    {
        parameters.Clear();
        parameters.Add("Ks", fp.Ks);
        parameters.Add("ng", fp.ng);
        p.PInC("1.117, 2.140", parameters);
        fp.ngn = fp.Ks * fp.ng;
        p.POutC("1.117, 2.140", "ngn", fp.ngn);
    }
    
    /// <summary>
    /// Вязкость газожидкостной смеси (Па*с) (1.118, 2.141)
    /// </summary>
    public void Mjugzh()
    {
        parameters.Clear();
        parameters.Add("MjuE", fp.MjuE);
        parameters.Add("ngn", fp.ngn);
        p.PInC("1.118, 2.141", parameters);
        fp.Mjugzh = (0.023 + 0.71 * fp.MjuE) * (1.0 - fp.ngn);
        p.POutC("1.118, 2.141", "Mjugzh", fp.Mjugzh);
    }
    
    /// <summary>
    /// Кинематическая вязкость газожидкостной смеси (м2/с) (1.119, 2.142)
    /// </summary>
    public void vgzh1()
    {
        parameters.Clear();
        parameters.Add("Mjugzh", fp.Mjugzh);
        parameters.Add("Rocm", fp.Rocm);
        p.PInC("1.119, 2.142", parameters);
        fp.vgzh = fp.Mjugzh / fp.Rocm;
        p.POutC("1.119, 2.142", "vgzh", fp.vgzh);
    }
    
    /// <summary>
    /// Плотность газожидкостной смеси (кг/м3) (1.120, 2.144)
    /// </summary>
    public void Rocm2()
    {
        parameters.Clear();
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("ngn", fp.ngn);
        parameters.Add("nv", fp.nv);
        p.PInC("1.120, 2.144", parameters);
        fp.Rocm = fp.densityOfGas * fp.ngn + (fp.densityOfWater * fp.nv + fp.densityOfOil * (1.0 - fp.nv)) *
            (1.0 - fp.ngn);
        p.POutC("1.120, 2.144", "Rocm", fp.Rocm);
    }
    
    /// <summary>
    /// Коэффициент быстроходности ступени (1.121, 2.145)
    /// </summary>
    public void ns()
    {
        parameters.Clear();
        parameters.Add("nbp", fp.nbp);
        parameters.Add("Qopt", fp.Qopt);
        parameters.Add("g", fp.g);
        parameters.Add("Hopt", fp.Hopt);
        parameters.Add("Zst", fp.Zst);
        p.PInC("1.121, 2.145", parameters);
        fp.ns = 193.0 * fp.nbp * Math.Pow(fp.Qopt, 0.5) * Math.Pow(fp.g * fp.Hopt / fp.Zst, 0.5);
        p.POutC("1.121, 2.145", "ns", fp.ns);
    }
    
    /// <summary>
    /// Подача насоса при работе на воде в соответственном режиме (м3/с) (1.122, 2.146)
    /// </summary>
    /// <param name="i">Номер режима работы насоса на воде</param>
    public void Qnvi(int i)
    {
        parameters.Clear();
        parameters.Add("Qnv[i]", fp.Qnv[i]);
        parameters.Add("nbp", fp.nbp);
        p.PInC("1.122, 2.146", parameters);
        fp.Qnv[i] = fp.Qnv[i] * fp.nbp / 50.0;
        p.POutC("1.122, 2.146", "Qnv[i]", fp.Qnv[i]);
    }
    
    /// <summary>
    /// Напор насоса при работе на воде в соответственном режиме (м) (1.123, 2.147)
    /// </summary>
    /// <param name="i">Номер режима работы насоса на воде</param>
    public void Hnvi(int i)
    {
        parameters.Clear();
        parameters.Add("Hnv[i]", fp.Hnv[i]);
        parameters.Add("nbp", fp.nbp);
        p.PInC("1.123, 2.147", parameters);
        fp.Hnv[i] = fp.Hnv[i] * Math.Pow(fp.nbp, 2) / 2500.0;
        p.POutC("1.123, 2.147", "Hnv[i]", fp.Hnv[i]);
    }
    
    /// <summary>
    /// Число Рейнольдса (1.124, 2.148)
    /// </summary>
    public void Re2()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("Qopt", fp.Qopt);
        parameters.Add("ns", fp.ns);
        parameters.Add("vgzh", fp.vgzh);
        parameters.Add("nbp", fp.nbp);
        p.PInC("1.124, 2.148", parameters);
        fp.Re = (4.3 + 0.816 * Math.Pow(fp.ns, 0.274)) * fp.Q / (Math.Pow(fp.ns, 0.575) * fp.vgzh) *
                Math.Pow(fp.nbp / fp.Qopt, 1.0 /3.0);
        p.POutC("1.124, 2.148", "Re", fp.Re);
    }
    
    /// <summary>
    /// Коэффициент зависимости напора и подачи от вязкости (1.125, 1.126; 2.149, 2.150)
    /// </summary>
    /// <param name="i">Номер режима работы насоса на воде</param>
    public void Khq(int i)
    {
        parameters.Clear();
        parameters.Add("Re", fp.Re);
        parameters.Add("Qnv[i]", fp.Qnv[i]);
        parameters.Add("Qopt", fp.Qopt);
        var Khq1 = 1.0 - (3.585 - 0.821 * Math.Log10(fp.Re)) * (0.027 + 0.0485 * fp.Qnv[i] / fp.Qopt);
        var Khq2 = fp.Re / (fp.Re - 50.0 + 200.0 * fp.Qnv[i] / fp.Qopt);
        parameters.Add("Khq1", Khq1);
        parameters.Add("Khq2", Khq2);
        p.PInC("1.125, 1.126; 2.149, 2.150", parameters);
        fp.Khq = Khq1 < Khq2 ? Khq1 : Khq2;
        p.POutC("1.125, 1.126; 2.149, 2.150", "Khq", fp.Khq);
    }
    
    /// <summary>
    /// Коэффициент зависимости КПД от вязкости (1.127, 1.128; 2.151, 2.152)
    /// </summary>
    /// <param name="i">Номер режима работы насоса на воде</param>
    public void Kk(int i)
    {
        parameters.Clear();
        parameters.Add("Re", fp.Re);
        parameters.Add("Qnv[i]", fp.Qnv[i]);
        parameters.Add("Qopt", fp.Qopt);
        var Kk1 = 0.274 * Math.Log10(fp.Re) - 0.06 - 0.14 * fp.Qnv[i] / fp.Qopt;
        var Kk2 = 0.485 * Math.Log10(fp.Re) - 0.63 - 0.26 * fp.Qnv[i] / fp.Qopt;
        parameters.Add("Kk1", Kk1);
        parameters.Add("Kk2", Kk2);
        p.PInC("1.127, 1.128; 2.151, 2.152", parameters);
        fp.Kk = Kk1 < Kk2 ? Kk1 : Kk2;
        p.POutC("1.127, 1.128; 2.151, 2.152", "Kk", fp.Kk);
    }
    
    /// <summary>
    /// Давление, развиваемое насосом (Па) (1.129, 2.128)
    /// </summary>
    public void Pn()
    {
        parameters.Clear();
        parameters.Add("Pvyh", fp.Pvyh);
        parameters.Add("Pprn", fp.Pprn);
        p.PInC("1.129, 2.128", parameters);
        fp.Pn = fp.Pvyh - fp.Pprn;
        p.POutC("1.129, 2.128", "Pn", fp.Pn);
    }
    
    /// <summary>
    /// Напор насоса (Н) (1.131, 2.130)
    /// </summary>
    public void Hn()
    {
        parameters.Clear();
        parameters.Add("Prn", fp.Prn);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        p.PInC("1.131, 2.130", parameters);
        fp.Hn = fp.Prn / (fp.Rozh * fp.g);
        p.POutC("1.131, 2.130", "Hn", fp.Hn);
    }
    
    /// <summary>
    /// Теплоемкость нефти (Дж/(кг*К)) (1.132, 2.153)
    /// </summary>
    public void Cn()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        p.PInC("1.132, 2.153", parameters);
        fp.Cn = 3391.0 - 1.675 * fp.densityOfOil;
        p.POutC("1.132, 2.153", "Cn", fp.Cn);
    }
    
    /// <summary>
    /// КПД двигателя (д. е.) (1.133, 2.154)
    /// </summary>
    public void EtaD()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        p.PInC("1.133, 2.154", parameters);
        fp.EtaD = 1.03 * Math.Pow(fp.Q, 0.045);
        p.POutC("1.133, 2.154", "EtaD", fp.EtaD);
    }
    
    /// <summary>
    /// Теплоемкость смеси (Дж/(кг*К)) (1.134, 2.155)
    /// </summary>
    public void Ccm()
    {
        parameters.Clear();
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("nv", fp.nv);
        parameters.Add("Cv", fp.Cv);
        parameters.Add("Cn", fp.Cn);
        p.PInC("1.134, 2.155", parameters);
        fp.Ccm = (fp.nv * fp.densityOfWater * fp.Cv + (1.0 - fp.nv) * fp.densityOfOil * fp.Cn) / 
                 (fp.nv * fp.densityOfWater + (1.0 - fp.nv) * fp.densityOfOil);
        p.POutC("1.134, 2.155", "Ccm", fp.Ccm);
    }
    
    /// <summary>
    /// Температура на выходе из насоса (К) (1.135, 2.156)
    /// </summary>
    public void Tvyh()
    {
        parameters.Clear();
        parameters.Add("Tprn", fp.Tprn);
        parameters.Add("g", fp.g);
        parameters.Add("Hn", fp.Hn);
        parameters.Add("EtaD", fp.EtaD);
        parameters.Add("EtaN", fp.EtaN);
        parameters.Add("Ccm", fp.Ccm);
        p.PInC("1.135, 2.156", parameters);
        fp.Tvyh = fp.Tprn + fp.g * fp.Hn * (1.0 / fp.EtaD - 1.0) / (2.0 * fp.Ccm * fp.EtaN) +
                  fp.g * fp.Hn * (1.0 / (fp.EtaN * fp.EtaD) - 0.5 / fp.EtaN - 0.5) / fp.Ccm;
        p.POutC("1.135, 2.156", "Tvyh", fp.Tvyh);
    }
    
    /// <summary>
    /// Мощность, потребляемая насосом (Вт) (1.136, 2.159)
    /// </summary>
    public void Nh()
    {
        parameters.Clear();
        parameters.Add("Rocm", fp.Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("Hn", fp.Hn);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("EtaN", fp.EtaN);
        p.PInC("1.136, 2.159", parameters);
        fp.Nh = fp.Rocm * fp.g * fp.Hn * fp.Qn / fp.EtaN;
        p.POutC("1.136, 2.159", "Nh", fp.Nh);
    }
    
    /// <summary>
    /// Сумма потерь мощности в ПЭД (Вт) (1.137, 2.160)
    /// </summary>
    public void SNDpot()
    {
        parameters.Clear();
        parameters.Add("Ng", fp.Ng);
        parameters.Add("Nh", fp.Nh);
        parameters.Add("EtaD", fp.EtaD);
        parameters.Add("b2", fp.b2);
        parameters.Add("c2", fp.c2);
        parameters.Add("d2", fp.d2);
        p.PInC("1.137, 2.160", parameters);
        fp.SNDpot = fp.Nh * (1.0 / (fp.EtaD * (Math.Pow(fp.b2, 2) - Math.Pow(fp.c2, 2) *
            Math.Pow(fp.Nh / fp.Ng - fp.d2, 2))) - 1.0);
        p.POutC("1.137, 2.160", "SNDpot", fp.SNDpot);
    }
    
    /// <summary>
    /// Температура перегрева ПЭД (К) (1.138, 2.161)
    /// </summary>
    public void tdp()
    {
        parameters.Clear();
        parameters.Add("b3", fp.b3);
        parameters.Add("c3", fp.c3);
        parameters.Add("SNDpot", fp.SNDpot);
        p.PInC("1.138, 2.161", parameters);
        fp.tdp = 1.0E-3 * fp.b3 * fp.SNDpot - fp.c3;
        p.POutC("1.138, 2.161", "tdp", fp.tdp);
    }
    
    /// <summary>
    /// Коэффициент, учитывающий изменение температуры перегрева от количества воды и свободного газа (1.139, 2.162)
    /// </summary>
    public void Kt()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        parameters.Add("ngn", fp.ngn);
        p.PInC("1.139, 2.162", parameters);
        fp.Kt = (2.0 - fp.nv) * (1.0 - 0.75 * fp.ngn);
        p.POutC("1.139, 2.162", "Kt", fp.Kt);
    }
    
    /// <summary>
    /// Коэффициент уменьшения потерь в ПЭД (1.140, 2.163)
    /// </summary>
    public void Kup()
    {
        parameters.Clear();
        parameters.Add("b5", fp.b5);
        parameters.Add("tdp", fp.tdp);
        parameters.Add("Kt", fp.Kt);
        parameters.Add("Tohl", fp.Tohl);
        p.PInC("1.140, 2.163", parameters);
        fp.Kup = 1.0 - fp.b5 * (1.0 - 0.0077 * (fp.tdp * fp.Kt + fp.Tohl - 293.0));
        p.POutC("1.140, 2.163", "Kup", fp.Kup);
    }
    
    /// <summary>
    /// Сумма потерь мощности в ПЭД при реальной температуре (1.141, 2.164)
    /// </summary>
    public void SN()
    {
        parameters.Clear();
        parameters.Add("Kup", fp.Kup);
        parameters.Add("SNGpot", fp.SNGpot);
        p.PInC("1.141, 2.164", parameters);
        fp.SN = fp.Kup * fp.SNGpot;
        p.POutC("1.141, 2.164", "SN", fp.SN);
    }
    
    /// <summary>
    /// Температура двигателя (K) (1.142, 2.165)
    /// </summary>
    public void Td()
    {
        parameters.Clear();
        parameters.Add("b3", fp.b3);
        parameters.Add("c3", fp.c3);
        parameters.Add("Tohl", fp.Tohl);
        parameters.Add("Kup", fp.Kup);
        parameters.Add("SN", fp.SN);
        p.PInC("1.142, 2.165", parameters);
        fp.Td = fp.Tohl + fp.Kup * (1.0E-3 * fp.b3 * fp.SN - fp.c3);
        p.POutC("1.142, 2.165", "Td", fp.Td);
    }
    
    /// <summary>
    /// Сила толка (А) (1.143, 2.166)
    /// </summary>
    public void I()
    {
        parameters.Clear();
        parameters.Add("In", fp.In);
        parameters.Add("b4", fp.b4);
        parameters.Add("c4", fp.c4);
        parameters.Add("Nh", fp.Nh);
        parameters.Add("Ng", fp.Ng);
        p.PInC("1.143, 2.166", parameters);
        fp.I = fp.In * (fp.b4 * fp.Nh / fp.Ng + fp.c4);
        p.POutC("1.143, 2.166", "I", fp.I);
    }

    /// <summary>
    /// Величина изменения динамического уровня в скважине (м) (1.144, 2.167)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void dhdin(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("1.144, 2.167", parameters);
        fp.dhdin = 4.0 * (fp.Qskv - fp.Qn) * (tz2 - tz1) / 
                   (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)); 
        p.POutC("1.144, 2.167", "dhdin", fp.dhdin);
    }

    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.145, 2.168)
    /// </summary>
    public void hdin1()
    {
        parameters.Clear();
        parameters.Add("hdin", fp.hdin);
        parameters.Add("dhdin", fp.dhdin);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Pzatr", fp.Pzatr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.145, 2.168", parameters);
        fp.hdin = fp.Lcp - (fp.Ppr - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.densityOfOil * fp.g * Math.Cos(fp.alfa)) + fp.dhdin; 
        p.POutC("1.145, 2.168", "hdin", fp.hdin);
    }

    /// <summary>
    /// Давление на затрубе (Па) (1.146, 2.169)
    /// </summary>
    public void Pzatr1()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.146, 2.169", parameters);
        fp.Pzatr = (fp.Ppr - fp.densityOfOil * fp.g * (fp.Lcp - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
        p.POutC("1.146, 2.169", "Pzatr", fp.Pzatr);
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (1.147, 2.170)
    /// </summary>
    public void DPpzo1()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pzost", fp.Pzost);
        p.PInC("1.147, 2.170", parameters);
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
        p.POutC("1.147, 2.170", "DPpzo1", fp.DPpzo1);
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (1.148, 2.171)
    /// </summary>
    public void tzab1()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("rc", fp.rc);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("1.148, 2.171", parameters);
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.oilViscosity)) * fp.rc / 
                   (2.25 * fp.piezoconductivityOfTheFormation); 
        p.POutC("1.148, 2.171", "tzab1", fp.tzab1);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (1.149, 1.150; 2.172, 2.173)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("tzab1", fp.tzab1);
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.149, 1.150; 2.172, 2.173", parameters);
        if (time < fp.tzab1)  fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        else  fp.Pz = fp.Pzost + fp.Qrezh * fp.oilViscosity / (4.0 * fp.pi * fp.k * fp.h) *
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
        p.POutC("1.149, 1.150; 2.172, 2.173", "Pz", fp.Pz);
    }

    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.151, 2.174)
    /// </summary>
    public void htr1()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.151, 2.174", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3) / (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.151, 2.174", "htr", fp.htr);
    }

    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.152, 2.175)
    /// </summary>
    public void htr2()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("htr", fp.htr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.152, 2.175", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3 * Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z))) / 
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.152, 2.175", "htr", fp.htr);
    }

    /// <summary>
    /// Буферное давление, рассчитанное по третьей формуле (Па) (1.153, 2.176)
    /// </summary>
    public void Pbuf3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.153, 2.176", parameters);
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("1.153, 2.176", "Pbuf3", fp.Pbuf3);
    }

    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.154, 2.177)
    /// </summary>
    public void hdin2()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pzatr", fp.Pzatr);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.154, 2.177", parameters);
        fp.hdin = fp.Lk - (fp.Pz - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.154, 2.177", "hdin", fp.hdin);
    }

    /// <summary>
    /// Давление на затрубе (Па) (1.155, 2.178)
    /// </summary>
    public void Pzatr2()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.155, 2.178", parameters);
        fp.Pzatr = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
        p.POutC("1.155, 2.178", "Pzatr", fp.Pzatr);
    }

    /// <summary>
    /// Число Рейнольдса в ЭК (2.4, 2.53)
    /// </summary>
    public void Reek2()
    {
        parameters.Clear();
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        p.PInC("2.4, 2.53", parameters);
        fp.Reek = fp.Vzhek * fp.densityOfKillingFluid * fp.ECInnerDiameter / fp.viscosityOfKillingFluid;
        p.POutC("2.4, 2.53", "Reek", fp.Reek);
    }

    /// <summary>
    /// Расход жидкости, поступающей из пласта  (м3/с) (2.47)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void Qskv2(double Pz, double rc)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("2.47", parameters);
        fp.Qskv = 2.0 * fp.pi * fp.k * fp.h * (fp.reservoirPressurePa - Pz) / 
                  (fp.viscosityOfKillingFluid * Math.Log(fp.Rk / (rc * Math.Exp(-fp.skinFactor))));
        p.POutC("2.47", "Qskv", fp.Qskv);
    }
    
    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта (м3) (2.48)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Vzhgzab(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("Vzhgzab", fp.Vzhgzab);
        p.PInC("2.48", parameters);
        fp.Vzhgzab = fp.Qskv * (tz2 - tz1) + fp.Vzhgzab;
        p.POutC("2.48", "Vzhgzab", fp.Vzhgzab);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (Па) (2.55)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ppr3(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.55", parameters);
        fp.Ppr = Pz - (fp.Lk - fp.Lcp) * (fp.densityOfKillingFluid * fp.g * Math.Cos(fp.alfa) +
                                          fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.densityOfKillingFluid / 
                                          (2.0 * fp.ECInnerDiameter));
        p.POutC("2.55", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Число Рейнольдса в НКТ (2.82, 2.124)
    /// </summary>
    public void Renkt2()
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.82, 2.124", parameters);
        fp.Renkt = fp.Vzhnkt * fp.densityOfKillingFluid * fp.dnkt / fp.viscosityOfKillingFluid;
        p.POutC("2.82, 2.124", "Renkt", fp.Renkt);
    }
    
    /// <summary>
    /// Давление на выходе из насоса (Па) (2.84)
    /// </summary>
    public void Pvyh2()
    {
        parameters.Clear();
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.84", parameters);
        fp.Pvyh = fp.Pbuf3 + fp.Lcp * (fp.densityOfKillingFluid * fp.g * Math.Cos(fp.alfa) +
                                       fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.densityOfKillingFluid / 
                                       (2.0 * fp.dnkt));
        p.POutC("2.84", "Pvyh", fp.Pvyh);
    }

    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.122, 2.158)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void htr3(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.122, 2.158", parameters);
        fp.htr -= 4.0 * fp.Qn * (tz2 - tz1) / (fp.pi * Math.Pow(fp.dnkt, 2)); 
        p.POutC("2.122, 2.158", "htr", fp.htr);
    }
    
    /// <summary>
    /// Напор на преодоление гидравлического трения (м) (2.126)
    /// </summary>
    public void htren()
    {
        parameters.Clear();
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("htr", fp.htr);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("g", fp.g);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.126", parameters);
        fp.htren = fp.LambdaNkt * (fp.Lcp - fp.htr) * Math.Pow(fp.Vzhnkt, 2) / (2.0 * fp.g * fp.dnkt);
        p.POutC("2.126", "htren", fp.htren);
    }
    
    /// <summary>
    /// Общий напор, который необходимо создать УЭЦН (м) (2.127)
    /// </summary>
    public void Hobshu()
    {
        parameters.Clear();
        parameters.Add("hdin", fp.hdin);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("htren", fp.htren);
        p.PInC("2.127", parameters);
        fp.Hobshu = (fp.hdin - fp.htr) * Math.Cos(fp.alfa) + fp.htren;
        p.POutC("2.127", "Hobshu", fp.Hobshu);
    }
    
    /// <summary>
    /// Кинематическая вязкость газожидкостной смеси (м2/с) (2.143)
    /// </summary>
    public void vgzh2()
    {
        parameters.Clear();
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        p.PInC("2.143", parameters);
        fp.vgzh = fp.viscosityOfKillingFluid / fp.densityOfKillingFluid;
        p.POutC("2.143", "vgzh", fp.vgzh);
    }
    
    /// <summary>
    /// Объем жидкости глушения, поступившей на поверхность (м3) (2.157)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Vzhgpov(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("Vzhgpov", fp.Vzhgpov);
        p.PInC("2.157", parameters);
        fp.Vzhgpov = fp.Qn * (tz2 - tz1) + fp.Vzhgpov;
        p.POutC("2.157", "Vzhgpov", fp.Vzhgpov);
    }
    
    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта при остановке скважины (м3) (2.179)
    /// </summary>
    /// <param name="hdin">Динамический уровень в скважине, м</param>
    public void Vost(double hdin)
    {
        parameters.Clear();
        parameters.Add("hdin", hdin);
        parameters.Add("pi", fp.pi);
        parameters.Add("hdin0", fp.hdin0);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("2.179", parameters);
        fp.Vost = fp.pi * (fp.hdin0 - hdin) * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)) / 4.0;
        p.POutC("2.179", "Vost", fp.Vost);
    }
}
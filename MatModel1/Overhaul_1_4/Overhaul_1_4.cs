using System;
using System.Collections.Generic;

public class Overhaul_1_4
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Overhaul_1_4(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
        fp.Hpl = fp.Hpr;
    }
    
    /// <summary>
    /// Плотность жидкости глушения скважины (кг/м3) (9.1) 
    /// </summary>
    public void densityOfKillingFluid()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Kbr", fp.Kbr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc); 
        p.PInC("9.1", parameters);
        fp.densityOfKillingFluid = fp.reservoirPressurePa * (1.0 + fp.Kbr) / (fp.g * fp.Lc);
        p.POutC("9.1", "densityOfKillingFluid", fp.densityOfKillingFluid);
    }
    
    /// <summary>
    /// Глубина скважины по вертикали (м) (9.2) 
    /// </summary>
    public void Lc()
    {
        parameters.Clear();
        parameters.Add("lc", fp.lc);
        parameters.Add("alfa", fp.alfa);
        p.PInC("9.2", parameters);
        fp.Lc = fp.lc * Math.Cos(fp.alfa);
        p.POutC("9.2", "Lc", fp.Lc);
    }

    /// <summary>
    /// Требуемый объем промывочной жидкости (м3) (9.3)
    /// </summary>
    public void Vpzh()
    {
        parameters.Clear();
        parameters.Add("Vek", fp.Vek);
        parameters.Add("Vtnkt", fp.Vtnkt);
        parameters.Add("Kz", fp.Kz);    
        p.PInC("9.3", parameters);
        fp.Vpzh = (fp.Vek - fp.Vtnkt) * (1.0 - fp.Kz);
        p.POutC("9.3", "Vpzh", fp.Vpzh);
    }

    /// <summary>
    /// Объем эксплуатационной колонны (м3) (9.4) 
    /// </summary>
    public void Vek()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("lc", fp.lc);
        p.PInC("9.4", parameters);
        fp.Vek = fp.pi * Math.Pow(fp.ECInnerDiameter, 2) * fp.lc / 4.0;
        p.POutC("9.4", "Vek", fp.Vek);
    }
        
    /// <summary>
    /// Объем жидкости, вытесняемой насосно-компрессорными трубами (м3) (9.5) 
    /// </summary>
    public void Vtnkt()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("9.5", parameters);
        fp.Vtnkt = fp.pi * (Math.Pow(fp.Dnkt, 2) - Math.Pow(fp.dnkt, 2)) * fp.Lcp / 4.0;
        p.POutC("9.5", "Vtnkt", fp.Vtnkt);
    }
    
    /// <summary>
    /// Внутренний объем НКТ (м3) (9.6) 
    /// </summary>
    public void Vnkt()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("9.6", parameters);
        fp.Vnkt = fp.pi * Math.Pow(fp.dnkt, 2) * fp.Lcp / 4.0;
        p.POutC("9.6", "Vnkt", fp.Vnkt);
    }
 
    /// <summary>
    /// Объем продавочной жидкости (м3) (9.7)
    /// </summary>
    public void Vprodzh()
    {
        parameters.Clear();
        parameters.Add("Vbek", fp.Vbek);
        parameters.Add("Vnkt", fp.Vnkt);
        p.PInC("9.7", parameters);
        fp.Vprodzh = fp.Vbek + fp.Vnkt;
        p.POutC("9.7", "Vprodzh", fp.Vprodzh);
    }

    /// <summary>
    /// Объем эксплуатационной колонны под башмаком НКТ (м3) (9.8) 
    /// </summary>
    public void Vbek()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("lc", fp.lc);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("9.8", parameters);
        fp.Vbek = fp.pi * Math.Pow(fp.ECInnerDiameter, 2) * (fp.lc - fp.Lcp) / 4.0;
        p.POutC("9.8", "Vbek", fp.Vbek);
    }
    
    /// <summary>
    /// Требуемый объем цементного раствора (м3) (9.9) 
    /// </summary>
    public void Vcr()
    {
        parameters.Clear();
        parameters.Add("Qc", fp.Qc);
        parameters.Add("tcem", fp.tcem);
        p.PInC("9.9", parameters);
        fp.Vcr = fp.Qc * fp.tcem;
        p.POutC("9.9", "Vcr", fp.Vcr);
    }
    
    /// <summary>
    /// Плотность цементного раствора (кг/м3) (9.10) 
    /// </summary>
    public void Rocr()
    {
        parameters.Clear();
        parameters.Add("mzhc", fp.mzhc);
        parameters.Add("Roc", fp.Roc);
        parameters.Add("Rozhz", fp.Rozhz);
        p.PInC("9.10", parameters);
        fp.Rocr = (1.0 + fp.mzhc) * fp.Roc * fp.Rozhz / (fp.Rozhz + fp.mzhc * fp.Roc);
        p.POutC("9.10", "Rocr", fp.Rocr);
    }

    /// <summary>
    /// Давление на устье (Па) (9.11, 9.40)
    /// </summary>
    /// <param name="Ropzhzhgs">Плотность ПЖ или ЖГС, кг/м3</param>
    public void Pu(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("9.11, 9.40", parameters);
        fp.Pu = fp.reservoirPressurePa - Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("9.11, 9.40", "Pu", fp.Pu);
    }

    /// <summary>
    /// Давление на манифольде (Па) (9.11, 9.40)
    /// </summary>
    /// <param name="Ropzhzhgs">Плотность ПЖ или ЖГС, кг/м3</param>
    public void Pm(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("9.11, 9.40", parameters);
        fp.Pm = fp.reservoirPressurePa - Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("9.11, 9.40", "Pm", fp.Pm);
    }

    /// <summary>
    /// Давление на затрубе (Па) (9.11, 9.40)
    /// </summary>
    /// <param name="Ropzhzhgs">Плотность ПЖ или ЖГС, кг/м3</param>
    public void Pzatr(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("9.11, 9.40", parameters);
        fp.Pzatr = fp.reservoirPressurePa - Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("9.11, 9.40", "Pzatr", fp.Pzatr);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (9.12, 9.41)
    /// </summary>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void Pz(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("Pu", fp.Pu);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("9.12, 9.41", parameters);
        fp.Pz = fp.Pu + Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("9.12, 9.41", "Pz", fp.Pz);
    }
    
    /// <summary>
    /// Потери давления на трение в НКТ ΔРНКТ для ньютоновской жидкости (жидкости глушения/продавочной жидкости)
    /// при течении по трубам лифтовой колонны (НКТ) (Па) (9.13)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void DPnkt(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("9.13", parameters);
        fp.DPnkt = fp.Lambda * 8.0 * Xnkt * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                   (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("9.13", "DPnkt", fp.DPnkt);
    }
    
    /// <summary>
    /// Зависимость изменения положения границы «ЦР/ЖГС – ЖГС/ЦР» (высоты столба ЦР в НКТ) в НКТ
    /// от подачи насоса (q) во времени (м) (9.14)
    /// </summary>
    /// <param name="q">объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Xnkt(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("9.14", parameters);
        fp.Xnkt = 4.0 * q * time / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("9.14", "Xnkt", fp.Xnkt);
    }
    
    /// <summary>
    /// Потери давления на трение в кольцевом пространстве ΔРКП для
    /// жидкости глушения/промывочной жидкости при течении по кольцевому пространству  (Па) (9.15)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void DPkp(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("9.15", parameters);
        fp.DPkp = fp.Lambda * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("9.15", "DPkp", fp.DPkp);
    }
    
    /// <summary>
    /// Потери давления на трение в ЭК ΔРЭК для ПЖС/ЖГС при течении по ЭК (Па) (9.16)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    public void DPek(double q, double Xek)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Xek", Xek);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("9.16", parameters);
        fp.DPek = fp.Lambda * 8.0 * Xek * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter, 5));
        p.POutC("9.16", "DPek", fp.DPek);
    }    
    
    /// <summary>
    /// Зависимость изменения положения границы «ЦР/ЖГС – ЖГС/ЦР» (высоты столба ЦР/ЖГС в ЭК от
    /// башмака НКТ до зумпфа (дна) скважины) в ЭК от подачи насоса (м) (9.17)
    /// </summary>
    /// <param name="q">объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Xek(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("pi", fp.pi);
        p.PInC("9.17", parameters);
        fp.Xek = 4.0 * q * time / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("9.17", "Xek", fp.Xek);
    }
    
    /// <summary>
    /// Зависимость изменения положения границы «ПЖ/ЖГС – ПМЖ/ПДЖ» в КП
    /// от подачи насоса (q) во времени  (м) (9.18)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Xkp(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("9.18", parameters);
        fp.Xkp = 4.0 * fp.q * time / (fp.pi * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)));
        p.POutC("9.18", "Xkp", fp.Xkp);
    }
       
    /// <summary>
    /// Коэффициент потерь на трение по длине (9.19 - 9.20a)
    /// </summary>
    /// <param name="Re">Число Рейнольдса</param>
    public void Lambda(double Re)
    {
        parameters.Clear();
        parameters.Add("Re", Re);
        p.PInC("9.19 - 9.20a", parameters);
        if (Re < 2320.0) fp.Lambda = 64.0 / Re;
        else if (1.0e+5 < Re && Re <= 1.0e+6) fp.Lambda = 0.0032 + 0.221 / Math.Pow(Re, 0.237);
        else fp.Lambda = 0.3164 / Math.Pow(Re, 0.25);
        p.POutC("9.19 - 9.20a", "Lambda", fp.Lambda);
    }
    
    /// <summary>
    /// Число Рейнольдса для труб (НКТ или ЭК) (9.21)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void Retr(double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("dnktek", dnktek);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("pi", fp.pi);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        p.PInC("9.21", parameters);
        fp.Retr = 4.0 * q * fp.densityOfKillingFluid / (fp.pi * dnktek * fp.viscosityOfKillingFluid);
        p.POutC("9.21", "Retr", fp.Retr);
    }
    
    /// <summary>
    /// Число Рейнольдса для кольцевого пространства (между внешним диаметром НКТ и внутренним диаметром ЭК) (9.22)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Rekp(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("9.22", parameters);
        fp.Rekp = 4.0 * q * fp.densityOfKillingFluid / 
                  (fp.pi * (fp.ECInnerDiameter + fp.Dnkt) * fp.viscosityOfKillingFluid);
        p.POutC("9.22", "Rekp", fp.Rekp);
    }
        
    /// <summary>
    /// Потери давления в перфорационных отверстиях при продавливании цементного раствора в призабойную зону пласта (Па) (9.23)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void DPpo(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Rocr", fp.Rocr);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("npo", fp.npo);
        parameters.Add("dpo", fp.dpo);
        parameters.Add("Kdelta", fp.Kdelta);
        p.PInC("9.23", parameters);
        fp.DPpo = 8.0 * Math.Pow(q, 2) * fp.Rocr / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("9.23", "DPpo", fp.DPpo);
    }
    
    /// <summary>
    /// Критическая скорость ЦР в НКТ/ЭК (м/с) (9.24)
    /// </summary>
    public void Wkr()
    {
        parameters.Clear();
        parameters.Add("Rocr", fp.Rocr);
        p.PInC("9.24", parameters);
        fp.Wkr = 25.0 * Math.Sqrt((0.0085 * fp.Rocr - 7.0) / fp.Rocr);
        p.POutC("9.24", "Wkr", fp.Wkr);
    }
    
    /// <summary>
    /// Фактическая средняя скорость ЦР в НКТ/ЭК (м/с) (9.25)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void Wf(double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnktek", dnktek);
        p.PInC("9.25", parameters);
        fp.Wf = 4.0 * q / (fp.pi * Math.Pow(dnktek, 2));
        p.POutC("9.25", "Wf", fp.Wf);
    }
    
    /// <summary>
    /// Потери ЦР на трение в НКТ/ЭК (Па) (9.26)
    /// </summary>
    /// <param name="Xnktek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в НКТ/ЭК от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void DPcrnktek1(double Xnktek, double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("Xnktek", Xnktek);
        parameters.Add("q", q);
        parameters.Add("dnktek", dnktek);
        parameters.Add("Rocr", fp.Rocr);
        parameters.Add("pi", fp.pi);
        p.PInC("9.26", parameters);
        fp.DPcrnktek1 = 4.0 * (0.0085 * fp.Rocr - 7.0) * Xnktek / 
                       ((0.1369 * Math.Log((0.0085 * fp.Rocr - 7.0) * dnktek / 
                       ((0.000033 * fp.Rocr - 0.022) * 4.0 * q / (fp.pi * Math.Pow(dnktek, 2)))) + 0.1356) * dnktek);
        p.POutC("9.26", "DPcrnktek1", fp.DPcrnktek1);
    }
        
    /// <summary>
    /// Потери ЦР на трение в НКТ/ЭК (Па) (9.27)
    /// </summary>
    /// <param name="Xnktek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в НКТ/ЭК от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void DPcrnktek2(double Xnktek, double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("Xnktek", Xnktek);
        parameters.Add("q", q);
        parameters.Add("Rocr", fp.Rocr);
        parameters.Add("dnktek", dnktek);
        parameters.Add("pi", fp.pi);
        p.PInC("9.27", parameters);
        fp.DPcrnktek2 = 0.012 * fp.Rocr * Xnktek * 4.0 * q / (fp.pi * Math.Pow(dnktek, 3));
        p.POutC("9.27", "DPcrnktek2", fp.DPcrnktek2);
    }
  
    /// <summary>
    /// Давление на насосном агрегате на первом временном этапе
    /// (заполнение цементным раствором НКТ от устья до башмака НКТ) на циркуляции (Па) (9.28.1)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    public void Pap11(double q, double Xnkt)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Rocr", fp.Rocr);
        parameters.Add("g", fp.g);
        parameters.Add("DPcrnktek1", fp.DPcrnktek1);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("9.28.1", parameters);
        fp.Pap1 = fp.Pkol + (fp.densityOfKillingFluid - fp.Rocr) * Xnkt * fp.g + fp.DPcrnktek1 +
                  fp.LambdaNkt * 8.0 * (fp.Lcp - Xnkt) * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("9.28.1", "Pap1", fp.Pap1);
    }   
    /// <summary>
    /// Давление на насосном агрегате на первом временном этапе
    /// (заполнение цементным раствором НКТ от устья до башмака НКТ) на циркуляции (Па) (9.28.2)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    public void Pap12(double q, double Xnkt)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Rocr", fp.Rocr);
        parameters.Add("g", fp.g);
        parameters.Add("DPcrnktek1", fp.DPcrnktek1);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("9.28.2", parameters);
        fp.Pap1 = fp.Pkol + (fp.densityOfKillingFluid - fp.Rocr) * Xnkt * fp.g + fp.DPcrnktek2 +
                  fp.LambdaNkt * 8.0 * (fp.Lcp - Xnkt) * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("9.28.2", "Pap1", fp.Pap1);
    }   
    
    
    
}
﻿ using System;
 using System.Collections.Generic;

 public class Overhaul_1_3
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Overhaul_1_3(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    /// <summary>
    /// Плотность жидкости глушения скважины (кг/м3) (10.1) 
    /// </summary>
    public void densityOfKillingFluid()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Kbr", fp.Kbr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("10.1", parameters);
        fp.densityOfKillingFluid = fp.reservoirPressurePa * (1.0 + fp.Kbr) / (fp.g * fp.Lc);
        p.POutC("10.1", "densityOfKillingFluid", fp.densityOfKillingFluid);
    }

    /// <summary>
    /// Глубина скважины по вертикали (м) (10.2) 
    /// </summary>
    public void Lc()
    {
        parameters.Clear();
        parameters.Add("lc", fp.lc);
        parameters.Add("alfa", fp.alfa);
        p.PInC("10.2", parameters);
        fp.Lc = fp.lc * Math.Cos(fp.alfa);
        p.POutC("10.2", "Lc", fp.Lc);
    }

    /// <summary>
    /// Общее требуемое количество жидкости для проведения ГПП (м3) (10.3) 
    /// </summary>
    public void Qrej()
    {
        parameters.Clear();
        parameters.Add("Vc", fp.Vc);
        parameters.Add("Vnkt", fp.Vnkt);
        p.PInC("10.3", parameters);
        fp.Qrej = 2.3 * fp.Vc + 1.0 * fp.Vnkt;
        p.POutC("10.3", "Qrej", fp.Qrej);
    }

    /// <summary>
    /// Объем скважины (м3) (10.4)
    /// </summary>
    public void Vc()
    {
        parameters.Clear();
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("lc", fp.lc);
        parameters.Add("Vtnkt", fp.Vtnkt);
        p.PInC("10.4", parameters);
        fp.Vc = fp.pi * Math.Pow(fp.ECInnerDiameter, 2) / 4.0 * fp.lc - fp.Vtnkt;
        p.POutC("10.4", "Vc", fp.Vc);
    }
    
    /// <summary>
    /// Объем жидкости, вытесняемой насосно-компрессорными трубами (м3) (10.5) 
    /// </summary>
    public void Vtnkt()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("10.5", parameters);
        fp.Vtnkt = fp.pi * (Math.Pow(fp.Dnkt, 2) - Math.Pow(fp.dnkt, 2)) / 4.0 * fp.Lcp;
        p.POutC("10.5", "Vtnkt", fp.Vtnkt);
    }
    
    /// <summary>
    /// Внутренний объем НКТ (м3) (10.6) 
    /// </summary>
    public void Vnkt()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("10.6", parameters);
        fp.Vnkt = fp.pi * Math.Pow(fp.dnkt, 2) * fp.Lcp / 4.0;
        p.POutC("10.6", "Vnkt", fp.Vnkt);
    }

    /// <summary>
    /// Объем эксплуатационной колонны под башмаком НКТ (м3) (10.7) 
    /// </summary>
    public void Vbek()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("lc", fp.lc);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("10.7", parameters);
        fp.Vbek = fp.pi * Math.Pow(fp.ECInnerDiameter, 2) / 4.0 * (fp.lc - fp.Lcp);
        p.POutC("10.7", "Vbek", fp.Vbek);
    }

    /// <summary>
    /// Объем кольцевого пространства скважины (м3) (10.8) 
    /// </summary>
    public void Vkp()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("10.8", parameters);
        fp.Vkp = fp.pi * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.dnkt, 2)) / 4.0 * fp.Lcp;
        p.POutC("10.8", "Vkp", fp.Vkp);
    }

    /// <summary>
    /// Требуемый объем песчано-жидкостной смеси (м3) (10.9)
    /// </summary>
    public void Vpzhs()
    {
        parameters.Clear();
        parameters.Add("Vc", fp.Vc);
        p.PInC("10.9", parameters);
        fp.Vpzhs = 1.3 * fp.Vc;
        p.POutC("10.9", "Vpzhs", fp.Vpzhs);
    }

    /// <summary>
    /// Требуемый объем промывочной жидкости (м3) (10.10)
    /// </summary>
    public void Vpzh()
    {
        parameters.Clear();
        parameters.Add("Vc", fp.Vc);
        p.PInC("10.10", parameters);
        fp.Vpzh = 1.0 * fp.Vc;
        p.POutC("10.10", "Vpzh", fp.Vpzh);
    }

    /// <summary>
    /// Требуемый объем продавочной жидкости (м3) (10.11)
    /// </summary>
    public void Vprodzh()
    {
        parameters.Clear();
        parameters.Add("Vnkt", fp.Vnkt);
        p.PInC("10.11", parameters);
        fp.Vprodzh = fp.Vnkt;
        p.POutC("10.11", "Vprodzh", fp.Vprodzh);
    }

    /// <summary>
    /// Требуемое количество кварцевого песка (кг) (10.12)
    /// </summary>
    public void Qp()
    {
        parameters.Clear();
        parameters.Add("Vc", fp.Vc);
        parameters.Add("Co", fp.Co);
        p.PInC("10.12", parameters);
        fp.Qp = 1.3 * fp.Vc * fp.Co;
        p.POutC("10.12", "Qp", fp.Qp);
    }

    /// <summary>
    /// Плотность песчано-жидкостной смеси (кг/м3) (10.13)
    /// </summary>
    public void RoPzhs()
    {
        parameters.Clear();
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("densityOfSand", fp.densityOfSand);
        parameters.Add("Cp", fp.Cp);
        p.PInC("10.13", parameters);
        fp.RoPzhs = fp.densityOfWater * (1.0 - fp.Cp) + fp.densityOfSand * fp.Cp;
        p.POutC("10.13", "RoPzhs", fp.RoPzhs);
    }

    /// <summary>
    /// Объемная доля песка (10.14)
    /// </summary>
    public void Cp()
    {
        parameters.Clear();
        parameters.Add("Co", fp.Co);
        parameters.Add("densityOfSand", fp.densityOfSand);
        p.PInC("10.14", parameters);
        fp.Cp = fp.Co / (fp.Co - fp.densityOfSand);
        p.POutC("10.14", "Cp", fp.Cp);
    }

    /// <summary>
    /// Необходимый темп закачки жидкости (расход) (м3/с) (10.15)
    /// </summary>
    public void Qzakzh()
    {
        parameters.Clear();
        parameters.Add("z", fp.z);
        parameters.Add("Nperf", fp.Nperf);
        parameters.Add("pi", fp.pi);
        parameters.Add("Dperf", fp.Dperf);
        parameters.Add("DPn", fp.DPn);
        parameters.Add("RoPzhs", fp.RoPzhs);
        p.PInC("10.15", parameters);
        fp.Qzakzh = 1.414 * fp.z * fp.Nperf * fp.pi * fp.Dperf / 4.0 * Math.Sqrt(fp.DPn / fp.RoPzhs);
        p.POutC("10.15", "Qzakzh", fp.Qzakzh);
    }

    /// <summary>
    /// Требуемое количество насосных агрегатов (шт) (10.16)
    /// </summary>
    public void Nagr(int speedNumber)
    {
        parameters.Clear();
        parameters.Add("speedNumber", speedNumber);
        parameters.Add("Qzakzh", fp.Qzakzh);
        parameters.Add("An500Q1", fp.An500Q1);
        parameters.Add("An500Q2", fp.An500Q2);
        parameters.Add("An500Q3", fp.An500Q3);
        parameters.Add("An500Q4", fp.An500Q4);
        parameters.Add("An500Q5", fp.An500Q5);
        parameters.Add("An500Q6", fp.An500Q6);
        p.PInC("10.16", parameters);
        switch (speedNumber)
        {
            case 1:
                fp.Nagr = Convert.ToInt32(Math.Floor(fp.Qzakzh / fp.An500Q1) + 1);
                break;
            case 2:
                fp.Nagr = Convert.ToInt32(Math.Floor(fp.Qzakzh / fp.An500Q2) + 1);
                break;
            case 3:
                fp.Nagr = Convert.ToInt32(Math.Floor(fp.Qzakzh / fp.An500Q3) + 1);
                break;
            case 4:
                fp.Nagr = Convert.ToInt32(Math.Floor(fp.Qzakzh / fp.An500Q4) + 1);
                break;
            case 5:
                fp.Nagr = Convert.ToInt32(Math.Floor(fp.Qzakzh / fp.An500Q5) + 1);
                break;
            case 6:
                fp.Nagr = Convert.ToInt32(Math.Floor(fp.Qzakzh / fp.An500Q6) + 1);
                break;
            default:
                fp.Nagr = 0;
                break;
        }
        p.POutC("10.16", "Nagr", fp.Nagr);
    }

    /// <summary>
    /// Давление на устье (Па) (10.17, 10.42)
    /// </summary>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void Pu(double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("10.17, 10.42", parameters);
        fp.Pu = fp.reservoirPressurePa - Ropzhszhgs * fp.g * fp.Lc;
        p.POutC("10.17, 10.42", "Pu", fp.Pu);
    }

    /// <summary>
    /// Давление на манифольде (Па) (10.17, 10.42)
    /// </summary>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void Pm(double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("10.17, 10.42", parameters);
        fp.Pm = fp.reservoirPressurePa - Ropzhszhgs * fp.g * fp.Lc;
        p.POutC("10.17, 10.42", "Pm", fp.Pm);
    }

    /// <summary>
    /// Давление на затрубе (Па) (10.17, 10.42)
    /// </summary>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void Pzatr(double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("10.17, 10.42", parameters);
        fp.Pzatr = fp.reservoirPressurePa - Ropzhszhgs * fp.g * fp.Lc;
        p.POutC("10.17, 10.42", "Pzatr", fp.Pzatr);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (10.18, 10.43)
    /// </summary>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void Pz(double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("Pu", fp.Pu);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("10.18, 10.43", parameters);
        fp.Pz = fp.Pu + Ropzhszhgs * fp.g * fp.Lc;
        p.POutC("10.18, 10.43", "Pz", fp.Pz);
    }

    /// <summary>
    /// Потери давления на трение в НКТ ΔРНКТ для ньютоновской жидкости (жидкости глушения/песчано-жидкостной смеси)
    /// при течении по трубам лифтовой колонны (НКТ)  (Па) (10.19)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void DPnkt(double Xnkt, double q, double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("10.19", parameters);
        fp.DPnkt = fp.Lambda * 8.0 * Xnkt * Math.Pow(q, 2) * Ropzhszhgs /
                    (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("10.19", "DPnkt", fp.DPnkt);
    }

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q)
    /// во времени (м) (10.20)
    /// </summary>
    /// <param name="q">объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Xnkt(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("10.20", parameters);
        fp.Xnkt = 4.0 * q * time / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("10.20", "Xnkt", fp.Xnkt);
    }

    /// <summary>
    /// Потери давления на трение в кольцевом пространстве ΔРКП для ПЖС/промывочной жидкости
    /// при течении по кольцевому пространству  (Па) (10.21)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void DPkp(double q, double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("10.21", parameters);
        fp.DPkp = fp.Lambda * 8.0 * fp.Lcp * Math.Pow(q, 2) * Ropzhszhgs /
                    (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                     Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("10.21", "DPkp", fp.DPkp);
    }

    /// <summary>
    /// Потери давления на трение в ЭК ΔРЭК для ПЖС/ЖГС при течении по ЭК (Па) (10.22)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void DPek(double q, double Xek, double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Xek", Xek);
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("10.22", parameters);
        fp.DPek = fp.Lambda * 8.0 * Xek * Math.Pow(q, 2) * Ropzhszhgs /
                    (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter, 5));
        p.POutC("10.22", "DPek", fp.DPek);
    }

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна)
    /// скважины) в ЭК от подачи насоса (q) во времени (м) (10.23)
    /// </summary>
    /// <param name="q">объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Xek(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("10.23", parameters);
        fp.Xek = 4.0 * q * time / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("10.23", "Xek", fp.Xek);
    }

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в КП от подачи насоса (q) во времени (м) (10.24)
    /// </summary>
    /// <param name="q">объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Xkp(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("10.24", parameters);
        fp.Xkp = 4.0 * q * time / (fp.pi * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)));
        p.POutC("10.24", "Xkp", fp.Xkp);
    }

    /// <summary>
    /// Число Рейнольдса для труб (НКТ или ЭК) (10.28)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void Retr(double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("dnktek", dnktek);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("pi", fp.pi);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        p.PInC("10.28", parameters);
        fp.Retr = 4.0 * q * fp.densityOfKillingFluid / (fp.pi * dnktek * fp.viscosityOfKillingFluid);
        p.POutC("10.28", "Retr", fp.Retr);
    }

    /// <summary>
    /// Коэффициент потерь на трение по длине (10.25 - 10.27)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void Lambda(double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("dnktek", dnktek);
        parameters.Add("Retr", fp.Retr);
        p.PInC("10.25 - 10.27", parameters);
        var re = fp.Retr;
        if (re < 2320.0) fp.Lambda = 64.0 / re;
        else if (1.0e+5 < re && re <= 1.0e+6) fp.Lambda = 0.0032 + 0.221 / Math.Pow(re, 0.237);
        else fp.Lambda = 0.3164 / Math.Pow(re, 0.25);
        p.POutC("10.25 - 10.27", "Lambda", fp.Lambda);
    }

    /// <summary>
    /// Число Рейнольдса для кольцевого пространства (между внешним диаметром НКТ и внутренним диаметром ЭК) (10.29)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="Mjupzhszhgs">Вязкость ПЖС или ЖГС, мПа•с</param>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void Rekp(double q, double Mjupzhszhgs, double Ropzhszhgs)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Mjupzhszhgs", Mjupzhszhgs);
        parameters.Add("Ropzhszhgs", Ropzhszhgs);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("10.29", parameters);
        fp.Rekp = 4.0 * q * Ropzhszhgs / (fp.pi * (fp.ECInnerDiameter + fp.Dnkt) * Mjupzhszhgs);
        p.POutC("10.29", "Rekp", fp.Rekp);
    }

    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (Па) (10.30)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pao1(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("10.30", parameters);
        fp.Pao1 = fp.Pkol + fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.Ropzh /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 *
                  Math.Pow(q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("10.30", "Pao1", fp.Pao1);
    }

    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ (Па) (10.31)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzo1(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pao1", fp.Pao1);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("g", fp.g);
        parameters.Add("Xkp", fp.Xkp);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("10.31", parameters);
        fp.Pzo1 = fp.Pao1 + fp.Ropzh * fp.g * fp.Xkp + fp.densityOfKillingFluid * fp.g * (fp.Lcp - fp.Xkp) -
                  fp.LambdaKp * 8.0 * fp.Xkp * q * fp.Ropzh /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) -
                  fp.LambdaKp * 8.0 * (fp.Lcp - fp.Xkp) * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("10.31", "Pzo1", fp.Pzo1);
    }

    /// <summary>
    /// Давление на насосном агрегате при осуществлени прямой промывки (Па) (10.32)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap2(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("Xnkt", fp.Xnkt);
        parameters.Add("g", fp.g);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("10.32", parameters);
        fp.Pap2 = fp.Pkol + (fp.densityOfKillingFluid - fp.RoPzhs) * fp.Xnkt * fp.g + 
                  fp.LambdaNkt * 8.0 * fp.Xnkt * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 
                  fp.LambdaNkt * 8.0 * (fp.Lcp - fp.Xnkt) * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(q, 2) * 1000.0 / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("10.32", "Pap2", fp.Pap2);
    }

    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖС НКТ от устья до башмака НКТ (до перфоратора) (Па) (10.33)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp2(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pap2", fp.Pap2);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("g", fp.g);
        parameters.Add("Xnkt", fp.Xnkt);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("10.33", parameters);
        fp.Pzp2 = fp.Pap2 + fp.RoPzhs * fp.g * fp.Xnkt + fp.densityOfKillingFluid * fp.g * (fp.Lcp - fp.Xnkt) -
                   fp.LambdaNkt * 8.0 * fp.Xnkt * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) -
                   fp.LambdaNkt * 8.0 * (fp.Lcp - fp.Xnkt) * Math.Pow(q, 2) * fp.densityOfKillingFluid /
                   (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("10.33", "Pzp2", fp.Pzp2);
    }

    /// <summary>
    /// Давление на насосном агрегате при продавливании  ПЖС через перфоратор на циркуляции (Па) (10.34)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap3(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("Xkp", fp.Xkp);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("DPn", fp.DPn);
        parameters.Add("DPp", fp.DPp);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("10.34", parameters);
        fp.Pap3 = fp.Pkol - fp.RoPzhs * fp.Lcp * fp.g + fp.RoPzhs * fp.Xkp * fp.g + 
                  fp.densityOfKillingFluid * (fp.Lcp - fp.Xkp) * fp.g + 
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  fp.LambdaKp * 8.0 * fp.Xkp * Math.Pow(q, 2) * fp.RoPzhs /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) + 
                  fp.LambdaKp * 8.0 * (fp.Lcp - fp.Xkp) * Math.Pow(q, 2) * fp.RoPzhs / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.DPn + fp.DPp + 4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(q, 2) * 1000.0 / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("10.34", "Pap3", fp.Pap3);
    }

    /// <summary>
    /// Забойное давление в скважине при ГПП (формировании перфорационных каналов) (Па) (10.35)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp3(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pap3", fp.Pap3);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("10.35", parameters);
        fp.Pzp3 = fp.Pap3 + fp.RoPzhs * fp.g * fp.Lcp - 
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("10.35", "Pzp3", fp.Pzp3);
    }

    /// <summary>
    /// Давление на насосном агрегате при продавливании ПЖС, находящейся в НКТ, продавочной жидкостью (Па) (10.36)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap4(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("Xnkt", fp.Xnkt);
        parameters.Add("g", fp.g);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("DPn", fp.DPn);
        parameters.Add("DPp", fp.DPp);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("10.36", parameters);
        fp.Pap4 = fp.Pkol + (fp.RoPzhs - fp.Ropzh) * fp.Xnkt * fp.g + 
                  fp.LambdaNkt * 8.0 * fp.Xnkt * Math.Pow(q, 2) * fp.Ropzh / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 
                  fp.LambdaNkt * 8.0 * (fp.Lcp - fp.Xnkt) * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.RoPzhs /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.DPn + fp.DPp + 4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(q, 2) * 1000.0 / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("10.36", "Pap4", fp.Pap4);
    }

    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖ НКТ от устья до башмака НКТ (до перфоратора) (Па) (10.37)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp4(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pap4", fp.Pap4);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("g", fp.g);
        parameters.Add("Xnkt", fp.Xnkt);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("10.37", parameters);
        fp.Pzp4 = fp.Pap4 + fp.Ropzh * fp.g * fp.Xnkt + fp.RoPzhs * fp.g * (fp.Lcp - fp.Xnkt) - 
                  fp.LambdaNkt * 8.0 * fp.Xnkt * Math.Pow(q, 2) * fp.Ropzh / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) -
                  fp.LambdaNkt * 8.0 * (fp.Lcp - fp.Xnkt) * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("10.37", "Pzp4", fp.Pzp4);
    }

    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из кольцевого пространства) (Па) (10.38)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pao5(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("Xkp", fp.Xkp);
        parameters.Add("g", fp.g);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Xnkt", fp.Xnkt);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("10.38", parameters);
        fp.Pao5 = fp.Pkol - fp.Ropzh * fp.Xkp * fp.g - fp.RoPzhs * (fp.Lcp - fp.Xkp) * fp.g + fp.RoPzhs * fp.Xnkt * fp.g +
                  fp.Ropzh * (fp.Lcp - fp.Xnkt) * fp.g + fp.LambdaKp * 8.0 * fp.Xkp * Math.Pow(q, 2) * fp.Ropzh /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.LambdaKp * 8.0 * (fp.Lcp - fp.Xkp) * Math.Pow(q, 2) * fp.RoPzhs /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) + 
                  fp.LambdaNkt * 8.0 * fp.Xnkt * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  fp.LambdaNkt * 8.0 * (fp.Lcp - fp.Xkp) * Math.Pow(q, 2) * fp.Ropzh / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(q, 2) * 1000.0 / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("10.38", "Pao5", fp.Pao5);
    }

    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ (Па) (10.39)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzo5(double q)
    {
        parameters.Clear();
        parameters.Add("Pao5", fp.Pao5);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("Xkp", fp.Xkp);
        parameters.Add("g", fp.g);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("10.39", parameters);
        fp.Pzo5 = fp.Pao5 + fp.Ropzh * fp.Xkp * fp.g + fp.RoPzhs * (fp.Lcp - fp.Xkp) * fp.g - 
                  fp.LambdaKp * 8.0 * fp.Xkp * Math.Pow(q, 2) * fp.Ropzh /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) -
                  fp.LambdaKp * 8.0 * (fp.Lcp - fp.Xkp) * Math.Pow(q, 2) * fp.RoPzhs /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("10.39", "Pzo5", fp.Pzo5);
    }

    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из НКТ) (Па) (10.40)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pao6(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("Xnkt", fp.Xnkt);
        parameters.Add("RoPzhs", fp.RoPzhs);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Xkp", fp.Xkp);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("10.40", parameters);
        fp.Pao6 = fp.Pkol - fp.Ropzh * fp.Lcp * fp.g + fp.Ropzh * fp.Xnkt * fp.g + fp.RoPzhs * (fp.Lcp - fp.Xnkt) * fp.g + 
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.Ropzh /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.LambdaNkt * 8.0 * fp.Xnkt * Math.Pow(q, 2) * fp.Ropzh / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 
                  fp.LambdaNkt * 8.0 * (fp.Lcp - fp.Xkp) * Math.Pow(q, 2) * fp.RoPzhs / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(q, 2) * 1000.0 / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("10.40", "Pao6", fp.Pao6);
    }

    /// <summary>
    /// Забойное давление в скважине при заполненном КП промывочной жидкостью (Па) (10.41)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzo6(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pao6", fp.Pao6);
        parameters.Add("Ropzh", fp.Ropzh);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("10.41", parameters);
        fp.Pzo6 = fp.Pao6 + fp.Ropzh * fp.Lcp * fp.g - fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.Ropzh /
                   (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("10.41", "Pzo6", fp.Pzo6);
    }

    /// <summary>
    /// Накопленный объем закачиваемой в скважину ПЖС/ПЖ, м3 (10.44)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Vnakpzh(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        p.PInC("10.44", parameters);
        fp.Vnakpzh += q * time;
        p.POutC("10.44", "Vnakpzh", fp.Vnakpzh);
    }
}
public class Overhaul_1_3_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;
    public bool PNPT = false;

    public Overhaul_1_3_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    public void Diagram(double time)
    {
        fp.o13.densityOfKillingFluid(); // 10.1
        fp.o13.Vpzhs(); // 10.9
        fp.o13.Vpzh(); // 10.10
        fp.o13.Vprodzh(); // 10.11
        fp.o13.Qp(); // 10.12
        fp.o13.RoPzhs(); // 10.13
        fp.o13.Qzakzh(); // 10.15
        fp.o13.Nagr(fp.speedNumber); // 10.16
        fp.o13.Pao1(fp.q); // 10.30
        fp.o13.Pzo1(fp.q); // 10.31
        fp.o13.Xnkt(fp.q, time); // 10.20
        fp.o13.Xkp(fp.q, time); // 10.24
        fp.o13.Pap2(fp.q); // 10.32
        fp.o13.Pzp2(fp.q); // 10.33
        fp.o13.Xnkt(fp.q, time); // 10.20
        fp.o13.Xkp(fp.q, time); // 10.24
        fp.o13.Pap3(fp.q); // 10.34
        fp.o13.Pzp3(fp.q); // 10.35
        fp.o13.Qzakzh(); // 10.15
        fp.o13.Xkp(fp.q, time); // 10.24
        fp.o13.Pap4(fp.q); // 10.36
        fp.o13.Pzp4(fp.q); // 10.37
        fp.o13.Xnkt(fp.q, time); // 10.20
        fp.o13.Xkp(fp.q, time); // 10.24
        fp.o13.Pao5(fp.q); // 10.38
        fp.o13.Pzo5(fp.q); // 10.39
        fp.o13.Xnkt(fp.q, time); // 10.20
        fp.o13.Xkp(fp.q, time); // 10.24
        fp.o13.Pao6(fp.q); // 10.40
        fp.o13.Pzo6(fp.q); // 10.41
        fp.o13.Xnkt(fp.q, time); // 10.20
        fp.o13.Pu(fp.Ropzh); // 10.42
        fp.o13.Pm(fp.Ropzh); // 10.42
        fp.o13.Pzatr(fp.Ropzh); // 10.42
        fp.o13.Pz(fp.Ropzh); // 10.43
    }
}
using System;
using System.Collections.Generic;

public class Overhaul_1_1
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Overhaul_1_1(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
    
    /// <summary>
    /// Плотность жидкости глушения скважины (кг/м3) (3.1) 
    /// </summary>
    public void densityOfKillingFluid()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Kbr", fp.Kbr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc); 
        p.PInC("3.1", parameters);
        fp.densityOfKillingFluid = fp.reservoirPressurePa * (1.0 + fp.Kbr) / (fp.g * fp.Lc);
        p.POutC("3.1", "densityOfKillingFluid", fp.densityOfKillingFluid);
    }

    /// <summary>
    /// Глубина скважины по вертикали (м) (3.2) 
    /// </summary>
    public void Lc()
    {
        parameters.Clear();
        parameters.Add("lc", fp.lc);
        parameters.Add("alfa", fp.alfa);
        p.PInC("3.2", parameters);
        fp.Lc = fp.lc * Math.Cos(fp.alfa);
        p.POutC("3.2", "Lc", fp.Lc);
    }

    /// <summary>
    /// Объем жидкости глушения, поступивший из насоса (м3) (3.3) 
    /// </summary>
    public void Vzhgn()
    {
        parameters.Clear();
        parameters.Add("Vek", fp.Vek);
        parameters.Add("Vtnkt", fp.Vtnkt);
        parameters.Add("Vsh", fp.Vsh);
        parameters.Add("Kz", fp.Kz);
        p.PInC("3.3", parameters);
        fp.Vzhgn = (fp.Vek - fp.Vtnkt - fp.Vsh) * (1.0 + fp.Kz);
        p.POutC("3.3", "Vzhgn", fp.Vzhgn);
    }

    /// <summary>
    /// Объем эксплуатационной колонны (м3) (3.4) 
    /// </summary>
    public void Vek()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("lc", fp.lc);
        p.PInC("3.4", parameters);
        fp.Vek = fp.pi * Math.Pow(fp.ECInnerDiameter, 2) * fp.lc / 4.0;
        p.POutC("3.4", "Vek", fp.Vek);
    }
    
    /// <summary>
    /// Объем жидкости, вытесняемой насосно-компрессорными трубами (м3) (3.5) 
    /// </summary>
    public void Vtnkt()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("3.5", parameters);
        fp.Vtnkt = fp.pi * (Math.Pow(fp.Dnkt, 2) - Math.Pow(fp.dnkt, 2)) * fp.Lcp / 4.0;
        p.POutC("3.5", "Vtnkt", fp.Vtnkt);
    }
    
    /// <summary>
    /// Объем, вытесняемый металлом штанг (при глушении скважин, оборудованных ШГН) (м3) (3.6) 
    /// </summary>
    public void Vsh()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("dsh", fp.dsh);
        parameters.Add("Lsh", fp.Lsh);
        p.PInC("3.6", parameters);
        fp.Vsh = fp.pi * Math.Pow(fp.dsh, 2) * fp.Lsh / 4.0;
        p.POutC("3.6", "Vsh", fp.Vsh);
    }
    
    /// <summary>
    /// Давление на устье (Па) (3.7, 3.32)
    /// </summary>
    /// <param name="Ronzhgs">Плотность нефти или ЖГС, кг/м3</param>
    public void Pu(double Ronzhgs)
    {
        parameters.Clear();
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("3.7, 3.32", parameters);
        fp.Pu = fp.reservoirPressurePa - Ronzhgs * fp.g * fp.Lc;
        p.POutC("3.7, 3.32", "Pu", fp.Pu);
    }

    /// <summary>
    /// Давление на манифольде (Па) (3.7, 3.32)
    /// </summary>
    /// <param name="Ronzhgs">Плотность нефти или ЖГС, кг/м3</param>
    public void Pm(double Ronzhgs)
    {
        parameters.Clear();
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("3.7, 3.32", parameters);
        fp.Pm = fp.reservoirPressurePa - Ronzhgs * fp.g * fp.Lc;
        p.POutC("3.7, 3.32", "Pm", fp.Pm);
    }

    /// <summary>
    /// Давление на затрубе (Па) (3.7, 3.32)
    /// </summary>
    /// <param name="Ronzhgs">Плотность нефти или ЖГС, кг/м3</param>
    public void Pzatr(double Ronzhgs)
    {
        parameters.Clear();
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("3.7, 3.32", parameters);
        fp.Pzatr = fp.reservoirPressurePa - Ronzhgs * fp.g * fp.Lc;
        p.POutC("3.7, 3.32", "Pzatr", fp.Pzatr);
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (3.8)
    /// </summary>
    public void Pz1()
    {
        parameters.Clear();
        parameters.Add("Pu", fp.Pu);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("3.8", parameters);
        fp.Pz = fp.Pu + fp.densityOfOil * fp.g * fp.Lc;
        p.POutC("3.8", "Pz", fp.Pz);
    }
        
    /// <summary>
    /// Пластовое давление (Па) (3.8)
    /// </summary>
    public void reservoirPressurePa()
    {
        parameters.Clear();
        parameters.Add("Pu", fp.Pu);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("3.8", parameters);
        fp.reservoirPressurePa = fp.Pu + fp.densityOfOil * fp.g * fp.Lc;
        p.POutC("3.8", "reservoirPressurePa", fp.reservoirPressurePa);
    }

    /// <summary>
    /// Потери давления на трение в НКТ ΔРНКТ для ньютоновской жидкости при течении по трубам лифтовой колонны (НКТ)  (Па) (3.9)
    /// </summary>
    public void DPnkt()
    {
        parameters.Clear();
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("q", fp.q);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("3.9", parameters);
        fp.DPnkt = fp.Lambda * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.Rozh /
                   (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("3.9", "DPnkt", fp.DPnkt);
    }    
    
    /// <summary>
    /// Потери давления на трение в кольцевом пространстве ΔРКП для ньютоновской жидкости при течении по кольцевому пространству  (Па) (3.10)
    /// </summary>
    public void DPkp()
    {
        parameters.Clear();
        parameters.Add("q", fp.q);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("3.10", parameters);
        fp.DPkp = fp.Lambda * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.Rozh /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("3.10", "DPkp", fp.DPkp);
    }
    
    /// <summary>
    /// Потери давления на трение в ЭК ΔРЭК для ПЖС/ЖГС при течении по ЭК (Па) (3.11)
    /// </summary>
    public void DPek()
    {
        parameters.Clear();
        parameters.Add("q", fp.q);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("3.11", parameters);
        fp.DPek = fp.Lambda * 8.0 * (fp.Lc - fp.Lcp) * Math.Pow(fp.q, 2) * fp.Rozh /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter, 5));
        p.POutC("3.11", "DPek", fp.DPek);
    }
   
    /// <summary>
    /// Коэффициент потерь на трение по длине (3.12 - 3.14)
    /// </summary>
    /// <param name="Re">Число Рейнольдса</param>
    public void Lambda(double Re)
    {
        parameters.Clear();
        parameters.Add("Re", Re);
        p.PInC("3.12 - 3.14", parameters);
        if (Re < 2320.0) fp.Lambda = 64.0 / Re;
        else if (1.0e+5 < Re && Re <= 1.0e+6) fp.Lambda = 0.0032 + 0.221 / Math.Pow(Re, 0.237);
        else fp.Lambda = 0.3164 / Math.Pow(Re, 0.25);
        p.POutC("3.12 - 3.14", "Lambda", fp.Lambda);
    } 

    /// <summary>
    /// Число Рейнольдса для труб (НКТ или ЭК) (3.15)
    /// </summary>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    /// <param name="Ronzhgs">Плотность жидкости (нефти (ρн) или водного раствора ЖГС (ρЖГС)), кг/м3</param>
    /// <param name="Myunzhgs">Динамическая вязкость жидкости (нефти (µн) или водного раствора ЖГС (µЖГС))), Па•с</param>
    public void Retr(double dnktek, double Ronzhgs, double Myunzhgs)
    {
        parameters.Clear();
        parameters.Add("dnktek", dnktek);
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("Myunzhgs", Myunzhgs);
        p.PInC("3.15", parameters);
        fp.Retr = 4.0 * fp.q * Ronzhgs / (fp.pi * dnktek * Myunzhgs);
        p.POutC("3.15", "Retr", fp.Retr);
    }

    /// <summary>
    /// Число Рейнольдса для кольцевого пространства (между внешним диаметром НКТ и внутренним диаметром ЭК) (3.16)
    /// </summary>
    /// <param name="Ronzhgs">Плотность жидкости (нефти (ρн) или водного раствора ЖГС (ρЖГС)), кг/м3</param>
    /// <param name="Myunzhgs">Динамическая вязкость жидкости (нефти (µн) или водного раствора ЖГС (µЖГС))), Па•с</param>
    public void Rekp(double Ronzhgs, double Myunzhgs)
    {
        parameters.Clear();
        parameters.Add("q", fp.q);
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("Myunzhgs", Myunzhgs);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("3.16", parameters);
        fp.Rekp = 4.0 * fp.q * Ronzhgs / (fp.pi * (fp.ECInnerDiameter + fp.Dnkt) * Myunzhgs);
        p.POutC("3.16", "Rekp", fp.Rekp);
    }    
    
    /// <summary>
    /// Зависимость изменения положения границы «ЖГС – нефть в НКТ» (высоты столба ЖГС в НКТ) в НКТ от подачи насоса  (q)
    /// во времени (м) (3.17)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Xnkt(double time)
    {
        parameters.Clear();
        parameters.Add("q", fp.q);
        parameters.Add("time", time);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("3.17", parameters);
        fp.Xnkt = 4.0 * fp.q * time / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("3.17", "Xnkt", fp.Xnkt);
    }
    
    /// <summary>
    /// Зависимость изменения положения границы «ЖГС – нефть в ЭК» (высоты столба ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК
    /// от подачи насоса (q) во времени  (м) (3.18)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Xek(double time)
    {
        parameters.Clear();
        parameters.Add("q", fp.q);
        parameters.Add("time", time);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("3.18", parameters);
        fp.Xek = 4.0 * fp.q * time / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("3.18", "Xek", fp.Xek);
    }
    
    /// <summary>
    /// Зависимость изменения положения границы «ЖГС – нефть в КП» (высоты столба ЖГС в КП)
    /// в кольцевом пространстве от подачи насоса (q) во времени  (м) (3.19)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Xkp(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("3.19", parameters);
        fp.Xkp = 4.0 * fp.q * time / (fp.pi * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)));
        p.POutC("3.19", "Xkp", fp.Xkp);
    }

    /// <summary>
    /// Давление на насосном агрегате на первом временном этапе
    /// (заполнение ЖГС НКТ от устья до башмака НКТ) (Па) (3.20)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    public void Pap1(double Xnkt)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("3.20", parameters);
        fp.Pap1 = fp.Pkol + (fp.densityOfOil - fp.densityOfKillingFluid) * Xnkt * fp.g +
                  fp.LambdaNkt * 8.0 * Xnkt * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  fp.LambdaNkt * 8.0 * (fp.Lcp - Xnkt) * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(fp.q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("3.20", "Pap1", fp.Pap1);
    }    

    /// <summary>
    /// Давление на насосном агрегате на втором временном этапе
    /// (продавливание ЖР в ЭК жидкостью разрыва от башмака НКТ до зумпфа (дна) скважины) (Па) (3.21)
    /// </summary>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    public void Pap2(double Xek)
    {
        parameters.Clear();
        parameters.Add("Xek", Xek);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("3.21", parameters);
        fp.Pap2 = fp.Pkol + (fp.densityOfOil - fp.densityOfKillingFluid) * fp.Lcp * fp.g -
                  fp.densityOfKillingFluid * Xek * fp.g - fp.densityOfOil * (fp.Lc - fp.Lcp - Xek) * fp.g +
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + fp.LambdaEk * 8.0 * Xek * Math.Pow(fp.q, 2) * 
                  fp.densityOfKillingFluid / (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter, 5)) + 
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) + 4.6E5 * Math.Pow(fp.Fi, -1.5) * 
                  8.0 * Math.Pow(fp.q, 2) * 1000.0 / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("3.21", "Pap2", fp.Pap2);
    }    

    /// <summary>
    /// Давление на насосном агрегате на третьем временном этапе
    /// (продавливание ЖР через отверстия перфорации в пласт жидкостью разрыва) (Па) (3.22)
    /// </summary>
    /// <param name="Xkp">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в КП от подачи насоса (q) во времени, м</param>
    public void Pap3(double Xkp)
    {
        parameters.Clear();
        parameters.Add("Xkp", Xkp);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("3.22", parameters);
        fp.Pap3 = fp.Pkol - fp.densityOfKillingFluid * fp.Lcp * fp.g + fp.densityOfKillingFluid * Xkp * fp.g + 
                  fp.densityOfOil * (fp.Lcp - Xkp) * fp.g + 
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 
                  fp.LambdaKp * 8.0 * Xkp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) + 
                  fp.LambdaKp * 8.0 * (fp.Lcp - Xkp) * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(fp.q, 2) * 1000.0 / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("3.22", "Pap3", fp.Pap3);
    }     
    
    /// <summary>
    /// Давление на насосном агрегате на первом временном этапе при осуществлени обратной промывки (Па) (3.23)
    /// </summary>
    /// <param name="Xkp">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в КП от подачи насоса (q) во времени, м</param>
    public void Pao1(double Xkp)
    {
        parameters.Clear();
        parameters.Add("Xkp", Xkp);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("3.23", parameters);
        fp.Pao1 = fp.Pkol + (fp.densityOfOil - fp.densityOfKillingFluid) * Xkp * fp.g +
                  fp.LambdaKp * 8.0 * Xkp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) + 
                  fp.LambdaKp * 8.0 * (fp.Lcp - Xkp) * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(fp.q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("3.23", "Pao1", fp.Pao1);
    }
        
    /// <summary>
    /// Давление на насосном агрегате на втором временном этапе при осуществлени обратной промывки (Па) (3.24)
    /// </summary>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    public void Pao2(double Xek)
    {
        parameters.Clear();
        parameters.Add("Xek", Xek);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("3.24", parameters);
        fp.Pao2 = fp.Pkol + (fp.densityOfOil - fp.densityOfKillingFluid) * fp.Lcp * fp.g - 
                  fp.densityOfKillingFluid * Xek * fp.g - fp.densityOfOil * (fp.Lc - fp.Lcp - Xek) * fp.g + 
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.LambdaEk * 8.0 * Xek * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter, 5)) +
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(fp.q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("3.24", "Pao2", fp.Pao2);
    }        
   
    /// <summary>
    /// Давление на насосном агрегате на третьем временном этапе при осуществлени обратной промывки (Па) (3.25)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    public void Pao3(double Xnkt)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("Pkol", fp.Pkol);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("3.25", parameters);
        fp.Pao3 = fp.Pkol - fp.densityOfKillingFluid * fp.Lcp * fp.g + fp.densityOfKillingFluid * Xnkt * fp.g +
                  fp.densityOfOil * (fp.Lcp - Xnkt) * fp.g +
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) * Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) +
                  fp.LambdaNkt * 8.0 * Xnkt * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) +                  
                  fp.LambdaNkt * 8.0 * (fp.Lcp - Xnkt) * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 
                  4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(fp.q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("3.25", "Pao3", fp.Pao3);
    }   
    
    /// <summary>
    /// Забойное давление в скважине на первом временном этапе
    /// (заполнение ЖГС НКТ от устья до башмака НКТ) (Па) (3.26)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    public void Pzp1(double Xnkt)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("Pap1", fp.Pap1);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("3.26", parameters);
        fp.Pzp1 = fp.Pap1 + fp.densityOfKillingFluid * Xnkt * fp.g + fp.densityOfOil * (fp.Lcp - Xnkt) * fp.g +
                  fp.densityOfOil * (fp.Lc - fp.Lcp) * fp.g -
                  fp.LambdaNkt * 8.0 * Xnkt * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) - 
                  fp.LambdaNkt * 8.0 * (fp.Lcp - Xnkt) * Math.Pow(fp.q, 2) * fp.densityOfOil /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("3.26", "Pzp1", fp.Pzp1);
    }
    
    /// <summary>
    /// Забойное давление в скважине на втором временном этапе
    /// (заполнение ЖГС ЭК от башмака НКТ до зумпфа (дна) скважины) (Па) (3.27)
    /// </summary>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    public void Pzp2(double Xek)
    {
        parameters.Clear();
        parameters.Add("Xek", Xek);
        parameters.Add("Pap2", fp.Pap2);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("g", fp.g);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("3.27", parameters);
        fp.Pzp2 = fp.Pap2 + fp.densityOfKillingFluid * fp.Lcp * fp.g + fp.densityOfKillingFluid * Xek * fp.g +
                  fp.densityOfOil * (fp.Lc - fp.Lcp - Xek) * fp.g -
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) - 
                  fp.LambdaEk * 8.0 * Xek * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter, 5));
        p.POutC("3.27", "Pzp2", fp.Pzp2);
    }
    
    /// <summary>
    /// Забойное давление в скважине на третьем временном этапе
    /// (заполнение ЖГС КП от башмака НКТ до устья скважины) (Па) (3.28)
    /// </summary>
    public void Pzp3()
    {
        parameters.Clear();
        parameters.Add("Pap3", fp.Pap3);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("g", fp.g);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("3.28", parameters);
        fp.Pzp3 = fp.Pap3 + fp.densityOfKillingFluid * fp.Lc * fp.g -
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("3.28", "Pzp3", fp.Pzp3);
    }
    
    /// <summary>
    /// Забойное давление в скважине при заполнении ЖГС КП от устья до башмака НКТ (Па) (3.29)
    /// </summary>
    /// <param name="Xkp">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в КП от подачи насоса (q) во времени, м</param>
    public void Pzo1(double Xkp)
    {
        parameters.Clear();
        parameters.Add("Xkp", Xkp);
        parameters.Add("Pao1", fp.Pao1);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
       p.PInC("3.29", parameters);
       fp.Pzo1 = fp.Pao1 + fp.densityOfKillingFluid * fp.g * Xkp + fp.densityOfOil * fp.g * (fp.Lcp - Xkp) +
                 fp.densityOfOil * fp.g * (fp.Lc - fp.Lcp) -
                 fp.LambdaKp * 8.0 * Xkp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                 (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                  Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) -
                 fp.LambdaKp * 8.0 * (fp.Lcp - Xkp) * Math.Pow(fp.q, 2) * fp.densityOfOil /
                 (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                  Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("3.29", "Pzo1", fp.Pzo1);
    }
    
    /// <summary>
    /// Забойное давление в скважине при заполнении ЖГС ЭК от башмака НКТ до зумпфа (дна) скважины (Па) (3.30)
    /// </summary>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    public void Pzo2(double Xek)
    {
        parameters.Clear();
        parameters.Add("Xek", Xek);
        parameters.Add("Pao2", fp.Pao2);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("LambdaEk", fp.LambdaEk);
        p.PInC("3.30", parameters);
        fp.Pzo2 = fp.Pao2 + fp.densityOfKillingFluid * fp.g * fp.Lcp + fp.densityOfKillingFluid * fp.g * Xek +
                  fp.densityOfOil * fp.g * (fp.Lc - fp.Lcp - Xek) - 
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2)) - 
                  fp.LambdaEk * 8.0 * Xek * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter, 5));
        p.POutC("3.30", "Pzo2", fp.Pzo2);
    }    
    
    /// <summary>
    /// Забойное давление в скважине при заполнении ЖГС КП от башмака НКТ до устья скважины (Па) (3.31)
    /// </summary>
    public void Pzo3()
    {
        parameters.Clear();
        parameters.Add("Pao3", fp.Pao3);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("LambdaKp", fp.LambdaKp);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("3.31", parameters);
        fp.Pzo3 = fp.Pao3 + fp.densityOfKillingFluid * fp.g * fp.Lc -
                  fp.LambdaKp * 8.0 * fp.Lcp * Math.Pow(fp.q, 2) * fp.densityOfKillingFluid /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.ECInnerDiameter - fp.Dnkt, 3) *
                   Math.Pow(fp.ECInnerDiameter + fp.Dnkt, 2));
        p.POutC("3.31", "Pzo3", fp.Pzo3);
    }
    
    
    /// <summary>
    /// Давление на устье (Па) (3.33)
    /// </summary>
    public void Hzhgs()
    {
        parameters.Clear();
        parameters.Add("Vzhgn", fp.Vzhgn);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("3.33", parameters);
        fp.Hzhgs = 4.0 * fp.Vzhgn / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("3.33", "Hzhgs", fp.Hzhgs);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (3.34)
    /// </summary>
    public void Pz2()
    {
        parameters.Clear();
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Pu", fp.Pu);
        parameters.Add("g", fp.g);
        parameters.Add("Hzhgs", fp.Hzhgs);
        p.PInC("3.34", parameters);
        fp.Pz = fp.Pu + fp.densityOfKillingFluid * fp.g * fp.Hzhgs;
        p.POutC("3.34", "Pz", fp.Pz);
    }
    
    /// <summary>
    /// Давление на агрегате (и устье скважины) при опрессовке нагнетательной линии (Па) (3.35)
    /// </summary>
    public void Popr()
    {
        parameters.Clear();
        parameters.Add("Fi", fp.Fi);
        parameters.Add("q", fp.q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dotv", fp.dotv);
        p.PInC("3.35", parameters);
        fp.Popr = 4.6E5 * Math.Pow(fp.Fi, -1.5) * 8.0 * Math.Pow(fp.q, 2) * 1000.0 /
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dotv, 4));
        p.POutC("3.35", "Popr", fp.Popr);
    }

    /// <summary>
    /// Накопленный объем закачиваемой в скважину ЖГС, м3 (3.36)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Vnakzhgs(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        p.PInC("3.36", parameters);
        fp.Vnakzhgs += q * time;
        p.POutC("3.36", "Vnakzhgs", fp.Vnakzhgs);
    }
    
    /// <summary>
    /// Дифференциальное давление в процессе глушения (Па) (3.37)
    /// </summary>
    public void Pdif()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pz", fp.Pz);
        p.PInC("3.37", parameters);
        fp.Pdif = fp.reservoirPressurePa - fp.Pz;
        p.POutC("3.37", "Pdif", fp.Pdif);
    }








}
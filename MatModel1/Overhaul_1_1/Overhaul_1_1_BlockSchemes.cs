using System;

public class Overhaul_1_1_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;

    public Overhaul_1_1_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    // Глушение фонтанной скважины методом прямой промывки
    public void Diagram_1(double time)
    {
        fp.o11.Pu(fp.densityOfOil); // 3.7
        fp.o11.Pm(fp.densityOfOil); // 3.7
        fp.o11.Pzatr(fp.densityOfOil); // 3.7
        fp.o11.Pz1(); // 3.8
        fp.o11.reservoirPressurePa(); // 3.8
        fp.o11.densityOfKillingFluid(); // 3.1
        fp.o11.Lc(); // 3.2
        fp.o11.Vek(); // 3.4
        fp.o11.Vtnkt(); // 3.5
        fp.o11.Vzhgn(); // 3.3
        fp.o11.Xnkt(time); // 3.17
        fp.o11.Pap1(fp.Xnkt); // 3.20
        fp.o11.Pzp1(fp.Xnkt); // 3.26
        fp.o11.Xek(time); // 3.18
        fp.o11.Pap2(fp.Xek); // 3.21
        fp.o11.Pzp2(fp.Xek); // 3.27
        fp.o11.Xkp(time); // 3.19
        fp.o11.Pap3(fp.Xkp); // 3.22
        fp.o11.Pzp3(); // 3.28
        fp.o11.Pu(fp.densityOfKillingFluid); // 3.32
        fp.o11.Pm(fp.densityOfKillingFluid); // 3.32
        fp.o11.Pzatr(fp.densityOfKillingFluid); // 3.32
        fp.o11.Hzhgs(); // 3.33
        fp.o11.Pz2(); // 3.34
        if (fp.Pu <= 0.0)
        {
            Console.WriteLine("Первое условие успешного глушения выполнено!");
            p.PTxt("Первое условие успешного глушения выполнено!");
        }
        else
        {
            Console.WriteLine("Глушение выполнено не успешно. Повторите упражнение заново!");
            p.PTxt("Глушение выполнено не успешно. Повторите упражнение заново!");
        }
        if (fp.Lc < fp.Hzhgs)
        {
            Console.WriteLine("Второе условие успешного глушения выполнено!");
            p.PTxt("Второе условие успешного глушения выполнено!");
        }
        else
        {
            Console.WriteLine("Глушение выполнено не успешно. Повторите упражнение заново!");
            p.PTxt("Глушение выполнено не успешно. Повторите упражнение заново!");
        }
        if ((fp.reservoirPressurePa < fp.Pz) && (fp.Pz < fp.Ppgl))
        {
            Console.WriteLine("Третье условие успешного глушения выполнено!");
            p.PTxt("Третье условие успешного глушения выполнено!");
        }
        else
        {
            Console.WriteLine("Глушение выполнено не успешно. Повторите упражнение заново!");
            p.PTxt("Глушение выполнено не успешно. Повторите упражнение заново!");
        }
    }
    
    // Глушение скважины, оборудованной УЭЦН, методом обратной промывки
    public void Diagram_2(double time)
    {
        fp.o11.Pu(fp.densityOfOil); // 3.7
        fp.o11.Pm(fp.densityOfOil); // 3.7
        fp.o11.Pzatr(fp.densityOfOil); // 3.7
        fp.o11.Pz1(); // 3.8
        fp.o11.reservoirPressurePa(); // 3.8
        fp.o11.densityOfKillingFluid(); // 3.1
        fp.o11.Lc(); // 3.2
        fp.o11.Vek(); // 3.4
        fp.o11.Vtnkt(); // 3.5
        fp.o11.Vzhgn(); // 3.3
        fp.o11.Xkp(time); // 3.19
        fp.o11.Pao1(fp.Xkp); // 3.23
        fp.o11.Pzo1(fp.Xkp); // 3.29
        fp.o11.Xek(time); // 3.18
        fp.o11.Pao2(fp.Xek); // 3.24
        fp.o11.Pzo2(fp.Xek); // 3.30
        fp.o11.Xnkt(time); // 3.17
        fp.o11.Pao3(fp.Xnkt); // 3.25
        fp.o11.Pzo3(); // 3.31
        fp.o11.Pu(fp.densityOfKillingFluid); // 3.32
        fp.o11.Pm(fp.densityOfKillingFluid); // 3.32
        fp.o11.Pzatr(fp.densityOfKillingFluid); // 3.32
        fp.o11.Hzhgs(); // 3.33
        fp.o11.Pz2(); // 3.34
        if (fp.Pu <= 0.0)
        {
            Console.WriteLine("Первое условие успешного глушения выполнено!");
            p.PTxt("Первое условие успешного глушения выполнено!");
        }
        else
        {
            Console.WriteLine("Глушение выполнено не успешно. Повторите упражнение заново!");
            p.PTxt("Глушение выполнено не успешно. Повторите упражнение заново!");
        }
        if (fp.Lc < fp.Hzhgs)
        {
            Console.WriteLine("Второе условие успешного глушения выполнено!");
            p.PTxt("Второе условие успешного глушения выполнено!");
        }
        else
        {
            Console.WriteLine("Глушение выполнено не успешно. Повторите упражнение заново!");
            p.PTxt("Глушение выполнено не успешно. Повторите упражнение заново!");
        }
        if ((fp.reservoirPressurePa < fp.Pz) && (fp.Pz < fp.Ppgl))
        {
            Console.WriteLine("Третье условие успешного глушения выполнено!");
            p.PTxt("Третье условие успешного глушения выполнено!");
        }
        else
        {
            Console.WriteLine("Глушение выполнено не успешно. Повторите упражнение заново!");
            p.PTxt("Глушение выполнено не успешно. Повторите упражнение заново!");
        }
    }
}
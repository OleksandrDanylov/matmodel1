using System;
using System.Collections.Generic;

public class Prod_1_1
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_1(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (1.1.1)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("1.1.1", parameters);
        fp.Pz = fp.Pzrezh + fp.Qrezh * fp.oilViscosity * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("1.1.1", "Pz", fp.Pz);
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (1.1.2)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz2(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("1.1.2", parameters);
        fp.Pz = fp.Pzrezh - fp.Qrezh * fp.oilViscosity * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
        p.POutC("1.1.2", "Pz", fp.Pz);
    }
    
    /// <summary>
    /// Количество свободного газа (д. ед.) (1.2)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void ng(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("nv", fp.nv);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Z", fp.Z);
        parameters.Add("Tnu", fp.Tnu);
        p.PInC("1.2", parameters);
        var tempVar = fp.gasFactor * (Pz - fp.Pnas0) / (fp.Pnu - fp.Pnas0) * 
            (1.0 - fp.nv) * fp.Pnu * fp.Tpl * fp.Z / (Pz * fp.Tnu);
        fp.ng = tempVar / (1.0 + tempVar);
        p.POutC("1.2", "ng", fp.ng);
    }

    /// <summary>
    /// Показатель степени в формуле (1.4) (д. ед.) (1.3)
    /// </summary>
    public void nst()
    {
        parameters.Clear();
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("ng", fp.ng);
        p.PInC("1.3", parameters);
        if (fp.Pnas0 < fp.Pz) fp.nst = 1.0;
        else fp.nst = 1.0 - 0.03 * fp.ng;
        p.POutC("1.3", "nst", fp.nst);
    }
    
    /// <summary>
    /// Дебит скважины  (м3 / с) (1.4)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Q(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("nst", fp.nst);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.4", parameters);
        fp.Q = 2.0 * fp.pi * fp.k * fp.h * Math.Pow(fp.reservoirPressurePa - Pz, fp.nst) / 
                  (fp.oilViscosity * Math.Log(fp.Rk / (fp.rc * Math.Exp(-fp.skinFactor))));
        p.POutC("1.4", "Q", fp.Q);
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (1.5, 1.6)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("deltaPpred", fp.deltaPpred);
        parameters.Add("A1", fp.A1);
        parameters.Add("A2", fp.A2);
        p.PInC("1.5, 1.6", parameters);
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
        p.POutC("1.5, 1.6", "MP", fp.MP);
    }

    /// <summary>
    /// Обводненность продукции (д. ед.) (1.7)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void nv(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("productWaterCut0", fp.productWaterCut0);
        parameters.Add("productWaterCutMax", fp.productWaterCutMax);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("DPmax", fp.DPmax);
        p.PInC("1.7", parameters);
        fp.nv = fp.productWaterCut0 + (fp.productWaterCutMax - fp.productWaterCut0) * 
            Math.Pow(fp.reservoirPressurePa - Pz, 2) / fp.DPmax;
        p.POutC("1.7", "nv", fp.nv);
    }    
        
    /// <summary>
    /// Скорость подъема жидкости в ЭК (м/с) (1.8, 1.45)
    /// </summary>
    public void Vzhek()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.8, 1.45", parameters);
        fp.Vzhek = 4.0 * fp.Q / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("1.8, 1.45", "Vzhek", fp.Vzhek);
    }
    
    /// <summary>
    /// Плотность жидкости (кг/м3) (1.9, 1.15, 1.41)
    /// </summary>
    public void Rozh()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("nv", fp.nv);
        p.PInC("1.9, 1.15, 1.41", parameters);
        fp.Rozh = fp.densityOfOil * (1.0 - fp.nv) + fp.densityOfWater * fp.nv;
        p.POutC("1.9, 1.15, 1.41", "Rozh", fp.Rozh);
    }
    
    /// <summary>
    /// Число Рейнольдса в ЭК (1.10, 1.46)
    /// </summary>
    public void Reek()
    {
        parameters.Clear();
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("oilViscosity", fp.oilViscosity);
        p.PInC("1.10, 1.46", parameters);
        fp.Reek = fp.Vzhek * fp.Rozh * fp.ECInnerDiameter / fp.oilViscosity;
        p.POutC("1.10, 1.46", "Reek", fp.Reek);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК (1.11, 1.47)
    /// </summary>
    public void LambdaEk()
    {
        parameters.Clear();
        parameters.Add("Reek", fp.Reek);
        parameters.Add("EpsEk", fp.EpsEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.11, 1.47", parameters);
        fp.LambdaEk = 0.067 * (158.0 / fp.Reek + 2.0 * fp.EpsEk / fp.ECInnerDiameter);
        p.POutC("1.11, 1.47", "LambdaEk", fp.LambdaEk);
    }
    
    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ (м) (1.12)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg1(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.12", parameters);
        fp.Hg1 = fp.Lk - (fp.reservoirPressurePa - Pz) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                            fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.12", "Hg1", fp.Hg1);
    }
        
    /// <summary>
    /// Давление на приеме в НКТ (Па) (Па) (1.13)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ppr2(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Hg1", fp.Hg1);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.13", parameters);
        if (fp.Hg1 < fp.Lcp) fp.Ppr = Pz - (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.13", "Ppr", fp.Ppr);
    }
      
    /// <summary>
    /// Скорость подъема жидкости в НКТ (м/с) (1.14, 1.42)
    /// </summary>
    public void Vzhnkt()
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.14, 1.42", parameters);
        fp.Vzhnkt = 4.0 * fp.Q / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("1.14, 1.42", "Vzhnkt", fp.Vzhnkt);
    }
      
    /// <summary>
    /// Число Рейнольдса в НКТ (1.16, 1.43)
    /// </summary>
    public void Renkt()
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Mjuzh", fp.Mjuzh);
        p.PInC("1.16, 1.43", parameters);
        fp.Renkt = fp.Vzhnkt * fp.Rozh * fp.dnkt / fp.Mjuzh;
        p.POutC("1.16, 1.43", "Renkt", fp.Renkt);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в НКТ (1.17, 1.44)
    /// </summary>
    public void LambdaNkt()
    {
        parameters.Clear();
        parameters.Add("Renkt", fp.Renkt);
        parameters.Add("EpsNkt", fp.EpsNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.17, 1.44", parameters);
        fp.LambdaNkt = 0.067 * (158.0 / fp.Renkt + 2.0 * fp.EpsNkt / fp.dnkt);
        p.POutC("1.17, 1.44", "LambdaNkt", fp.LambdaNkt);
    }
    
    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ в НКТ (м) (1.18)
    /// </summary>
    /// <param name="Ppr">Давление на приеме в НКТ, Па</param>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg2(double Ppr, double Pz)
    {
        parameters.Clear();
        parameters.Add("Ppr", Ppr);
        parameters.Add("Pz", Pz);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.18", parameters);
        fp.Hg2 = fp.Lcp - (Ppr - Pz) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                       fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("1.18", "Hg2", fp.Hg2);
    }    
    
    /// <summary>
    /// Буферное давление, рассчитанное при помощи распределения давления (Па) (1.19)
    /// </summary>
    /// <param name="Ppr">Давление на приеме в НКТ, Па</param>
    /// <param name="Vzhnkt">Скорость подъема жидкости в НКТ, м/с</param>
    public void Pbufpk(double Ppr, double Vzhnkt)
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", Vzhnkt);
        parameters.Add("Ppr", Ppr);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.19", parameters);
        fp.Pbufpk = Ppr - fp.Lcp * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                       fp.LambdaNkt * Math.Pow(Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("1.19", "Pbufpk", fp.Pbufpk);
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (1.20, 1.67)
    /// </summary>
    public void dl1()
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("L2", fp.L2);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.20, 1.67", parameters);
        fp.dl = (fp.L2 - fp.L1) / fp.Nint;
        p.POutC("1.20, 1.67", "dl", fp.dl);
    }
    
    /// <summary>
    /// Температурный градиент потока (К/м) (1.21, 1.51, 1.68)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    /// <param name="Q">Дебит скважины, м3/с</param>
    public void Tgrp(double InnerDiam, double Q)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Gtgr", fp.Gtgr);
        parameters.Add("Q", Q);
        p.PInC("1.21, 1.51, 1.68", parameters);
        fp.Tgrp = (0.0034 + 0.79 * fp.Gtgr) / Math.Pow(10, Q / (20.0 * Math.Pow(InnerDiam, 2.67)));
        p.POutC("1.21, 1.51, 1.68", "Tgrp", fp.Tgrp);
    }
    
    /// <summary> 
    /// Температура в интервале H[j] (К) (1.22, 1.52, 1.69)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Tj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgrp", fp.Tgrp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("H[j]", fp.H[j]);
        p.PInC("1.22, 1.52, 1.69", parameters);
        fp.T[j] = fp.Tpl - fp.Tgrp * (fp.Lk - fp.H[j]);
        p.POutC("1.22, 1.52, 1.69", "T[j]", fp.T[j]);
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (1.23)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pnasj1(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Nc", fp.Nc);
        parameters.Add("Na", fp.Na);
        p.PInC("1.23", parameters);
        fp.Pnas[j] = (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
        p.POutC("1.23", "Pnas[j]", fp.Pnas[j]);
    }
        
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.24, 1.54, 1.71)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void RfP(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("Pnas[j]", fp.Pnas[j]);
        p.PInC("1.24, 1.54, 1.71", parameters);
        fp.RfP = (1.0 + Math.Log10(1.0E-6 * fp.P[j])) / (1.0 + Math.Log10(1.0E-6 * fp.Pnas[j])) - 1.0;
        p.POutC("1.24, 1.54, 1.71", "RfP", fp.RfP);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.25, 1.55, 1.72)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void mfT(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("1.25, 1.55, 1.72", parameters);
        fp.mfT = 1.0 + 0.029 * (fp.T[j] - 293.0) * (fp.densityOfOil * fp.densityOfGas * 1.0E-3 - 0.7966);
        p.POutC("1.25, 1.55, 1.72", "mfT", fp.mfT);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.26, 1.56, 1.73)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void DfT(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("1.26, 1.56, 1.73", parameters);
        fp.DfT = 1.0E-3 * fp.densityOfOil * fp.densityOfGas * (4.5 - 0.00305 * (fp.T[j] - 293.0)) - 4.785;
        p.POutC("1.26, 1.56, 1.73", "DfT", fp.DfT);
    }
    
    /// <summary>
    /// Удельный объем выделившегося газа (м3/м3) (1.27, 1.57, 1.74)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vgvj(int j)
    {
        parameters.Clear();
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("RfP", fp.RfP);
        parameters.Add("mfT", fp.mfT);
        parameters.Add("DfT", fp.DfT);
        p.PInC("1.27, 1.57, 1.74", parameters);
        fp.Vgv[j] = fp.gasFactor * fp.RfP * fp.mfT * (fp.DfT * (1 + fp.RfP) - 1.0);
        p.POutC("1.27, 1.57, 1.74", "Vgv[j]", fp.Vgv[j]);
    }
    
    /// <summary>
    /// Удельный объем смеси (м3/м3) (1.28, 1.58, 1.75)
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vcm(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Vgv[j]", fp.Vgv[j]);
        parameters.Add("bOil", fp.bOil);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("Tnu", fp.Tnu);
        parameters.Add("nv", fp.nv);
        p.PInC("1.28, 1.58, 1.75", parameters);
        fp.Vcm = fp.bOil + fp.Vgv[j] * fp.Z * fp.Pnu * fp.T[j] / (fp.P[j] * fp.Tnu) + 
                 fp.nv / (1.0 - fp.nv);
        p.POutC("1.28, 1.58, 1.75", "Vcm", fp.Vcm);
    }
    
    /// <summary>
    /// Удельная масса смеси (кг/м3) (1.29, 1.59, 1.76)
    /// </summary>
    public void Mcm()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("nv", fp.nv);
        p.PInC("1.29, 1.59, 1.76", parameters);
        fp.Mcm = fp.densityOfOil + fp.densityOfGas * fp.gasFactor + 
                 fp.densityOfWater * fp.nv / (1.0 - fp.nv);
        p.POutC("1.29, 1.59, 1.76", "Mcm", fp.Mcm);
    }
    
    /// <summary>
    /// Плотность газожидкостной смеси (кг/м3) (1.30, 1.60, 1.77)
    /// </summary>
    public void Rocm()
    {
        parameters.Clear();
        parameters.Add("Vcm", fp.Vcm);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.30, 1.60, 1.77", parameters);
        fp.Rocm = fp.Mcm / fp.Vcm;
        p.POutC("1.30, 1.60, 1.77", "Rocm", fp.Rocm);
    }
    
    /// <summary>
    /// Число Рейнольдса (1.31, 1.61, 1.78)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Re(double InnerDiam)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.31, 1.61, 1.78", parameters);
        fp.Re = 0.99E-5 * 86400.0 * fp.Q * (1.0 - fp.nv) * fp.Mcm / InnerDiam;
        p.POutC("1.31, 1.61, 1.78", "Re", fp.Re);
    }
    
    /// <summary>
    /// Корреляционный коэффициент (1.32, 1.62, 1.79)
    /// </summary>
    public void f()
    {
        parameters.Clear();
        parameters.Add("Re", fp.Re);
        p.PInC("1.32, 1.62, 1.79", parameters);
        fp.f = Math.Pow(10, 19.66 * Math.Pow(1.0 + Math.Log10(fp.Re), -0.25) - 17.713);
        p.POutC("1.32, 1.62, 1.79", "f", fp.f);
    }
    
    /// <summary>
    /// Дифференциал (Па/м) (1.33, 1.63, 1.80)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    /// <param name="Rocm">Плотность газожидкостной смеси, кг/м3</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dPdH(double InnerDiam, double Rocm, int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Rocm", Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("f", fp.f);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.33, 1.63, 1.80", parameters);
        fp.dPdH[j] = Rocm * fp.g * Math.Cos(fp.alfa) + 
                     fp.f * Math.Pow(86400.0 * fp.Q, 2) * Math.Pow(1.0 - fp.nv, 2) * Math.Pow(fp.Mcm, 2) /
                     (2.3024E9 * Rocm * Math.Pow(InnerDiam, 5));
        p.POutC("1.33, 1.63, 1.80", "dPdH[j]", fp.dPdH[j]);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.34)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void Hj1(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("1.34", parameters);
        fp.H[j] = fp.L1 - fp.dl * j;
        p.POutC("1.345", "H[j]", fp.H[j]);
    }
  
    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.35)
    /// </summary>
    public void hdin1()
    {
        parameters.Clear();
        parameters.Add("hdin", fp.hdin);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Pzatr", fp.Pzatr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.35", parameters);
        fp.hdin = fp.Lcp - (fp.Ppr - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.densityOfOil * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.35", "hdin", fp.hdin);
    } 
    
    /// <summary>
    /// Давление на затрубе (Па) (1.36)
    /// </summary>
    public void Pzatr1()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.36", parameters);
        fp.Pzatr = (fp.Ppr - fp.densityOfOil * fp.g * (fp.Lcp - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
        p.POutC("1.36", "Pzatr", fp.Pzatr);
    }
     
    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле (Па) (1.37)
    /// </summary>
    public void Pbuf1()
    {
        parameters.Clear();
        parameters.Add("Qg", fp.Qg);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        p.PInC("1.37", parameters);
        fp.Pbuf1 = 0.0729 * Math.Pow(1.0, 2) * fp.Qg * 86400.0 * fp.densityOfGas * fp.Plin /
                   Math.Pow(1000.0 * fp.dsht, 2);
        p.POutC("1.37", "Pbuf1", fp.Pbuf1);
    }
       
    /// <summary>
    /// Расход газа (м3/с) (1.38)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Qg(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Vgvust", fp.Vgvust);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pbufm", fp.Pbufm);
        parameters.Add("Tst", fp.Tst);
        p.PInC("1.38", parameters);
        fp.Qg = Q * fp.Vgvust * fp.Pst * fp.Tu * fp.Z / (fp.Pbufm * fp.Tst);
        p.POutC("1.38", "Qg", fp.Qg);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле (Па) (1.39)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Pbuf2(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        parameters.Add("g", fp.g);
        p.PInC("1.39", parameters);
        fp.Pbuf2 = fp.Plin + Math.Pow(Q, 2) / (78.8768 * Math.Pow(1000.0 * fp.dsht, 4) * fp.g);
        p.POutC("1.39", "Pbuf2", fp.Pbuf2);
    }
        
    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по формулам (Па) (1.40)
    /// </summary>
    /// <param name="Pbuf1">Буферное давление, рассчитанное по первой формуле, Па</param>
    /// <param name="Pbuf2">Буферное давление, рассчитанное по второй формуле, Па</param>
    public void Pbufm(double Pbuf1, double Pbuf2)
    {
        parameters.Clear();
        parameters.Add("Pbuf1", Pbuf1);
        parameters.Add("Pbuf2", Pbuf2);
        p.PInC("1.40", parameters);
        fp.Pbufm = Math.Max(Pbuf1, Pbuf2);
        p.POutC("1.40", "Pbufm", fp.Pbufm);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (1.48)
    /// </summary>
    public void Ppr()
    {
        parameters.Clear();
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("L1", fp.L1);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.48", parameters);
        fp.Ppr = fp.P1nkt + (fp.Lcp - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("1.48", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Давление на выходе из насоса (Па) (1.49)
    /// </summary>
    public void Pzpk()
    {
        parameters.Clear();
        parameters.Add("P2ek", fp.P2ek);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("L1", fp.L1);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.49", parameters);
        fp.Pzpk = fp.P2ek + (fp.Lk - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.49", "Pzpk", fp.Pzpk);
    }
    
    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера (Па) (1.50)
    /// </summary>
    public void dP()
    {
        parameters.Clear();
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.50", parameters);
        fp.dP = (fp.Pnas0 - fp.P1nkt) / fp.Nint;
        p.POutC("1.50", "dP", fp.dP);
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (1.53, 1.70)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pnasj2(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Nc", fp.Nc);
        parameters.Add("Na", fp.Na);
        p.PInC("1.53, 1.70", parameters);
        fp.Pnas[j] = fp.Pnas0 - (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
        p.POutC("1.53, 1.70", "Pnas[j]", fp.Pnas[j]);
    }
    
    /// <summary>
    /// Число, обратное дифференциалу (1.63) (м/Па) (1.64)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dHdP(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("dPdH[j]", fp.dPdH[j]);
        p.PInC("1.64", parameters);
        fp.dHdP[j] = 1.0 / fp.dPdH[j];
        p.POutC("1.64", "dHdP[j]", fp.dHdP[j]);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.65)
    /// </summary>
    /// <param name="N">Количество элементов в массиве dHdP, шт.</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    /// <param name="dHdP">Массив значений дифференциала dH/dP на разных интервалах Hj</param>
    public void Hj2(int N, int j, double[] dHdP)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("j", j);
        parameters.Add("dHdP[j]", dHdP[j]);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("1.65", parameters);
        var sum = 0.0;
        for (var i = 0; i < N; i++) sum += dHdP[i];
        fp.H[j] = fp.L1 + fp.dl * ((dHdP[0] + dHdP[j]) / 2.0 + sum);
        p.POutC("1.65", "H[j]", fp.H[j]);
    }
    
    /// <summary>
    /// Давление в интервале Н[j] (Па) (1.66)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("dP", fp.dP);
        p.PInC("1.66", parameters);
        fp.P[j] = fp.P1nkt + fp.dP * j;
        p.POutC("1.66", "P[j]", fp.P[j]);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.81)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void Hj3(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        p.PInC("1.81", parameters);
        fp.H[j] = fp.L1 + fp.dl * j;
        p.POutC("1.81", "H[j]", fp.H[j]);
    }    

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (1.82)
    /// </summary>
    public void DPpzo1()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pzost", fp.Pzost);
        p.PInC("1.82", parameters);
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
        p.POutC("1.82", "DPpzo1", fp.DPpzo1);
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (1.83)
    /// </summary>
    public void tzab1()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("rc", fp.rc);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("1.83", parameters);
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.oilViscosity)) * fp.rc / 
                   (2.25 * fp.piezoconductivityOfTheFormation); 
        p.POutC("1.83", "tzab1", fp.tzab1);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (1.84, 1.85)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("tzab1", fp.tzab1);
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.84, 1.85", parameters);
        if (time < fp.tzab1)  fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        else  fp.Pz = fp.Pzost + fp.Qrezh * fp.oilViscosity / (4.0 * fp.pi * fp.k * fp.h) *
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
        p.POutC("1.84, 1.85", "Pz", fp.Pz);
    }
 
    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.86)
    /// </summary>
    public void htr1()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.86", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3) / (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.86", "htr", fp.htr);
    }   

    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.87)
    /// </summary>
    public void htr2()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("htr", fp.htr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.87", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3 * Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z))) / 
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.87", "htr", fp.htr);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по третьей формуле (Па) (1.88)
    /// </summary>
    public void Pbuf3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.88", parameters);
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("1.88", "Pbuf3", fp.Pbuf3);
    }

    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.89)
    /// </summary>
    public void hdin2()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pzatr", fp.Pzatr);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.89", parameters);
        fp.hdin = fp.Lk - (fp.Pz - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.89", "hdin", fp.hdin);
    }

    /// <summary>
    /// Давление на затрубе (Па) (1.90)
    /// </summary>
    public void Pzatr2()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.90", parameters);
        fp.Pzatr = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
        p.POutC("1.90", "Pzatr", fp.Pzatr);
    }
    
    
}
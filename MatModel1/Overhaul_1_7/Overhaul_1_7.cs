using System;
using System.Collections.Generic;

public class Overhaul_1_7
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Overhaul_1_7(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
        fp.Hpl = fp.Hpr;
    }
    
    /// <summary>
    /// Плотность жидкости глушения скважины (кг/м3) (16.1) 
    /// </summary>
    public void densityOfKillingFluid()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Kbr", fp.Kbr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc); 
        p.PInC("16.1", parameters);
        fp.densityOfKillingFluid = fp.reservoirPressurePa * (1.0 + fp.Kbr) / (fp.g * fp.Lc);
        p.POutC("16.1", "densityOfKillingFluid", fp.densityOfKillingFluid);
    }

    /// <summary>
    /// Глубина скважины по вертикали (м) (16.2) 
    /// </summary>
    public void Lc()
    {
        parameters.Clear();
        parameters.Add("lc", fp.lc);
        parameters.Add("alfa", fp.alfa);
        p.PInC("16.2", parameters);
        fp.Lc = fp.lc * Math.Cos(fp.alfa);
        p.POutC("16.2", "Lc", fp.Lc);
    }
    
    /// <summary>
    /// Давление на устье (Па) (16.3, 16.25)
    /// </summary>
    /// <param name="Ronzhgs">Плотность нефти или ЖГС, кг/м3</param>
    public void Pu(double Ronzhgs)
    {
        parameters.Clear();
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("16.3, 16.25", parameters);
        fp.Pu = fp.reservoirPressurePa - Ronzhgs * fp.g * fp.Lc;
        p.POutC("16.3, 16.25", "Pu", fp.Pu);
    }

    /// <summary>
    /// Давление на манифольде (Па) (16.3, 16.25)
    /// </summary>
    /// <param name="Ronzhgs">Плотность нефти или ЖГС, кг/м3</param>
    public void Pm(double Ronzhgs)
    {
        parameters.Clear();
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("16.3, 16.25", parameters);
        fp.Pm = fp.reservoirPressurePa - Ronzhgs * fp.g * fp.Lc;
        p.POutC("16.3, 16.25", "Pm", fp.Pm);
    }

    /// <summary>
    /// Давление на затрубе (Па) (16.3, 16.25)
    /// </summary>
    /// <param name="Ronzhgs">Плотность нефти или ЖГС, кг/м3</param>
    public void Pzatr(double Ronzhgs)
    {
        parameters.Clear();
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("16.3, 16.25", parameters);
        fp.Pzatr = fp.reservoirPressurePa - Ronzhgs * fp.g * fp.Lc;
        p.POutC("16.3, 16.25", "Pzatr", fp.Pzatr);
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (16.4, 16.26)
    /// </summary>
    /// <param name="Ronzhgs">Плотность нефти или ЖГС, кг/м3</param>
    public void Pz(double Ronzhgs)
    {
        parameters.Clear();
        parameters.Add("Ronzhgs", Ronzhgs);
        parameters.Add("Pu", fp.Pu);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("16.4, 16.26", parameters);
        fp.Pz = fp.Pu + Ronzhgs * fp.g * fp.Lc;
        p.POutC("16.4, 16.26", "Pz", fp.Pz);
    }
        
    /// <summary>
    /// Коэффициент бокового распора (16.6)
    /// </summary>
    public void Kle()
    {
        parameters.Clear();
        parameters.Add("KPgp", fp.KPgp);
        p.PInC("16.6", parameters);
        fp.Kle = fp.KPgp * (1.0 - fp.KPgp);
        p.POutC("16.6", "Kle", fp.Kle);
    }
        
    /// <summary>
    /// Горное давление, Па (16.7)
    /// </summary>
    public void Pg()
    {
        parameters.Clear();
        parameters.Add("Ropor", fp.Ropor);
        parameters.Add("g", fp.g);
        parameters.Add("Hpr", fp.Hpr);
        p.PInC("16.7", parameters);
        fp.Pg = fp.Ropor * fp.g * fp.Hpr;
        p.POutC("16.7", "Pg", fp.Pg);
    }
        
    /// <summary>
    /// Статический уровень жидкости в скважине, м (16.8)
    /// </summary>
    public void hst()
    {
        parameters.Clear();
        parameters.Add("Hpr", fp.Hpr);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        p.PInC("16.8", parameters);
        fp.hst = fp.Hpr - fp.reservoirPressurePa / (fp.densityOfKillingFluid * fp.g);
        p.POutC("16.8", "hst", fp.hst);
    }
        
    /// <summary>
    /// Предельный уровень жидкости в скважине, м (16.9)
    /// </summary>
    public void hpd()
    {
        parameters.Clear();
        parameters.Add("Hpr", fp.Hpr);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("DPmax", fp.DPmax);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        p.PInC("16.9", parameters);
        fp.hpd = fp.Hpr - (fp.reservoirPressurePa - fp.DPmax) / (fp.densityOfKillingFluid * fp.g);
        p.POutC("16.9", "hpd", fp.hpd);
    }
        
    /// <summary>
    /// Количество жидкости, отбираемое от устья до статического уровня, м3 (16.10)
    /// </summary>
    public void Qst()
    {
        parameters.Clear();
        parameters.Add("hst", fp.hst);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("16.10", parameters);
        fp.Qst = 0.785 * fp.hst * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2) + Math.Pow(fp.dnkt, 2));
        p.POutC("16.10", "Qst", fp.Qst);
    }
        
    /// <summary>
    /// Количество жидкости, отбираемое от устья до статического уровня, м3 (16.11)
    /// </summary>
    public void Qsvn()
    {
        parameters.Clear();
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("dk", fp.dk);
        parameters.Add("hsvn", fp.hsvn);
        parameters.Add("hypn", fp.hypn);
        p.PInC("16.11", parameters);
        fp.Qsvn = 0.785 * (Math.Pow(fp.dnkt, 2) - Math.Pow(fp.dk, 2)) * (fp.hsvn - fp.hypn);
        p.POutC("16.11", "Qsvn", fp.Qsvn);
    }
        
    /// <summary>
    /// Глубина отбора скважинной жидкости свабом в момент времени time, м3 (16.12)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void hsvn(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Vsv", fp.Vsv);
        p.PInC("16.12", parameters);
        fp.hsvn = fp.Vsv * time;
        p.POutC("16.12", "hsvn", fp.hsvn);
    }
        
    /// <summary>
    /// Количество рейсов (спуск и подъем) сваба для достижения статического уровня
    /// (при постоянном объеме отбора жидкости из скважины), шт. (16.13)
    /// </summary>
    public void nrs()
    {
        parameters.Clear();
        parameters.Add("Qst", fp.Qst);
        parameters.Add("Qsvn", fp.Qsvn);
        p.PInC("16.13", parameters);
        fp.nrs = (int)Math.Floor(fp.Qst / fp.Qsvn);
        p.POutC("16.13", "nrs", fp.nrs);
    }
        
    /// <summary>
    /// Количество рейсов (спуск и подъем) сваба для достижения статического уровня
    /// (при постоянном объеме отбора жидкости из скважины), шт. (16.14)
    /// </summary>
    public void Q()
    {
        parameters.Clear();
        parameters.Add("Vprn", fp.Vprn);
        parameters.Add("Toiln", fp.Toiln);
        p.PInC("16.14", parameters);
        fp.Q = fp.Vprn / fp.Toiln;
        p.POutC("16.14", "Q", fp.Q);
    }
         
    /// <summary>
    /// Количество жидкости, отбираемое от устья до статического уровня, м3 (16.15)
    /// </summary>
    public void Vprn()
    {
        parameters.Clear();
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("dk", fp.dk);
        parameters.Add("hsvn", fp.hsvn);
        parameters.Add("hypn", fp.hypn);
        p.PInC("16.15", parameters);
        fp.Vprn = 0.785 * (Math.Pow(fp.dnkt, 2) - Math.Pow(fp.dk, 2)) * (fp.hsvn - fp.hypnp1);
        p.POutC("16.15", "Vprn", fp.Vprn);
    }
         
    /// <summary>
    /// Глубина уровня скважинной жидкости, достигаемая свабом при последующем (n+1) спуске его в скважину
    /// в момент времени tур(n+1), м (16.16)
    /// </summary>
    public void hypnp1()
    {
        parameters.Clear();
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("hypn", fp.hypn);
        parameters.Add("hsvn", fp.hsvn);
        p.PInC("16.16", parameters);
        fp.hypnp1 = fp.Lcp - ((Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)) * (fp.Lcp - fp.hypn) + 
                              Math.Pow(fp.dnkt, 2) * (fp.Lcp - fp.hsvn)) /
                              (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2) + Math.Pow(fp.dnkt, 2));
        p.POutC("16.16", "hypnp1", fp.hypnp1);
    }
         
    /// <summary>
    /// Продолжительность притока нефти из пласта в скважину
    /// после достижения статического уровня жидкости в скважине, c (16.17)
    /// </summary>
    public void Toiln()
    {
        parameters.Clear();
        parameters.Add("hypnp1", fp.hypnp1);
        parameters.Add("hsvn", fp.hsvn);
        parameters.Add("Vsv", fp.Vsv);
        p.PInC("16.17", parameters);
        fp.Toiln = (fp.hypnp1 - fp.hsvn) / fp.Vsv;
        p.POutC("16.17", "Toiln", fp.Toiln);
    }
   
    /// <summary>
    /// Зависимость изменения положения границы «ЖГС – нефть» (высоты столба нефти в ЭК от зумпфа (дна) скважины до башмака НКТ) в ЭК
    /// от объема притока нефти из пласта в скважину (Q) во времени (м) (16.18)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Xek(double time)
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("time", time);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("pi", fp.pi);
        p.PInC("16.18", parameters);
        fp.Xek = 4.0 * fp.Q * time / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("16.18", "Xek", fp.Xek);
    }

    /// <summary>
    /// Зависимость изменения положения границы «ЖГС – нефть» (высоты столба нефти в НКТ) в НКТ
    /// от объема притока нефти из пласта в скважину (Q) во времени (м) (16.19)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Xnkt(double time)
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("time", time);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("16.19", parameters);
        fp.Xnkt = 4.0 * fp.Q * time / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("16.19", "Xnkt", fp.Xnkt);
    }

    /// <summary>
    /// Зависимость изменения положения границы «ЖГС – нефть» (высоты столба нефти в КП)
    /// в кольцевом пространстве от объема притока нефти из пласта в скважину (Q) во времени (м) (16.20)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Xkp1(double time)
    {
        parameters.Clear();
        parameters.Add("Q", fp.Q);
        parameters.Add("time", time);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("16.20", parameters);
        fp.Xkp = 4.0 * fp.Q * time / (fp.pi * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)));
        p.POutC("16.20", "Xkp", fp.Xkp);
    }

    /// <summary>
    /// Забойное давление на первом временном этапе отбора ЖГС с помощью сваба из скважины (м) (16.21)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1s(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Patm", fp.Patm);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("Hpl", fp.Hpl);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("hypn", fp.hypn);
        parameters.Add("Vsv", fp.Vsv);
        p.PInC("16.21", parameters);
        fp.Pz1s = fp.Patm + fp.densityOfKillingFluid * fp.g * (fp.Hpl - (fp.Lcp - 
                 ((Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)) * (fp.Lcp - fp.hypn) +
                 Math.Pow(fp.dnkt, 2) * (fp.Lcp - fp.Vsv * time)) / 
                 (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2) + Math.Pow(fp.dnkt, 2))));
        p.POutC("16.21", "Pz1s", fp.Pz1s);
    }

    /// <summary>
    /// Забойное давление на втором временном этапе отбора ЖГС с помощью сваба из скважины (м) (16.22)
    /// </summary>
    public void Pz2s()
    {
        parameters.Clear();
        parameters.Add("Patm", fp.Patm);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("Hpl", fp.Hpl);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("hsvn", fp.hsvn);
        parameters.Add("hypnp1", fp.hypnp1);
        parameters.Add("hypn", fp.hypn);
        p.PInC("16.22", parameters);
        fp.Pz2s = fp.Patm + fp.densityOfOil * fp.g * Math.Pow(fp.dnkt, 2) * (fp.hsvn - fp.hypnp1) / 
                 Math.Pow(fp.ECInnerDiameter, 2) + fp.densityOfKillingFluid * fp.g * (fp.Hpl - (fp.Lcp - 
                 ((Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)) * (fp.Lcp - fp.hypn) +
                 Math.Pow(fp.dnkt, 2) * (fp.Lcp - fp.hsvn)) / 
                 (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2) + Math.Pow(fp.dnkt, 2))) -
                 Math.Pow(fp.dnkt, 2) * (fp.hsvn - fp.hypnp1) / Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("16.22", "Pz2s", fp.Pz2s);
    }
    
    /// <summary>
    /// Забойное давление на третьем временном этапе отбора ЖГС с помощью сваба из скважины (м) (16.23)
    /// </summary>
    public void Pz3s()
    {
        parameters.Clear();
        parameters.Add("Patm", fp.Patm);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("Hpl", fp.Hpl);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("hsvn", fp.hsvn);
        parameters.Add("hypnp1", fp.hypnp1);
        parameters.Add("hypn", fp.hypn);
        p.PInC("16.23", parameters);
        fp.Pz3s = fp.Patm + fp.densityOfOil * fp.g * (fp.Hpl - fp.Lcp) + 
                 fp.densityOfOil * fp.g * (fp.hsvn - fp.hypnp1) +
                 fp.densityOfKillingFluid * fp.g * (fp.Lcp - (fp.Lcp - 
                 ((Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)) * (fp.Lcp - fp.hypn) +
                 Math.Pow(fp.dnkt, 2) * (fp.Lcp - fp.hsvn)) / 
                 (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2) + Math.Pow(fp.dnkt, 2))) -
                 (fp.hsvn - fp.hypnp1));
        p.POutC("16.23", "Pz3s", fp.Pz3s);
    }
    
    /// <summary>
    /// Зависимость изменения положения границы «ЖГС – нефть» (высоты столба нефти в КП)
    /// в кольцевом пространстве от объема притока нефти из пласта в скважину (Q) во времени (м) (16.24)
    /// </summary>
    public void Xkp2()
    {
        parameters.Clear();
        parameters.Add("hsvn", fp.hsvn);
        parameters.Add("hypnp1", fp.hypnp1);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("16.24", parameters);
        fp.Xkp = Math.Pow(fp.dnkt, 2) * (fp.hsvn - fp.hypnp1) / 
                 (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2));
        p.POutC("16.24", "Xkp", fp.Xkp);
    }
}
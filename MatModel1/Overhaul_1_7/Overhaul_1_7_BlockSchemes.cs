using System;

public class Overhaul_1_7_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;

    public Overhaul_1_7_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    // Математический алгоритм лабораторной работы «Освоение скважины с помощью сваба»
    public void Diagram_1(double time)
    {
        fp.o17.densityOfKillingFluid(); // 16.1
        fp.o17.Lc(); // 16.2
        fp.o17.Pu(fp.densityOfKillingFluid); // 16.3
        fp.o17.Pm(fp.densityOfKillingFluid); // 16.3
        fp.o17.Pzatr(fp.densityOfKillingFluid); // 16.3
        fp.o17.Pz(fp.densityOfKillingFluid); // 16.4
        fp.o17.hpd(); // 16.9
        fp.o17.hst(); // 16.8
        fp.o17.nrs(); // 16.13
        fp.o17.Pz1s(fp.time); // 16.21
        fp.o17.hypnp1(); // 16.16
        fp.o17.Pz2s(); // 16.22
        fp.o17.Xek(fp.time); // 16.18
        fp.o17.hypnp1(); // 16.16
        fp.o17.Pz3s(); // 16.23
        fp.o17.hypnp1(); // 16.16
        fp.o17.Xnkt(fp.time); // 16.19
        fp.o17.Xkp1(fp.time); // 16.20
        fp.o17.Pu(fp.densityOfOil); // 16.25
        fp.o17.Pm(fp.densityOfOil); // 16.25
        fp.o17.Pzatr(fp.densityOfOil); // 16.25
        fp.o17.Pz(fp.densityOfOil); // 16.26
        fp.o17.Xnkt(fp.time); // 16.19
        fp.o17.Xkp1(fp.time); // 16.20
        if ()
        {
            Console.WriteLine("Освоение выполнено успешно!");
            p.PTxt("Освоение выполнено успешно!");
        }
        else
        {
            Console.WriteLine("Освоение выполнено не успешно. Повторите упражнение заново!");
            p.PTxt("Освоение выполнено не успешно. Повторите упражнение заново!");
        } 
    }
}
using System;
using System.Collections.Generic;

public class Prod_1_5_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;
    public bool PNPT1 = false; 
    public bool PNPT2 = false; 
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_5_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    // 1.1. Алгоритм расчета забойного давления в скважине через динамический уровень при эксплуатации ШСНУ
    public void Diagram_1_1()
    {
        p.PTxt("p15.Diagram_1_1");
        fp.p15.Ppr1(fp.Pzatr, fp.hdin, fp.Tu); // 1.1.
        fp.p15.Rozh(); // 1.2.
        if (fp.Pbufm < fp.Pnas0) Diagram_1_1_Brench2();
        else Diagram_1_1_Brench1();
    } 
   
    private void Diagram_1_1_Brench1()
    {
        p.PTxt("p15.Diagram_1_1_Brench1");
        fp.L2 = fp.Lcp;
        fp.P2ek = fp.Ppr;
        parameters.Clear();
        parameters.Add("L2", fp.L2);
        parameters.Add("P2ek", fp.P2ek);
        p.PInBS("p15.Diagram_1_1_Brench1", parameters);
        fp.p15.Rozh(); // 1.3
        fp.p15.Vzhek(); // 1.4
        fp.p15.Reek1(); // 1.5
        fp.p15.LambdaEk(); // 1.6
        fp.p15.Pzpk1(); // 1.7
    }
   
    private void Diagram_1_1_Brench2()
    {
        p.PTxt("p15.Diagram_1_1_Brench2");
        fp.L1 = fp.Lcp;
        fp.P1nkt = fp.Ppr;
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("P1nkt", fp.P1nkt);
        p.PInBS("p15.Diagram_1_1_Brench2", parameters);
        fp.p15.dP(); // 1.8
        fp.p15.Tgrp(fp.ECInnerDiameter); // 1.9
        for (var j = 0; j < fp.N; j++)
        {
            fp.p15.Tj(j, 1); // 1.10
            fp.p15.Pnasj(j); // 1.11
            fp.p15.RfP(j); // 1.12
            fp.p15.mfT(j); // 1.13
            fp.p15.DfT(j); // 1.14
            fp.p15.Vgvj(j); // 1.15
            fp.p15.Vcm(j); // 1.16
            fp.p15.Mcm(); // 1.17
            fp.p15.Rocm(fp.Vcm); // 1.18
            fp.p15.Re(fp.ECInnerDiameter); // 1.19
            fp.p15.f(); // 1.20
            fp.p15.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.21
            fp.p15.dHdP(j); // 1.22
            fp.p15.Hj1(fp.N, j, fp.dHdP); // 1.23
            fp.p15.Pj(j); // 1.24
            if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg1;
        }
        if (fp.Lk < fp.Hg1)
        {
            fp.L1 = fp.Lcp;
            fp.L2 = fp.Lk;
            fp.P1nkt = fp.Ppr;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("L2", fp.L2);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_1_1_Brench2", parameters);
            fp.p15.dl1(); // 1.25
            fp.p15.Tgrp(fp.ECInnerDiameter); // 1.26
            parameters.Clear();
            for (var j = 0; j < fp.N; j++)
            {
                fp.p15.Tj(j, 1); // 1.27
                fp.p15.mfT(j); // 1.28
                fp.p15.DfT(j); // 1.29
                fp.p15.mfT(j); // 1.30
                fp.p15.DfT(j); // 1.31
                fp.p15.Vgvj(j); // 1.32
                fp.p15.Vcm(j); // 1.33
                fp.p15.Mcm(); // 1.34
                fp.p15.Rocm(fp.Vcm); // 1.35
                fp.p15.Re(fp.ECInnerDiameter); // 1.36
                fp.p15.f(); // 1.37
                fp.p15.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.38
                fp.p15.Hj2(j); // 1.39
                if (Math.Abs(fp.H[j] - fp.Lk) < 1.0E-5) fp.P[j] = fp.Pz;
                parameters.Add("P[j]", fp.P[j]);
            }
            p.PInBS("p15.Diagram_1_1_Brench2", parameters);
        }
        else
        {
            fp.L2 = fp.Hg1;
            fp.P2ek = fp.Pnas0;
            parameters.Clear();
            parameters.Add("L2", fp.L2);
            parameters.Add("P2ek", fp.P2ek);
            p.PInBS("p15.Diagram_1_1_Brench2", parameters);
            fp.p15.Rozh(); // 1.3
            fp.p15.Vzhek(); // 1.4
            fp.p15.Reek1(); // 1.5
            fp.p15.LambdaEk(); // 1.6
            fp.p15.Pzpk1(); // 1.7
        }
    }

    // 1.2. Алгоритм расчета давления на приеме насоса от забоя при эксплуатации ШСНУ
    public void Diagram_1_2()
    {
        p.PTxt("p15.Diagram_1_2");
        if (fp.Pnas0 < fp.Pz)
        {
            fp.p15.Vzhek(); // 1.46
            fp.p15.Rozh(); // 1.47
            fp.p15.Reek1(); // 1.48
            fp.p15.LambdaEk(); // 1.49
            fp.p15.Hg1(fp.Pz); // 1.50
            if (fp.Hg1 < fp.Lcp) 
            {
                fp.p15.Ppr2(fp.Pz); // 1.51
                fp.L1 = fp.Lcp;
                parameters.Clear();
                parameters.Add("L1", fp.L1);
                p.PInBS("p15.Diagram_1_2", parameters);
            }
            else
            {
                fp.L1 = fp.Hg1;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pnas0;
                parameters.Clear();
                parameters.Add("L1", fp.L1);
                parameters.Add("L2", fp.L2);
                parameters.Add("P1nkt", fp.P1nkt);
                p.PInBS("p15.Diagram_1_2", parameters);
            }
        }
        else
        {
            fp.L1 = fp.Lk;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pz;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("L2", fp.L2);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_1_2", parameters);
        }
        if (fp.Lcp < fp.L1)
        {
            fp.p15.dl2(); // 1.52
            fp.p15.Tgrp(fp.ECInnerDiameter); // 1.53
            parameters.Clear();
            for (var j = 0; j < fp.N; j++)
            {
                fp.p15.Tj(j, 1); // 1.54
                fp.p15.Pnasj(j); // 1.55
                fp.p15.RfP(j); // 1.56
                fp.p15.mfT(j); // 1.57
                fp.p15.DfT(j); // 1.58
                fp.p15.Vgvj(j); // 1.59
                fp.p15.Vcm(j); // 1.60
                fp.p15.Mcm(); // 1.61
                fp.p15.Rocm(fp.Vcm); // 1.62
                fp.p15.Re(fp.ECInnerDiameter); // 1.63
                fp.p15.f(); // 1.64
                fp.p15.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.65
                fp.p15.Hj3(j); // 1.66
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;                
                parameters.Add("P[j]", fp.P[j]);
            }
            p.PInBS("p15.Diagram_1_2", parameters);
        }
    } 
    
    // 1.3. Алгоритм расчета давления на выходе из насоса от устья при эксплуатации ШСНУ
 public void Diagram_1_3()
    {
        p.PTxt("p15.Diagram_1_3");
        if (fp.Pbufm < fp.Pnas0) Diagram_1_3_Brench2();
        else Diagram_1_3_Brench1();
    } 
   
    private void Diagram_1_3_Brench1()
    {
        p.PTxt("p15.Diagram_1_3_Brench1");
        fp.L1 = 0.0;
        fp.P1nkt = fp.Pbufm;
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("P1nkt", fp.P1nkt);
        p.PInBS("p15.Diagram_1_3_Brench1", parameters);
        fp.p15.Rozh(); // 1.71
        fp.p15.Vzhnkt(); // 1.72
        fp.p15.Renkt1(); // 1.73
        fp.p15.LambdaNkt(); // 1.74
        fp.p15.Pvyh1(); // 1.75
    }
   
    private void Diagram_1_3_Brench2()
    {
        p.PTxt("p15.Diagram_1_3_Brench2");
        fp.L1 = 0.0;
        fp.P1nkt = fp.Pbufm;
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("P1nkt", fp.P1nkt);
        p.PInBS("p15.Diagram_1_3_Brench2", parameters);
        fp.p15.dP(); // 1.76
        fp.p15.Tgrp(fp.dnkt); // 1.77
        parameters.Clear();
        for (var j = 0; j < fp.N; j++)
        {
            fp.p15.Tj(j, 0); // 1.78
            fp.p15.Pnasj(j); // 1.79
            fp.p15.RfP(j); // 1.80
            fp.p15.mfT(j); // 1.81
            fp.p15.DfT(j); // 1.82
            fp.p15.Vgvj(j); // 1.83
            fp.p15.Vcm(j); // 1.84
            fp.p15.Mcm(); // 1.85
            fp.p15.Rocm(fp.Vcm); // 1.86
            fp.p15.Re(fp.dnkt); // 1.87
            fp.p15.f(); // 1.88
            fp.p15.dPdH(fp.dnkt, fp.Rocm, j); // 1.89
            fp.p15.dHdP(j); // 1.90
            fp.p15.Hj1(fp.N, j, fp.dHdP); // 1.91
            fp.p15.Pj(j); // 1.92
            if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg2;
            parameters.Add("H[j]", fp.H[j]);
        }
        p.PInBS("p15.Diagram_1_3_Brench2", parameters);
        if (fp.Lcp < fp.Hg2)
        {
            fp.L1 = 0.0;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pbufm;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("L2", fp.L2);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_1_3_Brench2", parameters);
            fp.p15.dl1(); // 1.93
            fp.p15.Tgrp(fp.dnkt); // 1.94
            parameters.Clear();
            for (var j = 0; j < fp.N; j++)
            {
                fp.p15.Tj(j, 1); // 1.95
                fp.p15.mfT(j); // 1.96
                fp.p15.DfT(j); // 1.97
                fp.p15.mfT(j); // 1.98
                fp.p15.DfT(j); // 1.99
                fp.p15.Vgvj(j); // 1.100
                fp.p15.Vcm(j); // 1.101
                fp.p15.Mcm(); // 1.102
                fp.p15.Rocm(fp.Vcm); // 1.103
                fp.p15.Re(fp.dnkt); // 1.104
                fp.p15.f(); // 1.105
                fp.p15.dPdH(fp.dnkt, fp.Rocm, j); // 1.106
                fp.p15.Hj2(j); // 1.107
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;
                parameters.Add("P[j]", fp.P[j]);
            }
            p.PInBS("p15.Diagram_1_3_Brench2", parameters);
        }
        else
        {
            fp.L1 = fp.Hg2;
            fp.P1nkt = fp.Pnas0;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_1_3_Brench2", parameters);
            fp.p15.Rozh(); // 1.71
            fp.p15.Vzhnkt(); // 1.72
            fp.p15.Renkt1(); // 1.73
            fp.p15.LambdaNkt(); // 1.74
            fp.p15.Pvyh1(); // 1.75
        }
    }    
   
    // 1.4. Алгоритм расчета параметров работы насоса
    public void Diagram_1_4(double time)
    {
        p.PTxt("p15.Diagram_1_4");
        fp.p15.A2E(); // 1.109
        fp.p15.MjuE(); // 1.108, 1.110
        fp.p15.ng2(); // 1.111
        fp.p15.Wzhpr(); // 1.112
        fp.p15.Wgdr(); // 1.113
        fp.p15.Ksvh(); // 1.114
        fp.p15.Ksgs(); // 1.115
        fp.p15.Ks(); // 1.116
        fp.p15.ngn(); // 1.117
        fp.p15.Mjugzh(); // 1.118
        fp.p15.Pn1(); // 1.119
        fp.p15.Rozh(); // 1.120
        fp.p15.Hn(); // 1.121
        fp.p15.Eta1(); // 1.122
        fp.p15.Eta2(); // 1.123
        fp.p15.Kvpl(); // 1.124
        fp.p15.LMsh(); // 1.125
        fp.p15.LMsht(); // 1.126
        fp.p15.LMt(); // 1.127
        fp.p15.fsh(); // 1.128
        fp.p15.ft(); // 1.129
        fp.p15.Pzh(); // 1.130
        fp.p15.DPpl(); // 1.131
        fp.p15.Pgst(); // 1.132
        fp.p15.Pu(); // 1.133
        fp.p15.Renkt1(); // 1.134
        fp.p15.LambdaNkt(); // 1.135
        fp.p15.htren1(); // 1.136
        fp.p15.Ptren(); // 1.137
        fp.p15.hgl(); // 1.138
        fp.p15.Prgl(); // 1.139
        fp.p15.Pn2(); // 1.140
        fp.p15.Eta3(); // 1.141
        fp.p15.Eta4(); // 1.142
        fp.p15.Qut(); // 1.143
        fp.p15.Dzaz(); // 1.144
        fp.p15.Lshl(); // 1.145
        fp.p15.Etap(); // 1.146
        fp.p15.Qn(); // 1.147
        fp.p15.Pshtv(); // 1.148
        fp.p15.Pshtzh(); // 1.149
        fp.p15.Karh(); // 1.150
        fp.p15.mw(); // 1.151
        fp.p15.wkr(); // 1.152
        fp.p15.Psi(); // 1.153
        fp.p15.Pvibv(); // 1.154
        fp.p15.Pvibn(); // 1.155
        fp.p15.Pinv(); // 1.156
        fp.p15.Pinn(); // 1.157
        fp.p15.Pdinv(); // 1.158
        fp.p15.Pdinn(); // 1.159
        fp.p15.Ptrmeh(); // 1.160
        fp.p15.Ptrpl(); // 1.161
        fp.p15.Pkln(); // 1.162
        fp.p15.dpkln(); // 1.163
        fp.p15.Vmaxn(); // 1.164
        fp.p15.Pmaxsht(); // 1.165
        fp.p15.Pminsht(); // 1.166
        fp.p15.SGmax(); // 1.167
        fp.p15.SGmin(); // 1.168
        fp.p15.SGa(); // 1.169
        fp.p15.SGpr(); // 1.170
        if (fp.SGpr <= fp.SGprdop)
        {
            fp.p15.Jpol(); // 1.171
            fp.p15.Etaut(); // 1.172
            fp.p15.Jkl(); // 1.173
            fp.p15.dpklvs(); // 1.174
            fp.p15.Vmaxvs(); // 1.175
            fp.p15.Jtrm(); // 1.176
            fp.p15.Jtrg(); // 1.177
            fp.p15.Msht(); // 1.178
            fp.p15.Msht2(); // 1.179
            fp.p15.Jtrpl(); // 1.180
            fp.p15.Jpch(); // 1.181
            fp.p15.Etapch(); // 1.182
            fp.p15.Etashsnu(); // 1.183
            fp.p15.Jpoln(); // 1.184
            fp.p15.I(); // 1.185
            fp.p15.Per(); // 1.186
            fp.p15.tupr(); // 1.187
            fp.p15.Lteor(time); // 1.188-1.189
            fp.p15.Pteor(time); // 1.190-1.193
            fp.p15.Lprak(time); // 1.194
            fp.p15.tuprvv(); // 1.195
            fp.p15.tuprvn(); // 1.196
            fp.p15.Pprak(time); // 1.197-1.200
        }
    }
       
    // 1.5. Алгоритм работы скважины при эксплуатации ШСНУ
    public void Diagram_1_5(double time)
    {
        p.PTxt("p15.Diagram_1_5");
        Diagram_1_1();
        p.PTxt("p15.Diagram_1_5 - continue after p15.Diagram_1_1");
        if (fp.Pz <= fp.Pnas0)
        {
            fp.p15.ng1(fp.Pz); // 1.40
            fp.p15.nst(); // 1.41
        }
        fp.p15.Qskv1(fp.Pz, fp.rc); // 1.42
        fp.p15.MP(fp.Pz); // 1.43, 1.44
        fp.p15.nv(fp.Pz); // 1.45
        Diagram_1_2();
        p.PTxt("p15.Diagram_1_5 - continue after p15.Diagram_1_2");
        fp.p15.Pbuf1(); // 1.67
        fp.p15.Qg(fp.Q); // 1.68
        fp.p15.Pbuf2(fp.Q); // 1.69
        fp.p15.Pbufm(fp.Pbuf1, fp.Pbuf2); // 1.70
        Diagram_1_3();
        p.PTxt("p15.Diagram_1_5 - continue after p15.Diagram_1_3");
        Diagram_1_4(time);
        p.PTxt("p15.Diagram_1_5 - continue after p15.Diagram_1_4");
        fp.p15.dhdin(fp.tz1, fp.tz2); // 1.201
        fp.p15.hdin1(); // 1.202
        fp.p15.Pzatr1(); // 1.203
        if (fp.dsht <= 1.0E-5) Diagram_1_6(time);
        if (Math.Abs(fp.Qskv - fp.Qn) < 1.0E-5)
        {
            Console.WriteLine("p15.Diagram_1_5: Осуществлен выход на установившийся режим.");
            Console.WriteLine("Расчет завершен.");
            p.PTxt("p15.Diagram_1_5: Осуществлен выход на установившийся режим.");
            p.PTxt("Расчет завершен.");
            Environment.Exit(0);
        }        
    }
       
    // 1.6. Алгоритм остановки работы скважины при эксплуатации ШСНУ
    public void Diagram_1_6(double time)
    {
        p.PTxt("p15.Diagram_1_6");
        if (!PNPT1)
        {
            fp.p15.Ppr1(fp.Pzatr, fp.hdin, fp.Tu); // 1.1
            fp.p15.Rozh(); // 1.2
            PNPT1 = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            if (fp.tzab1 < time) fp.p15.Vzhek(); // 1.4
            else fp.p15.Rozh(); // 1.3
            fp.p15.Reek1(); // 1.5
            fp.p15.LambdaEk(); // 1.6
            fp.p15.Pzpk1(); // 1.7
            fp.p15.dP(); // 1.8
            fp.p15.Tgrp(fp.ECInnerDiameter); // 1.9
        }
    }
    

    // 2.1. Алгоритм расчета забойного давления через динамический уровень при освоении скважины с ШСНУ
    public void Diagram_2_1()
    {
        p.PTxt("p15.Diagram_2_1");
        fp.p15.Vzhgpl(); // 2.0
        fp.p15.Ppr1(fp.Pzatr, fp.hdin, fp.Tu); // 2.1.
        fp.p15.Rozh(); // 2.2.
        if (fp.Pbufm < fp.Pnas0) Diagram_2_1_Brench2();
        else Diagram_2_1_Brench1();
    } 
   
    private void Diagram_2_1_Brench1()
    {
        p.PTxt("p15.Diagram_2_1_Brench1");
        fp.L2 = fp.Lcp;
        fp.P2ek = fp.Ppr;
        parameters.Clear();
        parameters.Add("L2", fp.L2);
        parameters.Add("P2ek", fp.P2ek);
        p.PInBS("p15.Diagram_2_1_Brench1", parameters);
        fp.p15.Rozh(); // 2.7
        fp.p15.Vzhek(); // 2.8
        fp.p15.Reek1(); // 2.9
        fp.p15.LambdaEk(); // 2.10
        fp.p15.Pzpk1(); // 2.11
    }
   
    private void Diagram_2_1_Brench2()
    {
        p.PTxt("p15.Diagram_2_1_Brench2");
        fp.L1 = fp.Lcp;
        fp.P1nkt = fp.Ppr;
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("P1nkt", fp.P1nkt);
        p.PInBS("p15.Diagram_2_1_Brench2", parameters);
        fp.p15.dP(); // 2.12
        fp.p15.Tgrp(fp.ECInnerDiameter); // 2.13
        for (var j = 0; j < fp.N; j++)
        {
            fp.p15.Tj(j, 1); // 2.14
            fp.p15.Pnasj(j); // 2.15
            fp.p15.RfP(j); // 2.16
            fp.p15.mfT(j); // 2.17
            fp.p15.DfT(j); // 2.18
            fp.p15.Vgvj(j); // 2.19
            fp.p15.Vcm(j); // 2.20
            fp.p15.Mcm(); // 2.21
            fp.p15.Rocm(fp.Vcm); // 2.22
            fp.p15.Re(fp.ECInnerDiameter); // 2.23
            fp.p15.f(); // 2.24
            fp.p15.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 2.25
            fp.p15.dHdP(j); // 2.26
            fp.p15.Hj1(fp.N, j, fp.dHdP); // 2.27
            fp.p15.Pj(j); // 2.28
            if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg1;
        }
        if (fp.Lk < fp.Hg1)
        {
            fp.L1 = fp.Lcp;
            fp.L2 = fp.Lk;
            fp.P1nkt = fp.Ppr;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("L2", fp.L2);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_2_1_Brench2", parameters);
            fp.p15.dl1(); // 2.29
            fp.p15.Tgrp(fp.ECInnerDiameter); // 2.30
            parameters.Clear();
            for (var j = 0; j < fp.N; j++)
            {
                fp.p15.Tj(j, 1); // 2.31
                fp.p15.mfT(j); // 2.32
                fp.p15.DfT(j); // 2.33
                fp.p15.mfT(j); // 2.34
                fp.p15.DfT(j); // 2.35
                fp.p15.Vgvj(j); // 2.36
                fp.p15.Vcm(j); // 2.37
                fp.p15.Mcm(); // 2.38
                fp.p15.Rocm(fp.Vcm); // 2.39
                fp.p15.Re(fp.ECInnerDiameter); // 2.40
                fp.p15.f(); // 2.41
                fp.p15.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 2.42
                fp.p15.Hj2(j); // 2.43
                if (Math.Abs(fp.H[j] - fp.Lk) < 1.0E-5) fp.P[j] = fp.Pz;
                parameters.Add("P[j]", fp.P[j]);
            }
            p.PInBS("p15.Diagram_2_1_Brench2", parameters);
        }
        else
        {
            fp.L2 = fp.Hg1;
            fp.P2ek = fp.Pnas0;
            parameters.Clear();
            parameters.Add("L2", fp.L2);
            parameters.Add("P2ek", fp.P2ek);
            p.PInBS("p15.Diagram_2_1_Brench2", parameters);
            fp.p15.Rozh(); // 2.7
            fp.p15.Vzhek(); // 2.8
            fp.p15.Reek1(); // 2.9
            fp.p15.LambdaEk(); // 2.10
            fp.p15.Pzpk1(); // 2.11
        }
    }
    
    // 2.2. Алгоритм расчета давления на приеме насоса от забоя при освоении скважины с ШСНУ
    public void Diagram_2_2()
    {
        p.PTxt("p15.Diagram_2_2");
        if (fp.Pnas0 < fp.Pz)
        {
            fp.p15.Vzhek(); // 2.56
            fp.p15.Rozh(); // 2.57
            fp.p15.Reek1(); // 2.58
            fp.p15.LambdaEk(); // 2.59
            fp.p15.Hg1(fp.Pz); // 2.60
            if (fp.Hg1 < fp.Lcp) 
            {
                fp.p15.Ppr2(fp.Pz); // 2.61
                fp.L1 = fp.Lcp;
                parameters.Clear();
                parameters.Add("L1", fp.L1);
                p.PInBS("p15.Diagram_2_2", parameters);
            }
            else
            {
                fp.L1 = fp.Hg1;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pnas0;
                parameters.Clear();
                parameters.Add("L1", fp.L1);
                parameters.Add("L2", fp.L2);
                parameters.Add("P1nkt", fp.P1nkt);
                p.PInBS("p15.Diagram_2_2", parameters);
            }
        }
        else
        {
            fp.L1 = fp.Lk;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pz;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("L2", fp.L2);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_2_2", parameters);
        }
        if (fp.Lcp < fp.L1)
        {
            fp.p15.dl2(); // 2.62
            fp.p15.Tgrp(fp.ECInnerDiameter); // 2.63
            parameters.Clear();
            for (var j = 0; j < fp.N; j++)
            {
                fp.p15.Tj(j, 1); // 2.64
                fp.p15.Pnasj(j); // 2.65
                fp.p15.RfP(j); // 2.66
                fp.p15.mfT(j); // 2.67
                fp.p15.DfT(j); // 2.68
                fp.p15.Vgvj(j); // 2.69
                fp.p15.Vcm(j); // 2.70
                fp.p15.Mcm(); // 2.71
                fp.p15.Rocm(fp.Vcm); // 2.72
                fp.p15.Re(fp.ECInnerDiameter); // 2.73
                fp.p15.f(); // 2.74
                fp.p15.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 2.75
                fp.p15.Hj3(j); // 2.76
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;                
                parameters.Add("P[j]", fp.P[j]);
            }
            p.PInBS("p15.Diagram_2_2", parameters);
        }
    }  
    
    // 2.3. Алгоритм расчета давления на выходе из насоса от устья при освоении скважины с ШСНУ
 public void Diagram_2_3()
    {
        p.PTxt("p15.Diagram_2_3");
        if (fp.Pbufm < fp.Pnas0) Diagram_2_3_Brench2();
        else Diagram_2_3_Brench1();
    } 
   
    private void Diagram_2_3_Brench1()
    {
        p.PTxt("p15.Diagram_2_3_Brench1");
        fp.L1 = 0.0;
        fp.P1nkt = fp.Pbufm;
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("P1nkt", fp.P1nkt);
        p.PInBS("p15.Diagram_2_3_Brench1", parameters);
        fp.p15.Rozh(); // 2.85
        fp.p15.Vzhnkt(); // 2.86
        fp.p15.Renkt1(); // 2.87
        fp.p15.LambdaNkt(); // 2.88
        fp.p15.Pvyh1(); // 2.89
    }
   
    private void Diagram_2_3_Brench2()
    {
        p.PTxt("p15.Diagram_2_3_Brench2");
        fp.L1 = 0.0;
        fp.P1nkt = fp.Pbufm;
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("P1nkt", fp.P1nkt);
        p.PInBS("p15.Diagram_2_3_Brench2", parameters);
        fp.p15.dP(); // 2.90
        fp.p15.Tgrp(fp.dnkt); // 2.91
        parameters.Clear();
        for (var j = 0; j < fp.N; j++)
        {
            fp.p15.Tj(j, 0); // 2.92
            fp.p15.Pnasj(j); // 2.93
            fp.p15.RfP(j); // 2.94
            fp.p15.mfT(j); // 2.95
            fp.p15.DfT(j); // 2.96
            fp.p15.Vgvj(j); // 2.97
            fp.p15.Vcm(j); // 2.98
            fp.p15.Mcm(); // 2.99
            fp.p15.Rocm(fp.Vcm); // 2.100
            fp.p15.Re(fp.dnkt); // 2.101
            fp.p15.f(); // 2.102
            fp.p15.dPdH(fp.dnkt, fp.Rocm, j); // 2.103
            fp.p15.dHdP(j); // 2.104
            fp.p15.Hj1(fp.N, j, fp.dHdP); // 2.105
            fp.p15.Pj(j); // 2.106
            if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg2;
            parameters.Add("H[j]", fp.H[j]);
        }
        p.PInBS("p15.Diagram_2_3_Brench2", parameters);
        if (fp.Lcp < fp.Hg2)
        {
            fp.L1 = 0.0;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pbufm;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("L2", fp.L2);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_2_3_Brench2", parameters);
            fp.p15.dl1(); // 2.107
            fp.p15.Tgrp(fp.dnkt); // 2.108
            parameters.Clear();
            for (var j = 0; j < fp.N; j++)
            {
                fp.p15.Tj(j, 1); // 2.109
                fp.p15.mfT(j); // 2.110
                fp.p15.DfT(j); // 2.111
                fp.p15.mfT(j); // 2.112
                fp.p15.DfT(j); // 2.113
                fp.p15.Vgvj(j); // 2.114
                fp.p15.Vcm(j); // 2.115
                fp.p15.Mcm(); // 2.116
                fp.p15.Rocm(fp.Vcm); // 2.117
                fp.p15.Re(fp.dnkt); // 2.118
                fp.p15.f(); // 2.119
                fp.p15.dPdH(fp.dnkt, fp.Rocm, j); // 2.120
                fp.p15.Hj2(j); // 2.121
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;
                parameters.Add("P[j]", fp.P[j]);
            }
            p.PInBS("p15.Diagram_2_3_Brench2", parameters);
        }
        else
        {
            fp.L1 = fp.Hg2;
            fp.P1nkt = fp.Pnas0;
            parameters.Clear();
            parameters.Add("L1", fp.L1);
            parameters.Add("P1nkt", fp.P1nkt);
            p.PInBS("p15.Diagram_2_3_Brench2", parameters);
            fp.p15.Rozh(); // 2.85
            fp.p15.Vzhnkt(); // 2.86
            fp.p15.Renkt1(); // 2.87
            fp.p15.LambdaNkt(); // 2.88
            fp.p15.Pvyh1(); // 2.89
        }
    }    
   
    // 2.4. Алгоритм расчета параметров работы насоса
    public void Diagram_2_4(double time)
    {
        p.PTxt("p15.Diagram_2_4");
        fp.p15.A2E(); // 2.132
        fp.p15.MjuE(); // 2.131, 2.133
        fp.p15.ng2(); // 2.134
        fp.p15.Wzhpr(); // 2.135
        fp.p15.Wgdr(); // 2.136
        fp.p15.Ksvh(); // 2.137
        fp.p15.Ksgs(); // 2.138
        fp.p15.Ks(); // 2.139
        fp.p15.ngn(); // 2.140
        fp.p15.Mjugzh(); // 2.141
        fp.p15.Eta1(); // 2.142
        fp.p15.Eta2(); // 2.143
        fp.p15.Kvpl(); // 2.144
        fp.p15.LMsh(); // 2.145
        fp.p15.LMsht(); // 2.146
        fp.p15.LMt(); // 2.147
        fp.p15.fsh(); // 2.148
        fp.p15.ft(); // 2.149
        fp.p15.Pzh(); // 2.150
        fp.p15.DPpl(); // 2.151
        fp.p15.Pgst(); // 2.152
        fp.p15.Pu(); // 2.153
        fp.p15.Renkt1(); // 2.154
        fp.p15.LambdaNkt(); // 2.155
        fp.p15.htren1(); // 2.156
        fp.p15.Ptren(); // 2.157
        fp.p15.hgl(); // 2.158
        fp.p15.Prgl(); // 2.159
        fp.p15.Pn2(); // 2.160
        fp.p15.Eta3(); // 2.161
        fp.p15.Eta4(); // 2.162
        fp.p15.Qut(); // 2.163
        fp.p15.Dzaz(); // 2.164
        fp.p15.Lshl(); // 2.165
        fp.p15.Etap(); // 2.166
        fp.p15.Qn(); // 2.167
        fp.p15.Pshtv(); // 2.168
        fp.p15.Pshtzh(); // 2.169
        fp.p15.Karh(); // 2.170
        fp.p15.mw(); // 2.171
        fp.p15.wkr(); // 2.172
        fp.p15.Psi(); // 2.173
        fp.p15.Pvibv(); // 2.174
        fp.p15.Pvibn(); // 2.175
        fp.p15.Pinv(); // 2.176
        fp.p15.Pinn(); // 2.177
        fp.p15.Pdinv(); // 2.178
        fp.p15.Pdinn(); // 2.179
        fp.p15.Ptrmeh(); // 2.180
        fp.p15.Ptrpl(); // 2.181
        fp.p15.Pkln(); // 2.182
        fp.p15.dpkln(); // 2.183
        fp.p15.Vmaxn(); // 2.184
        fp.p15.Pmaxsht(); // 2.185
        fp.p15.Pminsht(); // 2.186
        fp.p15.SGmax(); // 2.187
        fp.p15.SGmin(); // 2.188
        fp.p15.SGa(); // 2.189
        fp.p15.SGpr(); // 2.190
        if (fp.SGpr <= fp.SGprdop)
        {
            fp.p15.Jpol(); // 2.191
            fp.p15.Etaut(); // 2.192
            fp.p15.Jkl(); // 2.193
            fp.p15.dpklvs(); // 2.194
            fp.p15.Vmaxvs(); // 2.195
            fp.p15.Jtrm(); // 2.196
            fp.p15.Jtrg(); // 2.197
            fp.p15.Msht(); // 2.198
            fp.p15.Msht2(); // 2.199
            fp.p15.Jtrpl(); // 2.200
            fp.p15.Jpch(); // 2.201
            fp.p15.Etapch(); // 2.202
            fp.p15.Etashsnu(); // 2.203
            fp.p15.Jpoln(); // 2.204
            fp.p15.I(); // 2.205
            fp.p15.Per(); // 2.206
            fp.p15.tupr(); // 2.207
            fp.p15.Lteor(time); // 2.208-2.209
            fp.p15.Pteor(time); // 2.210-2.213
            fp.p15.Lprak(time); // 2.214
            fp.p15.tuprvv(); // 2.215
            fp.p15.tuprvn(); // 2.216
            fp.p15.Pprak(time); // 2.217-2.220
        }
    }
    
    // 2.5. Алгоритм работы скважины с ШСНУ при освоении 
    public void Diagram_2_5(double time)
    {
        p.PTxt("p15.Diagram_2_5");
        parameters.Clear();
        parameters.Add("Vzhgpl", fp.Vzhgpl);
        parameters.Add("Vzhgzab", fp.Vzhgzab);
        p.PInBS("p15.Diagram_2_5", parameters);
        if (fp.Vzhgpl < fp.Vzhgzab) Diagram_2_5_Brench2();
        else Diagram_2_5_Brench1();
        fp.p15.Vzhgpov(fp.tz1, fp.tz2); // 2.221
        fp.p15.htr3(fp.tz1, fp.tz2); // 2.222
        fp.p15.dhdin(fp.tz1, fp.tz2); // 2.223
        fp.p15.hdin1(); // 2.224
        fp.p15.Pzatr1(); // 2.225
        if (fp.dsht <= 1.0E-5) Diagram_2_6(time);
        if (Math.Abs(fp.Qskv - fp.Qn) < 1.0E-5)
        {
            Console.WriteLine("p15.Diagram_2_5: Осуществлен выход на установившийся режим.");
            Console.WriteLine("Расчет завершен.");
            p.PTxt("p15.Diagram_2_5: Осуществлен выход на установившийся режим.");
            p.PTxt("Расчет завершен.");
            Environment.Exit(0);
        }
    }

    private void Diagram_2_5_Brench1()
    {
        p.PTxt("p15.Diagram_2_5_Brench1");
        fp.p15.Vzhek(); // 2.3
        fp.p15.Reek2(); // 2.4
        fp.p15.LambdaEk(); // 2.5
        fp.p15.Pzpk2(); // 2.6
        fp.p15.Qskv2(fp.Pz, fp.rc); // 2.47
        fp.p15.Vzhgzab(fp.tz1, fp.tz2); // 2.48
        fp.p15.MP(fp.Pz); // 2.49, 2.50
        fp.p15.nv(fp.Pz); // 2.51
        fp.p15.Vzhek(); // 2.52
        fp.p15.Reek2(); // 2.53
        fp.p15.LambdaEk(); // 2.54
        fp.p15.Ppr3(fp.Pz); // 2.55
        if (0.0 < fp.htr)
        {
            fp.Pbufm = 0.0;
            fp.p15.htr3(fp.tz1, fp.tz2); // 2.122
            fp.p15.Vzhnkt(); // 2.123
            fp.p15.Renkt2(); // 2.124
            fp.p15.LambdaNkt(); // 2.125
            fp.p15.htren2(); // 2.126
            fp.p15.Hobsh(); // 2.127
        }
        else
        {
            fp.p15.Pbuf1(); // 2.77
            fp.p15.Qg(fp.Q); // 2.78
            fp.p15.Pbuf2(fp.Q); // 2.79
            fp.p15.Pbufm(fp.Pbuf1, fp.Pbuf2); // 2.80
            fp.p15.Vzhnkt(); // 2.81
            fp.p15.Renkt2(); // 2.82
            fp.p15.LambdaNkt(); // 2.83
            fp.p15.Pvyh2(); // 2.84
            fp.p15.Pn1(); // 2.128
            fp.p15.Rozh(); // 2.129
            fp.p15.Hn(); // 2.130
        }
    }

    private void Diagram_2_5_Brench2()
    {
        p.PTxt("p15.Diagram_2_5_Brench2");
        Diagram_2_1();
        p.PTxt("p15.Diagram_2_5_Brench2 - continue after p15.Diagram_2_1");
        if (fp.Pz <= fp.Pnas0)
        {
            fp.p15.ng1(fp.Pz); // 2.44
            fp.p15.nst(); // 2.45
        }
        fp.p15.Qskv1(fp.Pz, fp.rc); // 2.46
        fp.p15.Vzhgzab(fp.tz1, fp.tz2); // 2.48
        fp.p15.MP(fp.Pz); // 2.49, 2.50
        fp.p15.nv(fp.Pz); // 2.51
        Diagram_2_2();
        p.PTxt("p15.Diagram_2_5_Brench2 - continue after p15.Diagram_2_2");
        if (0.0 < fp.htr)
        {
            fp.Pbufm = 0.0;
            fp.p15.htr3(fp.tz1, fp.tz2); // 2.122
            fp.p15.Vzhnkt(); // 2.123
            fp.p15.Renkt2(); // 2.124
            fp.p15.LambdaNkt(); // 2.125
            fp.p15.htren2(); // 2.126
            fp.p15.Hobsh(); // 2.127
        }
        else
        {
            fp.p15.Pbuf1(); // 2.77
            fp.p15.Qg(fp.Q); // 2.78
            fp.p15.Pbuf2(fp.Q); // 2.79
            fp.p15.Pbufm(fp.Pbuf1, fp.Pbuf2); // 2.80
            if (fp.Vzhgpl < fp.Vzhgzab)
            {
                Diagram_2_3();
                p.PTxt("p15.Diagram_2_5_Brench2 - continue after p15.Diagram_2_3");
            }
            else
            {
                fp.p15.Vzhnkt(); // 2.81
                fp.p15.Renkt2(); // 2.82
                fp.p15.LambdaNkt(); // 2.83
                fp.p15.Pvyh2(); // 2.84
            }
            fp.p15.Pn1(); // 2.128
            fp.p15.Rozh(); // 2.129
            fp.p15.Hn(); // 2.130
        }
    }
    
    // 2.6. Алгоритм остановки работы скважины с ШСНУ при освоении 
    public void Diagram_2_6(double time)
    {
        p.PTxt("p15.Diagram_2_6");
        if (!PNPT2)
        {
            fp.p15.DPpzo1(); // 2.226
            fp.p15.tzab1(); // 2.227
            PNPT2 = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            fp.p15.Pz(time); // 2.228, 2.229
            fp.p15.htr1(); // 2.230
            fp.p15.htr2(); // 2.231
            fp.p15.Pbuf3(); // 2.232
            fp.p15.hdin2(); // 2.233
            fp.p15.Pzatr2(); // 2.234
            fp.p15.Vost(fp.hdin); // 2.235
        }        
    }

}
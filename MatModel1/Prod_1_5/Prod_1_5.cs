using System;
using System.Collections.Generic;

public class Prod_1_5
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_5(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
        
    /// <summary>
    /// Объем жидкости глушения скважины находящейся в пласте (м3) (2.0)
    /// </summary>
    public void Vzhgpl()
    {
        parameters.Clear();
        parameters.Add("Vprom", fp.Vprom);
        parameters.Add("pi", fp.pi);
        parameters.Add("hst", fp.hst);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("2.0", parameters);
        fp.Vzhgpl = fp.Vprom + 0.25 * fp.pi * fp.hst * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2));
        p.POutC("2.0", "Vzhgpl", fp.Vzhgpl);
    }
    /// <summary>
    /// Давление на приеме в НКТ (Па) (1.1, 2.1)
    /// </summary>
    /// <param name="Pzatr">Давление на затрубе, Па</param>
    /// <param name="hdin">Динамический уровень в скважине, м</param>
    /// <param name="Tu">Температура на устье, К</param>
    public void Ppr1(double Pzatr, double hdin, double Tu)
    {
        parameters.Clear();
        parameters.Add("Pzatr", Pzatr);
        parameters.Add("hdin", hdin);
        parameters.Add("Tu", Tu);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Z", fp.Z);
        p.PInC("1.1, 2.1", parameters);
        fp.Ppr = fp.Rozh * fp.g * (fp.Lcp - hdin) * Math.Cos(fp.alfa) + 
                 Pzatr * Math.Exp(0.03415 * hdin * fp.densityOfGas / (Tu * fp.Z));
        p.POutC("1.1, 2.1", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Плотность жидкости (кг/м3) (1.2, 1.3, 1.47, 1.71, 1.120, 2.2, 2.7, 2.57, 2.85, 2.129)
    /// </summary>
    public void Rozh()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("nv", fp.nv);
        parameters.Add("densityOfWater", fp.densityOfWater);
        p.PInC("1.2, 1.3, 1.47, 1.71, 1.120, 2.2, 2.7, 2.57, 2.85, 2.129", parameters);
        fp.Rozh = fp.densityOfOil * (1.0 - fp.nv) + fp.densityOfWater * fp.nv;
        p.POutC("1.2, 1.3, 1.47, 1.71, 1.120, 2.2, 2.7, 2.57, 2.85, 2.129", "Rozh", fp.Rozh);
    }

    /// <summary>
    /// Число Рейнольдса в ЭК (1.5, 1.48, 2.9, 2.58)
    /// </summary>
    public void Reek1()
    {
        parameters.Clear();
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("oilViscosity", fp.oilViscosity);
        p.PInC("1.5, 1.48, 2.9, 2.58", parameters);
        fp.Reek = fp.Vzhek * fp.Rozh * fp.ECInnerDiameter / fp.oilViscosity;
        p.POutC("1.5, 1.48, 2.9, 2.58", "Reek", fp.Reek);
    }

    /// <summary>
    /// Скорость подъема жидкости в ЭК (м/с) (1.4, 1.46, 2.3, 2.8, 2.52, 2.56)
    /// </summary>
    public void Vzhek()
    {
        parameters.Clear();
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.4, 1.46, 2.3, 2.8, 2.52, 2.56", parameters);
        fp.Vzhek = 4.0 * fp.Qskv / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("1.4, 1.46, 2.3, 2.8, 2.52, 2.56", "Vzhek", fp.Vzhek);
    }

    /// <summary>
    /// Забойное давление, рассчитанное при помощи распределения давления (Па) (1.7, 2.11)
    /// </summary>
    public void Pzpk1()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.7, 2.11", parameters);
        fp.Pzpk = fp.Ppr + (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.7, 2.11", "Pzpk", fp.Pzpk);
    }

    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК (1.6, 1.49, 2.5, 2.10, 2.54, 2.59)
    /// </summary>
    public void LambdaEk()
    {
        parameters.Clear();
        parameters.Add("Reek", fp.Reek);
        parameters.Add("EpsEk", fp.EpsEk);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.6, 1.49, 2.5, 2.10, 2.54, 2.59", parameters);
        fp.LambdaEk = 0.067 * (158.0 / fp.Reek + 2.0 * fp.EpsEk / fp.ECInnerDiameter);
        p.POutC("1.6, 1.49, 2.5, 2.10, 2.54, 2.59", "LambdaEk", fp.LambdaEk);
    }

    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера (Па) (1.8, 1.76, 2.12, 2.90)
    /// </summary>
    public void dP()
    {
        parameters.Clear();
        parameters.Add("Pnas", fp.Pnas0);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.8, 1.76, 2.12, 2.90", parameters);
        fp.dP = (fp.Pnas0 - fp.P1nkt) / fp.Nint;
        p.POutC("1.8, 1.76, 2.12, 2.90", "dP", fp.dP);
    }
    
    /// <summary>
    /// Температурный градиент потока (К/м) (1.9, 1.26, 1.53, 1.77, 1.94, 2.13, 2.30, 2.63, 2.91, 2.108)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Tgrp(double InnerDiam)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Gtgr", fp.Gtgr);
        parameters.Add("Q", fp.Q);
        p.PInC("1.9, 1.26, 1.53, 1.77, 1.94, 2.13, 2.30, 2.63, 2.91, 2.108", parameters);
        fp.Tgrp = (0.0034 + 0.79 * fp.Gtgr) / Math.Pow(10, fp.Q / (20.0 * Math.Pow(InnerDiam, 2.67)));
        p.POutC("1.9, 1.26, 1.53, 1.77, 1.94, 2.13, 2.30, 2.63, 2.91, 2.108", "Tgrp", fp.Tgrp);
    }
    
    /// <summary> 
    /// Температура в интервале H[j] (К) (1.10, 1.27, 1.54, 1.78, 1.95, 2.14, 2.31, 2.64, 2.92, 2.109)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    /// <param name="PrEKNKT">Признак, характеризующий то, где производится расчет (1.10) (ЭК (PrEKNKT = 1) или НКТ (PrEKNKT = 0))</param>
    public void Tj(int j, int PrEKNKT)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("PrEKNKT", PrEKNKT);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgrp", fp.Tgrp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("H[j]", fp.H[j]);
        parameters.Add("Tvyh", fp.Tvyh);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("1.10, 1.27, 1.54, 1.78, 1.95, 2.14, 2.31, 2.64, 2.92, 2.109", parameters);
        if (PrEKNKT == 1) fp.T[j] = fp.Tpl - fp.Tgrp * (fp.Lk - fp.H[j]);
        else fp.T[j] = fp.Tvyh - fp.Tgrp * (fp.Lcp - fp.H[j]);
        p.POutC("1.10, 1.27, 1.54, 1.78, 1.95, 2.14, 2.31, 2.64, 2.92, 2.109", "T[j]", fp.T[j]);
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (1.11, 1.55, 1.79, 2.15, 2.65, 2.93)
    /// </summary>
    public void Pnasj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Nc", fp.Nc);
        parameters.Add("Na", fp.Na);
        p.PInC("1.11, 1.55, 1.79, 2.15, 2.65, 2.93", parameters);
        fp.Pnas[j] = fp.Pnas0 - (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
        p.POutC("1.11, 1.55, 1.79, 2.15, 2.65, 2.93", "Pnas[j]", fp.Pnas[j]);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.12, 1.56, 1.80, 2.16, 2.66, 2.94)
    /// </summary>
    public void RfP(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("Pnas[j]", fp.Pnas[j]);
        p.PInC("1.12, 1.56, 1.80, 2.16, 2.66, 2.94", parameters);
        fp.RfP = (1.0 + Math.Log10(1.0E-6 * fp.P[j])) / (1.0 + Math.Log10(1.0E-6 * fp.Pnas[j])) - 1.0;
        p.POutC("1.12, 1.56, 1.80, 2.16, 2.66, 2.94", "RfP", fp.RfP);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.13, 1.28, 1.30, 1.57, 1.81, 1.96, 1.98, 2.17, 2.32, 2.34, 2.67, 2.95, 2.110, 2.112)
    /// </summary>
    public void mfT(int j)
    {
        parameters.Clear();
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("1.13, 1.28, 1.30, 1.57, 1.81, 1.96, 1.98, 2.17, 2.32, 2.34, 2.67, 2.95, 2.110, 2.112", parameters);
        fp.mfT = 1.0 + 0.029 * (fp.T[j] - 293.0) * (fp.densityOfOil * fp.densityOfGas * 1.0E-3 - 0.7966);
        p.POutC("1.13, 1.28, 1.30, 1.57, 1.81, 1.96, 1.98, 2.17, 2.32, 2.34, 2.67, 2.95, 2.110, 2.112", "mfT", fp.mfT);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.14, 1.29, 1.31, 1.58, 1.82, 1.97, 1.99, 2.18, 2.33, 2.35, 2.68, 2.96, 2.111, 2.113)
    /// </summary>
    public void DfT(int j)
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("T[j]", fp.T[j]);
        p.PInC("1.14, 1.29, 1.31, 1.58, 1.82, 1.97, 1.99, 2.18, 2.33, 2.35, 2.68, 2.96, 2.111, 2.113", parameters);
        fp.DfT = 1.0E-3 * fp.densityOfOil * fp.densityOfGas * (4.5 - 0.00305 * (fp.T[j] - 293.0)) - 4.785;
        p.POutC("1.14, 1.29, 1.31, 1.58, 1.82, 1.97, 1.99, 2.18, 2.33, 2.35, 2.68, 2.96, 2.111, 2.113", "DfT", fp.DfT);
    }
    
    /// <summary>
    /// Удельный объем выделившегося газа (м3/м3) (1.15, 1.32, 1.59, 1.83, 1.100, 2.19, 2.36, 2.69, 2.97, 2.114)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vgvj(int j)
    {
        parameters.Clear();
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("RfP", fp.RfP);
        parameters.Add("mfT", fp.mfT);
        parameters.Add("DfT", fp.DfT);
        p.PInC("1.15, 1.32, 1.59, 1.83, 1.100, 2.19, 2.36, 2.69, 2.97, 2.114", parameters);
        fp.Vgv[j] = fp.gasFactor * fp.RfP * fp.mfT * (fp.DfT * (1 + fp.RfP) - 1.0);
        p.POutC("1.15, 1.32, 1.59, 1.83, 1.100, 2.19, 2.36, 2.69, 2.97, 2.114", "Vgv[j]", fp.Vgv[j]);
    }
    
    /// <summary>
    /// Удельный объем смеси (м3/м3) (1.16, 1.33, 1.60, 1.84, 1.101, 2.20, 2.37, 2.70, 2.98, 2.115)
    /// </summary>
    /// <param name="Vgv">Удельный объем выделившегося газа, м3/м3</param>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vcm(int j)
    {
        parameters.Clear();
        parameters.Add("Vgv[j]", fp.Vgv[j]);
        parameters.Add("T[j]", fp.T[j]);
        parameters.Add("P[j]", fp.P[j]);
        parameters.Add("bOil", fp.bOil);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("Tnu", fp.Tnu);
        parameters.Add("nv", fp.nv);
        p.PInC("1.16, 1.33, 1.60, 1.84, 1.101, 2.20, 2.37, 2.70, 2.98, 2.115", parameters);
        fp.Vcm = fp.bOil + fp.Vgv[j] * fp.Z * fp.Pnu * fp.T[j] / (fp.P[j] * fp.Tnu) + 
                 fp.nv / (1.0 - fp.nv);
        p.POutC("1.16, 1.33, 1.60, 1.84, 1.101, 2.20, 2.37, 2.70, 2.98, 2.115", "Vcm", fp.Vcm);
    }
    
    /// <summary>
    /// Удельная масса смеси (кг/м3) (1.17, 1.34, 1.61, 1.85, 1.102, 2.21, 2.38, 2.71, 2.99, 2.116)
    /// </summary>
    public void Mcm()
    {
        parameters.Clear();
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("nv", fp.nv);
        p.PInC("1.17, 1.34, 1.61, 1.85, 1.102, 2.21, 2.38, 2.71, 2.99, 2.116", parameters);
        fp.Mcm = fp.densityOfOil + fp.densityOfGas * fp.gasFactor + 
                 fp.densityOfWater * fp.nv / (1.0 - fp.nv);
        p.POutC("1.17, 1.34, 1.61, 1.85, 1.102, 2.21, 2.38, 2.71, 2.99, 2.116", "Mcm", fp.Mcm);
    }
    
    /// <summary>
    /// Плотность газожидкостной смеси (кг/м3) (1.18, 1.35, 1.62, 1.86, 1.103, 2.22, 2.39, 2.72, 2.100, 2.117)
    /// </summary>
    /// <param name="Vcm">Удельный объем смеси, м3/м3</param>
    public void Rocm(double Vcm)
    {
        parameters.Clear();
        parameters.Add("Vcm", Vcm);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.18, 1.35, 1.62, 1.86, 1.103, 2.22, 2.39, 2.72, 2.100, 2.117", parameters);
        fp.Rocm = fp.Mcm / Vcm;
        p.POutC("1.18, 1.35, 1.62, 1.86, 1.103, 2.22, 2.39, 2.72, 2.100, 2.117", "Rocm", fp.Rocm);
    }
    
    /// <summary>
    /// Число Рейнольдса (1.19, 1.36, 1.63, 1.87, 1.104, 2.23, 2.40, 2.73, 2.101, 2.118)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Re(double InnerDiam)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.19, 1.36, 1.63, 1.87, 1.104, 2.23, 2.40, 2.73, 2.101, 2.118", parameters);
        fp.Re = 0.99E-5 * 86400.0 * fp.Q * (1.0 - fp.nv) * fp.Mcm / InnerDiam;
        p.POutC("1.19, 1.36, 1.63, 1.87, 1.104, 2.23, 2.40, 2.73, 2.101, 2.118", "Re", fp.Re);
    }
    
    /// <summary>
    /// Корреляционный коэффициент (1.20, 1.37, 1.64, 1.88, 1.105, 2.24, 2.41, 2.74, 2.102, 2.119)
    /// </summary>
    public void f()
    {
        parameters.Clear();
        parameters.Add("Re", fp.Re);
        p.PInC("1.20, 1.37, 1.64, 1.88, 1.105, 2.24, 2.41, 2.74, 2.102, 2.119", parameters);
        fp.f = Math.Pow(10, 19.66 * Math.Pow(1.0 + Math.Log10(fp.Re), -0.25) - 17.13);
        p.POutC("1.20, 1.37, 1.64, 1.88, 1.105, 2.24, 2.41, 2.74, 2.102, 2.119", "f", fp.f);
    }
    
    /// <summary>
    /// Дифференциал (Па/м) (1.21, 1.38, 1.65, 1.89, 1.106, 2.25, 2.42, 2.75, 2.103, 2.120)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    /// <param name="Rocm">Плотность газожидкостной смеси, кг/м3</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dPdH(double InnerDiam, double Rocm, int j)
    {
        parameters.Clear();
        parameters.Add("InnerDiam", InnerDiam);
        parameters.Add("Rocm", Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("f", fp.f);
        parameters.Add("Q", fp.Q);
        parameters.Add("nv", fp.nv);
        parameters.Add("Mcm", fp.Mcm);
        p.PInC("1.21, 1.38, 1.65, 1.89, 1.106, 2.25, 2.42, 2.75, 2.103, 2.120", parameters);
        fp.dPdH[j] = Rocm * fp.g * Math.Cos(fp.alfa) + 
                   fp.f * Math.Pow(86400.0 * fp.Q, 2) * Math.Pow(1.0 - fp.nv, 2) * Math.Pow(fp.Mcm, 2) /
                   (2.3024E9 * Rocm * Math.Pow(InnerDiam, 5));
        p.POutC("1.21, 1.38, 1.65, 1.89, 1.106, 2.25, 2.42, 2.75, 2.103, 2.120", "dPdH[j]", fp.dPdH[j]);
    }
    
    /// <summary>
    /// Число, обратное дифференциалу (1.21) (м/Па) (1.22, 1.90, 2.26, 2.104)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dHdP(int j)
    {
        parameters.Clear();
        parameters.Add("dPdH[j]", fp.dPdH[j]);
        p.PInC("1.22, 1.90, 2.26, 2.104", parameters);
        fp.dHdP[j] = 1.0 / fp.dPdH[j];
        p.POutC("1.22, 1.90, 2.26, 2.104", "dHdP[j]", fp.dHdP[j]);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.23, 1.91, 2.27, 2.105)
    /// </summary>
    /// <param name="N">Количество элементов в массиве dHdP, шт.</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    /// <param name="dHdP">Массив значений дифференциала dH/dP на разных интервалах Hj</param>
    public void Hj1(int N, int j, double[] dHdP)
    {
        parameters.Clear();
        parameters.Add("N", N);
        var sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            parameters.Add("i", i);
            parameters.Add("dHdP[i]", dHdP[i]);
            sum += dHdP[i];
        }
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        parameters.Add("dHdP[1]", dHdP[1]);
        parameters.Add("j", j);
        parameters.Add("dHdP[j]", dHdP[j]);
        p.PInC("1.23, 1.91, 2.27, 2.105", parameters);
        fp.H[j] = fp.L1 + fp.dl * ((dHdP[1] + dHdP[j]) / 2.0 + sum);
        p.POutC("1.23, 1.91, 2.27, 2.105", "H[j]", fp.H[j]);
    }
    
    /// <summary>
    /// Давление в интервале Нj (Па) (1.24, 1.92, 2.28, 2.106)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pj(int j)
    {
        parameters.Clear();
        parameters.Add("j", j);
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("dP", fp.dP);
        p.PInC("1.24, 1.92, 2.28, 2.106", parameters);
        fp.P[j] = fp.P1nkt + fp.dP * j;
        p.POutC("1.24, 1.92, 2.28, 2.106", "P[j]", fp.P[j]);
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (1.25, 1.93, 2.29, 2.107)
    /// </summary>
    public void dl1()
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("L2", fp.L2);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.25, 1.93, 2.29, 2.107", parameters);
        fp.dl = (fp.L2 - fp.L1) / fp.Nint;
        p.POutC("1.25, 1.93, 2.29, 2.107", "dl", fp.dl);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.39, 1.107, 2.43, 2.121)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Hj2(int j)
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        parameters.Add("j", j);
        p.PInC("1.39, 1.107, 2.43, 2.121", parameters);
        fp.H[j] = fp.L1 + fp.dl * j;
        p.POutC("1.39, 1.107, 2.43, 2.121", "H[j]", fp.H[j]);
    }

    /// <summary>
    /// Показатель степени в формуле (1.42) (д. ед.) (1.41, 2.45)
    /// </summary>
    public void nst()
    {
        parameters.Clear();
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("nst", fp.nst);
        p.PInC("1.41, 2.45", parameters);
        if (fp.Pnas0 < fp.Pz) fp.nst = 1.0;
        else fp.nst = 1.0 - 0.03 * fp.ng;
        p.POutC("1.41, 2.45", "nst", fp.nst);
    }

    /// <summary>
    /// Количество свободного газа (д. ед.) (1.40, 2.44)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void ng1(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("gasFactor", fp.gasFactor);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("nv", fp.nv);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Z", fp.Z);
        parameters.Add("Tnu", fp.Tnu);
        p.PInC("1.40, 2.44", parameters);
        var tempVar = fp.gasFactor * (Pz - fp.Pnas0) / (fp.Pnu - fp.Pnas0) * 
            (1.0 - fp.nv) * fp.Pnu * fp.reservoirPressurePa * fp.Z / (Pz * fp.Tnu);
        fp.ng = tempVar / (1.0 + tempVar);
        p.POutC("1.40, 2.44", "ng", fp.ng);
    }

    /// <summary>
    /// Расход жидкости, поступающей из пласта  (м3 / с) (1.42, 2.46)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void Qskv1(double Pz, double rc)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("nst", fp.nst);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.42, 2.46", parameters);
        fp.Qskv = 2.0 * fp.pi * fp.k * fp.h * Math.Pow(fp.reservoirPressurePa - Pz, fp.nst) / 
                  (fp.oilViscosity * Math.Log(fp.Rk / (rc * Math.Exp(-fp.skinFactor))));
        p.POutC("1.42, 2.46", "Qskv", fp.Qskv);
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (1.43, 1.44; 2.49, 2.50)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("deltaPpred", fp.deltaPpred);
        parameters.Add("A1", fp.A1);
        parameters.Add("A2", fp.A2);
        p.PInC("1.43, 1.44; 2.49, 2.50", parameters);
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
        p.POutC("1.43, 1.44; 2.49, 2.50", "MP", fp.MP);
    }

    /// <summary>
    /// Обводненность продукции (д. ед.) (1.45, 2.51)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void nv(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("productWaterCut0", fp.productWaterCut0);
        parameters.Add("productWaterCutMax", fp.productWaterCutMax);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("DPmax", fp.DPmax);
        p.PInC("1.45, 2.51", parameters);
        fp.nv = fp.productWaterCut0 + (fp.productWaterCutMax - fp.productWaterCut0) * 
            Math.Pow(fp.reservoirPressurePa - Pz, 2) / fp.DPmax;
        p.POutC("1.45, 2.51", "nv", fp.nv);
    }

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ (м) (1.50, 2.60)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg1(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pnas0", fp.Pnas0);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.50, 2.60", parameters);
        fp.Hg1 = fp.Lk - (Pz - fp.Pnas0) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                            fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.50, 2.60", "Hg1", fp.Hg1);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (Па) (1.51, 2.61)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ppr2(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Hg1", fp.Hg1);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("1.51, 2.61", parameters);
        if (fp.Hg1 < fp.Lcp) fp.Ppr = Pz - (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("1.51, 2.61", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (1.52, 2.62)
    /// </summary>
    public void dl2()
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("L2", fp.L2);
        parameters.Add("Nint", fp.Nint);
        p.PInC("1.52, 2.62", parameters);
        fp.dl = (fp.L1 - fp.L2) / fp.Nint;
        p.POutC("1.52, 2.62", "dl", fp.dl);
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.66, 2.76)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Hj3(int j)
    {
        parameters.Clear();
        parameters.Add("L1", fp.L1);
        parameters.Add("dl", fp.dl);
        parameters.Add("j", j);
        p.PInC("1.66, 2.76", parameters);
        fp.H[j] = fp.L1 - fp.dl * j;
        p.POutC("1.66, 2.76", "H[j]", fp.H[j]);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле (Па) (1.67, 2.77)
    /// </summary>
    public void Pbuf1()
    {
        parameters.Clear();
        parameters.Add("Qg", fp.Qg);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        p.PInC("1.67, 2.77", parameters);
        fp.Pbuf1 = 0.0729 * Math.Pow(1.0, 2) * fp.Qg * 86400.0 * fp.densityOfGas * fp.Plin /
                   Math.Pow(1000.0 * fp.dsht, 2);
        p.POutC("1.67, 2.77", "Pbuf1", fp.Pbuf1);
    }
    
    /// <summary>
    /// Расход газа (м3/с) (1.68, 2.78)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Qg(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Vgvust", fp.Vgvust);
        parameters.Add("nv", fp.nv);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pbufm", fp.Pbufm);
        parameters.Add("Tst", fp.Tst);
        p.PInC("1.68, 2.78", parameters);
        fp.Qg = Q * fp.Vgvust * (1.0 - fp.nv) * fp.Pst * fp.Tbuf * fp.Z / (fp.Pbufm * fp.Tst);
        p.POutC("1.68, 2.78", "Qg", fp.Qg);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле (Па) (1.69, 2.79)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Pbuf2(double Q)
    {
        parameters.Clear();
        parameters.Add("Q", Q);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        parameters.Add("g", fp.g);
        p.PInC("1.69, 2.79", parameters);
        fp.Pbuf2 = fp.Plin + Math.Pow(Q, 2) / (78.8768 * Math.Pow(1000.0 * fp.dsht, 4) * fp.g);
        p.POutC("1.69, 2.79", "Pbuf2", fp.Pbuf2);
    }
    
    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по формулам (Па) (1.70, 2.80)
    /// </summary>
    /// <param name="Pbuf1">Буферное давление, рассчитанное по первой формуле, Па</param>
    /// <param name="Pbuf2">Буферное давление, рассчитанное по второй формуле, Па</param>
    public void Pbufm(double Pbuf1, double Pbuf2)
    {
        parameters.Clear();
        parameters.Add("Pbuf1", Pbuf1);
        parameters.Add("Pbuf2", Pbuf2);
        p.PInC("1.70, 2.80", parameters);
        fp.Pbufm = Math.Max(Pbuf1, Pbuf2);
        p.POutC("1.70, 2.80", "Pbufm", fp.Pbufm);
    }
    
    /// <summary>
    /// Скорость подъема жидкости в НКТ (м/с) (1.72, 2.81, 2.86, 2.123)
    /// </summary>
    public void Vzhnkt()
    {
        parameters.Clear();
        parameters.Add("Qn", fp.Qn);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.72, 2.81, 2.86, 2.123", parameters);
        fp.Vzhnkt = 4.0 * fp.Qn / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("1.72, 2.81, 2.86, 2.123", "Vzhnkt", fp.Vzhnkt);
    }
    
    /// <summary>
    /// Число Рейнольдса в НКТ (1.73, 1.134, 2.87, 2.154)
    /// </summary>
    public void Renkt1()
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Mjuzh", fp.Mjuzh);
        p.PInC("1.73, 1.134, 2.87, 2.154", parameters);
        fp.Renkt = fp.Vzhnkt * fp.Rozh * fp.dnkt / fp.Mjuzh;
        p.POutC("1.73, 1.134, 2.87, 2.154", "Renkt", fp.Renkt);
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в НКТ (1.74, 1.135, 2.83, 2.88, 2.125, 2.155)
    /// </summary>
    public void LambdaNkt()
    {
        parameters.Clear();
        parameters.Add("Renkt", fp.Renkt);
        parameters.Add("EpsNkt", fp.EpsNkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.74, 1.135, 2.83, 2.88, 2.125, 2.155", parameters);
        fp.LambdaNkt = 0.067 * (158.0 / fp.Renkt + 2.0 * fp.EpsNkt / fp.dnkt);
        p.POutC("1.74, 1.135, 2.83, 2.88, 2.125, 2.155", "LambdaNkt", fp.LambdaNkt);
    }
    
    /// <summary>
    /// Давление на выходе из насоса (Па) (1.75, 2.89)
    /// </summary>
    public void Pvyh1()
    {
        parameters.Clear();
        parameters.Add("P1nkt", fp.P1nkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("L1", fp.L1);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.75, 2.89", parameters);
        fp.Pvyh = fp.P1nkt + (fp.Lcp - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                              fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
        p.POutC("1.75, 2.89", "Pvyh", fp.Pvyh);
    }
    
    /// <summary>
    /// Вязкость эмульсии (Па•с) (1.108, 1.110; 2.131, 2.133)
    /// </summary>
    public void MjuE()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        parameters.Add("WaterViscosity", fp.WaterViscosity);
        parameters.Add("A2E", fp.A2E);
        parameters.Add("oilViscosity", fp.oilViscosity);
        if (0.5 < fp.nv) fp.MjuE = fp.WaterViscosity * Math.Pow(10.0, 3.2 * (1.0 - fp.nv));
        else
        {
            if (1.0 < fp.A2E) fp.B2E = fp.A2E * fp.oilViscosity;
            else fp.B2E = fp.A2E;
            parameters.Add("B2E", fp.B2E);
            fp.MjuE = fp.B2E * (1.0 + 2.9 * fp.nv) / (1.0 - fp.nv);
        }
        p.PInC("1.108, 1.110; 2.131, 2.133", parameters);
        p.POutC("1.108, 1.110; 2.131, 2.133", "MjuE", fp.MjuE);
    }
    
    /// <summary>
    /// Параметр, учитывающий скорость сдвига (1.109, 2.132)
    /// </summary>
    public void A2E()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        p.PInC("1.109, 2.132", parameters);
        fp.A2E = (1.0 + 20.0 * Math.Pow(fp.nv, 2)) * Math.Pow(80, -0.48 * fp.nv);
        p.POutC("1.109, 2.132", "A2E", fp.A2E);
    }

    /// <summary>
    /// Количество свободного газа (д. ед.) (1.111, 2.134)
    /// </summary>
    public void ng2()
    {
        parameters.Clear();
        parameters.Add("Vgvpr", fp.Vgvpr);
        parameters.Add("nv", fp.nv);
        parameters.Add("Pnu", fp.Pnu);
        parameters.Add("Tnu", fp.Tnu);
        parameters.Add("Tprn", fp.Tprn);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pprn", fp.Pprn);
        p.PInC("1.111, 2.134", parameters);
        var tempVar = fp.Vgvpr * (1.0 - fp.nv) * fp.Pnu * fp.Tprn * fp.Z / (fp.Pprn * fp.Tnu);
        fp.ng = tempVar / (1.0 + tempVar);
        p.POutC("1.111, 2.134", "ng", fp.ng);
    }
    
    /// <summary>
    /// Приведенная скорость жидкости (м/с) (1.112, 2.135)
    /// </summary>
    public void Wzhpr()
    {
        parameters.Clear();
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("nv", fp.nv);
        parameters.Add("bOil", fp.bOil);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("1.112, 2.135", parameters);
        fp.Wzhpr = 4.0 * fp.Qskv * (fp.nv + fp.bOil * (1.0 - fp.nv)) / 
                   (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2) * fp.pi);
        p.POutC("1.112, 2.135", "Wzhpr", fp.Wzhpr);
    }
    
    /// <summary>
    /// Скорость дрейфа газа (м/с) (1.113, 2.136)
    /// </summary>
    public void Wgdr()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        p.PInC("1.113, 2.136", parameters);
        fp.Wgdr = 0.17;
        if (fp.nv <= 0.5) fp.Wgdr = 0.02;
        p.POutC("1.113, 2.136", "Wgdr", fp.Wgdr);
    }
    
    /// <summary>
    /// Коэффициент сепарации газа при переходе откачиваемой продукции из кольцевого пространства скважины во всасывающую камеру насоса (1.114, 2.137)
    /// </summary>
    public void Ksvh()
    {
        parameters.Clear();
        parameters.Add("Wzhpr", fp.Wzhpr);
        parameters.Add("Wgdr", fp.Wgdr);
        parameters.Add("ng", fp.ng);
        p.PInC("1.114, 2.137", parameters);
        fp.Ksvh = 1.0 / (1.0 + 0.52 * fp.Wzhpr / (fp.Wgdr * (1.0 - 0.06 * fp.ng)));
        p.POutC("1.114, 2.137", "Ksvh", fp.Ksvh);
    }
    
    /// <summary>
    /// Коэффициент сепарации за счет работы газосепаратора, если такой установлен (1.115, 2.138)
    /// </summary>
    public void Ksgs()
    {
        parameters.Clear();
        parameters.Add("nv", fp.nv);
        p.PInC("1.115, 2.138", parameters);
        fp.Ksgs = 0.8;
        if (fp.nv <= 0.5) fp.Ksgs = 0.6;
        p.POutC("1.115, 2.138", "Ksgs", fp.Ksgs);
    }
    
    /// <summary>
    /// Коэффициент сепарации (1.116, 2.139)
    /// </summary>
    public void Ks()
    {
        parameters.Clear();
        parameters.Add("Ksvh", fp.Ksvh);
        parameters.Add("Ksgs", fp.Ksgs);
        p.PInC("1.116, 2.139", parameters);
        fp.Ks = 1.0 - (1.0 - fp.Ksvh) * (1.0 - fp.Ksgs);
        p.POutC("1.116, 2.139", "Ks", fp.Ks);
    }
    
    /// <summary>
    /// Количество свободного газа, поступившего в насос (д. ед.) (1.117, 2.140)
    /// </summary>
    public void ngn()
    {
        parameters.Clear();
        parameters.Add("Ks", fp.Ks);
        parameters.Add("ng", fp.ng);
        p.PInC("1.117, 2.140", parameters);
        fp.ngn = fp.Ks * fp.ng;
        p.POutC("1.117, 2.140", "ngn", fp.ngn);
    }
    
    /// <summary>
    /// Вязкость газожидкостной смеси (Па*с) (1.118, 2.141)
    /// </summary>
    public void Mjugzh()
    {
        parameters.Clear();
        parameters.Add("MjuE", fp.MjuE);
        parameters.Add("ngn", fp.ngn);
        p.PInC("1.118, 2.141", parameters);
        fp.Mjugzh = (0.023 + 0.71 * fp.MjuE) * (1.0 - fp.ngn);
        p.POutC("1.118, 2.141", "Mjugzh", fp.Mjugzh);
    }
    
    /// <summary>
    /// Давление, развиваемое насосом (Па) (1.119, 2.128)
    /// </summary>
    public void Pn1()
    {
        parameters.Clear();
        parameters.Add("Pvyh", fp.Pvyh);
        parameters.Add("Pprn", fp.Pprn);
        p.PInC("1.119, 2.128", parameters);
        fp.Pn = fp.Pvyh - fp.Pprn;
        p.POutC("1.119, 2.128", "Pn", fp.Pn);
    }
    
    /// <summary>
    /// Напор насоса (Н) (1.121, 2.130)
    /// </summary>
    public void Hn()
    {
        parameters.Clear();
        parameters.Add("Prn", fp.Prn);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        p.PInC("1.121, 2.130", parameters);
        fp.Hn = fp.Prn / (fp.Rozh * fp.g);
        p.POutC("1.121, 2.130", "Hn", fp.Hn);
    }
    
    /// <summary>
    /// Коэффициент наполнения насоса жидкостью, учитывающий влияние свободного газа (1.122, 2.142)
    /// </summary>
    public void Eta1()
    {
        parameters.Clear();
        parameters.Add("ngn", fp.ngn);
        p.PInC("1.122, 2.142", parameters);
        fp.Eta1 = 1.0 - fp.ngn;
        p.POutC("1.122, 2.142", "Eta1", fp.Eta1);
    }
    
    /// <summary>
    /// Коэффициент наполнения насоса жидкостью, учитывающий влияние потери хода плунжера (1.123, 2.143)
    /// </summary>
    public void Eta2()
    {
        parameters.Clear();
        parameters.Add("Kvpl", fp.Kvpl);
        parameters.Add("S", fp.S);
        parameters.Add("LMsh", fp.LMsh);
        p.PInC("1.123, 2.143", parameters);
        fp.Eta2 = (fp.Kvpl * fp.S - fp.LMsh) / fp.S;
        p.POutC("1.123, 2.143", "Eta2", fp.Eta2);
    }
    
    /// <summary>
    /// Коэффициент выигрыша хода плунжера (1.124, 2.144)
    /// </summary>
    public void Kvpl()
    {
        parameters.Clear();
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("nk", fp.nk);
        p.PInC("1.124, 2.144", parameters);
        fp.Kvpl = 1.0 + 7.474E-7 * Math.Pow(fp.Lcp, 2) * Math.Pow(fp.nk, 2);
        p.POutC("1.124, 2.144", "Kvpl", fp.Kvpl);
    }
    
    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций штанг и труб (м) (1.125, 2.145)
    /// </summary>
    public void LMsh()
    {
        parameters.Clear();
        parameters.Add("LMsht", fp.LMsht);
        parameters.Add("LMt", fp.LMt);
        parameters.Add("dLM", fp.dLM);
        p.PInC("1.125, 2.145", parameters);
        fp.LMsh = fp.LMsht + fp.LMt + fp.dLM;
        p.POutC("1.125, 2.145", "LMsh", fp.LMsh);
    }
    
    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций штанг (м) (1.126, 2.146)
    /// </summary>
    public void LMsht()
    {
        parameters.Clear();
        parameters.Add("Pzh", fp.Pzh);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("fsh", fp.fsh);
        parameters.Add("Esh", fp.Esh);
        p.PInC("1.126, 2.146", parameters);
        fp.LMsht = fp.Pzh * fp.Lcp / (fp.fsh * fp.Esh);
        p.POutC("1.126, 2.146", "LMsht", fp.LMsht);
    }
    
    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций труб (м) (1.127, 2.147)
    /// </summary>
    public void LMt()
    {
        parameters.Clear();
        parameters.Add("Pzh", fp.Pzh);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("ft", fp.ft);
        parameters.Add("Et", fp.Et);
        p.PInC("1.127, 2.147", parameters);
        fp.LMt = fp.Pzh * fp.Lcp / (fp.ft * fp.Et);
        p.POutC("1.127, 2.147", "LMt", fp.LMt);
    }
    
    /// <summary>
    /// Площадь сечения штанг (м2) (1.128, 2.148)
    /// </summary>
    public void fsh()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("dsh", fp.dsh);
        p.PInC("1.128, 2.148", parameters);
        fp.fsh = fp.pi * Math.Pow(fp.dsh, 2);
        p.POutC("1.128, 2.148", "fsh", fp.fsh);
    }
    
    /// <summary>
    /// Площадь сечения штанг (м2) (1.129, 2.149)
    /// </summary>
    public void ft()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Dnkt", fp.Dnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.129, 2.149", parameters);
        fp.ft = fp.pi * (Math.Pow(fp.Dnkt, 2) - Math.Pow(fp.dnkt, 2)) / 4.0;
        p.POutC("1.129, 2.149", "ft", fp.ft);
    }
    
    /// <summary>
    /// Вес столба жидкости, действующий на плунжер (Н) (1.130, 2.150)
    /// </summary>
    public void Pzh()
    {
        parameters.Clear();
        parameters.Add("Fpl", fp.Fpl);
        parameters.Add("DPpl", fp.DPpl);
        p.PInC("1.130, 2.150", parameters);
        fp.Pzh = fp.Fpl * fp.DPpl;
        p.POutC("1.130, 2.150", "Pzh", fp.Pzh);
    }
    
    /// <summary>
    /// Разность давлений, действующих на поверхность плунжера (Па) (1.131, 2.151)
    /// </summary>
    public void DPpl()
    {
        parameters.Clear();
        parameters.Add("Pn", fp.Pn);
        parameters.Add("Ppr", fp.Ppr);
        p.PInC("1.131, 2.151", parameters);
        fp.DPpl = fp.Pn - fp.Ppr;
        p.POutC("1.131, 2.151", "DPpl", fp.DPpl);
    }
    
    /// <summary>
    /// Гидростатическое давление столба жидкости (Па) (1.132, 2.152)
    /// </summary>
    public void Pgst()
    {
        parameters.Clear();
        parameters.Add("Rocm", fp.Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.132, 2.152", parameters);
        fp.Pgst = fp.Rocm * fp.g * fp.Lcp * Math.Cos(fp.alfa);
        p.POutC("1.132, 2.152", "Pgst", fp.Pgst);
    }
    
    /// <summary>
    /// Давление на устье (Па) (1.133, 2.153)
    /// </summary>
    public void Pu()
    {
        parameters.Clear();
        parameters.Add("Pbufm", fp.Pbufm);
        p.PInC("1.133, 2.153", parameters);
        fp.Pu = fp.Pbufm;
        p.POutC("1.133, 2.153", "Pu", fp.Pu);
    }
    
    /// <summary>
    /// Напор на преодоление гидравлического трения (м) (1.136, 2.156)
    /// </summary>
    public void htren1()
    {
        parameters.Clear();
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("g", fp.g);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("1.136, 2.156", parameters);
        fp.htren = fp.LambdaNkt * fp.Lcp * Math.Pow(fp.Vzhnkt, 2) / (2.0 * fp.g * fp.dnkt);
        p.POutC("1.136, 2.156", "htren", fp.htren);
    }
    
    /// <summary>
    /// Потери давления на трение жидкости в трубах (Па) (1.137, 2.157)
    /// </summary>
    public void Ptren()
    {
        parameters.Clear();
        parameters.Add("Rocm", fp.Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("htren", fp.htren);
        p.PInC("1.137, 2.157", parameters);
        fp.Ptren = fp.Rocm * fp.g * fp.htren;
        p.POutC("1.137, 2.157", "Ptren", fp.Ptren);
    }
    
    /// <summary>
    /// Напор, соответствующий газлифтному эффекту (м) (1.138, 2.158)
    /// </summary>
    public void hgl()
    {
        parameters.Clear();
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("gFactorMdl", fp.gFactorMdl);
        parameters.Add("Pbufm", fp.Pbufm);
        parameters.Add("nv", fp.nv);
        p.PInC("1.138, 2.158", parameters);
        fp.hgl = 4.0 * fp.dnkt * fp.gFactorMdl * (1.0 - Math.Pow(fp.Pbufm, 1.0 / 3.0)) * (1.0 - fp.nv) / 0.0254;
        p.POutC("1.138, 2.158", "hgl", fp.hgl);
    }
    
    /// <summary>
    /// Давление разгрузки в результате газлифтного эффекта (Па) (1.139, 2.159)
    /// </summary>
    public void Prgl()
    {
        parameters.Clear();
        parameters.Add("Rocm", fp.Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("hgl", fp.hgl);
        p.PInC("1.139, 2.159", parameters);
        fp.Prgl = fp.Rocm * fp.g * fp.hgl;
        p.POutC("1.139, 2.159", "Prgl", fp.Prgl);
    }
    
    /// <summary>
    /// Давление над плунжером насоса  (Па) (1.140, 2.160)
    /// </summary>
    public void Pn2()
    {
        parameters.Clear();
        parameters.Add("Pbufm", fp.Pbufm);
        parameters.Add("Rocm", fp.Rocm);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("htren", fp.htren);
        parameters.Add("hgl", fp.hgl);
        p.PInC("1.140, 2.160", parameters);
        fp.Pn = fp.Pbufm + fp.Rocm * fp.g * (fp.Lcp * Math.Cos(fp.alfa) + fp.htren - fp.hgl);
        p.POutC("1.140, 2.160", "Pn", fp.Pn);
    }
    
    /// <summary>
    /// Коэффициент усадки жидкости (1.141, 2.161)
    /// </summary>
    public void Eta3()
    {
        parameters.Clear();
        parameters.Add("bOil", fp.bOil);
        parameters.Add("nv", fp.nv);
        p.PInC("1.141, 2.161", parameters);
        fp.Eta3 = 1.0 / (fp.bOil * (1.0 - fp.nv) + fp.nv);
        p.POutC("1.141, 2.161", "Eta3", fp.Eta3);
    }
    
    /// <summary>
    /// Коэффициент утечек (1.142, 2.162)
    /// </summary>
    public void Eta4()
    {
        parameters.Clear();
        parameters.Add("Qut", fp.Qut);
        parameters.Add("Fpl", fp.Fpl);
        parameters.Add("S", fp.S);
        parameters.Add("nk", fp.nk);
        parameters.Add("Eta1", fp.Eta1);
        parameters.Add("Eta2", fp.Eta2);
        parameters.Add("Eta3", fp.Eta3);
        p.PInC("1.142, 2.162", parameters);
        fp.Eta4 = 1.0 - fp.Qut / (fp.Fpl * fp.S * fp.nk * fp.Eta1 * fp.Eta2 * fp.Eta3);
        p.POutC("1.142, 2.162", "Eta4", fp.Eta4);
    }
    
    /// <summary>
    /// Объем жидкости, протекающий через зазор между плунжером и цилиндром (м3/с) (1.143, 2.163)
    /// </summary>
    public void Qut()
    {
        parameters.Clear();
        parameters.Add("DPpl", fp.DPpl);
        parameters.Add("Dzaz", fp.Dzaz);
        parameters.Add("Lshl", fp.Lshl);
        parameters.Add("Lpl", fp.Lpl);
        parameters.Add("Mjugzh", fp.Mjugzh);
        parameters.Add("DQosl", fp.DQosl);
        p.PInC("1.143, 2.163", parameters);
        fp.Qut = fp.DPpl * Math.Pow(fp.Dzaz, 3) * fp.Lshl / (12.0 * fp.Lpl * fp.Mjugzh) + fp.DQosl;
        p.POutC("1.143, 2.163", "Qut", fp.Qut);
    }
    
    /// <summary>
    /// Ширина зазора между плунжером и цилиндром (м) (1.144, 2.164)
    /// </summary>
    public void Dzaz()
    {
        parameters.Clear();
        parameters.Add("Dc", fp.Dc);
        parameters.Add("dpl", fp.dpl);
        p.PInC("1.144, 2.164", parameters);
        fp.Dzaz = (fp.Dc - fp.dpl) / 2.0;
        p.POutC("1.144, 2.164", "Dzaz", fp.Dzaz);
    }
    
    /// <summary>
    /// Длина щели (м) (1.145, 2.165)
    /// </summary>
    public void Lshl()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("dpl", fp.dpl);
        p.PInC("1.145, 2.165", parameters);
        fp.Lshl = fp.pi * fp.dpl;
        p.POutC("1.145, 2.165", "Lshl", fp.Lshl);
    }
    
    /// <summary>
    /// Коэффициент подачи насосной установки (1.146, 2.166)
    /// </summary>
    public void Etap()
    {
        parameters.Clear();
        parameters.Add("Eta1", fp.Eta1);
        parameters.Add("Eta2", fp.Eta2);
        parameters.Add("Eta3", fp.Eta3);
        parameters.Add("Eta4", fp.Eta4);
        p.PInC("1.146, 2.166", parameters);
        fp.Etap = fp.Eta1 * fp.Eta2 * fp.Eta3 * fp.Eta4;
        p.POutC("1.146, 2.166", "Etap", fp.Etap);
    }
    
    /// <summary>
    /// Производительность насоса (м3/с) (1.147, 2.167)
    /// </summary>
    public void Qn()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("dpl", fp.dpl);
        parameters.Add("S", fp.S);
        parameters.Add("nk", fp.nk);
        parameters.Add("Etap", fp.Etap);
        p.PInC("1.147, 2.167", parameters);
        fp.Qn = fp.pi * Math.Pow(fp.dpl, 2) * fp.S * fp.nk * fp.Etap / 4.0;
        p.POutC("1.147, 2.167", "Qn", fp.Qn);
    }
    
    /// <summary>
    /// Вес колонны штанг в воздухе (Н) (1.148, 2.168)
    /// </summary>
    public void Pshtv()
    {
        parameters.Clear();
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("qsht", fp.qsht);
        p.PInC("1.148, 2.168", parameters);
        fp.Pshtv = fp.Lcp * fp.qsht;
        p.POutC("1.148, 2.168", "Pshtv", fp.Pshtv);
    }
    
    /// <summary>
    /// Вес колонны штанг в жидкости (Н) (1.149, 2.169)
    /// </summary>
    public void Pshtzh()
    {
        parameters.Clear();
        parameters.Add("Karh", fp.Karh);
        parameters.Add("Pshtv", fp.Pshtv);
        p.PInC("1.149, 2.169", parameters);
        fp.Pshtzh = fp.Karh * fp.Pshtv;
        p.POutC("1.149, 2.169", "Pshtzh", fp.Pshtzh);
    }
    
    /// <summary>
    /// Коэффициент плавучести штанг (1.150, 2.170)
    /// </summary>
    public void Karh()
    {
        parameters.Clear();
        parameters.Add("Rosht", fp.Rosht);
        parameters.Add("Rocm", fp.Rocm);
        p.PInC("1.150, 2.170", parameters);
        fp.Karh = (fp.Rosht - fp.Rocm) / fp.Rosht;
        p.POutC("1.150, 2.170", "Karh", fp.Karh);
    }
    
    /// <summary>
    /// Вспомогательный коэффициент (1.151, 2.171)
    /// </summary>
    public void mw()
    {
        parameters.Clear();
        parameters.Add("wkr", fp.wkr);
        parameters.Add("S", fp.S);
        parameters.Add("g", fp.g);
        p.PInC("1.151, 2.171", parameters);
        fp.mw = Math.Sqrt(Math.Pow(fp.wkr, 2) * fp.S / fp.g);
        p.POutC("1.151, 2.171", "mw", fp.mw);
    }
    
    /// <summary>
    /// Угловая скорость вращения вала кривошипа (рад/с) (1.152, 2.172)
    /// </summary>
    public void wkr()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("nk", fp.nk);
        p.PInC("1.152, 2.172", parameters);
        fp.wkr = 2.0 * fp.pi * fp.nk;
        p.POutC("1.152, 2.172", "wkr", fp.wkr);
    }
    
    /// <summary>
    /// Вспомогательный коэффициент (1.153, 2.173)
    /// </summary>
    public void Psi()
    {
        parameters.Clear();
        parameters.Add("LMsht", fp.LMsht);
        parameters.Add("LMt", fp.LMt);
        p.PInC("1.153, 2.173", parameters);
        fp.Psi = fp.LMsht / (fp.LMsht + fp.LMt);
        p.POutC("1.153, 2.173", "Psi", fp.Psi);
    }
    
    /// <summary>
    /// Вибрационная нагрузка при ходе вверх (Н) (1.154, 2.174)
    /// </summary>
    public void Pvibv()
    {
        parameters.Clear();
        parameters.Add("aln1", fp.aln1);
        parameters.Add("mw", fp.mw);
        parameters.Add("an1", fp.an1);
        parameters.Add("Psi", fp.Psi);
        parameters.Add("LMsht", fp.LMsht);
        parameters.Add("S", fp.S);
        parameters.Add("Pshtv", fp.Pshtv);
        parameters.Add("Pzh", fp.Pzh);
        p.PInC("1.154, 2.174", parameters);
        fp.Pvibv = Math.Pow(fp.aln1, 2) * fp.mw * Math.Sqrt((fp.an1 * fp.Psi - fp.LMsht / fp.S) * fp.Pshtv * fp.Pzh);
        p.POutC("1.154, 2.174", "Pvibv", fp.Pvibv);
    }
    
    /// <summary>
    /// Вибрационная нагрузка при ходе вниз (Н) (1.155, 2.175)
    /// </summary>
    public void Pvibn()
    {
        parameters.Clear();
        parameters.Add("aln2", fp.aln2);
        parameters.Add("mw", fp.mw);
        parameters.Add("an2", fp.an2);
        parameters.Add("Psi", fp.Psi);
        parameters.Add("LMsht", fp.LMsht);
        parameters.Add("S", fp.S);
        parameters.Add("Pshtv", fp.Pshtv);
        parameters.Add("Pzh", fp.Pzh);
        p.PInC("1.155, 2.175", parameters);
        fp.Pvibn = Math.Pow(fp.aln2, 2) * fp.mw * Math.Sqrt((fp.an2 * fp.Psi - fp.LMsht / fp.S) * fp.Pshtv * fp.Pzh);
        p.POutC("1.155, 2.175", "Pvibn", fp.Pvibn);
    }
    
    /// <summary>
    /// Инерционная нагрузка при ходе вверх (Н) (1.156, 2.176)
    /// </summary>
    public void Pinv()
    {
        parameters.Clear();
        parameters.Add("aln1", fp.aln1);
        parameters.Add("mw", fp.mw);
        parameters.Add("an1", fp.an1);
        parameters.Add("Psi", fp.Psi);
        parameters.Add("LMsht", fp.LMsht);
        parameters.Add("S", fp.S);
        parameters.Add("Pshtv", fp.Pshtv);
        p.PInC("1.156, 2.176", parameters);
        fp.Pinv = Math.Pow(fp.aln1, 2) * fp.mw * Math.Sqrt((fp.an1 - 2.0 * fp.LMsht / (fp.Psi * fp.S)) * fp.Pshtv / 2.0);
        p.POutC("1.156, 2.176", "Pinv", fp.Pinv);
    }
    
    /// <summary>
    /// Инерционная нагрузка при ходе вниз (Н) (1.157, 2.177)
    /// </summary>
    public void Pinn()
    {
        parameters.Clear();
        parameters.Add("aln2", fp.aln2);
        parameters.Add("mw", fp.mw);
        parameters.Add("an2", fp.an2);
        parameters.Add("Psi", fp.Psi);
        parameters.Add("LMsht", fp.LMsht);
        parameters.Add("S", fp.S);
        parameters.Add("Pshtv", fp.Pshtv);
        p.PInC("1.157, 2.177", parameters);
        fp.Pinn = Math.Pow(fp.aln2, 2) * fp.mw * Math.Sqrt((fp.an2 - 2.0 * fp.LMsht / (fp.Psi * fp.S)) * fp.Pshtv / 2.0);
        p.POutC("1.157, 2.177", "Pinn", fp.Pinn);
    }
    
    /// <summary>
    /// Динамическая нагрузка при ходе вверх (Н) (1.158, 2.178)
    /// </summary>
    public void Pdinv()
    {
        parameters.Clear();
        parameters.Add("Kdv", fp.Kdv);
        parameters.Add("Pvibv", fp.Pvibv);
        parameters.Add("Pinv", fp.Pinv);
        p.PInC("1.158, 2.178", parameters);
        fp.Pdinv = fp.Kdv * (fp.Pvibv + fp.Pinv);
        p.POutC("1.158, 2.178", "Pdinv", fp.Pdinv);
    }
    
    /// <summary>
    /// Динамическая нагрузка при ходе вниз (Н) (1.159, 2.179)
    /// </summary>
    public void Pdinn()
    {
        parameters.Clear();
        parameters.Add("Kdn", fp.Kdn);
        parameters.Add("Pvibn", fp.Pvibn);
        parameters.Add("Pinn", fp.Pinn);
        p.PInC("1.159, 2.179", parameters);
        fp.Pdinn = fp.Kdn * (fp.Pvibn + fp.Pinn);
        p.POutC("1.159, 2.179", "Pdinn", fp.Pdinn);
    }
    
    /// <summary>
    /// Сила механического трения колонны штанг о стенки НКТ (Н) (1.160, 2.180)
    /// </summary>
    public void Ptrmeh()
    {
        parameters.Clear();
        parameters.Add("Csht", fp.Csht);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Pzh", fp.Pzh);
        parameters.Add("Pshtv", fp.Pshtv);
        p.PInC("1.160, 2.180", parameters);
        fp.Ptrmeh = fp.Csht * fp.alfa * (fp.Pzh + fp.Pshtv);
        p.POutC("1.160, 2.180", "Ptrmeh", fp.Ptrmeh);
    }
    
    /// <summary>
    /// Сила трения плунжера о стенки цилиндра (Н) (1.161, 2.181)
    /// </summary>
    public void Ptrpl()
    {
        parameters.Clear();
        parameters.Add("dpl", fp.dpl);
        parameters.Add("Dzaz", fp.Dzaz);
        p.PInC("1.161, 2.181", parameters);
        fp.Ptrpl = 0.82 * fp.dpl / fp.Dzaz - 127.0;
        p.POutC("1.161, 2.181", "Ptrpl", fp.Ptrpl);
    }
    
    /// <summary>
    /// Сила гидравлического сопротивления от перепада давления в нагнетательном клапане насоса (Н) (1.162, 2.182)
    /// </summary>
    public void Pkln()
    {
        parameters.Clear();
        parameters.Add("dpkln", fp.dpkln);
        parameters.Add("Fpl", fp.Fpl);
        p.PInC("1.162, 2.182", parameters);
        fp.Pkln = fp.dpkln * fp.Fpl;
        p.POutC("1.162, 2.182", "Pkln", fp.Pkln);
    }
    
    /// <summary>
    /// Потери давления в нагнетательном клапане (Па) (1.163, 2.183)
    /// </summary>
    public void dpkln()
    {
        parameters.Clear();
        parameters.Add("Vmaxn", fp.Vmaxn);
        parameters.Add("Rocm", fp.Rocm);
        parameters.Add("Ksikln", fp.Ksikln);
        p.PInC("1.163, 2.183", parameters);
        fp.dpkln = Math.Pow(fp.Vmaxn, 2) * fp.Rocm / (2.0 * Math.Pow(fp.Ksikln, 2));
        p.POutC("1.163, 2.183", "dpkln", fp.dpkln);
    }
    
    /// <summary>
    /// Максимальная скорость движения газожидкостной смеси в отверстии седла нагнетательного клапана (м/с) (1.164, 2.184)
    /// </summary>
    public void Vmaxn()
    {
        parameters.Clear();
        parameters.Add("Qn", fp.Qn);
        parameters.Add("pi", fp.pi);
        parameters.Add("dkln", fp.dkln);
        p.PInC("1.164, 2.184", parameters);
        fp.Vmaxn = 4.0 * fp.Qn / (fp.pi * Math.Pow(fp.dkln, 2));
        p.POutC("1.164, 2.184", "Vmaxn", fp.Vmaxn);
    }
    
    /// <summary>
    /// Максимальная нагрузка в точке подвеса штанг (Н) (1.165, 2.185)
    /// </summary>
    public void Pmaxsht()
    {
        parameters.Clear();
        parameters.Add("Pshtzh", fp.Pshtzh);
        parameters.Add("Pzh", fp.Pzh);
        parameters.Add("Ptrmeh", fp.Ptrmeh);
        parameters.Add("Ptrpl", fp.Ptrpl);
        p.PInC("1.165, 2.185", parameters);
        fp.Pmaxsht = fp.Pshtzh + fp.Pzh + fp.Ptrmeh + fp.Ptrpl;
        p.POutC("1.165, 2.185", "Pmaxsht", fp.Pmaxsht);
    }
    
    /// <summary>
    /// Минимальная нагрузка в точке подвеса штанг (Н) (1.166, 2.186)
    /// </summary>
    public void Pminsht()
    {
        parameters.Clear();
        parameters.Add("Pshtzh", fp.Pshtzh);
        parameters.Add("Ptrmeh", fp.Ptrmeh);
        parameters.Add("Ptrpl", fp.Ptrpl);
        parameters.Add("Pkln", fp.Pkln);
        p.PInC("1.166, 2.186", parameters);
        fp.Pminsht = fp.Pshtzh - fp.Ptrmeh - fp.Ptrpl - fp.Pkln;
        p.POutC("1.166, 2.186", "Pminsht", fp.Pminsht);
    }
    
    /// <summary>
    /// Максимальное напряжение цикла (Па) (1.167, 2.187)
    /// </summary>
    public void SGmax()
    {
        parameters.Clear();
        parameters.Add("Pmaxsht", fp.Pmaxsht);
        parameters.Add("Pdinv", fp.Pdinv);
        parameters.Add("fsh", fp.fsh);
        p.PInC("1.167, 2.187", parameters);
        fp.SGmax = (fp.Pmaxsht + fp.Pdinv) / fp.fsh;
        p.POutC("1.167, 2.187", "SGmax", fp.SGmax);
    }
    
    /// <summary>
    /// Минимальное напряжение цикла (Па) (1.168, 2.188)
    /// </summary>
    public void SGmin()
    {
        parameters.Clear();
        parameters.Add("Pmaxsht", fp.Pmaxsht);
        parameters.Add("Pdinv", fp.Pdinv);
        parameters.Add("fsh", fp.fsh);
        p.PInC("1.168, 2.188", parameters);
        fp.SGmin = (fp.Pmaxsht - fp.Pdinv) / fp.fsh;
        p.POutC("1.168, 2.188", "SGmin", fp.SGmin);
    }
    
    /// <summary>
    /// Амплитудное напряжение цикла (Па) (1.169, 2.189)
    /// </summary>
    public void SGa()
    {
        parameters.Clear();
        parameters.Add("SGmax", fp.SGmax);
        parameters.Add("SGmin", fp.SGmin);
        p.PInC("1.169, 2.189", parameters);
        fp.SGa = (fp.SGmax - fp.SGmin) / 2.0;
        p.POutC("1.169, 2.189", "SGa", fp.SGa);
    }
    
    /// <summary>
    /// Приведенное напряжение цикла (Па) (1.170, 2.190)
    /// </summary>
    public void SGpr()
    {
        parameters.Clear();
        parameters.Add("SGa", fp.SGa);
        parameters.Add("SGmax", fp.SGmax);
        p.PInC("1.170, 2.190", parameters);
        fp.SGpr = Math.Sqrt(fp.SGa * fp.SGmax);
        p.POutC("1.170, 2.190", "SGpr", fp.SGpr);
    }
    
    /// <summary>
    /// Полезная мощность электродвигателя (Вт) (1.171, 2.191)
    /// </summary>
    public void Jpol()
    {
        parameters.Clear();
        parameters.Add("DPpl", fp.DPpl);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("nv", fp.nv);
        p.PInC("1.171, 2.191", parameters);
        fp.Jpol = fp.DPpl * fp.Qn / (1.0 - fp.nv);
        p.POutC("1.171, 2.191", "Jpol", fp.Jpol);
    }
    
    /// <summary>
    /// Потери мощности от утечек жидкости (Вт) (1.172, 2.192)
    /// </summary>
    public void Etaut()
    {
        parameters.Clear();
        parameters.Add("Qn", fp.Qn);
        parameters.Add("Qut", fp.Qut);
        parameters.Add("nv", fp.nv);
        p.PInC("1.172, 2.192", parameters);
        fp.Etaut = 2.0 * fp.Qn / (1.0 + fp.Qut * (1.0 - fp.nv));
        p.POutC("1.172, 2.192", "Etaut", fp.Etaut);
    }
    
    /// <summary>
    /// Потери мощности в клапанных узлах (Вт) (1.173, 2.193)
    /// </summary>
    public void Jkl()
    {
        parameters.Clear();
        parameters.Add("dpklvs", fp.dpklvs);
        parameters.Add("dpkln", fp.dpkln);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("nv", fp.nv);
        p.PInC("1.173, 2.193", parameters);
        fp.Jkl = (fp.dpklvs - fp.dpkln) * fp.Qn / (1.0 - fp.nv);
        p.POutC("1.173, 2.193", "Jkl", fp.Jkl);
    }
    
    /// <summary>
    /// Потери давления во всасывающем клапане (Па) (1.174, 2.194)
    /// </summary>
    public void dpklvs()
    {
        parameters.Clear();
        parameters.Add("Vmaxvs", fp.Vmaxvs);
        parameters.Add("Rocm", fp.Rocm);
        parameters.Add("Ksiklvs", fp.Ksiklvs);
        p.PInC("1.174, 2.194", parameters);
        fp.dpklvs = Math.Pow(fp.Vmaxvs, 2) * fp.Rocm / (2.0 * Math.Pow(fp.Ksiklvs, 2));
        p.POutC("1.174, 2.194", "dpklvs", fp.dpklvs);
    }
    
    /// <summary>
    /// Максимальная скорость движения газожидкостной смеси в отверстии седла всасывающего клапана (м/с) (1.175, 2.195)
    /// </summary>
    public void Vmaxvs()
    {
        parameters.Clear();
        parameters.Add("Qn", fp.Qn);
        parameters.Add("pi", fp.pi);
        parameters.Add("dklvs", fp.dklvs);
        p.PInC("1.175, 2.195", parameters);
        fp.Vmaxvs = 4.0 * fp.Qn / (fp.pi * Math.Pow(fp.dklvs, 2));
        p.POutC("1.175, 2.195", "Vmaxvs", fp.Vmaxvs);
    }
    
    /// <summary>
    /// Потери мощности на преодоление механического трения штанг в трубах (Вт) (1.176, 2.196)
    /// </summary>
    public void Jtrm()
    {
        parameters.Clear();
        parameters.Add("Csht", fp.Csht);
        parameters.Add("S", fp.S);
        parameters.Add("nk", fp.nk);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Pshtzh", fp.Pshtzh);
        parameters.Add("Pzh", fp.Pzh);
        p.PInC("1.176, 2.196", parameters);
        fp.Jtrm = 2.0 * fp.Csht * fp.S * fp.nk * fp.alfa * (fp.Pshtzh + fp.Pzh);
        p.POutC("1.176, 2.196", "Jtrm", fp.Jtrm);
    }

    /// <summary>
    /// Потери мощности на преодоление гидравлического трения штанг в трубах (Вт) (1.177, 2.197)
    /// </summary>
    public void Jtrg()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("vgzh", fp.vgzh);
        parameters.Add("S", fp.S);
        parameters.Add("nk", fp.nk);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Msht", fp.Msht);
        p.PInC("1.177, 2.197", parameters);
        fp.Jtrg = Math.Pow(fp.pi, 3) * fp.vgzh * Math.Pow(fp.S * fp.nk, 2) * fp.Rozh * fp.Lcp * fp.Msht; 
        p.POutC("1.177, 2.197", "Jtrg", fp.Jtrg);
    }

    /// <summary>
    /// Коэффициент для расчета потери мощности на преодоление гидравлического трения штанг в трубах (1.178, 2.198)
    /// </summary>
    public void Msht()
    {
        parameters.Clear();
        parameters.Add("Msht2", fp.Msht2);
        p.PInC("1.178, 2.198", parameters);
        fp.Msht = 1.0 / ((Math.Pow(fp.Msht2, 2) + 1.0) / (Math.Pow(fp.Msht2, 2) - 1.0) * Math.Log(fp.Msht2) - 1.0); 
        p.POutC("1.178, 2.198", "Msht", fp.Msht);
    }

    /// <summary>
    /// Коэффициент для расчета потери мощности на преодоление гидравлического трения штанг в трубах (1.179, 2.199)
    /// </summary>
    public void Msht2()
    {
        parameters.Clear();
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("dsh", fp.dsh);
        p.PInC("1.179, 2.199", parameters);
        fp.Msht2 = fp.dnkt / fp.dsh; 
        p.POutC("1.179, 2.199", "Msht2", fp.Msht2);
    }

    /// <summary>
    /// Потери мощности на преодоление трения плунжера в цилиндре (Вт) (1.180, 2.200)
    /// </summary>
    public void Jtrpl()
    {
        parameters.Clear();
        parameters.Add("Ptrpl", fp.Ptrpl);
        parameters.Add("S", fp.S);
        parameters.Add("nk", fp.nk);
        p.PInC("1.180, 2.200", parameters);
        fp.Jtrpl = 2.0 * fp.Ptrpl * fp.S * fp.nk; 
        p.POutC("1.180, 2.200", "Jtrpl", fp.Jtrpl);
    }

    /// <summary>
    /// Потери мощности подземной части (Вт) (1.181, 2.201)
    /// </summary>
    public void Jpch()
    {
        parameters.Clear();
        parameters.Add("Jpol", fp.Jpol);
        parameters.Add("Etaut", fp.Etaut);
        parameters.Add("Jkl", fp.Jkl);
        parameters.Add("Jtrm", fp.Jtrm);
        parameters.Add("Jtrg", fp.Jtrg);
        parameters.Add("Jtrpl", fp.Jtrpl);
        p.PInC("1.181, 2.201", parameters);
        fp.Jpch = fp.Jpol / fp.Etaut + fp.Jkl + fp.Jtrm + fp.Jtrg + fp.Jtrpl; 
        p.POutC("1.181, 2.201", "Jpch", fp.Jpch);
    }

    /// <summary>
    /// К.п.д. подземной части (1.182, 2.202)
    /// </summary>
    public void Etapch()
    {
        parameters.Clear();
        parameters.Add("Jpol", fp.Jpol);
        parameters.Add("Jpch", fp.Jpch);
        p.PInC("1.182, 2.202", parameters);
        fp.Etapch = fp.Jpol / fp.Jpch; 
        p.POutC("1.182, 2.202", "Etapch", fp.Etapch);
    }

    /// <summary>
    /// К.п.д. ШСНУ (1.183, 2.203)
    /// </summary>
    public void Etashsnu()
    {
        parameters.Clear();
        parameters.Add("Etapch", fp.Etapch);
        parameters.Add("Etask", fp.Etask);
        parameters.Add("Etaed", fp.Etaed);
        p.PInC("1.183, 2.203", parameters);
        fp.Etashsnu = fp.Etapch * fp.Etask * fp.Etaed; 
        p.POutC("1.183, 2.203", "Etashsnu", fp.Etashsnu);
    }

    /// <summary>
    /// Полная мощность, затрачиваемая на подъем жидкости (Вт) (1.184, 2.204)
    /// </summary>
    public void Jpoln()
    {
        parameters.Clear();
        parameters.Add("Jpol", fp.Jpol);
        parameters.Add("Etashsnu", fp.Etashsnu);
        p.PInC("1.184, 2.204", parameters);
        fp.Jpoln = fp.Jpol / fp.Etashsnu; 
        p.POutC("1.184, 2.204", "Jpoln", fp.Jpoln);
    }

    /// <summary>
    /// Сила тока (А) (1.185, 2.205)
    /// </summary>
    public void I()
    {
        parameters.Clear();
        parameters.Add("Jpoln", fp.Jpoln);
        parameters.Add("U", fp.U);
        p.PInC("1.185, 2.205", parameters);
        fp.I = fp.Jpoln / fp.U; 
        p.POutC("1.185, 2.205", "I", fp.I);
    }

    /// <summary>
    /// Длительность одного цикла качаний (с) (1.186, 2.206)
    /// </summary>
    public void Per()
    {
        parameters.Clear();
        parameters.Add("nk", fp.nk);
        p.PInC("1.186, 2.206", parameters);
        fp.Per = 1.0 / fp.nk; 
        p.POutC("1.186, 2.206", "Per", fp.Per);
    }

    /// <summary>
    /// Время упругого растяжения колонны штанг (с) (1.187, 2.207)
    /// </summary>
    public void tupr()
    {
        parameters.Clear();
        parameters.Add("Per", fp.Per);
        parameters.Add("LMsh", fp.LMsh);
        parameters.Add("S", fp.S);
        p.PInC("1.187, 2.207", parameters);
        fp.tupr = fp.Per * fp.LMsh / (2.0 * fp.S); 
        p.POutC("1.187, 2.207", "tupr", fp.tupr);
    }

    /// <summary>
    /// Теоретическое перемещение штока (м) (1.188, 1.189, 2.208, 2.209)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Lteor(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Per", fp.Per);
        parameters.Add("S", fp.S);
        p.PInC("1.188, 1.189, 2.208, 2.209", parameters);
        if (0 <= time && time < fp.Per) fp.Lteor = 2.0 * fp.S * time / fp.Per;
        else fp.Lteor = fp.S - 2.0 * fp.S * (time - fp.Per / 2.0) / fp.Per;
        p.POutC("1.188, 1.189, 2.208, 2.209", "Lteor", fp.Lteor);
    }

    /// <summary>
    /// Теоретическая нагрузка (Н) (1.190-1.193, 2.210-2.213)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pteor(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("tupr", fp.tupr);
        parameters.Add("Pmax", fp.Pmax);
        parameters.Add("Pmin", fp.Pmin);
        parameters.Add("Per", fp.Per);
        p.PInC("1.190-1.193, 2.210-2.213", parameters);
        fp.Pteor = 0.0;
        if (0 <= time && time < fp.tupr)  
            fp.Pteor = (fp.Pmax - fp.Pmin) * time / fp.tupr + fp.Pmin;
        if (fp.tupr <= time && time < fp.Per / 2.0)  
            fp.Pteor = fp.Pmax;
        if (fp.Per / 2.0 <= time && time < fp.Per / 2.0 + fp.tupr)  
            fp.Pteor = (fp.Pmin - fp.Pmax) * (time - fp.Per / 2.0) / fp.tupr + fp.Pmax;
        if (fp.Per / 2.0 + fp.tupr <= time && time < fp.Per)  
            fp.Pteor = fp.Pmin;
        p.POutC("1.190-1.193, 2.210-2.213", "Pteor", fp.Pteor);
    }

    /// <summary>
    /// Практическое перемещение штока (м) (1.194, 2.214)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Lprak(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("S", fp.S);
        parameters.Add("pi", fp.pi);
        parameters.Add("Per", fp.Per);
        p.PInC("1.194, 2.214", parameters);
        fp.Lprak = fp.S * (1.0 - Math.Cos(2.0 * fp.pi * time / fp.Per)) / 2.0;
        p.POutC("1.194, 2.214", "Lprak", fp.Lprak);
    }

    /// <summary>
    /// Время упругого растяжения колонны штанг для практической динамограммы при ходе вверх (с) (1.195, 2.215)
    /// </summary>
    public void tuprvv()
    {
        parameters.Clear();
        parameters.Add("tupr", fp.tupr);
        parameters.Add("dtvv", fp.dtvv);
        p.PInC("1.195, 2.215", parameters);
        fp.tuprvv = fp.tupr + fp.dtvv; 
        p.POutC("1.195, 2.215", "tuprvv", fp.tuprvv);
    }

    /// <summary>
    /// Время упругого растяжения колонны штанг для практической динамограммы при ходе вниз (с) (1.196, 2.216)
    /// </summary>
    public void tuprvn()
    {
        parameters.Clear();
        parameters.Add("tupr", fp.tupr);
        parameters.Add("dtvn", fp.dtvn);
        p.PInC("1.196, 2.216", parameters);
        fp.tuprvn = fp.tupr + fp.dtvn; 
        p.POutC("1.196, 2.216", "tuprvn", fp.tuprvn);
    }

    /// <summary>
    /// Теоретическая нагрузка (Н) (1.197-1.200, 2.217-2.220)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pprak(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("tuprvv", fp.tuprvv);
        parameters.Add("tuprvn", fp.tuprvn);
        parameters.Add("Pmax", fp.Pmax);
        parameters.Add("Pmin", fp.Pmin);
        parameters.Add("Per", fp.Per);
        parameters.Add("Pvd", fp.Pvd);
        parameters.Add("Pnd", fp.Pnd);
        parameters.Add("Kzat", fp.Kzat);
        parameters.Add("pi", fp.pi);
        parameters.Add("Tkol", fp.Tkol);
        p.PInC("1.197-1.200, 2.217-2.220", parameters);
        fp.Pprak = 0.0;
        if (0 <= time && time < fp.tuprvv)  
            fp.Pprak = (fp.Pmax - fp.Pmin) * time / fp.tuprvv + fp.Pmin;
        if (fp.tuprvv <= time && time < fp.Per / 2.0)  
            fp.Pprak = fp.Pmax - fp.Pvd * Math.Exp(-fp.Kzat * (time - fp.tuprvv)) * 
                Math.Cos(fp.pi / 2.0 + 2.0 * fp.pi * (time - fp.tuprvv) / fp.Tkol);
        if (fp.Per / 2.0 <= time && time < fp.Per / 2.0 + fp.tuprvn)  
            fp.Pprak = (fp.Pmin - fp.Pmax) * (time - fp.Per / 2.0) / fp.tuprvn + fp.Pmax;
        if (fp.Per / 2.0 + fp.tuprvn <= time && time < fp.Per)  
            fp.Pprak = fp.Pmin - fp.Pnd * Math.Exp(-fp.Kzat * (time - fp.tuprvn - fp.Per / 2.0)) * 
                Math.Cos(2.0 * fp.pi * (time - fp.tuprvn - fp.Per / 2.0) / fp.Tkol - fp.pi / 2.0);
        p.POutC("1.197-1.200, 2.217-2.220", "Pprak", fp.Pprak);
    }

    /// <summary>
    /// Величина изменения динамического уровня в скважине (м) (1.201, 2.223)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void dhdin(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("1.201, 2.223", parameters);
        fp.dhdin = 4.0 * (fp.Qskv - fp.Qn) * (tz2 - tz1) / 
                   (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)); 
        p.POutC("1.201, 2.223", "dhdin", fp.dhdin);
    }

    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.202, 2.224)
    /// </summary>
    public void hdin1()
    {
        parameters.Clear();
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Pzatr", fp.Pzatr);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("dhdin", fp.dhdin);
        p.PInC("1.202, 2.224", parameters);
        fp.hdin = fp.Lcp - (fp.Ppr - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.densityOfOil * fp.g * Math.Cos(fp.alfa)) + fp.dhdin; 
        p.POutC("1.202, 2.224", "hdin", fp.hdin);
    }

    /// <summary>
    /// Давление на затрубе (Па) (1.203, 2.225)
    /// </summary>
    public void Pzatr1()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.203, 2.225", parameters);
        fp.Pzatr = (fp.Ppr - fp.densityOfOil * fp.g * (fp.Lcp - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
        p.POutC("1.203, 2.225", "Pzatr", fp.Pzatr);
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (1.204, 2.226)
    /// </summary>
    public void DPpzo1()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pzost", fp.Pzost);
        p.PInC("1.204, 2.226", parameters);
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
        p.POutC("1.204, 2.226", "DPpzo1", fp.DPpzo1);
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (1.205, 2.227)
    /// </summary>
    public void tzab1()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("rc", fp.rc);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("1.205, 2.227", parameters);
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.oilViscosity)) * fp.rc / 
                   (2.25 * fp.piezoconductivityOfTheFormation); 
        p.POutC("1.205, 2.227", "tzab1", fp.tzab1);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (1.206, 1.207; 2.228, 2.229)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("tzab1", fp.tzab1);
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("1.206, 1.207; 2.228, 2.229", parameters);
        if (time < fp.tzab1)  fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        else  fp.Pz = fp.Pzost + fp.Qrezh * fp.oilViscosity / (4.0 * fp.pi * fp.k * fp.h) *
                Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
        p.POutC("1.206, 1.207; 2.228, 2.229", "Pz", fp.Pz);
    }

    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.208, 2.230)
    /// </summary>
    public void htr1()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.208, 2.230", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3) / (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.208, 2.230", "htr", fp.htr);
    }

    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.209, 2.231)
    /// </summary>
    public void htr2()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("htr", fp.htr);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.209, 2.231", parameters);
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3 * Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z))) / 
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.209, 2.231", "htr", fp.htr);
    }

    /// <summary>
    /// Буферное давление, рассчитанное по третьей формуле (Па) (1.210, 2.232)
    /// </summary>
    public void Pbuf3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.210, 2.232", parameters);
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("1.210, 2.232", "Pbuf3", fp.Pbuf3);
    }

    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.211, 2.233)
    /// </summary>
    public void hdin2()
    {
        parameters.Clear();
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pzatr", fp.Pzatr);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        p.PInC("1.211, 2.233", parameters);
        fp.hdin = fp.Lk - (fp.Pz - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
        p.POutC("1.211, 2.233", "hdin", fp.hdin);
    }

    /// <summary>
    /// Давление на затрубе (Па) (1.212, 2.234)
    /// </summary>
    public void Pzatr2()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("densityOfOil", fp.densityOfOil);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("hdin", fp.hdin);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("1.212, 2.234", parameters);
        fp.Pzatr = (fp.Pz - fp.densityOfOil * fp.g * (fp.Lk - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
        p.POutC("1.212, 2.234", "Pzatr", fp.Pzatr);
    }

    /// <summary>
    /// Число Рейнольдса в ЭК (2.4, 2.53)
    /// </summary>
    public void Reek2()
    {
        parameters.Clear();
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        p.PInC("2.4, 2.53", parameters);
        fp.Reek = fp.Vzhek * fp.densityOfKillingFluid * fp.ECInnerDiameter / fp.viscosityOfKillingFluid;
        p.POutC("2.4, 2.53", "Reek", fp.Reek);
    }
    
    /// <summary>
    /// Забойное давление, рассчитанное при помощи распределения давления (Па) (2.6)
    /// </summary>
    public void Pzpk2()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.6", parameters);
        fp.Pzpk = fp.Ppr + (fp.Lk - fp.Lcp) * (fp.densityOfKillingFluid * fp.g * Math.Cos(fp.alfa) +
                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
        p.POutC("2.6", "Pzpk", fp.Pzpk);
    }

    /// <summary>
    /// Расход жидкости, поступающей из пласта  (м3 / с) (2.47)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="rc">Радиус скважины, м</param>
    public void Qskv2(double Pz, double rc)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("rc", rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("kk", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("2.47", parameters);
        fp.Qskv = 2.0 * fp.pi * fp.k * fp.h * (fp.reservoirPressurePa - Pz) / 
                  (fp.viscosityOfKillingFluid * Math.Log(fp.Rk / (rc * Math.Exp(-fp.skinFactor))));
        p.POutC("2.47", "Qskv", fp.Qskv);
    }
    
    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта (м3) (2.48)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Vzhgzab(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("Vzhgzab", fp.Vzhgzab);
        p.PInC("2.48", parameters);
        fp.Vzhgzab = fp.Qskv * (tz2 - tz1) + fp.Vzhgzab;
        p.POutC("2.48", "Vzhgzab", fp.Vzhgzab);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (Па) (2.55)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ppr3(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaEk", fp.LambdaEk);
        parameters.Add("Vzhek", fp.Vzhek);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.55", parameters);
        fp.Ppr = Pz - (fp.Lk - fp.Lcp) * (fp.densityOfKillingFluid * fp.g * Math.Cos(fp.alfa) +
                                             fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.densityOfKillingFluid / 
                                             (2.0 * fp.ECInnerDiameter));
        p.POutC("2.55", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Число Рейнольдса в НКТ (2.82, 2.124)
    /// </summary>
    public void Renkt2()
    {
        parameters.Clear();
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.82, 2.124", parameters);
        fp.Renkt = fp.Vzhnkt * fp.densityOfKillingFluid * fp.dnkt / fp.viscosityOfKillingFluid;
        p.POutC("2.82, 2.124", "Renkt", fp.Renkt);
    }
    
    /// <summary>
    /// Давление на выходе из насоса (Па) (2.84)
    /// </summary>
    public void Pvyh2()
    {
        parameters.Clear();
        parameters.Add("Pbuf3", fp.Pbuf3);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("g", fp.g);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.84", parameters);
        fp.Pvyh = fp.Pbuf3 + fp.Lcp * (fp.densityOfKillingFluid * fp.g * Math.Cos(fp.alfa) +
                                       fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.densityOfKillingFluid / 
                                       (2.0 * fp.dnkt));
        p.POutC("2.84", "Pvyh", fp.Pvyh);
    }

    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.122, 2.222)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void htr3(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("htr", fp.htr);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.122, 2.222", parameters);
        fp.htr = fp.htr - 4.0 * fp.Qn * (tz2 - tz1) / (fp.pi * Math.Pow(fp.dnkt, 2)); 
        p.POutC("2.122, 2.222", "htr", fp.htr);
    }
    
    /// <summary>
    /// Напор на преодоление гидравлического трения (м) (2.126)
    /// </summary>
    public void htren2()
    {
        parameters.Clear();
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("htr", fp.htr);
        parameters.Add("Vzhnkt", fp.Vzhnkt);
        parameters.Add("g", fp.g);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.126", parameters);
        fp.htren = fp.LambdaNkt * (fp.Lcp - fp.htr) * Math.Pow(fp.Vzhnkt, 2) / (2.0 * fp.g * fp.dnkt);
        p.POutC("2.126", "htren", fp.htren);
    }
    
    /// <summary>
    /// Общий напор, который необходимо создать ШСНУ (м) (2.127)
    /// </summary>
    public void Hobsh()
    {
        parameters.Clear();
        parameters.Add("hdin", fp.hdin);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("htren", fp.htren);
        p.PInC("2.127", parameters);
        fp.Hobsh = (fp.hdin - fp.htr) * Math.Cos(fp.alfa) + fp.htren;
        p.POutC("2.127", "Hobsh", fp.Hobsh);
    }
    
    /// <summary>
    /// Объем жидкости глушения, поступившей на поверхность (м3) (2.221)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Vzhgpov(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qn", fp.Qn);
        parameters.Add("Vzhgpov", fp.Vzhgpov);
        p.PInC("2.221", parameters);
        fp.Vzhgpov = fp.Qn * (tz2 - tz1) + fp.Vzhgpov;
        p.POutC("2.221", "Vzhgpov", fp.Vzhgpov);
    }
    
    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта при остановке скважины (м3) (2.235)
    /// </summary>
    /// <param name="hdin">Динамический уровень в скважине, м</param>
    public void Vost(double hdin)
    {
        parameters.Clear();
        parameters.Add("hdin", hdin);
        parameters.Add("pi", fp.pi);
        parameters.Add("hdin0", fp.hdin0);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("2.235", parameters);
        fp.Vost = fp.pi * (fp.hdin0 - hdin) * (Math.Pow(fp.ECInnerDiameter, 2) - Math.Pow(fp.Dnkt, 2)) / 4.0;
        p.POutC("2.235", "Vost", fp.Vost);
    }
}
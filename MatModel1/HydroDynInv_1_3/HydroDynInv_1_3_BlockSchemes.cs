using System;

public class HydroDynInv_1_3_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;
    public bool PNPT = false; 

    public HydroDynInv_1_3_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    public void Diagram_4_1(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_1");
        if (fp.Vgf < fp.Vgkr)
        {
            if (fp.Hzhek < (fp.Lk - fp.Lcp)) Diagram_4_1_Brench2(time_1, time);
            else Diagram_4_1_Brench3(time_1, time);
        } 
        else Diagram_4_1_Brench1();
        Diagram_4_1_Common();
    }

    public void Diagram_4_2(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_2");
        Diagram_4_2_Common();
        if (fp.Vgf < fp.Vgkr)
        {
            if (fp.Hzhek < (fp.Lk - fp.Lcp)) Diagram_4_2_Brench2(time_1, time);
            else Diagram_4_2_Brench3(time_1, time);
        } 
        else Diagram_4_2_Brench1();
    }

    public void Diagram_4_3(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_3");
        fp.h13.Pz1(time); // 3.26.1
        fp.h13.Afs3(); // 3.28
        fp.h13.Bfs3(); // 3.29
        fp.h13.reservoirPressurePa(); // 3.3, 3.27
        fp.h13.Kmshpl(); // 3.30
        fp.h13.MP(fp.Pz); // 3.31, 3.32
        if (fp.DPkr < (fp.reservoirPressurePa - fp.Pz)) 
        {
            fp.h13.Qzh(); // 3.33
            fp.h13.Vgf(fp.Tz); // 2.9
            fp.h13.densityOfGas(fp.Tz); // 2.10
            fp.h13.Vgkr(); // 2.11
        }
        Diagram_4_1(time_1, time);
        p.PTxt("h13.Diagram_4_3_2 - continue after h13.Diagram_4_1");
        fp.h13.Cdikt(); // 3.41, 3.42
        fp.h13.Pdikt2(); // 3.39
        fp.h13.Qgpt2(); // 3.40
    }

    public void Diagram_4_4(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_4");
        fp.h13.Pdikt1(); // 2.58
        fp.h13.Pdikt2(); // 3.39
        fp.PdiktMdl = 0.5 * (fp.Pdikt1 + fp.Pdikt2);
        Diagram_4_2(time_1, time);
        p.PTxt("h13.Diagram_4_4");
        fp.h13.Pz4(); // 2.78
        var PzMdl = 0.5 * (fp.Pz_1 + fp.Pz);
        fp.h13.Afs3(); // 3.28
        fp.h13.Bfs3(); // 3.29
        fp.h13.reservoirPressurePa(); // 3.3, 3.27
        fp.h13.Kmshpl(); // 3.30
        fp.h13.MP(fp.Pz); // 3.31, 3.32
        if (fp.DPkr < (fp.reservoirPressurePa - PzMdl))
        {
            fp.h13.Qzh(); // 3.33
            fp.h13.Vgf(fp.Tz); // 2.9
            fp.h13.densityOfGas(fp.Tz); // 2.10
            fp.h13.Vgkr(); // 2.11
        }
        else
        {
            Diagram_4_1(time_1, time);
            p.PTxt("h13.Diagram_4_4 - continue after h13.Diagram_4_1");
            fp.h13.Cdikt(); // 3.41, 3.42
            fp.h13.Pdikt2(); // 3.39
            fp.h13.Qgpt2(); // 3.40
            if (1.0E-5 < Math.Abs(fp.dsht - fp.dsht_1)) Diagram_4_3(time_1, time);
        }
    }

    public void Diagram_4_5(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_5");
        if (!PNPT)
        {
            fp.h13.DPpzo1(); // 2.98
            fp.h13.tzab1(); // 2.99
            PNPT = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            if (fp.tzab1 < time)
            {
                fp.h13.AlfaFes(); // 3.12
                fp.h13.BettaFes(); // 3.13, 3.20
                fp.h13.Pz3(time); // 3.11
            }
            else
            {
                fp.h13.Pz5(time); // 2.100
            }
            fp.h13.Pbuf3(); // 2.104
        }
    }

    private void Diagram_4_1_Brench1()
    {
        p.PTxt("h13.Diagram_4_1_Brench1");
        fp.h13.G(); // 2.14
        fp.h13.Dpl(); // 2.15
        fp.h13.DTplz(); // 2.16
        fp.h13.fes(); // 2.17
        fp.h13.alfa(); // 2.18
        fp.h13.Tz(); // 2.19
        fp.h13.Dz(); // 2.20
        fp.h13.Tpr(); // 2.21
        fp.h13.Ppr1(); // 2.22
        fp.h13.Tcp1(); // 2.23
        fp.h13.Pcp1(); // 2.24
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS1(); // 2.26
        fp.h13.Teta1(); // 2.27
        fp.h13.G(); // 2.28
        fp.h13.Dsp(); // 2.29
        fp.h13.Tbuf1(); // 2.30
        fp.h13.fes(); // 2.31
        fp.h13.alfa(); // 2.32
        fp.h13.Pbuf1(); // 3.34
        fp.h13.Tcp2(); // 2.34
        fp.h13.Pcp2(); // 2.35
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS2(); // 2.37
        fp.h13.Teta2(); // 2.38
    }

    private void Diagram_4_1_Brench2(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_1_Brench2");
        fp.h13.Hzhek(time_1, time); // 2.12
        fp.h13.G(); // 2.14
        fp.h13.Dpl(); // 2.15
        fp.h13.DTplz(); // 2.16
        fp.h13.fes(); // 2.17
        fp.h13.alfa(); // 2.18
        fp.h13.Tz(); // 2.19
        fp.h13.Dz(); // 2.20
        fp.h13.Tpr(); // 2.21
        fp.h13.Ppr1(); // 2.22
        fp.h13.Tcp1(); // 2.23
        fp.h13.Pcp1(); // 2.24
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS1(); // 2.26
        fp.h13.Teta1(); // 2.27
        fp.h13.G(); // 2.28
        fp.h13.Dsp(); // 2.29
        fp.h13.Tbuf1(); // 2.30
        fp.h13.fes(); // 2.31
        fp.h13.alfa(); // 2.32
        fp.h13.Pbuf1(); // 3.34
        fp.h13.Tcp2(); // 2.34
        fp.h13.Pcp2(); // 2.35
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS2(); // 2.37
        fp.h13.Teta2(); // 2.38
    }

    private void Diagram_4_1_Brench3(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_1_Brench3");
        fp.h13.htr(time_1, time); // 2.13
        fp.h13.G(); // 2.14
        fp.h13.Dpl(); // 2.15
        fp.h13.DTplz(); // 2.16
        fp.h13.fes(); // 2.17
        fp.h13.alfa(); // 2.18
        fp.h13.Tz(); // 2.19
        fp.h13.Dz(); // 2.20
        fp.h13.Tpr(); // 2.21
        fp.h13.Pur1(); // 2.39
        fp.h13.Ppr2(); // 2.40
        fp.h13.Tcp1(); // 2.41
        fp.h13.Pcp1(); // 2.42
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS1(); // 2.44
        fp.h13.Teta1(); // 2.45
        fp.h13.G(); // 2.28
        fp.h13.Dsp(); // 2.29
        fp.h13.Tbuf1(); // 2.30
        fp.h13.fes(); // 2.31
        fp.h13.alfa(); // 2.32
        fp.h13.Ppr3(); // 2.46
        fp.h13.Pur2(); // 2.47
        fp.h13.Pbuf2(); // 2.48
        fp.h13.Tcp2(); // 2.49
        fp.h13.Pcp2(); // 2.50
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS2(); // 2.52
        fp.h13.Teta2(); // 2.53
    }

    private void Diagram_4_1_Common()
    {
        p.PTxt("h13.Diagram_4_1_Common");
        fp.h13.Plin1(); // 2.54
        fp.h13.Qgpt2(); // 3.40
        fp.h13.Dbuf1(); // 2.56
        fp.h13.Tsep1(); // 2.57
        fp.h13.Pdikt1(); // 2.58
        fp.h13.Ddikt(); // 2.59
        fp.h13.Tdikt(); // 2.60
    }

    private void Diagram_4_2_Common()
    {
        p.PTxt("h13.Diagram_4_2_Common");
        fp.h13.Plin2(); // 3.36
        fp.h13.Dbuf2(); // 3.37
        fp.h13.Tsep2(); // 3.38
        fp.h13.Pbuf1(); // 3.34
        fp.h13.Qgpt1(); // 3.35
        fp.h13.Dbuf1(); // 2.70
        fp.h13.Tbuf2(); // 2.71
    }

    private void Diagram_4_2_Brench1()
    {
        p.PTxt("h13.Diagram_4_2_Brench1");
        fp.h13.Ppr4(); // 2.72
        fp.h13.Tcp2(); // 2.73
        fp.h13.Pcp2(); // 2.74
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS2(); // 2.76
        fp.h13.Teta2(); // 2.77
        fp.h13.Pz4(); // 2.78
        fp.h13.Tcp1(); // 2.79
        fp.h13.Pcp1(); // 2.80
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS1(); // 2.82
        fp.h13.Teta1(); // 2.83
    }

    private void Diagram_4_2_Brench2(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_2_Brench2");
        fp.h13.Hzhek(time_1, time); // 2.12
        fp.h13.Pur2(); // 2.84
        fp.h13.Pbuf2(); // 2.85
        fp.h13.Tcp2(); // 2.86
        fp.h13.Pcp2(); // 2.87
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS2(); // 2.89
        fp.h13.Teta2(); // 2.90
        fp.h13.Ppr3(); // 2.91
        fp.h13.Pur1(); // 2.92
        fp.h13.Pz4(); // 2.78
        fp.h13.Tcp1(); // 2.79
        fp.h13.Pcp1(); // 2.80
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS1(); // 2.82
        fp.h13.Teta1(); // 2.83
    }

    private void Diagram_4_2_Brench3(double time_1, double time)
    {
        p.PTxt("h13.Diagram_4_2_Brench3");
        fp.h13.htr(time_1, time); // 2.13
        fp.h13.Ppr3(); // 2.91
        fp.h13.Pur1(); // 2.92
        fp.h13.Tcp1(); // 2.93
        fp.h13.Pcp1(); // 2.94
        fp.h13.Qgpt1(); // 3.35
        fp.h13.KS1(); // 2.96
        fp.h13.Teta1(); // 2.97
    }
}
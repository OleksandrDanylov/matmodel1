using System;
using System.Collections.Generic;

public class HydroDynInv_1_3
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public HydroDynInv_1_3(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
    
    /// <summary>
    /// Период стабилизации (c) (3.1)
    /// </summary>
    /// <param name="Kstab">Коэффициент, находящийся в диапазоне 0.12 - 0.15 </param>
    public void Tstab(double Kstab)
    {
        parameters.Clear();
        parameters.Add("Kstab", Kstab);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("3.1", parameters);
        fp.Tstab = Kstab * Math.Pow(fp.Rk, 2) / fp.piezoconductivityOfTheFormation;
        p.POutC("3.1", "", fp.Tstab);
    }
    
    /// <summary>
    /// Зависимость депрессии на пласт от значения забойного давления (Па) (3.2)
    /// </summary>
    /// <param name="Pzab">Значение забойного давления, Па</param>
    public void DP(double Pzab)
    {
        parameters.Clear();
        parameters.Add("Pzab", Pzab);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInC("3.2", parameters);
        fp.DP = fp.reservoirPressurePa - Pzab;
        p.POutC("3.2", "DP", fp.DP);
    }
        
    /// <summary>
    /// Пластовое давление (Па) (3.3, 3.27)
    /// </summary>
    public void reservoirPressurePa()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Afs", fp.Afs);
        parameters.Add("Bfs", fp.Bfs);
        p.PInC("3.3, 3.27", parameters);
        fp.reservoirPressurePa = Math.Sqrt(Math.Pow(fp.Pz, 2) + fp.Afs * fp.Qg0 + fp.Bfs * Math.Pow(fp.Qg0, 2));
        p.POutC("3.3, 3.27", "reservoirPressurePa", fp.reservoirPressurePa);
    }

    /// <summary>
    /// Коэффициент фильтрационного сопротивления (3.4)
    /// </summary>
    public void Afs1()
    {
        parameters.Clear();
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("C1ss", fp.C1ss);
        parameters.Add("C2ss", fp.C2ss);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Tst", fp.Tst);
        p.PInC("3.4", parameters);
        fp.Afs = 3.68 * fp.densityOfGas * fp.Z * fp.Pst * fp.Tpl * (Math.Log(fp.Rk / fp.rc) + fp.C1ss + fp.C2ss) /
                 (fp.k * fp.h * fp.Tst);
        p.POutC("3.4", "Afs", fp.Afs);
    }
    
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (3.5)
    /// </summary>
    public void Bfs1()
    {
        parameters.Clear();
        parameters.Add("Rogst", fp.Rogst);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("C3ss", fp.C3ss);
        parameters.Add("C4ss", fp.C4ss);
        parameters.Add("Kmshpl", fp.Kmshpl);
        parameters.Add("h", fp.h);
        parameters.Add("Tst", fp.Tst);
        p.PInC("3.5", parameters);
        fp.Bfs = 6.8E-6 * fp.Rogst * fp.Z * fp.Pst * fp.Tpl * (1.0 / fp.rc - 1.0 / fp.Rk + fp.C3ss + fp.C4ss) /
                 (fp.Kmshpl * Math.Pow(fp.h, 2) * fp.Tst);
        p.POutC("3.5", "Bfs", fp.Bfs);
    }
    
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (3.7)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="reservoirPressurePa">Массив значений пластовых давлений, Па</param>
    /// <param name="Pz">Массив значений давлений на забое скважины, Па</param>
    /// <param name="Q">Массив значений дебита скважины, м3/с</param>
    public void Afs2(int N, double[] reservoirPressurePa, double[] Pz, double[] Q)
    {
        var sum1 = 0.0;
        var sum2 = 0.0; 
        var sum3 = 0.0;
        var sum4 = 0.0;
        for (int i = 0; i < N; i++)
        {
            sum1 += (Math.Pow(reservoirPressurePa[i], 2) - Math.Pow(Pz[i], 2)) / Q[i];
            sum2 += Math.Pow(Q[i], 2);
            sum3 += Q[i];
            sum4 += Math.Pow(reservoirPressurePa[i], 2) - Math.Pow(Pz[i], 2);
        }
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("reservoirPressurePa[0]", reservoirPressurePa[0]);
        parameters.Add("Pz[0]", Pz[0]);
        parameters.Add("Q[0]", Q[0]);
        p.PInC("3.7", parameters);
        fp.Afs = (sum1 * sum2 - sum3 * sum4) / (N * sum2 - Math.Pow(sum3, 2));
        p.POutC("3.7", "Afs", fp.Afs);
    }
    
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (3.8)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="reservoirPressurePa">Давление на контуре питания (=== пластовое давление), Па</param>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    /// <param name="Q">Массив значений дебита скважины, м3/с</param>
    public void Bfs2(int N, double[] reservoirPressurePa, double[] Pz, double[] Q)
    {
        var sum1 = 0.0;
        var sum2 = 0.0; 
        var sum3 = 0.0;
        var sum4 = 0.0;
        for (int i = 0; i < N; i++)
        {
            sum1 += (Math.Pow(reservoirPressurePa[i], 2) - Math.Pow(Pz[i], 2)) / Q[i];
            sum2 += Math.Pow(Q[i], 2);
            sum3 += Q[i];
            sum4 += Math.Pow(reservoirPressurePa[i], 2) - Math.Pow(Pz[i], 2);
        }
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("reservoirPressurePa[0]", reservoirPressurePa[0]);
        parameters.Add("Pz[0]", Pz[0]);
        parameters.Add("Q[0]", Q[0]);
        p.PInC("3.8", parameters);
        fp.Bfs = (N * sum4 - sum3 * sum1) / (N * sum2 - Math.Pow(sum3, 2));
        p.POutC("3.8", "Bfs", fp.Bfs);
    }
    
    /// <summary>
    /// Гидропроводность (м3/(с*Па)) (3.9)
    /// </summary>
    public void eps1()
    {
        parameters.Clear();
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("C1ss", fp.C1ss);
        parameters.Add("C2ss", fp.C2ss);
        parameters.Add("Afs", fp.Afs);
        parameters.Add("Tst", fp.Tst);
        p.PInC("3.9", parameters);
        fp.eps = 3.68 * fp.Z * fp.Pst * fp.Tpl * (Math.Log(fp.Rk / fp.rc) + fp.C1ss + fp.C2ss) / (fp.Afs * fp.Tst);
        p.POutC("3.9", "eps", fp.eps);
    }
    
    /// <summary>
    /// Проницаемость пласта (м2) (3.10)
    /// </summary>
    public void k1()
    {
        parameters.Clear();
        parameters.Add("Z", fp.Z);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("C1ss", fp.C1ss);
        parameters.Add("C2ss", fp.C2ss);
        parameters.Add("Afs", fp.Afs);
        parameters.Add("h", fp.h);
        parameters.Add("Tst", fp.Tst);
        p.PInC("3.10", parameters);
        fp.k = 3.68 * fp.Z * fp.Mjug * fp.Pst * fp.Tpl * (Math.Log(fp.Rk / fp.rc) + fp.C1ss + fp.C2ss) / 
               (fp.Afs * fp.h * fp.Tst);
        p.POutC("3.10", "k", fp.k);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (3.11)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz3(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("AlfaFes", fp.AlfaFes);
        parameters.Add("BettaFes", fp.BettaFes);
        p.PInC("3.11", parameters);
        fp.Pz = Math.Sqrt(fp.AlfaFes + fp.BettaFes * Math.Log10(time));
        p.POutC("3.11", "Pz", fp.Pz);
    }

    /// <summary>
    /// Коэффициент, зависящий от ФЕС пласта (3.12)
    /// </summary>
    public void AlfaFes()
    {
        parameters.Clear();
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("BettaFes", fp.BettaFes);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rpr", fp.rpr);
        parameters.Add("bkv", fp.bkv);
        parameters.Add("Q", fp.Q);
        p.PInC("3.12", parameters);
        fp.AlfaFes = Math.Pow(fp.Pzost, 2) + 
                     fp.BettaFes * Math.Log(2.246 * fp.piezoconductivityOfTheFormation / Math.Pow(fp.rpr, 2)) + 
                     fp.bkv * Math.Pow(fp.Q, 2);
        p.POutC("3.12", "AlfaFes", fp.AlfaFes);
    }
    
    /// <summary>
    /// Коэффициент, зависящий от ФЕС пласта (3.13, 3.20)
    /// </summary>
    public void BettaFes()
    {
        parameters.Clear();
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Q", fp.Q);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Zpl", fp.Zpl);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("3.13, 3.20", parameters);
        fp.BettaFes = 2.3 * 11.6 * fp.Q * fp.Mjug * fp.Tpl * fp.Zpl * fp.Pst / 
                      (2.0 * fp.pi * fp.k * fp.h * fp.Tst);
        p.POutC("3.13, 3.20", "BettaFes", fp.BettaFes);
    }
    
    /// <summary>
    /// Коэффициент пьезопроводности (м2/с) (3.14)
    /// </summary>
    public void piezoconductivityOfTheFormation()
    {
        parameters.Clear();
        parameters.Add("k", fp.k);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("m", fp.m);
        parameters.Add("Mjug", fp.Mjug);
        p.PInC("3.14", parameters);
        fp.piezoconductivityOfTheFormation = 0.01 * fp.k * fp.reservoirPressurePa / (fp.m * fp.Mjug);
        p.POutC("3.14", "piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
    }
    
    /// <summary>
    /// Продолжительность исследования (секунды) (3.15, 3.22)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void dt(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("t0", fp.t0);
        p.PInC("3.15, 3.22", parameters);
        fp.dt = time - fp.t0;
        p.POutC("3.15, 3.22", "dt", fp.dt);
    }
    
    /// <summary>
    /// Гидропроводность (м3/(с*Па)) (3.16, 3.23)
    /// </summary>
    public void eps2()
    {
        parameters.Clear();
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Mjug", fp.Mjug);
        p.PInC("3.16, 3.23", parameters);
        fp.eps = fp.k * fp.h / fp.Mjug;
        p.POutC("3.16, 3.23", "eps", fp.eps);
    }
    
    /// <summary>
    /// Коэффициент подвижности пласта (м2/(с*Па)) (3.17, 3.24)
    /// </summary>
    public void Kpodv()
    {
        parameters.Clear();
        parameters.Add("eps", fp.eps);
        parameters.Add("h", fp.h);
        p.PInC("3.17, 3.24", parameters);
        fp.Kpodv = fp.eps / fp.h;
        p.POutC("3.17, 3.24", "Kpodv", fp.Kpodv);
    }
    
    /// <summary>
    /// Проницаемость пласта (м2) (3.18, 3.25)
    /// </summary>
    public void k2()
    {
        parameters.Clear();
        parameters.Add("eps", fp.eps);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("h", fp.h);
        p.PInC("3.18, 3.25", parameters);
        fp.k = fp.eps * fp.Mjug / fp.h;
        p.POutC("3.18, 3.25", "k", fp.k);
    }
    
    /// <summary>
    /// Время Хорнера (с) (3.21)
    /// </summary>
    public void dte()
    {
        parameters.Clear();
        parameters.Add("dt", fp.dt);
        parameters.Add("tp", fp.tp);
        p.PInC("3.21", parameters);
        fp.dte = (fp.dt + fp.tp) / fp.dt;
        p.POutC("3.21", "dte", fp.dte);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (3.26.1)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Patm", fp.Patm);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("3.26.1", parameters);
        fp.Pz = Math.Sqrt(Math.Pow(fp.Pzrezh, 2) + fp.Qrezh * fp.Mjug * fp.Patm * 
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                     Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / 
            (2.0 * fp.pi * fp.k * fp.h));
        p.POutC("3.26.1", "Pz", fp.Pz);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (3.26.2)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz2(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Patm", fp.Patm);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("3.26.2", parameters);
        fp.Pz = Math.Sqrt(Math.Pow(fp.Pzrezh, 2) - fp.Qrezh * fp.Mjug * fp.Patm * 
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                     Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / 
            (2.0 * fp.pi * fp.k * fp.h));
        p.POutC("3.26.2", "Pz", fp.Pz);
    }
  
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (3.28)
    /// </summary>
    public void Afs3()
    {
        parameters.Clear();
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Tst", fp.Tst);
        p.PInC("3.28", parameters);
        fp.Afs = fp.Mjug * fp.Z * fp.Pst * fp.Tpl * Math.Log(fp.Rk / (fp.rc * Math.Exp(-fp.skinFactor))) / 
                 (fp.pi * fp.k * fp.h * fp.Tst);
        p.POutC("3.28", "Afs", fp.Afs);
    }
    
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (3.29)
    /// </summary>
    public void Bfs3()
    {
        parameters.Clear();
        parameters.Add("Rogst", fp.Rogst);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("Kmshpl", fp.Kmshpl);
        parameters.Add("h", fp.h);
        parameters.Add("Tst", fp.Tst);
        p.PInC("3.29", parameters);
        fp.Bfs = fp.Rogst * fp.Z * fp.Pst * fp.Tpl * (1.0 / fp.rc - 1.0 / fp.Rk) /
                 (fp.Kmshpl * Math.Pow(fp.h, 2) * fp.Tst);
        p.POutC("3.29", "Bfs", fp.Bfs);
    }
    
    /// <summary>
    /// Коэффициент макрошероховатости пласта (м) (3.30)
    /// </summary>
    public void Kmshpl()
    {
        parameters.Clear();
        parameters.Add("m", fp.m);
        parameters.Add("k", fp.k);
        p.PInC("3.30", parameters);
        fp.Kmshpl = fp.m * Math.Pow(fp.k / fp.m, 1.0 / 6.0) / 0.05;
        p.POutC("3.30", "Kmshpl", fp.Kmshpl);
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (3.31, 3.32)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("deltaPpred", fp.deltaPpred);
        parameters.Add("A1", fp.A1);
        parameters.Add("A2", fp.A2);
        p.PInC("3.31, 3.32", parameters);
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
        p.POutC("3.31, 3.32", "MP", fp.MP);
    }
    
    /// <summary>
    /// Дебит скважины по воде (м3/с) (3.33)
    /// </summary>
    public void Qzh()
    {
        parameters.Clear();
        parameters.Add("Bzh", fp.Bzh);
        parameters.Add("DP", fp.DP);
        parameters.Add("DPkr", fp.DPkr);
        p.PInC("3.33", parameters);
        fp.Qzh = fp.Bzh * fp.DP / fp.DPkr - fp.Bzh;
        p.POutC("3.33", "Qzh", fp.Qzh);
    }
    
    /// <summary>
    /// Буферное давление (Па) (3.34)
    /// </summary>
    public void Pbuf1()
    {
        parameters.Clear();
        parameters.Add("Plin", fp.Plin);
        parameters.Add("Fi", fp.Fi);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("dsht", fp.dsht);
        p.PInC("3.34", parameters);
        fp.Pbuf1 = fp.Plin * 0.00729 * Math.Pow(fp.Fi, 2) * fp.Qgpt * 86400.0 * fp.densityOfGas / 
                   Math.Pow(1000.0 * fp.dsht, 2);
        p.POutC("3.34", "Pbuf1", fp.Pbuf1);
    }
    
    /// <summary>
    /// Объемный расход газа при Р и Т (м3/с) (3.35)
    /// </summary>
    public void Qgpt1()
    {
        parameters.Clear();
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Z", fp.Z);
        parameters.Add("Plin", fp.Plin);
        p.PInC("3.35", parameters);
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tsep * fp.Z / (fp.Plin * fp.Tst);
        p.POutC("3.35", "Qgpt", fp.Qgpt);
    }
    
    /// <summary>
    /// Линейное давление (Па)  (3.36)
    /// </summary>
    public void Plin2()
    {
        parameters.Clear();
        parameters.Add("Pdikt1", fp.Pdikt1);
        parameters.Add("DPsep", fp.DPsep);
        p.PInC("3.36", parameters);
        fp.Plin = fp.Pdikt1 + fp.DPsep;
        p.POutC("3.36", "Plin", fp.Plin);
    }
    
    /// <summary>
    /// ???, K/Па  (3.37)
    /// </summary>
    public void Dbuf2()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Pdikt1", fp.Pdikt1);
        p.PInC("3.37", parameters);
        fp.Dbuf = (-0.02034 * (fp.Tsep - 273.0) - 0.124 * fp.Pdikt1 * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("3.37", "Dbuf", fp.Dbuf);
    }
    
    /// <summary>
    /// Температура газа после штуцера (К)  (3.38)
    /// </summary>
    public void Tsep2()
    {
        parameters.Clear();
        parameters.Add("Tdikt", fp.Tdikt);
        parameters.Add("DPsep", fp.DPsep);
        parameters.Add("Dbuf", fp.Dbuf);
        p.PInC("3.38", parameters);
        fp.Tsep = fp.Tdikt + fp.DPsep * fp.Dbuf;
        p.POutC("3.38", "Tsep", fp.Tsep);
    }
    
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по второй формуле для ДИКТ (Па) (3.39)
    /// </summary>
    public void Pdikt2()
    {
        parameters.Clear();
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("Tdikt", fp.Tdikt);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rootn", fp.Rootn);
        parameters.Add("Cdikt", fp.Cdikt);
        p.PInC("3.39", parameters);
        fp.Pdikt2 = fp.Qgpt * Math.Sqrt(fp.Tdikt * fp.Z * fp.Rootn) / (0.864 * fp.Cdikt);
        p.POutC("3.39", "Pdikt2", fp.Pdikt2);
    }
    
    /// <summary>
    /// Объемный расход газа при Р и Т (м3/с) (3.40)
    /// </summary>
    public void Qgpt2()
    {
        parameters.Clear();
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tdikt", fp.Tdikt);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pdikt1", fp.Pdikt1);
        p.PInC("3.40", parameters);
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tdikt * fp.Z / (fp.Pdikt1 * fp.Tst);
        p.POutC("3.40", "Qgpt", fp.Qgpt);
    }
        
    /// <summary>
    /// Коэффициент (3.41, 3.42)
    /// </summary>
    public void Cdikt()
    {
        parameters.Clear();
        parameters.Add("PrDikt", fp.PrDikt);
        parameters.Add("dsht", fp.dsht);
        p.PInC("3.41, 3.42", parameters);
        if (fp.PrDikt == 0) fp.Cdikt = 238966.0 * Math.Pow(fp.dsht, 2.0581);
        else fp.Cdikt = 197817.0 * Math.Pow(fp.dsht, 2.0172);
        p.POutC("3.41, 3.42", "Cdikt", fp.Cdikt);
    }
    
    /// <summary>
    /// Фактическая скорость газа (м/с) (2.9)
    /// </summary>
    /// <param name="Tz">Температура на забое скважины, Па</param>
    public void Vgf(double Tz)
    {
        parameters.Clear();
        parameters.Add("Tz", Tz);
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Z", fp.Z);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Tst", fp.Tst);
        p.PInC("2.9", parameters);
        fp.Vgf = 4.0 * fp.Qg0 * fp.Pst * Tz * fp.Z / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2) * fp.Pz * fp.Tst);
        p.POutC("2.9", "Vgf", fp.Vgf);
    }
    
    /// <summary>
    /// Плотность газа (кг/м3) (2.10)
    /// </summary>
    /// <param name="Tz">Температура на забое скважины, Па</param>
    public void densityOfGas(double Tz)
    {
        parameters.Clear();
        parameters.Add("Tz", Tz);
        parameters.Add("Rogst", fp.Rogst);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Tst", fp.Tst);
        p.PInC("2.9", parameters);
        fp.densityOfGas = fp.Rogst * fp.Pz * fp.Tst / (fp.Pst * Tz * fp.Z);
        p.POutC("2.10", "densityOfGas", fp.densityOfGas);
    }
    
    /// <summary>
    /// Критическая скорость газа (м/с) (2.11)
    /// </summary>
    public void Vgkr()
    {
        parameters.Clear();
        parameters.Add("g", fp.g);
        parameters.Add("dv", fp.dv);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.11", parameters);
        fp.Vgkr = 2.5 * Math.Sqrt(fp.g * fp.dv * (fp.densityOfWater - fp.densityOfGas) / fp.densityOfGas);
        p.POutC("2.11", "Vgkr", fp.Vgkr);
    }
    
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по первой формуле для ДИКТ (Па) (2.58)
    /// </summary>
    public void Pdikt1()
    {
        parameters.Clear();
        parameters.Add("Plin", fp.Plin);
        parameters.Add("DPsep", fp.DPsep);
        p.PInC("2.", parameters);
        fp.Pdikt1 = Math.Abs(fp.Plin - fp.DPsep); // Д. Гуменюк, отладка (Abs)
        p.POutC("2.58", "Pdikt1", fp.Pdikt1);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (2.78)
    /// </summary>
    public void Pz4()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("KS", fp.KS);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        p.PInC("2.78", parameters);
        fp.Pz = Math.Sqrt(Math.Pow(fp.Ppr, 2) * Math.Exp(2.0 * fp.KS) + fp.Teta * Math.Pow(fp.Qgpt, 2));
        p.POutC("2.78", "Pz", fp.Pz);
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (2.98)
    /// </summary>
    public void DPpzo1()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pzost", fp.Pzost);
        p.PInC("2.9", parameters);
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
        p.POutC("2.98", "DPpzo1", fp.DPpzo1);
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (2.99)
    /// </summary>
    public void tzab1()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("rc", fp.rc);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("2.99", parameters);
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.Mjug)) * fp.rc /  // Д. Гуменюк, отладка (вязкость)
                                                      (2.25 * fp.piezoconductivityOfTheFormation); 
        p.POutC("2.99", "tzab1", fp.tzab1);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (2.100)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz5(double time)
    {
        parameters.Clear();
        parameters.Add("time", fp.time);
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("tzab1", fp.tzab1);
        p.PInC("2.100", parameters);
        fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        p.POutC("2.100", "Pz", fp.Pz);
    }

    /// <summary>
    /// Буферное давление (Па) (2.104)
    /// </summary>
    public void Pbuf3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("2.104", parameters);
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("2.104", "Pbuf3", fp.Pbuf3);
    }
    
    /// <summary>
    /// Массовый расход газа (кг/с) (2.14, 2.28)
    /// </summary>
    public void G()
    {
        parameters.Clear();
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("Rogst", fp.Rogst);
        p.PInC("2.14, 2.28", parameters);
        fp.G = fp.Qgpt * fp.Rogst;
        p.POutC("2.14, 2.28", "G", fp.G);
    }
        
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на уровне пласта (K/Па) (2.15)
    /// </summary>
    public void Dpl()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInC("2.15", parameters);
        fp.Dpl = (-0.02034 * (fp.Tpl - 273.0) - 0.124 * fp.reservoirPressurePa * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.15", "Dpl", fp.Dpl);
    }
    
    /// <summary>
    /// Разность температур на уровне пласта и на забое (K) (2.16)
    /// </summary>
    public void DTplz()
    {
        parameters.Clear();
        parameters.Add("Dpl", fp.Dpl);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("G", fp.G);
        parameters.Add("Cg", fp.Cg);
        parameters.Add("tn", fp.tn);
        parameters.Add("pi", fp.pi);
        parameters.Add("h", fp.h);
        parameters.Add("Cgp", fp.Cgp);
        parameters.Add("rc", fp.rc);
        parameters.Add("Rk", fp.Rk);
        p.PInC("2.16", parameters);
        fp.DTplz = fp.Dpl * (fp.reservoirPressurePa - fp.Pz) * 
                   Math.Log10(1.0 + fp.G * fp.Cg * fp.tn / (fp.pi * fp.h * fp.Cgp * Math.Pow(fp.rc, 2))) / 
                   Math.Log10(fp.Rk / fp.rc);
        p.POutC("2.16", "DTplz", fp.DTplz);
    }
    /// <summary>
    /// ??? (???) (2.17, 2.31)
    /// </summary>
    public void fes()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Lgp", fp.Lgp);
        parameters.Add("tn", fp.tn);
        parameters.Add("rc", fp.rc);
        p.PInC("2.17, 2.31", parameters);
        fp.fes = Math.Log(1.0 + Math.Sqrt(fp.pi * fp.Lgp * fp.tn / Math.Pow(Math.Log(fp.rc), 2)));
        p.POutC("2.17, 2.31", "fes", fp.fes);
    }
        
    /// <summary>
    /// Средний зенитный угол ствола скважины (град.) (2.18, 2.32)
    /// </summary>
    public void alfa()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Lgp", fp.Lgp);
        parameters.Add("G", fp.G);
        parameters.Add("Cg", fp.Cg);
        parameters.Add("fes", fp.fes);
        p.PInC("2.18, 2.32", parameters);
        fp.alfa = 2.0 * fp.pi * fp.Lgp / (fp.G * fp.Cg * fp.fes);
        p.POutC("2.18, 2.32", "alfa", fp.alfa);
    }
    
    /// <summary>
    /// Температура на забое скважины (К) (2.19)
    /// </summary>
    public void Tz()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("DTplz", fp.DTplz);
        p.PInC("2.19", parameters);
        fp.Tz = fp.Tpl - fp.DTplz;
        p.POutC("2.19", "Tz", fp.Tz);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на забое (K/Па) (2.20)
    /// </summary>
    public void Dz()
    {
        parameters.Clear();
        parameters.Add("Tz", fp.Tz);
        parameters.Add("Pz", fp.Pz);
        p.PInC("2.20", parameters);
        fp.Dz = (-0.02034 * (fp.Tz - 273.0) - 0.124 * fp.Pz * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.20", "Dz", fp.Dz);
    }
    
    /// <summary>
    /// Температура на приеме НКТ (K) (2.21)
    /// </summary>
    public void Tpr()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgr", fp.Tgr);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("DTplz", fp.DTplz);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Dz", fp.Dz);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Ppr", fp.Ppr);
        p.PInC("2.21", parameters);
        fp.Tpr = fp.Tpl - fp.Tgr * (fp.Lk - fp.Lcp) - fp.DTplz * Math.Exp(-fp.alfa * (fp.Lk - fp.Lcp)) +
                 (fp.Tgr - fp.Dz * (fp.Pz - fp.Ppr) / fp.Lk) * (1.0 - Math.Exp(-fp.alfa * (fp.Lk - fp.Lcp))) / fp.alfa;
        p.POutC("2.21", "Tpr", fp.Tpr);
    }

    /// <summary>
    /// Давление на приеме в НКТ (Па) (2.22)
    /// </summary>
    public void Ppr1()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("KS", fp.KS);
        p.PInC("2.22", parameters);
        fp.Ppr = Math.Sqrt((Math.Pow(fp.Pz, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
        p.POutC("2.22", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    ///  Средняя температура (K) (2.23, 2.41, 2.79, 2.93)
    /// </summary>
    public void Tcp1()
    {
        parameters.Clear();
        parameters.Add("Tz", fp.Tz);
        parameters.Add("Tpr", fp.Tpr);
        p.PInC("2.23, 2.41, 2.79, 2.93", parameters);
        fp.Tcp = (fp.Tz + fp.Tpr) / 2.0;
        p.POutC("2.23, 2.41, 2.79, 2.93", "Tcp", fp.Tcp);
    }
    
    /// <summary>
    ///  Среднее давление (Па) (2.24, 2.42, 2.80, 2.94)
    /// </summary>
    public void Pcp1()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Ppr", fp.Ppr);
        p.PInC("2.24, 2.42, 2.80, 2.94", parameters);
        fp.Pcp = (fp.Pz + fp.Ppr) / 2.0;
        p.POutC("2.24, 2.42, 2.80, 2.94", "Pcp", fp.Pcp);
    }
    
    /// <summary>
    ///  Коэффициент (2.26, 2.44, 2.82, 2.96)
    /// </summary>
    public void KS1()
    {
        parameters.Clear();
        parameters.Add("Rogotn", fp.Rogotn);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Hzhek", fp.Hzhek);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        p.PInC("2.26, 2.44, 2.82, 2.96", parameters);
        fp.KS = 0.03415 * fp.Rogotn * (fp.Lk - fp.Lcp - fp.Hzhek) / (fp.Tcp * fp.Z);
        p.POutC("2.26, 2.44, 2.82, 2.96", "Pcp", fp.Pcp);
    }
    
    /// <summary>
    ///  Коэффициент (2.27, 2.45, 2.83, 2.97)
    /// </summary>
    public void Teta1()
    {
        parameters.Clear();
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        parameters.Add("KS", fp.KS);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.27, 2.45, 2.83, 2.97", parameters);
        fp.Teta = 0.01377 * fp.Lambda * Math.Pow(fp.Tcp * fp.Z, 2) *
            (Math.Exp(2.0 * fp.KS) - 1.0) / Math.Pow(fp.ECInnerDiameter, 5); 
        p.POutC("2.27, 2.45, 2.83, 2.97", "Teta", fp.Teta);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на спуске НКТ (K/Па) (2.29)
    /// </summary>
    public void Dsp()
    {
        parameters.Clear();
        parameters.Add("Tsp", fp.Tsp);
        parameters.Add("Psp", fp.Psp);
        p.PInC("2.29", parameters);
        fp.Dsp = (-0.02034 * (fp.Tsp - 273.0) - 0.124 * fp.Psp * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.29", "Dsp", fp.Dsp);
    }
        
    /// <summary>
    /// Температура на буфере (К) (2.30)
    /// </summary>
    public void Tbuf1()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgr", fp.Tgr);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("DTplz", fp.DTplz);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Dsp", fp.Dsp);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pu", fp.Pu);
        p.PInC("2.30", parameters);
        fp.Tbuf = fp.Tpl - fp.Tgr * fp.Lk - fp.DTplz * Math.Exp(-fp.alfa * fp.Lk) +
                  (fp.Tgr - fp.Dsp * (fp.Pz - fp.Pu) / fp.Lk) * (1.0 - Math.Exp(-fp.alfa * fp.Lk)) / fp.alfa;
        p.POutC("2.30", "Tbuf", fp.Tbuf);
    }
    
    /// <summary>
    ///  Средняя температура (K) (2.34, 2.49, 2.73, 2.86)
    /// </summary>
    public void Tcp2()
    {
        parameters.Clear();
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Tpr", fp.Tpr);
        p.PInC("2.34, 2.49, 2.73, 2.86", parameters);
        fp.Tcp = (fp.Tbuf + fp.Tpr) / 2.0;
        p.POutC("2.34, 2.49, 2.73, 2.86", "Tcp", fp.Tcp);
    }
    
    /// <summary>
    ///  Среднее давление (Па) (2.35, 2.50, 2.74, 2.87)
    /// </summary>
    public void Pcp2()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Pbuf1", fp.Pbuf1);
        p.PInC("2.35, 2.50, 2.74, 2.87", parameters);
        fp.Pcp = (fp.Ppr + fp.Pbuf1) / 2.0;
        p.POutC("2.35, 2.50, 2.74, 2.87", "Pcp", fp.Pcp);
    }
    
    /// <summary>
    ///  Коэффициент (2.37, 2.52, 2.76, 2.89)
    /// </summary>
    public void KS2()
    {
        parameters.Clear();
        parameters.Add("Rogotn", fp.Rogotn);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("htr", fp.htr);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        p.PInC("2.37, 2.52, 2.76, 2.89", parameters);
        fp.KS = 0.03415 * fp.Rogotn * (fp.Lcp - fp.htr) / (fp.Tcp * fp.Z);
        p.POutC("2.37, 2.52, 2.76, 2.89", "KS", fp.KS);
    }
    
    /// <summary>
    ///  Коэффициент (2.38, 2.53, 2.77, 2.90)
    /// </summary>
    public void Teta2()
    {
        parameters.Clear();
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        parameters.Add("KS", fp.KS);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.38, 2.53, 2.77, 2.90", parameters);
        fp.Teta = 0.01377 * fp.Lambda * Math.Pow(fp.Tcp * fp.Z, 2) *
            (Math.Exp(2.0 * fp.KS) - 1.0) / Math.Pow(fp.dnkt, 5); 
        p.POutC("2.38, 2.53, 2.77, 2.90", "Teta", fp.Teta);
    }
    
    /// <summary>
    /// Уровень жидкости в ЭК (м) (2.12)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Hzhek(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.12", parameters);
        fp.Hzhek = 4.0 * fp.Qskv * (tz2 - tz1) / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("2.12", "Hzhek", fp.Hzhek);
    }
    
    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.13)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void htr(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.13", parameters);
        fp.htr = 4.0 * fp.Qskv * (tz2 - tz1) / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("2.13", "htr", fp.htr);
    }
    
    /// <summary>
    /// Давление на уровне жидкости в ЭК (Па) (2.39, 2.92-1)
    /// </summary>
    public void Pur1()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("g", fp.g);
        parameters.Add("Hzhek", fp.Hzhek);
        p.PInC("2.39, 2.92-1", parameters);
        fp.Pur1 = fp.Pz - fp.densityOfWater * fp.g * fp.Hzhek;
        p.POutC("2.39, 2.92-1", "Pur1", fp.Pur1);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.40, 2.92-2)
    /// </summary>
    public void Ppr2()
    {
        parameters.Clear();
        parameters.Add("Pur1", fp.Pur1);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("KS", fp.KS);
        p.PInC("2.40, 2.92-2", parameters);
        fp.Ppr = Math.Sqrt((Math.Pow(fp.Pur1, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
        p.POutC("2.40, 2.92-2", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.46, 2.91)
    /// </summary>
    public void Ppr3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("2.46, 2.91", parameters);
        fp.Ppr = fp.Pz - fp.densityOfWater * fp.g * (fp.Lk - fp.Lcp);
        p.POutC("2.46, 2.91", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Давление на уровне жидкости в НКТ (Па) (2.47, 2.84)
    /// </summary>
    public void Pur2()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("g", fp.g);
        parameters.Add("htr", fp.htr);
        p.PInC("2.47, 2.84", parameters);
        fp.Pur2 = fp.Ppr - fp.densityOfWater * fp.g * fp.htr;
        p.POutC("2.47, 2.84", "Pur2", fp.Pur2);
    }
    
    /// <summary>
    /// Буферное давление (Па) (2.48, 2.85)
    /// </summary>
    public void Pbuf2()
    {
        parameters.Clear();
        parameters.Add("Pur2", fp.Pur2);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("KS", fp.KS);
        p.PInC("2.48, 2.85", parameters);
        fp.Pbuf2 = Math.Sqrt((Math.Pow(fp.Pur2, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
        p.POutC("2.48, 2.85", "Pbuf2", fp.Pbuf2);
    }
        
    /// <summary>
    /// Линейное давление (Па)  (2.54)
    /// </summary>
    public void Plin1()
    {
        parameters.Clear();
        parameters.Add("dsht", fp.dsht);
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.54", parameters);
        fp.Plin = Math.Pow(1000.0 * fp.dsht, 2) * fp.Pbuf1 / 
                  (0.0729 * Math.Pow(1.0, 2) * fp.Qg0 * 86400.0 * fp.densityOfGas);
        p.POutC("2.54", "Plin", fp.Plin);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на буфере (K/Па) (2.56, 2.70)
    /// </summary>
    public void Dbuf1()
    {
        parameters.Clear();
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Pbuf1", fp.Pbuf1);
        p.PInC("2.56, 2.70", parameters);
        fp.Dbuf = (-0.02034 * (fp.Tbuf - 273.0) - 0.124 * fp.Pbuf1 * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.56, 2.70", "Dbuf", fp.Dbuf);
    }
    
    /// <summary>
    /// Температура газа после штуцера (К)  (2.57)
    /// </summary>
    public void Tsep1()
    {
        parameters.Clear();
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("Dbuf", fp.Dbuf);
        p.PInC("2.57", parameters);
        fp.Tsep = fp.Tbuf - (fp.Pbuf1 - fp.Plin) * fp.Dbuf;
        p.POutC("2.57", "Tsep", fp.Tsep);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на ДИКТе (K/Па) (2.59)
    /// </summary>
    public void Ddikt()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Pdikt1", fp.Pdikt1);
        p.PInC("2.59", parameters);
        fp.Ddikt = (-0.02034 * (fp.Tsep - 273.0) - 0.124 * fp.Pdikt1 * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.59", "Ddikt", fp.Ddikt);
    }
    
    /// <summary>
    /// Температура на ДИКТе (K) (2.60)
    /// </summary>
    public void Tdikt()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("DPsep", fp.DPsep);
        parameters.Add("Ddikt", fp.Ddikt);
        p.PInC("2.60", parameters);
        fp.Tdikt = fp.Tsep - fp.DPsep * fp.Ddikt;
        p.POutC("2.60", "Tdikt", fp.Tdikt);
    }
    
    /// <summary>
    /// Температура на буфере (К) (2.71)
    /// </summary>
    public void Tbuf2()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("Dbuf", fp.Dbuf);
        p.PInC("2.71", parameters);
        fp.Tbuf = fp.Tsep + (fp.Pbuf1 - fp.Plin) * fp.Dbuf;
        p.POutC("2.71", "Tbuf", fp.Tbuf);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.72)
    /// </summary>
    public void Ppr4()
    {
        parameters.Clear();
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("KS", fp.KS);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        p.PInC("2.72", parameters);
        fp.Ppr = Math.Sqrt(Math.Pow(fp.Pbuf1, 2) * Math.Exp(2.0 * fp.KS) + fp.Teta * Math.Pow(fp.Qgpt, 2));
        p.POutC("2.72", "Ppr", fp.Ppr);
    }

}

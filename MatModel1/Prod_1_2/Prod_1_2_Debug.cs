using System;

public class Prod_1_2_Debug
{
    private FormulaParameters fp;
    private Print p;

    public Prod_1_2_Debug(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    public void Prod_1_2_Debug_Main()
    {
        // 2.1.1.
        p.PfNameD("2.1.1.");
        var time = 1.0;
        fp.Pzrezh = 16.0E6;
        fp.Qrezh = 67.36854134;
        fp.Mjug = 0.000016;
        fp.Patm = 101300.0;
        fp.piezoconductivityOfTheFormation = 6.0;
        fp.rc = 0.073;
        fp.skinFactor = 2.0;
        fp.k = 1.0E-12;
        fp.h = 12.0;
        p.PInD("time", time);
        p.PInD("Pzrezh", fp.Pzrezh);
        p.PInD("Qrezh", fp.Qrezh);
        p.PInD("Mjug", fp.Mjug);
        p.PInD("Patm", fp.Patm);
        p.PInD("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInD("rc", fp.rc);
        p.PInD("skinFactor", fp.skinFactor);
        p.PInD("k", fp.k);
        p.PInD("h", fp.h);
        fp.p12.Pz1(time);
        p.POutD("Pz", fp.Pz);
        
        // 2.1.2.
        p.PfNameD("2.1.2.");
        time = 7200.0;
        fp.Pzrezh = 16.0E6;
        fp.Qrezh = 67.36854134;
        fp.Mjug = 0.000016;
        fp.Patm = 101300.0;
        fp.piezoconductivityOfTheFormation = 6.0;
        fp.rc = 0.073;
        fp.skinFactor = 2.0;
        fp.k = 1.0E-12;
        fp.h = 12.0;
        p.PInD("time", time);
        p.PInD("Pzrezh", fp.Pzrezh);
        p.PInD("Qrezh", fp.Qrezh);
        p.PInD("Mjug", fp.Mjug);
        p.PInD("Patm", fp.Patm);
        p.PInD("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInD("rc", fp.rc);
        p.PInD("skinFactor", fp.skinFactor);
        p.PInD("k", fp.k);
        p.PInD("h", fp.h);
        fp.p12.Pz2(time);
        p.POutD("Pz", fp.Pz);
        
        // 2.2.
        p.PfNameD("2.2.");
        fp.Pz = 15505347.2;
        fp.Afs = 8.84428E11;
        fp.Qg0 = 1.14;
        fp.Bfs = 20141985264388.0;
        p.PInD("Pz", fp.Pz);
        p.PInD("Afs", fp.Afs);
        p.PInD("Qg0", fp.Qg0);
        p.PInD("Bfs", fp.Bfs);
        fp.p12.reservoirPressurePa();
        p.POutD("reservoirPressurePa", fp.reservoirPressurePa);
        
        // 2.3.
        p.PfNameD("2.3.");
        fp.Mjug = 0.000016;
        fp.Z = 1.2;
        fp.Pst = 101300.0;
        fp.Tpl = 273.0;
        fp.Rk = 250.0;
        fp.rc = 0.073;
        fp.skinFactor = 2.0;
        fp.k = 1.0E-12;
        fp.h = 12.0;
        fp.Tst = 293.0;
        p.PInD("Mjug", fp.Mjug);
        p.PInD("Z", fp.Z);
        p.PInD("Pst", fp.Pst);
        p.PInD("Tpl", fp.Tpl);
        p.PInD("Rk", fp.Rk);
        p.PInD("rc", fp.rc);
        p.PInD("skinFactor", fp.skinFactor);
        p.PInD("pi", fp.pi);
        p.PInD("k", fp.k);
        p.PInD("h", fp.h);
        p.PInD("Tst", fp.Tst);
        fp.p12.Afs();
        p.POutD("Afs", fp.Afs);
        
        // 2.4.
        p.PfNameD("2.4.");
        fp.Rogst = 15.0;
        fp.Z = 1.2;
        fp.Pst = 101300.0;
        fp.Tpl = 273.0;
        fp.rc = 0.073;
        fp.Rk = 250.0;
        fp.Kmshpl = 1.0E-9;    
        fp.h = 12.0;
        fp.Tst = 293.0;
        p.PInD("Rogst", fp.Rogst);
        p.PInD("Z", fp.Z);
        p.PInD("Pst", fp.Pst);
        p.PInD("Tpl", fp.Tpl);
        p.PInD("rc", fp.rc);
        p.PInD("Rk", fp.Rk);
        p.PInD("Kmshpl", fp.Kmshpl);
        p.PInD("h", fp.h);
        p.PInD("Tst", fp.Tst);
        fp.p12.Bfs();
        p.POutD("Bfs", fp.Bfs);
        
        // 2.5.
        p.PfNameD("2.5.");
        fp.k = 1.0E-12;
        fp.m = 0.2;
        p.PInD("k", fp.k);
        p.PInD("m", fp.m);
        fp.p12.Kmshpl();
        p.POutD("Kmshpl", fp.Kmshpl);
        
        // 2.6, 2.7.
        p.PfNameD("2.6, 2.7.");
        fp.Pz = 15123575.42;
        fp.reservoirPressurePa = 16.0E6;
        fp.deltaPpred = 25.0E6;
        fp.A1 = 3.0E-3;
        fp.A2 = 3.0E-3;
        p.PInD("Pz", fp.Pz);
        p.PInD("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInD("deltaPpred", fp.deltaPpred);
        p.PInD("A1", fp.A1);
        p.PInD("A2", fp.A2);
        fp.p12.MP(fp.Pz);
        p.POutD("MP", fp.MP);
        
        // 2.8.
        p.PfNameD("2.8.");
        fp.Bzh = 5.0 * 1.157E-5;
        fp.DP = 1.0E6;
        fp.DPkr = 6.0E6;
        p.PInD("Bzh", fp.Bzh);
        p.PInD("DP", fp.DP);
        p.PInD("DPkr", fp.DPkr);
        fp.p12.Qzh();
        p.POutD("Qzh", fp.Qzh);
        
        // 2.9.
        p.PfNameD("2.9.");
        fp.Qg0 = 1.14;
        fp.Pst = 101300.0;
        fp.Tz = 287.0;
        fp.Z = 1.2;
        fp.ECInnerDiameter = 153.4E-3;
        fp.Pz = 15123575.42;
        fp.Tst = 293.0;
        p.PInD("Qg0", fp.Qg0);
        p.PInD("Pst", fp.Pst);
        p.PInD("Tz", fp.Tz);
        p.PInD("Z", fp.Z);
        p.PInD("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInD("Pz", fp.Pz);
        p.PInD("Tst", fp.Tst);
        fp.p12.Vgf(fp.Tz);
        p.POutD("Vgf", fp.Vgf);
        
        // 2.10.
        p.PfNameD("2.10.");
        fp.Tz = 287.0;
        fp.Rogst = 15.0;
        fp.Pz = 15123575.42;
        fp.Tst = 293.0;
        fp.Pst = 101300.0;
        fp.Z = 1.2;
        p.PInD("Tz", fp.Tz);
        p.PInD("Rogst", fp.Rogst);
        p.PInD("Pz", fp.Pz);
        p.PInD("Tst", fp.Tst);
        p.PInD("Pst", fp.Pst);
        p.PInD("Z", fp.Z);
        fp.p12.densityOfGas(fp.Tz);
        p.POutD("densityOfGas", fp.densityOfGas);
        
        // 2.11.
        p.PfNameD("2.11.");
        fp.dv = 0.009;
        fp.densityOfWater = 1100.0;
        fp.densityOfGas = 1.5;
        p.PInD("g", fp.g);
        p.PInD("dv", fp.dv);
        p.PInD("densityOfWater", fp.densityOfWater);
        p.PInD("densityOfGas", fp.densityOfGas);
        fp.p12.Vgkr();
        p.POutD("Vgkr", fp.Vgkr);
        
        // 2.12.
        p.PfNameD("2.12.");
        fp.tz1 = time - 1.0;
        fp.tz2 = time;
        fp.Qskv = fp.Qzh;
        fp.ECInnerDiameter = 153.4E-3;
        p.PInD("pi", fp.pi);
        p.PInD("tz1", fp.tz1);
        p.PInD("tz2", fp.tz2);
        p.PInD("Qskv", fp.Qskv);
        p.PInD("ECInnerDiameter", fp.ECInnerDiameter);
        fp.p12.Hzhek(fp.tz1, fp.tz2);
        p.POutD("Hzhek", fp.Hzhek);
        
        // 2.13.
        p.PfNameD("2.13.");
        fp.tz1 = time - 1.0;
        fp.tz2 = time;
        fp.Qskv = fp.Qzh;
        fp.dnkt = 62.0E-3;
        p.PInD("pi", fp.pi);
        p.PInD("tz1", fp.tz1);
        p.PInD("tz2", fp.tz2);
        p.PInD("Qskv", fp.Qskv);
        p.PInD("dnkt", fp.dnkt);
        fp.p12.htr(fp.tz1, fp.tz2);
        p.POutD("htr", fp.htr);
        
        // 2.14, 2.28.
        p.PfNameD("2.14, 2.28.");
        fp.Qgpt = 0.00162;
        fp.Rogst = 15.0;
        p.PInD("Qgpt", fp.Qgpt);
        p.PInD("Rogst", fp.Rogst);
        fp.p12.G();
        p.POutD("G", fp.G);
        
        // 2.15
        p.PfNameD("2.15.");
        fp.Tpl = 273.0;
        fp.reservoirPressurePa = 16.0E6;
        p.PInD("Tpl", fp.Tpl);
        p.PInD("reservoirPressurePa", fp.reservoirPressurePa);
        fp.p12.Dpl();
        p.POutD("Dpl", fp.Dpl);
        
        // 2.16
        p.PfNameD("2.16.");
        fp.reservoirPressurePa = 16.0E6;
        fp.Pz = 15123575.42;
        fp.Cg = 2300.0;
        fp.tn = 1.0;
        fp.h = 12.0;
        fp.Cgp = 2000.0;
        fp.rc = 0.073;
        fp.Rk = 250.0;
        p.PInD("Dpl", fp.Dpl);
        p.PInD("G", fp.G);
        p.PInD("pi", fp.pi);
        p.PInD("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInD("Pz", fp.Pz);
        p.PInD("Cg", fp.Cg);
        p.PInD("tn", fp.tn);
        p.PInD("h", fp.h);
        p.PInD("Cgp", fp.Cgp);
        p.PInD("rc", fp.rc);
        p.PInD("Rk", fp.Rk);
        fp.p12.DTplz();
        p.POutD("DTplz", fp.DTplz);
        
        // 2.17, 2.31
        p.PfNameD("2.17, 2.31.");
        fp.Lgp = 5000.0;
        fp.tn = 1.0;
        fp.rc = 0.073;
        p.PInD("pi", fp.pi);
        p.PInD("Lgp", fp.Lgp);
        p.PInD("tn", fp.tn);
        p.PInD("rc", fp.rc);
        fp.p12.fes();
        p.POutD("fes", fp.fes);
        
        // 2.18, 2.32
        p.PfNameD("2.18, 2.32.");
        fp.Lgp = 5000.0;
        fp.Cg = 2300.0;
        p.PInD("pi", fp.pi);
        p.PInD("Lgp", fp.Lgp);
        p.PInD("G", fp.G);
        p.PInD("Cg", fp.Cg);
        p.PInD("fes", fp.fes);
        fp.p12.alfa();
        p.POutD("alfa", fp.alfa);
        
        // 2.19
        p.PfNameD("2.19.");
        fp.Tpl = 273.0;
        p.PInD("Tpl", fp.Tpl);
        p.PInD("DTplz", fp.DTplz);
        fp.p12.Tz();
        p.POutD("Tz", fp.Tz);
        
        // 2.20
        p.PfNameD("2.20.");
        fp.Pz = 15123575.42;
        p.PInD("Pz", fp.Pz);
        p.PInD("Tz", fp.Tz);
        fp.p12.Dz();
        p.POutD("Dz", fp.Dz);
        
        // 2.21
        p.PfNameD("2.21.");
        fp.Tpl = 273.0;
        fp.Tgr = 0.017;
        fp.Lk = 2000.0;
        fp.Lcp = 1990.0;
        fp.Pz = 15123575.42;
        fp.Ppr = 15.0E6;
        p.PInD("Tpl", fp.Tpl);
        p.PInD("Tgr", fp.Tgr);
        p.PInD("Lk", fp.Lk);
        p.PInD("Lcp", fp.Lcp);
        p.PInD("DTplz", fp.DTplz);
        p.PInD("alfa", fp.alfa);
        p.PInD("Dz", fp.Dz);
        p.PInD("Pz", fp.Pz);
        p.PInD("Ppr", fp.Ppr);
        fp.p12.Tpr();
        p.POutD("Tpr", fp.Tpr);
        
        // 2.23, 2.41, 2.79, 2.93
        p.PfNameD("2.23, 2.41, 2.79, 2.93.");
        p.PInD("Tz", fp.Tz);
        p.PInD("Tpr", fp.Tpr);
        fp.p12.Tcp1();
        p.POutD("Tcp", fp.Tcp);
        
        // 2.24, 2.42, 2.80, 2.94
        p.PfNameD("2.24, 2.42, 2.80, 2.94.");
        fp.Pz = 15123575.42;
        fp.Ppr = 15.0E6;
        p.PInD("Pz", fp.Pz);
        p.PInD("Ppr", fp.Ppr);
        fp.p12.Pcp1();
        p.POutD("Pcp", fp.Pcp);
        
        // 2.25, 2.36, 2.43, 2.51, 2.75, 2.81, 2.88, 2.95
        p.PfNameD("2.25, 2.36, 2.43, 2.51, 2.75, 2.81, 2.88, 2.95.");
        fp.Qg0 = 1.14;
        fp.Pst = 101300.0;
        fp.Z = 1.2;
        fp.Tst = 293.0;
        p.PInD("Qg0", fp.Qg0);
        p.PInD("Pst", fp.Pst);
        p.PInD("Tcp", fp.Tcp);
        p.PInD("Z", fp.Z);
        p.PInD("Pcp", fp.Pcp);
        p.PInD("Tst", fp.Tst);
        fp.p12.Qgpt1();
        p.POutD("Qgpt", fp.Qgpt);
        
        // 2.26, 2.44, 2.82, 2.96
        p.PfNameD("2.26, 2.44, 2.82, 2.96.");
        fp.Rogotn = 15.0;
        fp.Lk = 2000.0;
        fp.Lcp = 1990.0;
        fp.Z = 1.2;
        p.PInD("Rogotn", fp.Rogotn);
        p.PInD("Lk", fp.Lk);
        p.PInD("Lcp", fp.Lcp);
        p.PInD("Hzhek", fp.Hzhek);
        p.PInD("Tcp", fp.Tcp);
        p.PInD("Z", fp.Z);
        fp.p12.KS1();
        p.POutD("KS", fp.KS);
        
        // 2.27, 2.45, 2.83, 2.97
        p.PfNameD("2.27, 2.45, 2.83, 2.97.");
        fp.Lambda = 0.014;
        fp.Z = 1.2;
        fp.ECInnerDiameter = 153.4E-3;
        p.PInD("Lambda", fp.Lambda);
        p.PInD("Tcp", fp.Tcp);
        p.PInD("Z", fp.Z);
        p.PInD("KS", fp.KS);
        p.PInD("ECInnerDiameter", fp.ECInnerDiameter);
        fp.p12.Teta1();
        p.POutD("Teta", fp.Teta);
        
        // 2.22
        p.PfNameD("2.22.");
        fp.Pz = 15123575.42;
        p.PInD("Pz", fp.Pz);
        p.PInD("Teta", fp.Teta);
        p.PInD("Qgpt", fp.Qgpt);
        p.PInD("KS", fp.KS);
        fp.p12.Ppr1();
        p.POutD("Ppr", fp.Ppr);
        
        // 2.29
        p.PfNameD("2.29.");
        fp.Tsp = 340.286373;
        fp.Psp = 15091077.07;
        p.PInD("Tsp", fp.Tsp);
        p.PInD("Psp", fp.Psp);
        fp.p12.Dsp();
        p.POutD("Dsp", fp.Dsp);
        
        // 2.30
        p.PfNameD("2.30.");
        fp.Tpl = 273.0;
        fp.Tgr = 0.017;
        fp.Lk = 2000.0;
        fp.Pz = 15123575.42;
        fp.Pu = 15.0E6;
        p.PInD("Tpl", fp.Tpl);
        p.PInD("Tgr", fp.Tgr);
        p.PInD("Lk", fp.Lk);
        p.PInD("DTplz", fp.DTplz);
        p.PInD("alfa", fp.alfa);
        p.PInD("Dsp", fp.Dsp);
        p.PInD("Pz", fp.Pz);
        p.PInD("Pu", fp.Pu);
        fp.p12.Tbuf1();
        p.POutD("Tbuf", fp.Tbuf);
        
        // 2.33
        p.PfNameD("2.33.");
        p.PInD("Ppr", fp.Ppr);
        p.PInD("Qgpt", fp.Qgpt);
        p.PInD("Teta", fp.Teta);
        p.PInD("KS", fp.KS);
        fp.p12.Pbuf1();
        p.POutD("Pbuf1", fp.Pbuf1);

        // 2.34, 2.49, 2.73, 2.86
        p.PfNameD("2.34, 2.49, 2.73, 2.86");
        p.PInD("Tbuf", fp.Tbuf);
        p.PInD("Tpr", fp.Tpr);
        fp.p12.Tcp2();
        p.POutD("Tcp", fp.Tcp);

        // 2.35, 2.50, 2.74, 2.87
        p.PfNameD("2.35, 2.50, 2.74, 2.87");
        p.PInD("Ppr", fp.Ppr);
        p.PInD("Pbuf1", fp.Pbuf1);
        fp.p12.Pcp2();
        p.POutD("Pcp", fp.Pcp);

        // 2.37, 2.52, 2.76, 2.89
        p.PfNameD("2.37, 2.52, 2.76, 2.89");
        fp.Rogotn = 15.0;
        fp.Lcp = 1990.0;
        fp.Z = 1.2;
        p.PInD("Rogotn", fp.Rogotn);
        p.PInD("Lcp", fp.Lcp);
        p.PInD("htr", fp.htr);
        p.PInD("Tcp", fp.Tcp);
        p.PInD("Z", fp.Z);
        fp.p12.KS2();
        p.POutD("KS", fp.KS);

        // 2.38, 2.53, 2.77, 2.90
        p.PfNameD("2.38, 2.53, 2.77, 2.90");
        fp.Lambda = 0.014;
        fp.Z = 1.2;
        fp.dnkt = 62.0E-3;
        p.PInD("Lambda", fp.Lambda);
        p.PInD("Tcp", fp.Tcp);
        p.PInD("Z", fp.Z);
        p.PInD("KS", fp.KS);
        p.PInD("dnkt", fp.dnkt);
        fp.p12.Teta2();
        p.POutD("Teta", fp.Teta);

        // 2.39, 2.92
        p.PfNameD("2.39, 2.92-1");
        fp.Pz = 15123575.42;
        fp.densityOfWater = 1100.0;
        p.PInD("Pz", fp.Pz);
        p.PInD("densityOfWater", fp.densityOfWater);
        p.PInD("g", fp.g);
        p.PInD("Hzhek", fp.Hzhek);
        fp.p12.Pur1();
        p.POutD("Pur1", fp.Pur1);

        // 2.40, 2.92-2
        p.PfNameD("2.40, 2.92-2");
        p.PInD("Pur1", fp.Pur1);
        p.PInD("Teta", fp.Teta);
        p.PInD("Qgpt", fp.Qgpt);
        p.PInD("KS", fp.KS);
        fp.p12.Ppr2();
        p.POutD("Ppr", fp.Ppr);

        // 2.46, 2.91
        p.PfNameD("2.46, 2.91");
        fp.Pz = 15123575.42;
        fp.densityOfWater = 1100.0;
        fp.Lk = 2000.0;
        fp.Lcp = 1990.0;
        p.PInD("Pz", fp.Pz);
        p.PInD("densityOfWater", fp.densityOfWater);
        p.PInD("g", fp.g);
        p.PInD("Lk", fp.Lk);
        p.PInD("Lcp", fp.Lcp);
        fp.p12.Ppr3();
        p.POutD("Ppr", fp.Ppr);

        // 2.47, 2.84
        p.PfNameD("2.47, 2.84");
        fp.densityOfWater = 1100.0;
        p.PInD("Ppr", fp.Ppr);
        p.PInD("densityOfWater", fp.densityOfWater);
        p.PInD("g", fp.g);
        p.PInD("htr", fp.htr);
        fp.p12.Pur2();
        p.POutD("Pur2", fp.Pur2);

        // 2.48, 2.85
        p.PfNameD("2.48, 2.85");
        p.PInD("Pur2", fp.Pur2);
        p.PInD("Teta", fp.Teta);
        p.PInD("Qgpt", fp.Qgpt);
        p.PInD("KS", fp.KS);
        fp.p12.Pbuf2();
        p.POutD("Pbuf2", fp.Pbuf2);

        // 2.54
        p.PfNameD("2.54");
        fp.dsht = 0.018;
        fp.Qg0 = 1.14;
        p.PInD("dsht", fp.dsht);
        p.PInD("Pbuf1", fp.Pbuf1);
        p.PInD("Qg0", fp.Qg0);
        p.PInD("densityOfGas", fp.densityOfGas);
        fp.p12.Plin1();
        p.POutD("Plin", fp.Plin);

        // 2.55
        p.PfNameD("2.55");
        fp.Qg0 = 1.14;
        fp.Pst = 101300.0;
        fp.Z = 1.2;
        fp.Tst = 293.0;
        p.PInD("Qg0", fp.Qg0);
        p.PInD("Pst", fp.Pst);
        p.PInD("Tbuf", fp.Tbuf);
        p.PInD("Z", fp.Z);
        p.PInD("Pbuf1", fp.Pbuf1);
        p.PInD("Tst", fp.Tst);
        fp.p12.Qgpt2();
        p.POutD("Qgpt", fp.Qgpt);

        // 2.56, 2.70
        p.PfNameD("2.56, 2.70");
        p.PInD("Tbuf", fp.Tbuf);
        p.PInD("Pbuf1", fp.Pbuf1);
        fp.p12.Dbuf1();
        p.POutD("Dbuf", fp.Dbuf);

        // 2.57
        p.PfNameD("2.57");
        p.PInD("Tbuf", fp.Tbuf);
        p.PInD("Pbuf1", fp.Pbuf1);
        p.PInD("Plin", fp.Plin);
        p.PInD("Dbuf", fp.Dbuf);
        fp.p12.Tsep1();
        p.POutD("Tsep", fp.Tsep);

        // 2.58
        p.PfNameD("2.58");
        fp.DPsep = 3.0E4;
        p.PInD("Plin", fp.Plin);
        p.PInD("DPsep", fp.DPsep);
        fp.p12.Pdikt1();
        p.POutD("Pdikt1", fp.Pdikt1);

        // 2.59
        p.PfNameD("2.59");
        p.PInD("Tsep", fp.Tsep);
        p.PInD("Pdikt1", fp.Pdikt1);
        fp.p12.Ddikt();
        p.POutD("Ddikt", fp.Ddikt);

        // 2.60
        p.PfNameD("2.60");
        fp.DPsep = 3.0E4;
        p.PInD("Tsep", fp.Tsep);
        p.PInD("DPsep", fp.DPsep);
        p.PInD("Ddikt", fp.Ddikt);
        fp.p12.Tdikt();
        p.POutD("Tdikt", fp.Tdikt);

        // 2.63, 2.64
        p.PfNameD("2.63, 2.64");
        fp.PrDikt = 0;
        fp.dsht = 0.018;
        p.PInD("PrDikt", fp.PrDikt);
        p.PInD("dsht", fp.dsht);
        fp.p12.Cdikt();
        p.POutD("Cdikt", fp.Cdikt);
        
        // 2.61
        p.PfNameD("2.61");
        fp.Z = 1.2;
        fp.Rootn = 15.0;
        p.PInD("Qgpt", fp.Qgpt);
        p.PInD("Tdikt", fp.Tdikt);
        p.PInD("Z", fp.Z);
        p.PInD("Rootn", fp.Rootn);
        p.PInD("Cdikt", fp.Cdikt);
        fp.p12.Pdikt2();
        p.POutD("Pdikt2", fp.Pdikt2);
        
        // 2.62
        p.PfNameD("2.62");
        fp.Qg0 = 1.14;
        fp.Pst = 101300.0;
        fp.Z = 1.2;
        fp.Tst = 293.0;
        p.PInD("Qg0", fp.Qg0);
        p.PInD("Pst", fp.Pst);
        p.PInD("Tdikt", fp.Tdikt);
        p.PInD("Z", fp.Z);
        p.PInD("Pdikt1", fp.Pdikt1);
        p.PInD("Tst", fp.Tst);
        fp.p12.Qgpt3();
        p.POutD("Qgpt", fp.Qgpt);
        
        // 2.65
        p.PfNameD("2.65");
        fp.DPsep = 3.0E4;
        p.PInD("Pdikt1", fp.Pdikt1);
        p.PInD("DPsep", fp.DPsep);
        fp.p12.Plin2();
        p.POutD("Plin", fp.Plin);
        
        // 2.66
        p.PfNameD("2.66");
        p.PInD("Tsep", fp.Tsep);
        p.PInD("Pdikt1", fp.Pdikt1);
        fp.p12.Dbuf2();
        p.POutD("Dbuf", fp.Dbuf);
        
        // 2.67
        p.PfNameD("2.67");
        fp.DPsep = 3.0E4;
        p.PInD("Tdikt", fp.Tdikt);
        p.PInD("DPsep", fp.DPsep);
        p.PInD("Dbuf", fp.Dbuf);
        fp.p12.Tsep2();
        p.POutD("Tsep", fp.Tsep);
        
        // 2.68
        p.PfNameD("2.68");
        fp.Fi = 100.0;
        fp.dsht = 0.018;
        p.PInD("Plin", fp.Plin);
        p.PInD("Fi", fp.Fi);
        p.PInD("Qgpt", fp.Qgpt);
        p.PInD("densityOfGas", fp.densityOfGas);
        p.PInD("dsht", fp.dsht);
        fp.p12.Pbuf4();
        p.POutD("Pbuf1", fp.Pbuf1);
        
        // 2.69
        p.PfNameD("2.69");
        fp.Qg0 = 1.14;
        fp.Pst = 101300.0;
        fp.Z = 1.2;
        fp.Tst = 293.0;
        p.PInD("Qg0", fp.Qg0);
        p.PInD("Pst", fp.Pst);
        p.PInD("Tsep", fp.Tsep);
        p.PInD("Z", fp.Z);
        p.PInD("Plin", fp.Plin);
        p.PInD("Tst", fp.Tst);
        fp.p12.Qgpt4();
        p.POutD("Qgpt", fp.Qgpt);
        
        // 2.71
        p.PfNameD("2.71");
        p.PInD("Tsep", fp.Tsep);
        p.PInD("Pbuf1", fp.Pbuf1);
        p.PInD("Plin", fp.Plin);
        p.PInD("Dbuf", fp.Dbuf);
        fp.p12.Tbuf2();
        p.POutD("Tbuf", fp.Tbuf);
        
        // 2.72
        p.PfNameD("2.72");
        p.PInD("Pbuf1", fp.Pbuf1);
        p.PInD("KS", fp.KS);
        p.PInD("Teta", fp.Teta);
        p.PInD("Qgpt", fp.Qgpt);
        fp.p12.Ppr4();
        p.POutD("Ppr", fp.Ppr);
        
        // 2.78
        p.PfNameD("2.78");
        p.PInD("Ppr", fp.Ppr);
        p.PInD("KS", fp.KS);
        p.PInD("Teta", fp.Teta);
        p.PInD("Qgpt", fp.Qgpt);
        fp.p12.Pz3();
        p.POutD("Pz", fp.Pz);
        
        // 2.98
        p.PfNameD("2.98");
        fp.Pzost = 14580387.75;
        p.PInD("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInD("Pzost", fp.Pzost);
        fp.p12.DPpzo1();
        p.POutD("DPpzo1", fp.DPpzo1);
        
        // 2.99
        p.PfNameD("2.99");
        fp.k = 1.0E-12;
        fp.h = 12.0;
        fp.Qrezh = 67.36854134;
        fp.oilViscosity = 5.0E-3;
        fp.rc = 0.073;
        fp.piezoconductivityOfTheFormation = 6.0;
        p.PInD("pi", fp.pi);
        p.PInD("k", fp.k);
        p.PInD("h", fp.h);
        p.PInD("DPpzo1", fp.DPpzo1);
        p.PInD("Qrezh", fp.Qrezh);
        p.PInD("oilViscosity", fp.oilViscosity);
        p.PInD("rc", fp.rc);
        p.PInD("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        fp.p12.tzab1();
        p.POutD("tzab1", fp.tzab1);
        
        // 2.100
        p.PfNameD("2.100");
        fp.Pzost = 14580387.75;
        p.PInD("Pzost", fp.Pzost);
        p.PInD("DPpzo1", fp.DPpzo1);
        p.PInD("tzab1", fp.tzab1);
        fp.p12.Pz4(time);
        p.POutD("Pz", fp.Pz);

        // 2.103
        p.PfNameD("2.103");
        fp.Qrezh = 67.36854134;
        fp.oilViscosity = 5.0E-3;
        fp.Z = 1.2;
        fp.Pst = 101300.0;
        fp.k = 1.0E-12;
        fp.h = 12.0;
        fp.Tst = 293.0;
        fp.piezoconductivityOfTheFormation = 6.0;
        fp.rc = 0.073;
        fp.skinFactor = 2.0;
        p.PInD("Qrezh", fp.Qrezh);
        p.PInD("oilViscosity", fp.oilViscosity);
        p.PInD("Z", fp.Z);
        p.PInD("Pst", fp.Pst);
        p.PInD("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInD("pi", fp.pi);
        p.PInD("k", fp.k);
        p.PInD("h", fp.h);
        p.PInD("Tst", fp.Tst);
        p.PInD("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInD("rc", fp.rc);
        p.PInD("skinFactor", fp.skinFactor);
        fp.p12.KB(time);
        p.POutD("KB", fp.KB);

        // 2.102
        p.PfNameD("2.102");
        fp.Pzost = 14580387.75;
        fp.piezoconductivityOfTheFormation = 6.0;
        fp.rc = 0.073;
        fp.skinFactor = 2.0;
        fp.Kb = 884427965754.0;
        fp.Qrezh = 67.36854134;
        p.PInD("Pzost", fp.Pzost);
        p.PInD("KB", fp.KB);
        p.PInD("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInD("rc", fp.rc);
        p.PInD("skinFactor", fp.skinFactor);
        p.PInD("Kb", fp.Kb);
        p.PInD("Qrezh", fp.Qrezh);
        fp.p12.KA();
        p.POutD("KA", fp.KA);
 
        // 2.101
        p.PfNameD("2.101");
        p.PInD("KA", fp.KA);
        p.PInD("KB", fp.KB);
        fp.p12.Pz5(time);
        p.POutD("Pz", fp.Pz);
 
        // 2.104
        p.PfNameD("2.104");
        fp.Pz = 15123575.42;
        // fp.Rozh = ;
        fp.Lk = 2000.0;
        // fp.htr = ;
        // fp.Tu = ;
        fp.Z = 1.2;
        p.PInD("Pz", fp.Pz);
        // p.PInD("Rozh", fp.Rozh);
        p.PInD("g", fp.g);
        p.PInD("Lk", fp.Lk);
        // p.PInD("htr", fp.htr);
        p.PInD("alfa", fp.alfa);
        p.PInD("densityOfGas", fp.densityOfGas);
        // p.PInD("Tu", fp.Tu);
        p.PInD("Z", fp.Z);
        // fp.p12.Pbuf3();
        // p.POutD("Pbuf3", fp.Pbuf3);
    }
}
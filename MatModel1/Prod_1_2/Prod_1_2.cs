using System;
using System.Collections.Generic;

public class Prod_1_2
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Prod_1_2(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (2.1.1)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Patm", fp.Patm);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.1.1", parameters);
        fp.Pz = Math.Sqrt(Math.Pow(fp.Pzrezh, 2) + fp.Qrezh * fp.Mjug * fp.Patm * 
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                     Math.Pow((fp.rc * Math.Exp(-fp.skinFactor)), 2)) / 
            (2.0 * fp.pi * fp.k * fp.h));
        p.POutC("2.1.1", "Pz", fp.Pz);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (2.1.2)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz2(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzrezh", fp.Pzrezh);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Patm", fp.Patm);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.1.2", parameters);
        fp.Pz = Math.Sqrt(Math.Pow(fp.Pzrezh, 2) - fp.Qrezh * fp.Mjug * fp.Patm * 
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                     Math.Pow((fp.rc * Math.Exp(-fp.skinFactor)), 2)) / 
            (2.0 * fp.pi * fp.k * fp.h));
        p.POutC("2.1.2", "Pz", fp.Pz);
    }
    
    /// <summary>
    /// Пластовое давление (Па) (2.2)
    /// </summary>
    public void reservoirPressurePa()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Afs", fp.Afs);
        parameters.Add("Bfs", fp.Bfs);
        parameters.Add("Qg0", fp.Qg0);
        p.PInC("2.2", parameters);
        fp.reservoirPressurePa = Math.Sqrt(Math.Pow(fp.Pz, 2) + fp.Afs * fp.Qg0 + fp.Bfs * Math.Pow(fp.Qg0, 2));
        p.POutC("2.2", "reservoirPressurePa", fp.reservoirPressurePa);
    }
        
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (2.3)
    /// </summary>
    public void Afs()
    {
        parameters.Clear();
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        p.PInC("2.3", parameters);
        fp.Afs = fp.Mjug * fp.Z * fp.Pst * fp.Tpl * Math.Log(fp.Rk / (fp.rc * Math.Exp(-fp.skinFactor))) / 
                 (fp.pi * fp.k * fp.h * fp.Tst);
        p.POutC("2.3", "Afs", fp.Afs);
    }
    
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (2.4)
    /// </summary>
    public void Bfs()
    {
        parameters.Clear();
        parameters.Add("Rogst", fp.Rogst);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("rc", fp.rc);
        parameters.Add("Rk", fp.Rk);
        parameters.Add("Kmshpl", fp.Kmshpl);
        parameters.Add("h", fp.h);
        p.PInC("2.4", parameters);
        fp.Bfs = fp.Rogst * fp.Z * fp.Pst * fp.Tpl * (1.0 / fp.rc - 1.0 / fp.Rk) /
                 (fp.Kmshpl * Math.Pow(fp.h, 2) * fp.Tst);
        p.POutC("2.4", "Bfs", fp.Bfs);
    }
    
    /// <summary>
    /// Коэффициент макрошероховатости пласта (м) (2.5)
    /// </summary>
    public void Kmshpl()
    {
        parameters.Clear();
        parameters.Add("m", fp.m);
        parameters.Add("k", fp.k);
        p.PInC("2.5", parameters);
        fp.Kmshpl = fp.m * Math.Pow(fp.k / fp.m, 1.0 / 6.0) / 0.05;
        p.POutC("2.5", "Kmshpl", fp.Kmshpl);
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (2.6, 2.7)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        parameters.Clear();
        parameters.Add("Pz", Pz);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("deltaPpred", fp.deltaPpred);
        parameters.Add("A1", fp.A1);
        parameters.Add("A2", fp.A2);
        p.PInC("2.6, 2.7", parameters);
        fp.MP = 0.0;
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
        p.POutC("2.6, 2.7", "MP", fp.MP);
    }
    
    /// <summary>
    /// Дебит скважины по воде (м3/с) (2.8)
    /// </summary>
    public void Qzh()
    {
        parameters.Clear();
        parameters.Add("Bzh", fp.Bzh);
        parameters.Add("DP", fp.DP);
        parameters.Add("DPkr", fp.DPkr);
        p.PInC("2.8", parameters);
        fp.Qzh = fp.Bzh * fp.DP / fp.DPkr - fp.Bzh;
        p.POutC("2.8", "Qzh", fp.Qzh);
    }
    
    /// <summary>
    /// Фактическая скорость газа (м/с) (2.9)
    /// </summary>
    /// <param name="Tz">Температура на забое скважины, Па</param>
    public void Vgf(double Tz)
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Tz", Tz);
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Z", fp.Z);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.9", parameters);
        fp.Vgf = 4.0 * fp.Qg0 * fp.Pst * Tz * fp.Z / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2) * fp.Pz * fp.Tst);
        p.POutC("2.9", "Vgf", fp.Vgf);
    }
    
    /// <summary>
    /// Плотность газа (кг/м3) (2.10
    /// </summary>
    /// <param name="Tz">Температура на забое скважины, Па</param>
    public void densityOfGas(double Tz)
    {
        parameters.Clear();
        parameters.Add("Tz", Tz);
        parameters.Add("Rogst", fp.Rogst);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Z", fp.Z);
        p.PInC("2.10", parameters);
        fp.densityOfGas = fp.Rogst * fp.Pz * fp.Tst / (fp.Pst * Tz * fp.Z);
        p.POutC("2.10", "densityOfGas", fp.densityOfGas);
    }
    
    /// <summary>
    /// Критическая скорость газа (м/с) (2.11)
    /// </summary>
    public void Vgkr()
    {
        parameters.Clear();
        parameters.Add("g", fp.g);
        parameters.Add("dv", fp.dv);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.11", parameters);
        fp.Vgkr = 2.5 * Math.Sqrt(fp.g * fp.dv * (fp.densityOfWater - fp.densityOfGas) / fp.densityOfGas);
        p.POutC("2.11", "Vgkr", fp.Vgkr);
    }
    
    /// <summary>
    /// Уровень жидкости в ЭК (м) (2.12)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Hzhek(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.12", parameters);
        fp.Hzhek = 4.0 * fp.Qskv * (tz2 - tz1) / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("2.12", "Hzhek", fp.Hzhek);
    }
    
    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.13)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void htr(double tz1, double tz2)
    {
        parameters.Clear();
        parameters.Add("tz1", tz1);
        parameters.Add("tz2", tz2);
        parameters.Add("Qskv", fp.Qskv);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.13", parameters);
        fp.htr = 4.0 * fp.Qskv * (tz2 - tz1) / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("2.13", "htr", fp.htr);
    }
    
    /// <summary>
    /// Массовый расход газа (кг/с) (2.14, 2.28)
    /// </summary>
    public void G()
    {
        parameters.Clear();
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("Rogst", fp.Rogst);
        p.PInC("2.14, 2.28", parameters);
        fp.G = fp.Qgpt * fp.Rogst;
        p.POutC("2.14, 2.28", "G", fp.G);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на уровне пласта (K/Па) (2.15)
    /// </summary>
    public void Dpl()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        p.PInC("2.15", parameters);
        fp.Dpl = (-0.02034 * (fp.Tpl - 273.0) - 0.124 * fp.reservoirPressurePa * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.15", "Dpl", fp.Dpl);
    }
    
    /// <summary>
    /// Разность температур на уровне пласта и на забое (K) (2.16)
    /// </summary>
    public void DTplz()
    {
        parameters.Clear();
        parameters.Add("Dpl", fp.Dpl);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("G", fp.G);
        parameters.Add("Cg", fp.Cg);
        parameters.Add("tn", fp.tn);
        parameters.Add("pi", fp.pi);
        parameters.Add("h", fp.h);
        parameters.Add("Cgp", fp.Cgp);
        parameters.Add("rc", fp.rc);
        parameters.Add("Rk", fp.Rk);
        p.PInC("2.16", parameters);
        fp.DTplz = fp.Dpl * (fp.reservoirPressurePa - fp.Pz) * 
                   Math.Log10(1.0 + fp.G * fp.Cg * fp.tn / (fp.pi * fp.h * fp.Cgp * Math.Pow(fp.rc, 2))) / 
                   Math.Log10(fp.Rk / fp.rc);
        p.POutC("2.16", "DTplz", fp.DTplz);
    }
    
    /// <summary>
    /// ??? (???) (2.17, 2.31)
    /// </summary>
    public void fes()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Lgp", fp.Lgp);
        parameters.Add("tn", fp.tn);
        parameters.Add("rc", fp.rc);
        p.PInC("2.17, 2.31", parameters);
        fp.fes = Math.Log(1.0 + Math.Sqrt(fp.pi * fp.Lgp * fp.tn / Math.Pow(Math.Log(fp.rc), 2)));
        p.POutC("2.17, 2.31", "fes", fp.fes);
    }
    
    /// <summary>
    /// Средний зенитный угол ствола скважины (град.) (2.18, 2.32)
    /// </summary>
    public void alfa()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("Lgp", fp.Lgp);
        parameters.Add("G", fp.G);
        parameters.Add("Cg", fp.Cg);
        parameters.Add("fes", fp.fes);
        p.PInC("2.18, 2.32", parameters);
        fp.alfa = 2.0 * fp.pi * fp.Lgp / (fp.G * fp.Cg * fp.fes);
        p.POutC("2.18, 2.32", "alfa", fp.alfa);
    }
    
    /// <summary>
    /// Температура на забое скважины (К) (2.19)
    /// </summary>
    public void Tz()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("DTplz", fp.DTplz);
        p.PInC("2.19", parameters);
        fp.Tz = fp.Tpl - fp.DTplz;
        p.POutC("2.19", "Tz", fp.Tz);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на забое (K/Па) (2.20)
    /// </summary>
    public void Dz()
    {
        parameters.Clear();
        parameters.Add("Tz", fp.Tz);
        parameters.Add("Pz", fp.Pz);
        p.PInC("2.20", parameters);
        fp.Dz = (-0.02034 * (fp.Tz - 273.0) - 0.124 * fp.Pz * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.20", "Dz", fp.Dz);
    }
    
    /// <summary>
    /// Температура на приеме НКТ (K) (2.21)
    /// </summary>
    public void Tpr()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgr", fp.Tgr);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("DTplz", fp.DTplz);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Dz", fp.Dz);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Ppr", fp.Ppr);
        p.PInC("2.21", parameters);
        fp.Tpr = fp.Tpl - fp.Tgr * (fp.Lk - fp.Lcp) - fp.DTplz * Math.Exp(-fp.alfa * (fp.Lk - fp.Lcp)) +
                 (fp.Tgr - fp.Dz * (fp.Pz - fp.Ppr) / fp.Lk) * (1.0 - Math.Exp(-fp.alfa * (fp.Lk - fp.Lcp))) / fp.alfa;
        p.POutC("2.21", "Tpr", fp.Tpr);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (2.22)
    /// </summary>
    public void Ppr1()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("KS", fp.KS);
        p.PInC("2.22", parameters);
        fp.Ppr = Math.Sqrt((Math.Pow(fp.Pz, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
        p.POutC("2.22", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    ///  Средняя температура (K) (2.23, 2.41, 2.79, 2.93)
    /// </summary>
    public void Tcp1()
    {
        parameters.Clear();
        parameters.Add("Tz", fp.Tz);
        parameters.Add("Tpr", fp.Tpr);
        p.PInC("2.23, 2.41, 2.79, 2.93", parameters);
        fp.Tcp = (fp.Tz + fp.Tpr) / 2.0;
        p.POutC("2.23, 2.41, 2.79, 2.93", "Tcp", fp.Tcp);
    }
    
    /// <summary>
    ///  Среднее давление (Па) (2.24, 2.42, 2.80, 2.94)
    /// </summary>
    public void Pcp1()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Ppr", fp.Ppr);
        p.PInC("2.24, 2.42, 2.80, 2.94", parameters);
        fp.Pcp = (fp.Pz + fp.Ppr) / 2.0;
        p.POutC("2.24, 2.42, 2.80, 2.94", "Pcp", fp.Pcp);
    }
    
    /// <summary>
    ///  Объемный расход газа при Р и Т (м3/с) (2.25, 2.36, 2.43, 2.51, 2.75, 2.81, 2.88, 2.95)
    /// </summary>
    public void Qgpt1()
    {
        parameters.Clear();
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Pcp", fp.Pcp);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        p.PInC("2.25, 2.36, 2.43, 2.51, 2.75, 2.81, 2.88, 2.95", parameters);
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tcp * fp.Z / (fp.Pcp * fp.Tst);
        p.POutC("2.25, 2.36, 2.43, 2.51, 2.75, 2.81, 2.88, 2.95", "Qgpt", fp.Qgpt);
    }
    
    /// <summary>
    ///  Коэффициент (2.26, 2.44, 2.82, 2.96)
    /// </summary>
    public void KS1()
    {
        parameters.Clear();
        parameters.Add("Rogotn", fp.Rogotn);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Hzhek", fp.Hzhek);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        p.PInC("2.26, 2.44, 2.82, 2.96", parameters);
        fp.KS = 0.03415 * fp.Rogotn * (fp.Lk - fp.Lcp - fp.Hzhek) / (fp.Tcp * fp.Z);
        p.POutC("2.26, 2.44, 2.82, 2.96", "KS", fp.KS);
    }
    
    /// <summary>
    ///  Коэффициент (2.27, 2.45, 2.83, 2.97)
    /// </summary>
    public void Teta1()
    {
        parameters.Clear();
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        parameters.Add("KS", fp.KS);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        p.PInC("2.27, 2.45, 2.83, 2.97", parameters);
        fp.Teta = 0.01377 * fp.Lambda * Math.Pow(fp.Tcp * fp.Z, 2) *
            (Math.Exp(2.0 * fp.KS) - 1.0) / Math.Pow(fp.ECInnerDiameter, 5); 
        p.POutC("2.27, 2.45, 2.83, 2.97", "Teta", fp.Teta);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на спуске НКТ (K/Па) (2.29)
    /// </summary>
    public void Dsp()
    {
        parameters.Clear();
        parameters.Add("Tsp", fp.Tsp);
        parameters.Add("Psp", fp.Psp);
        p.PInC("2.29", parameters);
        fp.Dsp = (-0.02034 * (fp.Tsp - 273.0) - 0.124 * fp.Psp * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.29", "Dsp", fp.Dsp);
    }
    
    /// <summary>
    /// Температура на буфере (К) (2.30)
    /// </summary>
    public void Tbuf1()
    {
        parameters.Clear();
        parameters.Add("Tpl", fp.Tpl);
        parameters.Add("Tgr", fp.Tgr);
        parameters.Add("DTplz", fp.DTplz);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("Dsp", fp.Dsp);
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Pu", fp.Pu);
        p.PInC("2.30", parameters);
        fp.Tbuf = fp.Tpl - fp.Tgr * fp.Lk - fp.DTplz * Math.Exp(-fp.alfa * fp.Lk) +
                  (fp.Tgr - fp.Dsp * (fp.Pz - fp.Pu) / fp.Lk) * (1.0 - Math.Exp(-fp.alfa * fp.Lk)) / fp.alfa;
        p.POutC("2.30", "Tbuf", fp.Tbuf);
    }
    
    /// <summary>
    /// Буферное давление (Па) (2.33)
    /// </summary>
    public void Pbuf1()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("KS", fp.KS);
        p.PInC("2.33", parameters);
        fp.Pbuf1 = Math.Sqrt((Math.Pow(fp.Ppr, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
        p.POutC("2.33", "Pbuf1", fp.Pbuf1);
    }
    
    /// <summary>
    ///  Средняя температура (K) (2.34, 2.49, 2.73, 2.86)
    /// </summary>
    public void Tcp2()
    {
        parameters.Clear();
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Tpr", fp.Tpr);
        p.PInC("2.34, 2.49, 2.73, 2.86", parameters);
        fp.Tcp = (fp.Tbuf + fp.Tpr) / 2.0;
        p.POutC("2.34, 2.49, 2.73, 2.86", "Tcp", fp.Tcp);
    }
    
    /// <summary>
    ///  Среднее давление (Па) (2.35, 2.50, 2.74, 2.87)
    /// </summary>
    public void Pcp2()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("Pbuf1", fp.Pbuf1);
        p.PInC("2.35, 2.50, 2.74, 2.87", parameters);
        fp.Pcp = (fp.Ppr + fp.Pbuf1) / 2.0;
        p.POutC("2.35, 2.50, 2.74, 2.87", "Pcp", fp.Pcp);
    }
    
    /// <summary>
    ///  Коэффициент (2.37, 2.52, 2.76, 2.89)
    /// </summary>
    public void KS2()
    {
        parameters.Clear();
        parameters.Add("Rogotn", fp.Rogotn);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("htr", fp.htr);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        p.PInC("2.37, 2.52, 2.76, 2.89", parameters);
        fp.KS = 0.03415 * fp.Rogotn * (fp.Lcp - fp.htr) / (fp.Tcp * fp.Z);
        p.POutC("2.37, 2.52, 2.76, 2.89", "KS", fp.KS);
    }

    /// <summary>
    /// Давление на уровне жидкости в ЭК (Па) (2.39, 2.92-1)
    /// </summary>
    public void Pur1()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("g", fp.g);
        parameters.Add("Hzhek", fp.Hzhek);
        p.PInC("2.39, 2.92-1", parameters);
        fp.Pur1 = fp.Pz - fp.densityOfWater * fp.g * fp.Hzhek;
        p.POutC("2.39, 2.92-1", "Pur1", fp.Pur1);
    }

    /// <summary>
    ///  Коэффициент (2.38, 2.53, 2.77, 2.90)
    /// </summary>
    public void Teta2()
    {
        parameters.Clear();
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Tcp", fp.Tcp);
        parameters.Add("Z", fp.Z);
        parameters.Add("KS", fp.KS);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("2.38, 2.53, 2.77, 2.90", parameters);
        fp.Teta = 0.01377 * fp.Lambda * Math.Pow(fp.Tcp * fp.Z, 2) *
            (Math.Exp(2.0 * fp.KS) - 1.0) / Math.Pow(fp.dnkt, 5); 
        p.POutC("2.38, 2.53, 2.77, 2.90", "Teta", fp.Teta);
    }

    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.40, 2.92-2)
    /// </summary>
    public void Ppr2()
    {
        parameters.Clear();
        parameters.Add("Pur1", fp.Pur1);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("KS", fp.KS);
        p.PInC("2.40, 2.92-2", parameters);
        fp.Ppr = Math.Sqrt((Math.Pow(fp.Pur1, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
        p.POutC("2.40, 2.92-2", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.46, 2.91)
    /// </summary>
    public void Ppr3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("2.46, 2.91", parameters);
        fp.Ppr = fp.Pz - fp.densityOfWater * fp.g * (fp.Lk - fp.Lcp);
        p.POutC("2.46, 2.91", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Давление на уровне жидкости в НКТ (Па) (2.47, 2.84)
    /// </summary>
    public void Pur2()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("densityOfWater", fp.densityOfWater);
        parameters.Add("g", fp.g);
        parameters.Add("htr", fp.htr);
        p.PInC("2.47, 2.84", parameters);
        fp.Pur2 = fp.Ppr - fp.densityOfWater * fp.g * fp.htr;
        p.POutC("2.47, 2.84", "Pur2", fp.Pur2);
    }
    
    /// <summary>
    /// Буферное давление (Па) (2.48, 2.85)
    /// </summary>
    public void Pbuf2()
    {
        parameters.Clear();
        parameters.Add("Pur2", fp.Pur2);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("KS", fp.KS);
        p.PInC("2.48, 2.85", parameters);
        fp.Pbuf2 = Math.Sqrt((Math.Pow(fp.Pur2, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
        p.POutC("2.48, 2.85", "Pbuf2", fp.Pbuf2);
    }
        
    /// <summary>
    /// Линейное давление (Па)  (2.54)
    /// </summary>
    public void Plin1()
    {
        parameters.Clear();
        parameters.Add("dsht", fp.dsht);
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("densityOfGas", fp.densityOfGas);
        p.PInC("2.54", parameters);
        fp.Plin = Math.Pow(1000.0 * fp.dsht, 2) * fp.Pbuf1 / 
                  (0.0729 * Math.Pow(1.0, 2) * fp.Qg0 * 86400.0 * fp.densityOfGas);
        p.POutC("2.54", "Plin", fp.Plin);
    }
    
    /// <summary>
    ///  Объемный расход газа при Р и Т (м3/с) (2.55)
    /// </summary>
    public void Qgpt2()
    {
        parameters.Clear();
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pbuf1", fp.Pbuf1);
        p.PInC("2.55", parameters);
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tbuf * fp.Z / (fp.Pbuf1 * fp.Tst);
        p.POutC("2.55", "Qgpt", fp.Qgpt);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на буфере (K/Па) (2.56, 2.70)
    /// </summary>
    public void Dbuf1()
    {
        parameters.Clear();
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Pbuf1", fp.Pbuf1);
        p.PInC("2.56, 2.70", parameters);
        fp.Dbuf = (-0.02034 * (fp.Tbuf - 273.0) - 0.124 * fp.Pbuf1 * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.56, 2.70", "Dbuf", fp.Dbuf);
    }
    
    /// <summary>
    /// Температура газа после штуцера (К)  (2.57)
    /// </summary>
    public void Tsep1()
    {
        parameters.Clear();
        parameters.Add("Tbuf", fp.Tbuf);
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("Dbuf", fp.Dbuf);
        p.PInC("2.57", parameters);
        fp.Tsep = fp.Tbuf - (fp.Pbuf1 - fp.Plin) * fp.Dbuf;
        p.POutC("2.57", "Tsep", fp.Tsep);
    }
    
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по первой формуле для ДИКТ (Па) (2.58)
    /// </summary>
    public void Pdikt1()
    {
        parameters.Clear();
        parameters.Add("Plin", fp.Plin);
        parameters.Add("DPsep", fp.DPsep);
        p.PInC("2.58", parameters);
        fp.Pdikt1 = Math.Abs(fp.Plin - fp.DPsep); // Д. Гуменюк, отладка (Abs)
        p.POutC("2.58", "Pdikt1", fp.Pdikt1);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на ДИКТе (K/Па) (2.59)
    /// </summary>
    public void Ddikt()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Pdikt1", fp.Pdikt1);
        p.PInC("2.59", parameters);
        fp.Ddikt = (-0.02034 * (fp.Tsep - 273.0) - 0.124 * fp.Pdikt1 * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.59", "Ddikt", fp.Ddikt);
    }
    
    /// <summary>
    /// Температура на ДИКТе (K) (2.60)
    /// </summary>
    public void Tdikt()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("DPsep", fp.DPsep);
        parameters.Add("Ddikt", fp.Ddikt);
        p.PInC("2.60", parameters);
        fp.Tdikt = fp.Tsep - fp.DPsep * fp.Ddikt;
        p.POutC("2.60", "Tdikt", fp.Tdikt);
    }
    
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по второй формуле для ДИКТ (Па) (2.61)
    /// </summary>
    public void Pdikt2()
    {
        parameters.Clear();
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("Tdikt", fp.Tdikt);
        parameters.Add("Z", fp.Z);
        parameters.Add("Rootn", fp.Rootn);
        parameters.Add("Cdikt", fp.Cdikt);
        p.PInC("2.61", parameters);
        fp.Pdikt2 = fp.Qgpt * Math.Sqrt(fp.Tdikt * fp.Z * fp.Rootn) / (0.864 * fp.Cdikt);
        p.POutC("2.61", "Pdikt2", fp.Pdikt2);
    }
    
    /// <summary>
    /// Объемный расход газа при Р и Т (м3/с) (2.62)
    /// </summary>
    public void Qgpt3()
    {
        parameters.Clear();
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tdikt", fp.Tdikt);
        parameters.Add("Pdikt1", fp.Pdikt1);
        parameters.Add("Z", fp.Z);
        p.PInC("2.62", parameters);
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tdikt * fp.Z / (fp.Pdikt1 * fp.Tst);
        p.POutC("2.62", "Qgpt", fp.Qgpt);
    }
    
    /// <summary>
    /// Коэффициент (2.63, 2.64)
    /// </summary>
    public void Cdikt()
    {
        parameters.Clear();
        parameters.Add("PrDikt", fp.PrDikt);
        parameters.Add("dsht", fp.dsht);
        p.PInC("2.63, 2.64", parameters);
        if (fp.PrDikt == 0) fp.Cdikt = 238966.0 * Math.Pow(fp.dsht, 2.0581);
        else fp.Cdikt = 197817.0 * Math.Pow(fp.dsht, 2.0172);
        p.POutC("2.63, 2.64", "Cdikt", fp.Cdikt);
    }
    
    /// <summary>
    /// Линейное давление (Па)  (2.65)
    /// </summary>
    public void Plin2()
    {
        parameters.Clear();
        parameters.Add("Pdikt1", fp.Pdikt1);
        parameters.Add("DPsep", fp.DPsep);
        p.PInC("2.65", parameters);
        fp.Plin = fp.Pdikt1 + fp.DPsep;
        p.POutC("2.65", "Plin", fp.Plin);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на буфере (K/Па) (2.66)
    /// </summary>
    public void Dbuf2()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Pdikt1", fp.Pdikt1);
        p.PInC("2.66", parameters);
        fp.Dbuf = (-0.02034 * (fp.Tsep - 273.0) - 0.124 * fp.Pdikt1 * 1.0E-6 + 4.6437) * 1.0E-6;
        p.POutC("2.66", "Dbuf", fp.Dbuf);
    }
    
    /// <summary>
    /// Температура газа после штуцера (К)  (2.67)
    /// </summary>
    public void Tsep2()
    {
        parameters.Clear();
        parameters.Add("Tdikt", fp.Tdikt);
        parameters.Add("DPsep", fp.DPsep);
        parameters.Add("Dbuf", fp.Dbuf);
        p.PInC("2.67", parameters);
        fp.Tsep = fp.Tdikt + fp.DPsep * fp.Dbuf;
        p.POutC("2.67", "Tsep", fp.Tsep);
    }
    
    /// <summary>
    /// Буферное давление (Па) (2.68)
    /// </summary>
    public void Pbuf4()
    {
        parameters.Clear();
        parameters.Add("Fi", fp.Fi);
        parameters.Add("Qgpt", fp.Qgpt);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("dsht", fp.dsht);
        p.PInC("2.68", parameters);
        fp.Pbuf1 = fp.Plin * 0.00729 * Math.Pow(fp.Fi, 2) * fp.Qgpt * 86400.0 * fp.densityOfGas / 
                   Math.Pow(1000.0 * fp.dsht, 2);
        p.POutC("2.68", "Pbuf1", fp.Pbuf1);
    }
    
    /// <summary>
    ///  Объемный расход газа при Р и Т (м3/с) (2.69)
    /// </summary>
    public void Qgpt4()
    {
        parameters.Clear();
        parameters.Add("Qg0", fp.Qg0);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Z", fp.Z);
        parameters.Add("Plin", fp.Plin);
        p.PInC("2.69", parameters);
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tsep * fp.Z / (fp.Plin * fp.Tst);
        p.POutC("2.69", "Qgpt", fp.Qgpt);
    }
    
    /// <summary>
    /// Температура на буфере (К) (2.71)
    /// </summary>
    public void Tbuf2()
    {
        parameters.Clear();
        parameters.Add("Tsep", fp.Tsep);
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("Plin", fp.Plin);
        parameters.Add("Dbuf", fp.Dbuf);
        p.PInC("2.71", parameters);
        fp.Tbuf = fp.Tsep + (fp.Pbuf1 - fp.Plin) * fp.Dbuf;
        p.POutC("2.71", "Tbuf", fp.Tbuf);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.72)
    /// </summary>
    public void Ppr4()
    {
        parameters.Clear();
        parameters.Add("Pbuf1", fp.Pbuf1);
        parameters.Add("KS", fp.KS);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        p.PInC("2.72", parameters);
        fp.Ppr = Math.Sqrt(Math.Pow(fp.Pbuf1, 2) * Math.Exp(2.0 * fp.KS) + fp.Teta * Math.Pow(fp.Qgpt, 2));
        p.POutC("2.72", "Ppr", fp.Ppr);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (2.78)
    /// </summary>
    public void Pz3()
    {
        parameters.Clear();
        parameters.Add("Ppr", fp.Ppr);
        parameters.Add("KS", fp.KS);
        parameters.Add("Teta", fp.Teta);
        parameters.Add("Qgpt", fp.Qgpt);
        p.PInC("2.78", parameters);
        fp.Pz = Math.Sqrt(Math.Pow(fp.Ppr, 2) * Math.Exp(2.0 * fp.KS) + fp.Teta * Math.Pow(fp.Qgpt, 2));
        p.POutC("2.78", "Pz", fp.Pz);
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (2.98)
    /// </summary>
    public void DPpzo1()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Pzost", fp.Pzost);
        p.PInC("2.98", parameters);
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
        p.POutC("2.98", "DPpzo1", fp.DPpzo1);
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (2.99)
    /// </summary>
    public void tzab1()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("Mjug", fp.Mjug);
        parameters.Add("rc", fp.rc);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        p.PInC("2.99", parameters);
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.Mjug)) * fp.rc /  // Д. Гуменюк, отладка (вязкость)
                   (2.25 * fp.piezoconductivityOfTheFormation); 
        p.POutC("2.99", "tzab1", fp.tzab1);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (2.100)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz4(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("DPpzo1", fp.DPpzo1);
        parameters.Add("tzab1", fp.tzab1);
        p.PInC("2.100", parameters);
        fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        p.POutC("2.100", "Pz", fp.Pz);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (2.101)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz5(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("KA", fp.KA);
        parameters.Add("KB", fp.KB);
        p.PInC("2.101", parameters);
        fp.Pz = Math.Sqrt(fp.KA + fp.KB * Math.Log10(time));
        p.POutC("2.101", "Pz", fp.Pz);
    }

    /// <summary>
    /// Коэффициент (2.102)
    /// </summary>
    public void KA()
    {
        parameters.Clear();
        parameters.Add("Pzost", fp.Pzost);
        parameters.Add("KB", fp.KB);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        parameters.Add("Kb", fp.Kb);
        parameters.Add("Qrezh", fp.Qrezh);
        p.PInC("2.102", parameters);
        fp.KA = Math.Pow(fp.Pzost, 2) + fp.KB * Math.Log10(2.25 * fp.piezoconductivityOfTheFormation / 
                                                           Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) + fp.Kb * Math.Pow(fp.Qrezh, 2); 
        p.POutC("2.102", "KA", fp.KA);
    }

    /// <summary>
    /// Коэффициент (2.103)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void KB(double time)
    {
        parameters.Clear();
        parameters.Add("time", time);
        parameters.Add("Qrezh", fp.Qrezh);
        parameters.Add("oilViscosity", fp.oilViscosity);
        parameters.Add("Z", fp.Z);
        parameters.Add("Pst", fp.Pst);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Tst", fp.Tst);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("skinFactor", fp.skinFactor);
        p.PInC("2.103", parameters);
        fp.KB = 2.3 * fp.Qrezh * fp.oilViscosity * fp.Z * fp.Pst * fp.reservoirPressurePa * 1.0E-2 / 
            (2.0 * fp.pi * fp.k * fp.h * fp.Tst) * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                                                            Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
        p.POutC("2.103", "KB", fp.KB);
    }

    /// <summary>
    /// Буферное давление (Па) (2.104)
    /// </summary>
    public void Pbuf3()
    {
        parameters.Clear();
        parameters.Add("Pz", fp.Pz);
        parameters.Add("Rozh", fp.Rozh);
        parameters.Add("g", fp.g);
        parameters.Add("Lk", fp.Lk);
        parameters.Add("htr", fp.htr);
        parameters.Add("alfa", fp.alfa);
        parameters.Add("densityOfGas", fp.densityOfGas);
        parameters.Add("Tu", fp.Tu);
        parameters.Add("Z", fp.Z);
        p.PInC("2.104", parameters);
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
        p.POutC("2.104", "Pbuf3", fp.Pbuf3);
    }
    
}
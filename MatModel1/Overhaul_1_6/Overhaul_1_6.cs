using System;
using System.Collections.Generic;

public class Overhaul_1_6
{
    FormulaParameters fp;
    private Print p;
    private Dictionary<string, double> parameters = new Dictionary<string, double>();

    public Overhaul_1_6(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }
    
    /// <summary>
    /// Плотность жидкости глушения скважины (кг/м3) (13.1) 
    /// </summary>
    public void densityOfKillingFluid()
    {
        parameters.Clear();
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Kbr", fp.Kbr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc); 
        p.PInC("13.1", parameters);
        fp.densityOfKillingFluid = fp.reservoirPressurePa * (1.0 + fp.Kbr) / (fp.g * fp.Lc);
        p.POutC("13.1", "densityOfKillingFluid", fp.densityOfKillingFluid);
    }

    /// <summary>
    /// Глубина скважины по вертикали (м) (13.2) 
    /// </summary>
    public void Lc()
    {
        parameters.Clear();
        parameters.Add("lc", fp.lc);
        parameters.Add("alfa", fp.alfa);
        p.PInC("13.2", parameters);
        fp.Lc = fp.lc * Math.Cos(fp.alfa);
        p.POutC("13.2", "Lc", fp.Lc);
    }

    /// <summary>
    /// Давление на устье (Па) (13.3, 13.52)
    /// </summary>
    /// <param name="Ropzhzhgs">Плотность ПЖ или ЖГС, кг/м3</param>
    public void Pu(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("13.3, 13.52", parameters);
        fp.Pu = fp.reservoirPressurePa - Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("13.3, 13.52", "Pu", fp.Pu);
    }

    /// <summary>
    /// Давление на манифольде (Па) (13.3, 13.52)
    /// </summary>
    /// <param name="Ropzhzhgs">Плотность ПЖ или ЖГС, кг/м3</param>
    public void Pm(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("13.3, 13.52", parameters);
        fp.Pm = fp.reservoirPressurePa - Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("13.3, 13.52", "Pm", fp.Pm);
    }

    /// <summary>
    /// Давление на затрубе (Па) (13.3, 13.52)
    /// </summary>
    /// <param name="Ropzhzhgs">Плотность ПЖ или ЖГС, кг/м3</param>
    public void Pzatr(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("13.3, 13.52", parameters);
        fp.Pzatr = fp.reservoirPressurePa - Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("13.3, 13.52", "Pzatr", fp.Pzatr);
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (13.4, 13.53)
    /// </summary>
    /// <param name="Ropzhszhgs">Плотность ПЖС или ЖГС, кг/м3</param>
    public void Pz(double Ropzhzhgs)
    {
        parameters.Clear();
        parameters.Add("Ropzhzhgs", Ropzhzhgs);
        parameters.Add("Pu", fp.Pu);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        p.PInC("13.4, 13.53", parameters);
        fp.Pz = fp.Pu + Ropzhzhgs * fp.g * fp.Lc;
        p.POutC("13.4, 13.53", "Pz", fp.Pz);
    }
    
    /// <summary>
    /// Давление разрыва пласта (Па) (13.5)
    /// </summary>
    public void Prazr()
    {
        parameters.Clear();
        parameters.Add("Pvg", fp.Pvg);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Prp", fp.Prp);
        p.PInC("13.5", parameters);
        fp.Prazr = fp.Pvg - fp.reservoirPressurePa + fp.Prp;
        p.POutC("13.5", "Prazr", fp.Prazr);
    }
    
    /// <summary>
    /// Вертикальная составляющая горного давления (Па) (13.6)
    /// </summary>
    public void Pvg()
    {
        parameters.Clear();
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Rovp", fp.Rovp);
        parameters.Add("g", fp.g);
        p.PInC("13.6", parameters);
        fp.Pvg = fp.Lc * fp.Rovp * fp.g;
        p.POutC("13.6", "Pvg", fp.Pvg);
    }
    
    /// <summary>
    /// Горизонтальная составляющая горного давления (Па) (13.7)
    /// </summary>
    public void Pgg()
    {
        parameters.Clear();
        parameters.Add("Pvg", fp.Pvg);
        parameters.Add("KPgp", fp.KPgp);
        p.PInC("13.7", parameters);
        fp.Pgg = fp.Pvg * fp.KPgp / (1.0 - fp.KPgp);
        p.POutC("13.7", "Pgg", fp.Pgg);
    }
    
    /// <summary>
    /// Расчетное давление гидроразрыва на забое скважины (Па) (13.8)
    /// </summary>
    public void Pzabr()
    {
        parameters.Clear();
        parameters.Add("Azr", fp.Azr);
        parameters.Add("Prazr", fp.Prazr);
        p.PInC("13.8", parameters);
        fp.Pzabr = fp.Azr * fp.Prazr;
        p.POutC("13.8", "Pzabr", fp.Pzabr);
    }
    
    /// <summary>
    /// Требуемое давление на устье скважины при гидроразрыве (Па) (13.9)
    /// </summary>
    public void Pu()
    {
        parameters.Clear();
        parameters.Add("Pzabr", fp.Pzabr);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("g", fp.g);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        p.PInC("13.9", parameters);
        fp.Pu = fp.Pzabr - fp.Rozhr * fp.Lc * fp.g + fp.DPzhrnkt;
        p.POutC("13.9", "Pu", fp.Pu);
    }
    
    /// <summary>
    /// Критическая скорость ЖР/ЖП в НКТ (м/с) (13.10)
    /// </summary>
    /// <param name="Rozhrzhp">Плотность ЖР или ЖП, кг/м3</param>
    public void Wkr(double Rozhrzhp)
    {
        parameters.Clear();
        parameters.Add("Rozhrzhp", Rozhrzhp);
        p.PInC("13.10", parameters);
        fp.Wkr = 25.0 * Math.Sqrt((0.0085 * Rozhrzhp - 7.0) / Rozhrzhp);
        p.POutC("13.10", "Wkr", fp.Wkr);
    }
    
    /// <summary>
    /// Фактическая средняя скорость ЖР/ЖП в НКТ (м/с) (13.11)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    public void Wf(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        p.PInC("13.11", parameters);
        fp.Wf = 4.0 * q / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("13.11", "Wf", fp.Wf);
    }
    
    /// <summary>
    /// Потери давления на трение жидкости разрыва в НКТ (Па) (13.12.1)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    public void DPzhrnkt1(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("13.12.1", parameters);
        fp.DPzhrnkt = 4.0 * (0.0085 * fp.Rozhr - 7.0) * Xnkt / 
                      ((0.1369 * Math.Log((0.0085 * fp.Rozhr - 7.0) * fp.dnkt / 
                                          ((0.000033 * fp.Rozhr - 0.022) * 4.0 * q / (fp.pi * Math.Pow(fp.dnkt, 2)))) + 0.1356) * fp.dnkt);
        p.POutC("13.12.1", "DPzhrnkt", fp.DPzhrnkt);
    }
    
    /// <summary>
    /// Потери давления на трение жидкости-пропантоносителя в НКТ (Па) (13.12.2)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    public void DPzhpnkt1(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("13.12.2", parameters);
        fp.DPzhpnkt = 4.0 * (0.0085 * fp.Rozhp - 7.0) * Xnkt / 
                      ((0.1369 * Math.Log((0.0085 * fp.Rozhp - 7.0) * fp.dnkt / 
                                          ((0.000033 * fp.Rozhp - 0.022) * 4.0 * q / (fp.pi * Math.Pow(fp.dnkt, 2)))) + 0.1356) * fp.dnkt);
        p.POutC("13.12.2", "DPzhpnkt", fp.DPzhpnkt);
    }
    
    /// <summary>
    /// Потери давления на трение жидкости разрыва в НКТ (Па) (13.13.1)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    public void DPzhrnkt2(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("13.13.1", parameters);
        fp.DPzhrnkt = 0.012 * fp.Rozhr * Xnkt * 4.0 * q / (fp.pi * Math.Pow(fp.dnkt, 3));
        p.POutC("13.13.1", "DPzhrnkt", fp.DPzhrnkt);
    }
    
    /// <summary>
    /// Потери давления на трение жидкости-пропантоносителя в НКТ (Па) (13.13.2)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с</param>
    public void DPzhpnkt2(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("13.13.2", parameters);
        fp.DPzhpnkt = 0.012 * fp.Rozhp * Xnkt * 4.0 * q / (fp.pi * Math.Pow(fp.dnkt, 3));
        p.POutC("13.13.2", "DPzhpnkt", fp.DPzhpnkt);
    }

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ
    /// от подачи насоса (q) во времени (м) (13.14)
    /// </summary>
    /// <param name="q">объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Xnkt(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("13.14", parameters);
        fp.Xnkt = 4.0 * q * time / (fp.pi * Math.Pow(fp.dnkt, 2));
        p.POutC("13.14", "Xnkt", fp.Xnkt);
    }

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ
    /// до зумпфа (дна) скважины) в ЭК  от подачи насоса (q) во времени (м) (13.15)
    /// </summary>
    /// <param name="q">объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Xek(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("pi", fp.pi);
        p.PInC("13.15", parameters);
        fp.Xek = 4.0 * q * time / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
        p.POutC("13.15", "Xek", fp.Xek);
    }

    /// <summary>
    /// Потери давления на трение в НКТ ΔРНКТ для ньютоновской жидкости (жидкости глушения/продавочной жидкости)
    /// при течении по трубам лифтовой колонны (НКТ) (Па) (13.16)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void DPnkt(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Lambda", fp.Lambda);
        parameters.Add("Roprodzh", fp.Roprodzh);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("pi", fp.pi);
        p.PInC("13.16", parameters);
        fp.DPnkt = fp.Lambda * 8.0 * Xnkt * Math.Pow(q, 2) * fp.Roprodzh /
                    (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5));
        p.POutC("13.16", "DPnkt", fp.DPnkt);
    }

    /// <summary>
    /// Коэффициент потерь на трение по длине (13.17 - 13.19)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void Lambda(double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("dnktek", dnktek);
        parameters.Add("Retr", fp.Retr);
        p.PInC("13.17 - 13.19", parameters);
        var re = fp.Retr;
        if (re < 2320.0) fp.Lambda = 64.0 / re;
        else if (1.0e+5 < re && re <= 1.0e+6) fp.Lambda = 0.0032 + 0.221 / Math.Pow(re, 0.237);
        else fp.Lambda = 0.3164 / Math.Pow(re, 0.25);
        p.POutC("13.17 - 13.19", "Lambda", fp.Lambda);
    }

    /// <summary>
    /// Число Рейнольдса для труб (НКТ или ЭК) (13.20)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="dnktek">Внутренний диаметр НКТ ил ЭК, м</param>
    public void Retr(double q, double dnktek)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("dnktek", dnktek);
        parameters.Add("pi", fp.pi);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        p.PInC("13.20", parameters);
        fp.Retr = 4.0 * q * fp.densityOfKillingFluid / (fp.pi * dnktek * fp.viscosityOfKillingFluid);
        p.POutC("13.20", "Retr", fp.Retr);
    }

    /// <summary>
    /// Число Рейнольдса для кольцевого пространства (между внешним диаметром НКТ и внутренним диаметром ЭК) (13.21)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Rekp(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("Dnkt", fp.Dnkt);
        p.PInC("13.21", parameters);
        fp.Rekp = 4.0 * q * fp.densityOfKillingFluid / 
                  (fp.pi * (fp.ECInnerDiameter + fp.Dnkt) * fp.viscosityOfKillingFluid);
        p.POutC("13.21", "Rekp", fp.Rekp);
    }
    
    /// <summary>
    /// Потери давления в перфорационных отверстиях при продавливании цементного раствора в призабойную зону пласта (Па) (13.22)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="Rozhrzhppzh">Плотность ЖР, ЖП или ПЖ, кг/м3</param>
    public void DPpo(double q, double Rozhrzhppzh)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Rozhrzhppzh", Rozhrzhppzh);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("npo", fp.npo);
        parameters.Add("dpo", fp.dpo);
        parameters.Add("Kdelta", fp.Kdelta);
        p.PInC("13.22", parameters);
        fp.DPpo = 8.0 * Math.Pow(q, 2) * Rozhrzhppzh / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.22", "DPpo", fp.DPpo);
    }
    
    /// <summary>
    /// Плотность жидкости-пропантоносителя (кг/м3) (13.23)
    /// </summary>
    public void Rozhp()
    {
        parameters.Clear();
        parameters.Add("Cprop", fp.Cprop);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("Ropr", fp.Ropr);
        p.PInC("13.23", parameters);
        fp.Rozhp = fp.Rozhr * (1.0 - fp.Cprop) + fp.Ropr * fp.Cprop;
        p.POutC("13.23", "Rozhp", fp.Rozhp);
    }
    
    /// <summary>
    /// Объемная доля пропанта (13.24)
    /// </summary>
    public void Cprop()
    {
        parameters.Clear();
        parameters.Add("C0pr", fp.C0pr);
        parameters.Add("Ropr", fp.Ropr);
        p.PInC("13.24", parameters);
        fp.Cprop = fp.C0pr / (fp.C0pr + fp.Ropr); 
        p.POutC("13.24", "Cprop", fp.Cprop);
    }
    
    /// <summary>
    /// Вязкость жидкости-пропантоносителя (Па•с) (13.25)
    /// </summary>
    public void Mjuzhp()
    {
        parameters.Clear();
        parameters.Add("Mjuzhr", fp.Mjuzhr);
        parameters.Add("Cprop", fp.Cprop);
        p.PInC("13.25", parameters);
        fp.Mjuzhp = fp.Mjuzhr * Math.Exp(3.18 * fp.Cprop); 
        p.POutC("13.25", "Mjuzhp", fp.Mjuzhp);
    }
    
    /// <summary>
    /// Требуемый объем жидкости разрыва (м3) (13.26)
    /// </summary>
    public void Vzhr()
    {
        parameters.Clear();
        parameters.Add("Vnkt", fp.Vnkt);
        parameters.Add("Vbek", fp.Vbek);
        p.PInC("13.26", parameters);
        fp.Vzhr = 1.8 * (fp.Vnkt + fp.Vbek); 
        p.POutC("13.26", "Vzhr", fp.Vzhr);
    }
    
    /// <summary>
    /// Внутренний объем НКТ (м3) (13.27) 
    /// </summary>
    public void Vnkt()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("13.27", parameters);
        fp.Vnkt = fp.pi * Math.Pow(fp.dnkt, 2) * fp.Lcp / 4.0;
        p.POutC("13.27", "Vnkt", fp.Vnkt);
    }

    /// <summary>
    /// Объем эксплуатационной колонны под башмаком НКТ (м3) (13.28) 
    /// </summary>
    public void Vbek()
    {
        parameters.Clear();
        parameters.Add("pi", fp.pi);
        parameters.Add("ECInnerDiameter", fp.ECInnerDiameter);
        parameters.Add("lc", fp.lc);
        parameters.Add("Lcp", fp.Lcp);
        p.PInC("13.28", parameters);
        fp.Vbek = fp.pi * Math.Pow(fp.ECInnerDiameter, 2) * (fp.lc - fp.Lcp) / 4.0;
        p.POutC("13.28", "Vbek", fp.Vbek);
    }

    /// <summary>
    /// Требуемый объем жидкости разрыва с пропантом (ЖП) (м3) (13.29) 
    /// </summary>
    public void Vzhp()
    {
        parameters.Clear();
        parameters.Add("Qpr", fp.Qpr);
        parameters.Add("C0pr", fp.C0pr);
        p.PInC("13.29", parameters);
        fp.Vzhp = fp.Qpr / fp.C0pr;
        p.POutC("13.29", "Vzhp", fp.Vzhp);
    }

    /// <summary>
    /// Объем продавочной жидкости (м3) (13.30)
    /// </summary>
    public void Vprodzh()
    {
        parameters.Clear();
        parameters.Add("Vbek", fp.Vbek);
        parameters.Add("Vnkt", fp.Vnkt);
        p.PInC("13.30", parameters);
        fp.Vprodzh = 1.3 * (fp.Vbek + fp.Vnkt);
        p.POutC("13.30", "Vprodzh", fp.Vprodzh);
    }

    /// <summary>
    /// Общий объем закачиваемой жидкости при ГРП (м3) (13.31)
    /// </summary>
    public void Vzhgrp()
    {
        parameters.Clear();
        parameters.Add("Vzhr", fp.Vzhr);
        parameters.Add("Vzhp", fp.Vzhp);
        parameters.Add("Vprodzh", fp.Vprodzh);
        p.PInC("13.31", parameters);
        fp.Vzhgrp = fp.Vzhr + fp.Vzhp + fp.Vprodzh;
        p.POutC("13.31", "Vzhgrp", fp.Vzhgrp);
    }

    /// <summary>
    /// Минимальный темп закачки при создании вертикальной трещины (м3/c) (13.32)
    /// </summary>
    public void Qminv()
    {
        parameters.Clear();
        parameters.Add("Ksi0", fp.Ksi0);
        parameters.Add("h", fp.h);
        parameters.Add("Mjuzhr", fp.Mjuzhr);
        p.PInC("13.32", parameters);
        fp.Qminv = fp.Ksi0 * fp.h / fp.Mjuzhr;
        p.POutC("13.32", "Qminv", fp.Qminv);
    }

    /// <summary>
    /// Требуемое количество насосных агрегатов (шт) (13.33)
    /// </summary>
    public void Nagr()
    {
        parameters.Clear();
        parameters.Add("Pu", fp.Pu);
        parameters.Add("Pp", fp.Pp);
        parameters.Add("Qminv", fp.Qminv);
        parameters.Add("qp", fp.qp);
        parameters.Add("Kts", fp.Kts);
        p.PInC("13.33", parameters);
        fp.Nagr = (int)(fp.Pu * fp.Qminv / (fp.Pp * fp.qp * fp.Kts)) + 1;
        p.POutC("13.33", "Nagr", fp.Nagr);
    }

    /// <summary>
    /// Общая продолжительность процесса гидроразрыва (c) (13.34)
    /// </summary>
    public void tgr()
    {
        parameters.Clear();
        parameters.Add("Vzhgrp", fp.Vzhgrp);
        parameters.Add("Qminv", fp.Qminv);
        p.PInC("13.34", parameters);
        fp.tgr = fp.Vzhgrp / fp.Qminv;
        p.POutC("13.34", "tgr", fp.tgr);
    }

    /// <summary>
    /// Давление на насосном агрегате на первом временном этапе
    /// (заполнение жидкостью разрыва НКТ от устья до башмака НКТ) (Па) (13.35)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap1(int N, double[] tsw, double[] speedNumber, double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.35", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.viscosityOfKillingFluid * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                                               Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.k * fp.h);
        }
        fp.Pap1 = fp.reservoirPressurePa + sum - fp.Rozhr * Xnkt * fp.g - 
                  fp.densityOfKillingFluid * (fp.Lcp - Xnkt) * fp.g - fp.densityOfKillingFluid * (fp.Lc - fp.Lcp) * fp.g + 
                  fp.DPzhrnkt + fp.LambdaNkt * 8.0 * (fp.Lcp - Xnkt) * Math.Pow(q, 2) * fp.densityOfKillingFluid / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 8.0 * Math.Pow(q, 2) * fp.densityOfKillingFluid / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.35", "Pap1", fp.Pap1);
    }

    /// <summary>
    /// Давление на насосном агрегате на втором временном этапе
    /// (продавливание ЖР в ЭК жидкостью разрыва от башмака НКТ до зумпфа (дна) скважины) (Па) (13.36)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap2(int N, double[] tsw, double[] speedNumber, double Xek, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("Xek", Xek);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("viscosityOfKillingFluid", fp.viscosityOfKillingFluid);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("g", fp.g);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.36", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.viscosityOfKillingFluid * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                                               Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.k * fp.h);
        }
        fp.Pap2 = fp.reservoirPressurePa + sum - fp.Rozhr * fp.Lcp * fp.g - 
                  fp.Rozhr * Xek * fp.g - fp.densityOfKillingFluid * (fp.Lc - fp.Lcp - Xek) * fp.g + 
                  fp.DPzhrnkt + 8.0 * Math.Pow(q, 2) * fp.densityOfKillingFluid / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.36", "Pap2", fp.Pap2);
    }

    /// <summary>
    /// Давление на насосном агрегате на третьем временном этапе
    /// (продавливание ЖР через отверстия перфорации в пласт жидкостью разрыва) (Па) (13.37)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap31(int N, double[] tsw, double[] speedNumber, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("Mjuzhr", fp.Mjuzhr);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.37", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.Mjuzhr * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                              Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.k * fp.h);
        }
        fp.Pap3 = fp.reservoirPressurePa + sum - fp.Rozhr * fp.Lc * fp.g + fp.DPzhrnkt + 
                  8.0 * Math.Pow(q, 2) * fp.Rozhr / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.37", "Pap3", fp.Pap3);
    }

    /// <summary>
    /// Давление на насосном агрегате на третьем временном этапе
    /// (продавливание ЖР через отверстия перфорации в пласт жидкостью разрыва)
    /// при достижении давления на устье скважины расчетного давления на устье Pу = Pap3 (Па) (13.38)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap32(int N, double[] tsw, double[] speedNumber, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("Mjuzhr", fp.Mjuzhr);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Psigrp", fp.Psigrp);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.38", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.Mjuzhr * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                              Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.Psigrp * fp.k * fp.h);
        }
        fp.Pap3 = fp.reservoirPressurePa + sum - fp.Rozhr * fp.Lc * fp.g + fp.DPzhrnkt + 
                  8.0 * Math.Pow(q, 2) * fp.Rozhr / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.38", "Pap3", fp.Pap3);
    }

    /// <summary>
    /// Давление на насосном агрегате на четвертом временном этапе
    /// (продавливание ЖР через отверстия перфорации жидкостью пропантоносителем) (Па) (13.39)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap4(int N, double[] tsw, double[] speedNumber, double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("Mjuzhr", fp.Mjuzhr);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("Psigrp", fp.Psigrp);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.39", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.Mjuzhr * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                              Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.Psigrp * fp.k * fp.h);
        }
        fp.Pap4 = fp.reservoirPressurePa + sum - fp.Rozhp * Xnkt * fp.g - 
                  fp.Rozhr * (fp.Lcp - Xnkt) * fp.g - fp.Rozhr * (fp.Lc - fp.Lcp) * fp.g
                  + fp.DPzhrnkt + fp.DPzhpnkt + 8.0 * Math.Pow(q, 2) * fp.Rozhr / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.39", "Pap4", fp.Pap4);
    }

    /// <summary>
    /// Давление на насосном агрегате на пятом временном этапе
    /// (заполнение ЖП ЭК от башмака НКТ до зумпфа (дна) скважины,
    /// полное продавливание ЖР через отверстия перфорации) (Па) (13.40)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap5(int N, double[] tsw, double[] speedNumber, double Xek, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("Xek", Xek);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("Mjuzhr", fp.Mjuzhr);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Psigrp", fp.Psigrp);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.40", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.Mjuzhr * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                              Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.Psigrp * fp.k * fp.h);
        }
        fp.Pap5 = fp.reservoirPressurePa + sum - fp.Rozhp * fp.Lcp * fp.g - 
                  fp.Rozhp * Xek * fp.g - fp.Rozhr * (fp.Lc - fp.Lcp - Xek) * fp.g +
                  fp.DPzhpnkt + 8.0 * Math.Pow(q, 2) * fp.Rozhr / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.40", "Pap5", fp.Pap5);
    }

    /// <summary>
    /// Давление на насосном агрегате на шестом временном этапе
    /// (продавливание ЖП через отверстия перфорации в пласт жидкостью пропантоносителем,
    /// требуемый объем ЖП должен быть больше VНКТ+VЭК (под башмаком НКТ)) (Па) (13.41)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap6(int N, double[] tsw, double[] speedNumber, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("Mjuzhp", fp.Mjuzhp);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Psigrp", fp.Psigrp);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.41", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.Mjuzhp * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                              Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.Psigrp * fp.k * fp.h);
        }
        fp.Pap6 = fp.reservoirPressurePa + sum - fp.Rozhp * fp.Lc * fp.g +
                  fp.DPzhpnkt + 8.0 * Math.Pow(q, 2) * fp.Rozhp / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.41", "Pap6", fp.Pap6);
    }

    /// <summary>
    /// Давление на насосном агрегате на седьмом временном этапе
    /// (продавливание ЖП через отверстия перфорации продавочной жидкостью ПЖ (ЖГС)) (Па) (13.42)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap7(int N, double[] tsw, double[] speedNumber, double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("Mjuzhp", fp.Mjuzhp);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Psigrp", fp.Psigrp);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("Roprodzh", fp.Roprodzh);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.42", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.Mjuzhp * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                              Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.Psigrp * fp.k * fp.h);
        }
        fp.Pap7 = fp.reservoirPressurePa + sum - fp.Roprodzh * Xnkt * fp.g - 
                  fp.Rozhp * (fp.Lcp - Xnkt) * fp.g - fp.Rozhp * (fp.Lc - fp.Lcp) * fp.g  + 
                  fp.LambdaNkt * 8.0 * Xnkt * Math.Pow(q, 2) * fp.Roprodzh / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + fp.DPzhpnkt + 8.0 * Math.Pow(q, 2) * fp.Rozhp / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.42", "Pap7", fp.Pap7);
    }
    
    /// <summary>
    /// Давление на насосном агрегате на восьмом временном этапе
    /// (заполнение ПЖ ЭК от башмака НКТ до зумпфа (дна) скважины,
    /// полное продавливание ЖП через отверстия перфорации) (Па) (13.43)
    /// </summary>
    /// <param name="N">Количество элементов в массивах, являющихся формальными параметрами метода, шт.</param>
    /// <param name="tsw">Массив значений моментов времени переключения режима работы насосного агрегата, с</param>
    /// <param name="speedNumber">Массив значений режимов работы насосного агрегата</param>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pap8(int N, double[] tsw, double[] speedNumber, double Xek, double q)
    {
        parameters.Clear();
        parameters.Add("N", N);
        parameters.Add("tsw[0]", tsw[0]);
        parameters.Add("speedNumber[0]", speedNumber[0]);
        parameters.Add("Xek", Xek);
        parameters.Add("q", q);
        parameters.Add("time", fp.time);
        parameters.Add("piezoconductivityOfTheFormation", fp.piezoconductivityOfTheFormation);
        parameters.Add("rc", fp.rc);
        parameters.Add("pi", fp.pi);
        parameters.Add("k", fp.k);
        parameters.Add("h", fp.h);
        parameters.Add("Mjuzhr", fp.Mjuzhr);
        parameters.Add("Psigrp", fp.Psigrp);
        parameters.Add("reservoirPressurePa", fp.reservoirPressurePa);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("Roprodzh", fp.Roprodzh);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("g", fp.g);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.43", parameters);
        double sum = 0.0;
        for (var i = 0; i < N; i++)
        {
            var dsN = i == 0 ? speedNumber[0] : speedNumber[i] - speedNumber[i - 1];
            var dt = i == 0 ? fp.time : fp.time - tsw[i - 1];
            sum += dsN * fp.Mjuzhr * Math.Log(2.246 * fp.piezoconductivityOfTheFormation * dt / 
                                              Math.Pow(fp.rc, 2)) / (4.0 * fp.pi * fp.Psigrp * fp.k * fp.h);
        }
        fp.Pap8 = fp.reservoirPressurePa + sum - fp.Roprodzh * fp.Lcp * fp.g - 
                  fp.Roprodzh * Xek * fp.g - fp.Rozhp * (fp.Lc - fp.Lcp - Xek) * fp.g +
                  fp.LambdaNkt * 8.0 * fp.Lcp * Math.Pow(q, 2) * fp.Roprodzh / 
                  (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) + 8.0 * Math.Pow(q, 2) * fp.Rozhp / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.43", "Pap8", fp.Pap8);
    }
    
    /// <summary>
    /// Забойное давление в скважине на первом временном этапе
    /// (заполнение жидкостью разрыва НКТ от устья до башмака НКТ) (Па) (13.44)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp1(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Pap1", fp.Pap1);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("pi", fp.pi);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("g", fp.g);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.44", parameters);
        fp.Pzp1 = fp.Pap1 + fp.Rozhr * Xnkt * fp.g + fp.densityOfKillingFluid * (fp.Lcp - Xnkt) * fp.g + 
                                              fp.densityOfKillingFluid * (fp.Lc - fp.Lcp) * fp.g - fp.DPzhrnkt -
                                              fp.LambdaNkt * 8.0 * (fp.Lcp - Xnkt) * Math.Pow(q, 2) * fp.densityOfKillingFluid / 
                                              (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) - 8.0 * Math.Pow(q, 2) * fp.densityOfKillingFluid / 
                                              (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.44", "Pzp1", fp.Pzp1);
    }
    
    /// <summary>
    /// Забойное давление в скважине на втором временном этапе
    /// (продавливание ЖР в ЭК жидкостью разрыва от башмака НКТ до зумпфа (дна) скважины) (Па) (13.45)
    /// </summary>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp2(double Xek, double q)
    {
        parameters.Clear();
        parameters.Add("Xek", Xek);
        parameters.Add("q", q);
        parameters.Add("Pap2", fp.Pap2);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("densityOfKillingFluid", fp.densityOfKillingFluid);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.45", parameters);
        fp.Pzp2 = fp.Pap2 + fp.Rozhr * fp.Lcp * fp.g + fp.Rozhr * Xek * fp.g + 
                  fp.densityOfKillingFluid * (fp.Lc - fp.Lcp - Xek) * fp.g - fp.DPzhrnkt -
                  8.0 * Math.Pow(q, 2) * fp.densityOfKillingFluid / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.45", "Pzp2", fp.Pzp2);
    }
    
    /// <summary>
    /// Забойное давление в скважине на третьем временном этапе после подключения необходимого количества насосных агрегатов
    /// (продавливание ЖР через отверстия перфорации в пласт жидкостью разрыва) (Па) (13.46)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp3(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pap3", fp.Pap3);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.46", parameters);
        fp.Pzp3 = fp.Pap3 + fp.Rozhr * fp.Lc * fp.g - fp.DPzhrnkt - 8.0 * Math.Pow(q, 2) * fp.Rozhr / 
            (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.46", "Pzp3", fp.Pzp3);
    }
    
    /// <summary>
    /// Забойное давление в скважине на четвертом временном этапе
    /// (продавливание ЖР через отверстия перфорации жидкостью пропантоносителем) (Па) (13.47)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp4(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Pap4", fp.Pap4);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("DPzhrnkt", fp.DPzhrnkt);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.47", parameters);
        fp.Pzp4 = fp.Pap4 + fp.Rozhp * Xnkt * fp.g + fp.Rozhr * (fp.Lcp - Xnkt) * fp.g +
            fp.Rozhr * (fp.Lc - fp.Lcp) * fp.g - fp.DPzhpnkt - fp.DPzhrnkt - 8.0 * Math.Pow(q, 2) * fp.Rozhr / 
            (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.47", "Pzp4", fp.Pzp4);
    }
    
    /// <summary>
    /// Забойное давление в скважине на пятом временном этапе (заполнение ЖП ЭК от башмака НКТ
    /// до зумпфа (дна) скважины, полное продавливание ЖР через отверстия перфорации) (Па) (13.48)
    /// </summary>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp5(double Xek, double q)
    {
        parameters.Clear();
        parameters.Add("Xek", Xek);
        parameters.Add("q", q);
        parameters.Add("Pap5", fp.Pap5);
        parameters.Add("Rozhr", fp.Rozhr);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.48", parameters);
        fp.Pzp5 = fp.Pap5 + fp.Rozhp * fp.Lcp * fp.g + fp.Rozhp * Xek * fp.g + 
            fp.Rozhr * (fp.Lc - fp.Lcp - Xek) * fp.g - fp.DPzhpnkt - 8.0 * Math.Pow(q, 2) * fp.Rozhr / 
            (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.48", "Pzp5", fp.Pzp5);
    }
    
    /// <summary>
    /// Забойное давление в скважине на пятом временном этапе (заполнение ЖП ЭК от башмака НКТ
    /// до зумпфа (дна) скважины, полное продавливание ЖР через отверстия перфорации) (Па) (13.49)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp6(double q)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("Pap6", fp.Pap6);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.49", parameters);
        fp.Pzp6 = fp.Pap6 + fp.Rozhp * fp.Lc * fp.g - fp.DPzhpnkt - 8.0 * Math.Pow(q, 2) * fp.Rozhp / 
            (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.49", "Pzp6", fp.Pzp6);
    }
    
    /// <summary>
    /// Забойное давление в скважине на седьмом временном этапе
    /// (продавливание ЖП через отверстия перфорации продавочной жидкостью ПЖ (ЖГС)) (Па) (13.50)
    /// </summary>
    /// <param name="Xnkt">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp7(double Xnkt, double q)
    {
        parameters.Clear();
        parameters.Add("Xnkt", Xnkt);
        parameters.Add("q", q);
        parameters.Add("Pap7", fp.Pap7);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("Roprodzh", fp.Roprodzh);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("DPzhpnkt", fp.DPzhpnkt);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.50", parameters);
        fp.Pzp7 = fp.Pap7 + fp.Roprodzh * Xnkt * fp.g + fp.Rozhp * (fp.Lcp - Xnkt) * fp.g + 
            fp.Rozhp * (fp.Lc - fp.Lcp) * fp.g - fp.LambdaNkt * 8.0 * Xnkt * Math.Pow(q, 2) * fp.Roprodzh / 
            (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) - fp.DPzhpnkt - 8.0 * Math.Pow(q, 2) * fp.Rozhp / 
            (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.50", "Pzp7", fp.Pzp7);
    }
    
    /// <summary>
    /// Забойное давление в скважине на восьмом временном этапе (заполнение ПЖ ЭК от башмака НКТ до зумпфа (дна) скважины,
    /// полное продавливание ЖП через отверстия перфорации) (Па) (13.51)
    /// </summary>
    /// <param name="Xek">Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м  от подачи насоса (q) во времени, м</param>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    public void Pzp8(double Xek, double q)
    {
        parameters.Clear();
        parameters.Add("Xek", Xek);
        parameters.Add("q", q);
        parameters.Add("Pap8", fp.Pap8);
        parameters.Add("Rozhp", fp.Rozhp);
        parameters.Add("Roprodzh", fp.Roprodzh);
        parameters.Add("pi", fp.pi);
        parameters.Add("g", fp.g);
        parameters.Add("dnkt", fp.dnkt);
        parameters.Add("Lc", fp.Lc);
        parameters.Add("Lcp", fp.Lcp);
        parameters.Add("LambdaNkt", fp.LambdaNkt);
        parameters.Add("npo", fp.npo);
        parameters.Add("Kdelta", fp.Kdelta);
        parameters.Add("dpo", fp.dpo);
        p.PInC("13.51", parameters);
        fp.Pzp8 = fp.Pap8 + fp.Roprodzh * fp.Lcp * fp.g + fp.Roprodzh * Xek * fp.g + 
                  fp.Rozhp * (fp.Lc - fp.Lcp - Xek) * fp.g - fp.LambdaNkt * 8.0 * fp.Lcp * 
                  Math.Pow(q, 2) * fp.Roprodzh / (Math.Pow(fp.pi, 2) * Math.Pow(fp.dnkt, 5)) - 
                  8.0 * Math.Pow(q, 2) * fp.Rozhp / 
                  (Math.Pow(fp.pi * fp.npo * fp.Kdelta, 2) * Math.Pow(fp.dpo, 4) * fp.g);
        p.POutC("13.51", "Pzp8", fp.Pzp8);
    }

    /// <summary>
    /// Накопленный объем закачиваемой в скважину ЖР/ЖП/ПЖ, м3 (13.54)
    /// </summary>
    /// <param name="q">Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с;</param>
    /// <param name="time">Время, секунды</param>
    public void Vnakzhr(double q, double time)
    {
        parameters.Clear();
        parameters.Add("q", q);
        parameters.Add("time", time);
        p.PInC("13.54", parameters);
        fp.Vnakzhr += q * time;
        p.POutC("13.54", "Vnakzhr", fp.Vnakzhr);
    }
}
using System;

public class Overhaul_1_6_BlockSchemes
{
    private FormulaParameters fp;
    private Print p;

    public Overhaul_1_6_BlockSchemes(FormulaParameters _fp, Print _p)
    {
        fp = _fp;
        p = _p;
    }

    public void Diagram(double time)
    {
        p.PTxt("o16.Diagram");
        fp.o16.densityOfKillingFluid(); // 13.1
        fp.Ropzhzhgs = fp.densityOfKillingFluid;
        fp.o16.Lc(); // 13.2
        fp.o16.DPzhrnkt2(fp.Xnkt,  fp.q); // 13.13.1
        fp.o16.DPzhpnkt2(fp.Xnkt,  fp.q); // 13.13.2
        fp.o16.Pu(fp.Ropzhzhgs); // 13.3
        fp.o16.Pm(fp.Ropzhzhgs); // 13.3
        fp.o16.Pzatr(fp.Ropzhzhgs); // 13.3
        fp.o16.Pz(fp.Ropzhzhgs); // 13.4
        fp.o16.Prazr(); // 13.5
        fp.o16.Pzabr(); // 13.8
        fp.o16.DPzhrnkt1(fp.Xnkt, fp.q); // 13.12.1
        fp.o16.DPzhpnkt1(fp.Xnkt, fp.q); // 13.12.2
        fp.o16.Pu(); // 13.9
        fp.o16.Vzhr(); // 13.26
        fp.o16.Vzhp(); // 13.29
        fp.o16.Vprodzh(); // 13.30
        fp.o16.Rozhp(); // 13.23
        fp.o16.Nagr(); // 13.33
        fp.o16.tgr(); // 13.34
        var tsw = new double[] {60.0};
        var speedNumber = new double[] {0.0085};
        fp.o16.Pap1(1, tsw, speedNumber, fp.Xnkt, fp.q); // 13.35
        fp.o16.Pzp1(fp.Xnkt, fp.q); // 13.44
        fp.o16.Xnkt(fp.q, time); // 13.14
        fp.o16.Pap2(1, tsw, speedNumber, fp.Xek, fp.q); // 13.36
        fp.o16.Pzp2(fp.Xek, fp.q); // 13.45
        fp.o16.Xek(fp.q, time); // 13.15
        fp.o16.Pap31(1, tsw, speedNumber, fp.q); // 13.37
        fp.o16.Pap32(1, tsw, speedNumber, fp.q); // 13.38
        fp.o16.Pzp3(fp.q); // 13.46
        fp.o16.Pap4(1, tsw, speedNumber, fp.Xnkt, fp.q); // 13.39
        fp.o16.Pzp4(fp.Xnkt, fp.q); // 13.47
        fp.o16.Xnkt(fp.q, time); // 13.14
        fp.o16.Pap5(1, tsw, speedNumber, fp.Xek, fp.q); // 13.40
        fp.o16.Pzp5(fp.Xek, fp.q); // 13.48
        fp.o16.Xek(fp.q, time); // 13.15
        fp.o16.Pap6(1, tsw, speedNumber, fp.q); // 13.41
        fp.o16.Pzp6(fp.q); // 13.49
        fp.o16.Pap7(1, tsw, speedNumber, fp.Xnkt, fp.q); // 13.42
        fp.o16.Pzp7(fp.Xnkt, fp.q); // 13.50
        fp.o16.Xnkt(fp.q, time); // 13.14
        fp.o16.Pap8(1, tsw, speedNumber, fp.Xek, fp.q); // 13.43
        fp.o16.Pzp8(fp.Xek, fp.q); // 13.51
        fp.o16.Xek(fp.q, time); // 13.15
        fp.o16.Pu(fp.Ropzhzhgs); // 13.52
        fp.o16.Pm(fp.Ropzhzhgs); // 13.52
        fp.o16.Pzatr(fp.Ropzhzhgs); // 13.52
        fp.o16.Pz(fp.Ropzhzhgs); // 13.53
        fp.o16.Vnakzhr(fp.q, time); // 13.54
        fp.o16.Vzhgrp(); // 13.31
        }
}